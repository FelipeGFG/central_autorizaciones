package co.fsfb.ws.rest.vo;

public class DocumentoVO extends Documento {
  private static final long serialVersionUID = 1L;
  
  private Long accIdNumero;
  
  private Long domShaIdentificador;
  
  private Long eomIdCodigo;
  
  private Long pacPacNumero;
  
  private String pacPacTipoIdentCodigo;
  
  private String documento;
  
  private String tipTipIDav;
  
  private String pcaAgeCodigRecep;
  
  private String serEspCodigo;
  
  private String serSerCodSubEspe;
  
  private String tdsSigla;
  
  public Long getAccIdNumero() {
    return this.accIdNumero;
  }
  
  public void setAccIdNumero(Long accIdNumero) {
    this.accIdNumero = accIdNumero;
  }
  
  public Long getDomShaIdentificador() {
    return this.domShaIdentificador;
  }
  
  public void setDomShaIdentificador(Long domShaIdentificador) {
    this.domShaIdentificador = domShaIdentificador;
  }
  
  public Long getEomIdCodigo() {
    return this.eomIdCodigo;
  }
  
  public void setEomIdCodigo(Long eomIdCodigo) {
    this.eomIdCodigo = eomIdCodigo;
  }
  
  public Long getPacPacNumero() {
    return this.pacPacNumero;
  }
  
  public void setPacPacNumero(Long pacPacNumero) {
    this.pacPacNumero = pacPacNumero;
  }
  
  public String getDocumento() {
    return this.documento;
  }
  
  public void setDocumento(String documento) {
    this.documento = documento;
  }
  
  public String getPacPacTipoIdentCodigo() {
    return this.pacPacTipoIdentCodigo;
  }
  
  public void setPacPacTipoIdentCodigo(String pacPacTipoIdentCodigo) {
    this.pacPacTipoIdentCodigo = pacPacTipoIdentCodigo;
  }
  
  public String getTipTipIDav() {
    return this.tipTipIDav;
  }
  
  public void setTipTipIDav(String tipTipIDav) {
    this.tipTipIDav = tipTipIDav;
  }
  
  public String getPcaAgeCodigRecep() {
    return this.pcaAgeCodigRecep;
  }
  
  public void setPcaAgeCodigRecep(String pcaAgeCodigRecep) {
    this.pcaAgeCodigRecep = pcaAgeCodigRecep;
  }
  
  public String getSerEspCodigo() {
    return this.serEspCodigo;
  }
  
  public void setSerEspCodigo(String serEspCodigo) {
    this.serEspCodigo = serEspCodigo;
  }
  
  public String getSerSerCodSubEspe() {
    return this.serSerCodSubEspe;
  }
  
  public void setSerSerCodSubEspe(String serSerCodSubEspe) {
    this.serSerCodSubEspe = serSerCodSubEspe;
  }
  
  public String getTdsSigla() {
    return this.tdsSigla;
  }
  
  public void setTdsSigla(String tdsSigla) {
    this.tdsSigla = tdsSigla;
  }
}

package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class Examen implements Serializable {
  private static final long serialVersionUID = -3664317130837591551L;
  
  private String tipo;
  
  private String numero;
  
  private String descripcion;
  
  private String descFechaResultado;
  
  private String fechaResultado;
  
  private String codPrestacion;
  
  private String descPrestacion;
  
  private String indFueraRango;
  
  private String ordenAlterna;
  
  private String tipoOrdenVarios;
  
  private String numeroOrdenVarios;
  
  public static final String CAMPO_TIPO = "tipo";
  
  public static final String CAMPO_FECHA = "fecha";
  
  public String getTipoOrdenVarios() {
    return this.tipoOrdenVarios;
  }
  
  public void setTipoOrdenVarios(String tipoOrdenVarios) {
    this.tipoOrdenVarios = tipoOrdenVarios;
  }
  
  public String getNumeroOrdenVarios() {
    return this.numeroOrdenVarios;
  }
  
  public void setNumeroOrdenVarios(String numeroOrdenVarios) {
    this.numeroOrdenVarios = numeroOrdenVarios;
  }
  
  public String getTipo() {
    return this.tipo;
  }
  
  public void setTipo(String tipo) {
    this.tipo = tipo;
  }
  
  public String getNumero() {
    return this.numero;
  }
  
  public void setNumero(String numero) {
    this.numero = numero;
  }
  
  public String getDescripcion() {
    return this.descripcion;
  }
  
  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }
  
  public String getDescFechaResultado() {
    return this.descFechaResultado;
  }
  
  public void setDescFechaResultado(String descFechaResultado) {
    this.descFechaResultado = descFechaResultado;
  }
  
  public String getFechaResultado() {
    return this.fechaResultado;
  }
  
  public void setFechaResultado(String fechaResultado) {
    this.fechaResultado = fechaResultado;
  }
  
  public String getIndFueraRango() {
    return this.indFueraRango;
  }
  
  public void setIndFueraRango(String indFueraRango) {
    this.indFueraRango = indFueraRango;
  }
  
  public String getCodPrestacion() {
    return this.codPrestacion;
  }
  
  public void setCodPrestacion(String codPrestacion) {
    this.codPrestacion = codPrestacion;
  }
  
  public String getDescPrestacion() {
    return this.descPrestacion;
  }
  
  public void setDescPrestacion(String descPrestacion) {
    this.descPrestacion = descPrestacion;
  }
  
  public String getOrdenAlterna() {
    if (this.ordenAlterna != null)
      this.ordenAlterna = this.ordenAlterna.trim(); 
    return this.ordenAlterna;
  }
  
  public void setOrdenAlterna(String ordenAlterna) {
    this.ordenAlterna = ordenAlterna;
  }
}

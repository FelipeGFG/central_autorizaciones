package co.fsfb.ws.rest.vo;

import java.io.Serializable;
import java.sql.Date;

public class PagosHistoricoPersona implements Serializable {
  private static final long serialVersionUID = -8820699724602373914L;
  
  private Long id;
  
  private String celular;
  
  private Date fechaRegistro;
  
  private String email;
  
  private String nombres;
  
  private String apellidos;
  
  public Long getId() {
    return this.id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public String getCelular() {
    return this.celular;
  }
  
  public void setCelular(String celular) {
    this.celular = celular;
  }
  
  public Date getFechaRegistro() {
    return this.fechaRegistro;
  }
  
  public void setFechaRegistro(Date fechaRegistro) {
    this.fechaRegistro = fechaRegistro;
  }
  
  public String getEmail() {
    return this.email;
  }
  
  public void setEmail(String email) {
    this.email = email;
  }
  
  public String getNombres() {
    return this.nombres;
  }
  
  public void setNombres(String nombres) {
    this.nombres = nombres;
  }
  
  public String getApellidos() {
    return this.apellidos;
  }
  
  public void setApellidos(String apellidos) {
    this.apellidos = apellidos;
  }
  
  public static long getSerialversionuid() {
    return -8820699724602373914L;
  }
}

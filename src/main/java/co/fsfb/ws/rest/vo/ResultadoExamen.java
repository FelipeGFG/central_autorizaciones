package co.fsfb.ws.rest.vo;

import java.io.Serializable;
import java.util.Calendar;

public class ResultadoExamen implements Serializable {
  private static final long serialVersionUID = 1L;
  
  private Usuario usr;
  
  private String tipo;
  
  private String numero;
  
  private String descripcion;
  
  private String descFechaResultado;
  
  private Calendar fechaResultado;
  
  private String fechaInicial;
  
  private String fechaFinal;
  
  private String codPrestacion;
  
  private String descPrestacion;
  
  private String indFueraRango;
  
  private String ordenAlterna;
  
  private String tipoOrdenVarios;
  
  private String numeroOrdenVarios;
  
  private long evento;
  
  public static final String CAMPO_TIPO = "tipo";
  
  public static final String CAMPO_FECHA = "fecha";
  
  public String getFechaInicial() {
    return this.fechaInicial;
  }
  
  public void setFechaInicial(String fechaInicial) {
    this.fechaInicial = fechaInicial;
  }
  
  public String getFechaFinal() {
    return this.fechaFinal;
  }
  
  public void setFechaFinal(String fechaFinal) {
    this.fechaFinal = fechaFinal;
  }
  
  public long getEvento() {
    return this.evento;
  }
  
  public void setEvento(long evento) {
    this.evento = evento;
  }
  
  public Usuario getUsr() {
    return this.usr;
  }
  
  public void setUsr(Usuario usr) {
    this.usr = usr;
  }
  
  public String getTipo() {
    return this.tipo;
  }
  
  public void setTipo(String tipo) {
    this.tipo = tipo;
  }
  
  public String getNumero() {
    return this.numero;
  }
  
  public void setNumero(String numero) {
    this.numero = numero;
  }
  
  public String getDescripcion() {
    return this.descripcion;
  }
  
  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }
  
  public String getDescFechaResultado() {
    return this.descFechaResultado;
  }
  
  public void setDescFechaResultado(String descFechaResultado) {
    this.descFechaResultado = descFechaResultado;
  }
  
  public Calendar getFechaResultado() {
    return this.fechaResultado;
  }
  
  public void setFechaResultado(Calendar fechaResultado) {
    this.fechaResultado = fechaResultado;
  }
  
  public String getCodPrestacion() {
    return this.codPrestacion;
  }
  
  public void setCodPrestacion(String codPrestacion) {
    this.codPrestacion = codPrestacion;
  }
  
  public String getDescPrestacion() {
    return this.descPrestacion;
  }
  
  public void setDescPrestacion(String descPrestacion) {
    this.descPrestacion = descPrestacion;
  }
  
  public String getIndFueraRango() {
    return this.indFueraRango;
  }
  
  public void setIndFueraRango(String indFueraRango) {
    this.indFueraRango = indFueraRango;
  }
  
  public String getOrdenAlterna() {
    return this.ordenAlterna;
  }
  
  public void setOrdenAlterna(String ordenAlterna) {
    this.ordenAlterna = ordenAlterna;
  }
  
  public String getTipoOrdenVarios() {
    return this.tipoOrdenVarios;
  }
  
  public void setTipoOrdenVarios(String tipoOrdenVarios) {
    this.tipoOrdenVarios = tipoOrdenVarios;
  }
  
  public String getNumeroOrdenVarios() {
    return this.numeroOrdenVarios;
  }
  
  public void setNumeroOrdenVarios(String numeroOrdenVarios) {
    this.numeroOrdenVarios = numeroOrdenVarios;
  }
  
  public static String getCampoTipo() {
    return "tipo";
  }
  
  public static String getCampoFecha() {
    return "fecha";
  }
}

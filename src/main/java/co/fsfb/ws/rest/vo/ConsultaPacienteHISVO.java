package co.fsfb.ws.rest.vo;

public class ConsultaPacienteHISVO {
  private String tipoDocumento;
  
  private String numeroDocumento;
  
  public String getTipoDocumento() {
    return this.tipoDocumento;
  }
  
  public void setTipoDocumento(String tipoDocumento) {
    this.tipoDocumento = tipoDocumento;
  }
  
  public String getNumeroDocumento() {
    return this.numeroDocumento;
  }
  
  public void setNumeroDocumento(String numeroDocumento) {
    this.numeroDocumento = numeroDocumento;
  }
}

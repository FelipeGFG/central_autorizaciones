package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class PagosDatosCompletos implements Serializable {
  private static final long serialVersionUID = -8820699724602373914L;
  
  private Long id;
  
  private PagosHistoricoPersona pagosHistoricoPersona;
  
  private PagosPersona pagosPersona;
  
  private PagosMetodoPago pagosMetodoPago;
  
  private PagosFranquicia pagosFranquicia;
  
  private PagosDatosTransaccion pagosDatosTransaccion;
  
  private PagosEstadoTransaccion pagosEstadoTransaccion;
  
  private PagosTrazabilidadTransaccion pagosTrazabilidadTransaccion;
  
  private PagosDetalleTransaccion pagosDetalleTransaccion;
  
  public Long getId() {
    return this.id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public PagosHistoricoPersona getPagosHistoricoPersona() {
    return this.pagosHistoricoPersona;
  }
  
  public void setPagosHistoricoPersona(PagosHistoricoPersona pagosHistoricoPersona) {
    this.pagosHistoricoPersona = pagosHistoricoPersona;
  }
  
  public PagosPersona getPagosPersona() {
    return this.pagosPersona;
  }
  
  public void setPagosPersona(PagosPersona pagosPersona) {
    this.pagosPersona = pagosPersona;
  }
  
  public PagosMetodoPago getPagosMetodoPago() {
    return this.pagosMetodoPago;
  }
  
  public void setPagosMetodoPago(PagosMetodoPago pagosMetodoPago) {
    this.pagosMetodoPago = pagosMetodoPago;
  }
  
  public PagosFranquicia getPagosFranquicia() {
    return this.pagosFranquicia;
  }
  
  public void setPagosFranquicia(PagosFranquicia pagosFranquicia) {
    this.pagosFranquicia = pagosFranquicia;
  }
  
  public PagosDatosTransaccion getPagosDatosTransaccion() {
    return this.pagosDatosTransaccion;
  }
  
  public void setPagosDatosTransaccion(PagosDatosTransaccion pagosDatosTransaccion) {
    this.pagosDatosTransaccion = pagosDatosTransaccion;
  }
  
  public PagosEstadoTransaccion getPagosEstadoTransaccion() {
    return this.pagosEstadoTransaccion;
  }
  
  public void setPagosEstadoTransaccion(PagosEstadoTransaccion pagosEstadoTransaccion) {
    this.pagosEstadoTransaccion = pagosEstadoTransaccion;
  }
  
  public PagosTrazabilidadTransaccion getPagosTrazabilidadTransaccion() {
    return this.pagosTrazabilidadTransaccion;
  }
  
  public void setPagosTrazabilidadTransaccion(PagosTrazabilidadTransaccion pagosTrazabilidadTransaccion) {
    this.pagosTrazabilidadTransaccion = pagosTrazabilidadTransaccion;
  }
  
  public PagosDetalleTransaccion getPagosDetalleTransaccion() {
    return this.pagosDetalleTransaccion;
  }
  
  public void setPagosDetalleTransaccion(PagosDetalleTransaccion pagosDetalleTransaccion) {
    this.pagosDetalleTransaccion = pagosDetalleTransaccion;
  }
  
  public static long getSerialversionuid() {
    return -8820699724602373914L;
  }
}

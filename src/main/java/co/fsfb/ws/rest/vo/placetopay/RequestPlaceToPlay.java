package co.fsfb.ws.rest.vo.placetopay;

import java.io.Serializable;

public class RequestPlaceToPlay implements Serializable {
  private Person buyer;
  
  private Person payer;
  
  private Payment payment;
  
  private String expiration;
  
  private String ipAddress;
  
  private String returnUrl;
  
  private String userAgent;
  
  public Person getBuyer() {
    return this.buyer;
  }
  
  public void setBuyer(Person buyer) {
    this.buyer = buyer;
  }
  
  public Person getPayer() {
    return this.payer;
  }
  
  public void setPayer(Person payer) {
    this.payer = payer;
  }
  
  public Payment getPayment() {
    return this.payment;
  }
  
  public void setPayment(Payment payment) {
    this.payment = payment;
  }
  
  public String getExpiration() {
    return this.expiration;
  }
  
  public void setExpiration(String expiration) {
    this.expiration = expiration;
  }
  
  public String getIpAddress() {
    return this.ipAddress;
  }
  
  public void setIpAddress(String ipAddress) {
    this.ipAddress = ipAddress;
  }
  
  public String getReturnUrl() {
    return this.returnUrl;
  }
  
  public void setReturnUrl(String returnUrl) {
    this.returnUrl = returnUrl;
  }
  
  public String getUserAgent() {
    return this.userAgent;
  }
  
  public void setUserAgent(String userAgent) {
    this.userAgent = userAgent;
  }
}

package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class PagosEstadoPlaceToPay implements Serializable {
  private static final long serialVersionUID = -8820699724602373914L;
  
  private String status;
  
  private String message;
  
  private String reason;
  
  private String date;
  
  public String getStatus() {
    return this.status;
  }
  
  public void setStatus(String status) {
    this.status = status;
  }
  
  public String getMessage() {
    return this.message;
  }
  
  public void setMessage(String message) {
    this.message = message;
  }
  
  public String getReason() {
    return this.reason;
  }
  
  public void setReason(String reason) {
    this.reason = reason;
  }
  
  public String getDate() {
    return this.date;
  }
  
  public void setDate(String date) {
    this.date = date;
  }
  
  public static long getSerialversionuid() {
    return -8820699724602373914L;
  }
}

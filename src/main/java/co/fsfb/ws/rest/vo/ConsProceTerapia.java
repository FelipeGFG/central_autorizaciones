package co.fsfb.ws.rest.vo;

import java.io.Serializable;
import java.sql.Timestamp;

public class ConsProceTerapia implements Serializable {
  private static final long serialVersionUID = 1L;
  
  private Usuario usr;
  
  private String nombre;
  
  private Timestamp fechaHora;
  
  private String strAmbito;
  
  private String strEspecialidad;
  
  private String strMedicoTrata;
  
  private String strEdad;
  
  private String strPlanAlta;
  
  public ConsProceTerapia(Usuario usr, String nombre, Timestamp fechaHora, String strAmbito, String strEspecialidad, String strMedicoTrata, String strEdad, String strPlanAlta) {
    this.usr = usr;
    this.nombre = nombre;
    this.fechaHora = fechaHora;
    this.strAmbito = strAmbito;
    this.strEspecialidad = strEspecialidad;
    this.strMedicoTrata = strMedicoTrata;
    this.strEdad = strEdad;
    this.strPlanAlta = strPlanAlta;
  }
  
  public ConsProceTerapia() {}
  
  public Usuario getUsr() {
    return this.usr;
  }
  
  public void setUsr(Usuario usr) {
    this.usr = usr;
  }
  
  public String getNombre() {
    return this.nombre;
  }
  
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }
  
  public Timestamp getFechaHora() {
    return this.fechaHora;
  }
  
  public void setFechaHora(Timestamp fechaHora) {
    this.fechaHora = fechaHora;
  }
  
  public String getStrAmbito() {
    return this.strAmbito;
  }
  
  public void setStrAmbito(String strAmbito) {
    this.strAmbito = strAmbito;
  }
  
  public String getStrEspecialidad() {
    return this.strEspecialidad;
  }
  
  public void setStrEspecialidad(String strEspecialidad) {
    this.strEspecialidad = strEspecialidad;
  }
  
  public String getStrMedicoTrata() {
    return this.strMedicoTrata;
  }
  
  public void setStrMedicoTrata(String strMedicoTrata) {
    this.strMedicoTrata = strMedicoTrata;
  }
  
  public String getStrEdad() {
    return this.strEdad;
  }
  
  public void setStrEdad(String strEdad) {
    this.strEdad = strEdad;
  }
  
  public String getStrPlanAlta() {
    return this.strPlanAlta;
  }
  
  public void setStrPlanAlta(String strPlanAlta) {
    this.strPlanAlta = strPlanAlta;
  }
}

package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class CitaPorPagarVO implements Serializable {
  private static final long serialVersionUID = 1L;
  
  private String idCita;
  
  private String pacNumero;
  
  private String nombreCompleto;
  
  private String tipoDocumento;
  
  private String documento;
  
  private String fechaCita;
  
  private String horaCita;
  
  private String fechaCitaCa;
  
  private String codUser;
  
  private String codigoConvenio;
  
  private String codigoPrestacion;
  
  private String estadoProceso;
  
  private String costoPaciente;
  
  private String fechaHoraAutorizacion;
  
  private String descripcion;
  
  public String getDescripcion() {
    return this.descripcion;
  }
  
  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }
  
  public String getIdCita() {
    return this.idCita;
  }
  
  public void setIdCita(String idCita) {
    this.idCita = idCita;
  }
  
  public String getPacNumero() {
    return this.pacNumero;
  }
  
  public void setPacNumero(String pacNumero) {
    this.pacNumero = pacNumero;
  }
  
  public String getNombreCompleto() {
    return this.nombreCompleto;
  }
  
  public void setNombreCompleto(String nombreCompleto) {
    this.nombreCompleto = nombreCompleto;
  }
  
  public String getTipoDocumento() {
    return this.tipoDocumento;
  }
  
  public void setTipoDocumento(String tipoDocumento) {
    this.tipoDocumento = tipoDocumento;
  }
  
  public String getDocumento() {
    return this.documento;
  }
  
  public void setDocumento(String documento) {
    this.documento = documento;
  }
  
  public String getFechaCita() {
    return this.fechaCita;
  }
  
  public void setFechaCita(String fechaCita) {
    this.fechaCita = fechaCita;
  }
  
  public String getHoraCita() {
    return this.horaCita;
  }
  
  public void setHoraCita(String horaCita) {
    this.horaCita = horaCita;
  }
  
  public String getFechaCitaCa() {
    return this.fechaCitaCa;
  }
  
  public void setFechaCitaCa(String fechaCitaCa) {
    this.fechaCitaCa = fechaCitaCa;
  }
  
  public String getCodUser() {
    return this.codUser;
  }
  
  public void setCodUser(String codUser) {
    this.codUser = codUser;
  }
  
  public String getCodigoConvenio() {
    return this.codigoConvenio;
  }
  
  public void setCodigoConvenio(String codigoConvenio) {
    this.codigoConvenio = codigoConvenio;
  }
  
  public String getCodigoPrestacion() {
    return this.codigoPrestacion;
  }
  
  public void setCodigoPrestacion(String codigoPrestacion) {
    this.codigoPrestacion = codigoPrestacion;
  }
  
  public String getEstadoProceso() {
    return this.estadoProceso;
  }
  
  public void setEstadoProceso(String estadoProceso) {
    this.estadoProceso = estadoProceso;
  }
  
  public String getCostoPaciente() {
    return this.costoPaciente;
  }
  
  public void setCostoPaciente(String costoPaciente) {
    this.costoPaciente = costoPaciente;
  }
  
  public String getFechaHoraAutorizacion() {
    return this.fechaHoraAutorizacion;
  }
  
  public void setFechaHoraAutorizacion(String fechaHoraAutorizacion) {
    this.fechaHoraAutorizacion = fechaHoraAutorizacion;
  }
  
  public static long getSerialversionuid() {
    return 1L;
  }
}

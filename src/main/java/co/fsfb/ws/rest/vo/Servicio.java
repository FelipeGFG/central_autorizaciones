package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class Servicio implements Serializable {
  private static final long serialVersionUID = 1L;
  
  String strNombrePrestacion;
  
  String strCodigoPrestacion;
  
  String strSerCodigo;
  
  String strSerDescripcion;
  
  String strSerCodSubEsp;
  
  public String getStrCodigoPrestacion() {
    return this.strCodigoPrestacion;
  }
  
  public void setStrCodigoPrestacion(String strCodigoPrestacion) {
    this.strCodigoPrestacion = strCodigoPrestacion;
  }
  
  public String getStrNombrePrestacion() {
    return this.strNombrePrestacion;
  }
  
  public void setStrNombrePrestacion(String strNombrePrestacion) {
    this.strNombrePrestacion = strNombrePrestacion;
  }
  
  public String getStrSerCodigo() {
    return this.strSerCodigo;
  }
  
  public void setStrSerCodigo(String strSerCodigo) {
    this.strSerCodigo = strSerCodigo;
  }
  
  public String getStrSerDescripcion() {
    return this.strSerDescripcion;
  }
  
  public void setStrSerDescripcion(String strSerDescripcion) {
    this.strSerDescripcion = strSerDescripcion;
  }
  
  public String getStrSerCodSubEsp() {
    return this.strSerCodSubEsp;
  }
  
  public void setStrSerCodSubEsp(String strSerCodSubEsp) {
    this.strSerCodSubEsp = strSerCodSubEsp;
  }
}

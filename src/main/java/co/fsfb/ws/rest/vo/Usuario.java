package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class Usuario implements Serializable {
 
	private static final long serialVersionUID = 1L;

	  

	  private String usuario;

	  

	  private String uid;

	  

	  private String password;

	  

	  private String new_password;

	  

	  private boolean userValido;

	  

	  private String mail;

	  

	  private String cn;

	  

	  private String sn;

	  

	  private String givenName;

	  

	  private String tipoDoc;

	  

	  private String telNumber;

	  

	  private String sexo;

	  

	  private String fechaNacimiento;

	  

	  private String apellidoPaterno;

	  

	  private String pacnumero;

	  

	  private String role;

	  

	  private Email email;

	  

	  public Email getEmail() {

	    return this.email;

	  }

	  

	  public void setEmail(Email email) {

	    this.email = email;

	  }

	  

	  public String getRole() {

	    return this.role;

	  }

	  

	  public void setRole(String role) {

	    this.role = role;

	  }

	  

	  public String getPacnumero() {

	    return this.pacnumero;

	  }

	  

	  public void setPacnumero(String pacnumero) {

	    this.pacnumero = pacnumero;

	  }

	  

	  public String getUsuario() {

	    return this.usuario;

	  }

	  

	  public void setUsuario(String usuario) {

	    this.usuario = usuario;

	  }

	  

	  public String getApellidoPaterno() {

	    return this.apellidoPaterno;

	  }

	  

	  public void setApellidoPaterno(String apellidoPaterno) {

	    this.apellidoPaterno = apellidoPaterno;

	  }

	  

	  public String getSexo() {

	    return this.sexo;

	  }

	  

	  public void setSexo(String sexo) {

	    this.sexo = sexo;

	  }

	  

	  public String getFechaNacimiento() {

	    return this.fechaNacimiento;

	  }

	  

	  public void setFechaNacimiento(String fechaNacimiento) {

	    this.fechaNacimiento = fechaNacimiento;

	  }

	  

	  public String getTelNumber() {

	    return this.telNumber;

	  }

	  

	  public void setTelNumber(String telNumber) {

	    this.telNumber = telNumber;

	  }

	  

	  public String getTipoDoc() {

	    return this.tipoDoc;

	  }

	  

	  public void setTipoDoc(String tipoDoc) {

	    this.tipoDoc = tipoDoc;

	  }

	  

	  public String getUid() {

	    return this.uid;

	  }

	  

	  public void setUid(String uid) {

	    this.uid = uid;

	  }

	  

	  public String getPassword() {

	    return this.password;

	  }

	  

	  public void setPassword(String password) {

	    this.password = password;

	  }

	  

	  public boolean isUserValido() {

	    return this.userValido;

	  }

	  

	  public void setUserValido(boolean userValido) {

	    this.userValido = userValido;

	  }

	  

	  public String getNew_password() {

	    return this.new_password;

	  }

	  

	  public void setNew_password(String new_password) {

	    this.new_password = new_password;

	  }

	  

	  public String getMail() {

	    return this.mail;

	  }

	  

	  public void setMail(String mail) {

	    this.mail = mail;

	  }
	  
	  public String getCn() {
	    return this.cn;
	  }
	  
	  public void setCn(String cn) {
	    this.cn = cn;
	  }

	  public String getSn() {
	    return this.sn;
	  }
	  
	  public void setSn(String sn) {
	    this.sn = sn;
	  }

	  public String getGivenName() {
	    return this.givenName;
	  }
	  
	  public void setGivenName(String givenName) {
	    this.givenName = givenName;
	  }
	
}

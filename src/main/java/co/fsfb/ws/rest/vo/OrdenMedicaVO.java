package co.fsfb.ws.rest.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class OrdenMedicaVO implements Serializable {
  private static final long serialVersionUID = 1L;
  
  private String nombreCompletoPaciente;
  
  private String tipTipIDav;
  
  private Long eomIdCodigo;
  
  private Long ormIdOrdmNumero;
  
  private Long pacPacNumero;
  
  private String pacPacTipoIdentCodigo;
  
  private String pacPacRut;
  
  private String pcaAgeCodigoRecep;
  
  private Date cgFechaProceso;
  
  private String ormFilename;
  
  private DetalleOrdenMedicaVO caDetalleOrdenesMedicas;
  
  private List<PrestacionesOrdMedVO> caPrestacionesOrdMed;
  
  private String fechaRegistroFile;
  
  public String getNombreCompletoPaciente() {
    return this.nombreCompletoPaciente;
  }
  
  public void setNombreCompletoPaciente(String nombreCompletoPaciente) {
    this.nombreCompletoPaciente = nombreCompletoPaciente;
  }
  
  public String getTipTipIDav() {
    return this.tipTipIDav;
  }
  
  public void setTipTipIDav(String tipTipIDav) {
    this.tipTipIDav = tipTipIDav;
  }
  
  public Long getEomIdCodigo() {
    return this.eomIdCodigo;
  }
  
  public void setEomIdCodigo(Long eomIdCodigo) {
    this.eomIdCodigo = eomIdCodigo;
  }
  
  public Long getOrmIdOrdmNumero() {
    return this.ormIdOrdmNumero;
  }
  
  public void setOrmIdOrdmNumero(Long ormIdOrdmNumero) {
    this.ormIdOrdmNumero = ormIdOrdmNumero;
  }
  
  public Long getPacPacNumero() {
    return this.pacPacNumero;
  }
  
  public void setPacPacNumero(Long pacPacNumero) {
    this.pacPacNumero = pacPacNumero;
  }
  
  public String getPacPacTipoIdentCodigo() {
    return this.pacPacTipoIdentCodigo;
  }
  
  public void setPacPacTipoIdentCodigo(String pacPacTipoIdentCodigo) {
    this.pacPacTipoIdentCodigo = pacPacTipoIdentCodigo;
  }
  
  public String getPacPacRut() {
    return this.pacPacRut;
  }
  
  public void setPacPacRut(String pacPacRut) {
    this.pacPacRut = pacPacRut;
  }
  
  public String getPcaAgeCodigoRecep() {
    return this.pcaAgeCodigoRecep;
  }
  
  public void setPcaAgeCodigoRecep(String pcaAgeCodigoRecep) {
    this.pcaAgeCodigoRecep = pcaAgeCodigoRecep;
  }
  
  public Date getCgFechaProceso() {
    return this.cgFechaProceso;
  }
  
  public void setCgFechaProceso(Date cgFechaProceso) {
    this.cgFechaProceso = cgFechaProceso;
  }
  
  public String getOrmFilename() {
    return this.ormFilename;
  }
  
  public void setOrmFilename(String ormFilename) {
    this.ormFilename = ormFilename;
  }
  
  public DetalleOrdenMedicaVO getCaDetalleOrdenesMedicas() {
    return this.caDetalleOrdenesMedicas;
  }
  
  public void setCaDetalleOrdenesMedicas(DetalleOrdenMedicaVO caDetalleOrdenesMedicas) {
    this.caDetalleOrdenesMedicas = caDetalleOrdenesMedicas;
  }
  
  public List<PrestacionesOrdMedVO> getCaPrestacionesOrdMed() {
    return this.caPrestacionesOrdMed;
  }
  
  public void setCaPrestacionesOrdMed(List<PrestacionesOrdMedVO> caPrestacionesOrdMed) {
    this.caPrestacionesOrdMed = caPrestacionesOrdMed;
  }
  
  public String getFechaRegistroFile() {
    return this.fechaRegistroFile;
  }
  
  public void setFechaRegistroFile(String fechaRegistroFile) {
    this.fechaRegistroFile = fechaRegistroFile;
  }
}

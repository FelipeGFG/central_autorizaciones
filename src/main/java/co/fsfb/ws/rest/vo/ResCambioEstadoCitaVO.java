package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class ResCambioEstadoCitaVO implements Serializable {
  private static final long serialVersionUID = 1L;
  
  private Long cGIdCitaNumero;
  
  public Long getcGIdCitaNumero() {
    return this.cGIdCitaNumero;
  }
  
  public void setcGIdCitaNumero(Long cGIdCitaNumero) {
    this.cGIdCitaNumero = cGIdCitaNumero;
  }
}

package co.fsfb.ws.rest.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class PagosTransaccionConfirmada implements Serializable {
  private static final long serialVersionUID = -8820699724602373914L;
  
  private Long idTransaccion;
  
  private String metodoDePago;
  
  private String franquicia;
  
  private String estadoTransaccion;
  
  private BigDecimal totalPagado;
  
  private String autorizacionCus;
  
  private String idPlaceTopay;
  
  private String idRecibo;
  
  private String fecha;
  
  public String getIdRecibo() {
    return this.idRecibo;
  }
  
  public void setIdRecibo(String idRecibo) {
    this.idRecibo = idRecibo;
  }
  
  public String getIdPlaceTopay() {
    return this.idPlaceTopay;
  }
  
  public void setIdPlaceTopay(String idPlaceTopay) {
    this.idPlaceTopay = idPlaceTopay;
  }
  
  public Long getIdTransaccion() {
    return this.idTransaccion;
  }
  
  public void setIdTransaccion(Long idTransaccion) {
    this.idTransaccion = idTransaccion;
  }
  
  public String getMetodoDePago() {
    return this.metodoDePago;
  }
  
  public void setMetodoDePago(String metodoDePago) {
    this.metodoDePago = metodoDePago;
  }
  
  public String getFranquicia() {
    return this.franquicia;
  }
  
  public void setFranquicia(String franquicia) {
    this.franquicia = franquicia;
  }
  
  public String getEstadoTransaccion() {
    return this.estadoTransaccion;
  }
  
  public void setEstadoTransaccion(String estadoTransaccion) {
    this.estadoTransaccion = estadoTransaccion;
  }
  
  public BigDecimal getTotalPagado() {
    return this.totalPagado;
  }
  
  public void setTotalPagado(BigDecimal totalPagado) {
    this.totalPagado = totalPagado;
  }
  
  public String getAutorizacionCus() {
    return this.autorizacionCus;
  }
  
  public void setAutorizacionCus(String autorizacionCus) {
    this.autorizacionCus = autorizacionCus;
  }
  
  public static long getSerialversionuid() {
    return -8820699724602373914L;
  }
  
  public String getFecha() {
    return this.fecha;
  }
  
  public void setFecha(String fechaRegistro) {
    this.fecha = fechaRegistro;
  }
}

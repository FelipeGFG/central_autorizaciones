package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class FinalizarOrdenMedicaVO implements Serializable {
  private static final long serialVersionUID = 1L;
  
  private Long ormIdOrdmNumero;
  
  private String pcaAgeCodigRecep;
  
  private Long razon;
  
  public Long getRazon() {
    return this.razon;
  }
  
  public void setRazon(Long razon) {
    this.razon = razon;
  }
  
  public static long getSerialversionuid() {
    return 1L;
  }
  
  public Long getOrmIdOrdmNumero() {
    return this.ormIdOrdmNumero;
  }
  
  public void setOrmIdOrdmNumero(Long ormIdOrdmNumero) {
    this.ormIdOrdmNumero = ormIdOrdmNumero;
  }
  
  public String getPcaAgeCodigRecep() {
    return this.pcaAgeCodigRecep;
  }
  
  public void setPcaAgeCodigRecep(String pcaAgeCodigRecep) {
    this.pcaAgeCodigRecep = pcaAgeCodigRecep;
  }
}

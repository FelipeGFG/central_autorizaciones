package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class EnviarEmailVO implements Serializable {
  private static final long serialVersionUID = 1L;
  
  private Long accIdNumero;
  
  private Long notIdentificador;
  
  private String pcaAgeCodigRecep;
  
  private String pacnumero;
  
  private String givenName;
  
  private Email email;
  
  private String mail;
  
  public Long getAccIdNumero() {
    return this.accIdNumero;
  }
  
  public void setAccIdNumero(Long accIdNumero) {
    this.accIdNumero = accIdNumero;
  }
  
  public Long getNotIdentificador() {
    return this.notIdentificador;
  }
  
  public void setNotIdentificador(Long notIdentificador) {
    this.notIdentificador = notIdentificador;
  }
  
  public String getPcaAgeCodigRecep() {
    return this.pcaAgeCodigRecep;
  }
  
  public void setPcaAgeCodigRecep(String pcaAgeCodigRecep) {
    this.pcaAgeCodigRecep = pcaAgeCodigRecep;
  }
  
  public String getGivenName() {
    return this.givenName;
  }
  
  public void setGivenName(String givenName) {
    this.givenName = givenName;
  }
  
  public Email getEmail() {
    return this.email;
  }
  
  public void setEmail(Email email) {
    this.email = email;
  }
  
  public String getPacnumero() {
    return this.pacnumero;
  }
  
  public void setPacnumero(String pacnumero) {
    this.pacnumero = pacnumero;
  }
  
  public String getMail() {
    return this.mail;
  }
  
  public void setMail(String mail) {
    this.mail = mail;
  }
}

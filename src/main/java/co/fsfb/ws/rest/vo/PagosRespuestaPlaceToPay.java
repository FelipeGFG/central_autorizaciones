package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class PagosRespuestaPlaceToPay implements Serializable {
  private static final long serialVersionUID = -8820699724602373914L;
  
  private String requestId;
  
  private String reference;
  
  private String signature;
  
  private PagosEstadoPlaceToPay status;
  
  public String getRequestId() {
    return this.requestId;
  }
  
  public void setRequestId(String requestId) {
    this.requestId = requestId;
  }
  
  public String getReference() {
    return this.reference;
  }
  
  public void setReference(String reference) {
    this.reference = reference;
  }
  
  public String getSignature() {
    return this.signature;
  }
  
  public void setSignature(String signature) {
    this.signature = signature;
  }
  
  public PagosEstadoPlaceToPay getStatus() {
    return this.status;
  }
  
  public void setStatus(PagosEstadoPlaceToPay status) {
    this.status = status;
  }
  
  public static long getSerialversionuid() {
    return -8820699724602373914L;
  }
}

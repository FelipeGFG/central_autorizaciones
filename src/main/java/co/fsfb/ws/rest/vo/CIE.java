package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class CIE implements Serializable {
  private static final long serialVersionUID = 1L;
  
  String codServicio;
  
  String servicio;
  
  String codPrestacion;
  
  String prestacion;
  
  public String getCodServicio() {
    return this.codServicio;
  }
  
  public void setCodServicio(String codServicio) {
    this.codServicio = codServicio;
  }
  
  public String getServicio() {
    return this.servicio;
  }
  
  public void setServicio(String servicio) {
    this.servicio = servicio;
  }
  
  public String getCodPrestacion() {
    return this.codPrestacion;
  }
  
  public void setCodPrestacion(String codPrestacion) {
    this.codPrestacion = codPrestacion;
  }
  
  public String getPrestacion() {
    return this.prestacion;
  }
  
  public void setPrestacion(String prestacion) {
    this.prestacion = prestacion;
  }
}

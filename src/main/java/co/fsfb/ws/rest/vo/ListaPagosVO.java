package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class ListaPagosVO implements Serializable {
  private static final long serialVersionUID = -8820699724602373914L;
  
  private String idTransaccion;
  
  private String fechaRegistro;
  
  private Long referencia;
  
  private String descripcion;
  
  private String autorizacion;
  
  private String estado;
  
  private String valor;
  
  private String numeroFactura;
  
  public String getEstado() {
    return this.estado;
  }
  
  public void setEstado(String estado) {
    this.estado = estado;
  }
  
  public String getIdTransaccion() {
    return this.idTransaccion;
  }
  
  public void setIdTransaccion(String idTransaccion) {
    this.idTransaccion = idTransaccion;
  }
  
  public String getFechaRegistro() {
    return this.fechaRegistro;
  }
  
  public void setFechaRegistro(String fechaRegistro) {
    this.fechaRegistro = fechaRegistro;
  }
  
  public Long getReferencia() {
    return this.referencia;
  }
  
  public void setReferencia(Long referencia) {
    this.referencia = referencia;
  }
  
  public String getDescripcion() {
    return this.descripcion;
  }
  
  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }
  
  public String getAutorizacion() {
    return this.autorizacion;
  }
  
  public void setAutorizacion(String autorizacion) {
    this.autorizacion = autorizacion;
  }
  
  public String getValor() {
    return this.valor;
  }
  
  public void setValor(String valor) {
    this.valor = valor;
  }
  
  public static long getSerialversionuid() {
    return -8820699724602373914L;
  }
  
  public String getNumeroFactura() {
    return this.numeroFactura;
  }
  
  public void setNumeroFactura(String numeroFactura) {
    this.numeroFactura = numeroFactura;
  }
}

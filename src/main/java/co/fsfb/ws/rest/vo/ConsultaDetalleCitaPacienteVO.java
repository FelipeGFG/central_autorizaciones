package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class ConsultaDetalleCitaPacienteVO implements Serializable {
  private static final long serialVersionUID = 1L;
  
  private String nombreCompleto;
  
  private String tipoDocumento;
  
  private String documento;
  
  private String telefono;
  
  private String correoElectronico;
  
  private String fechaCita;
  
  private String horaCita;
  
  private String especialidad;
  
  private String profesional;
  
  private String consultorio;
  
  private String prestacion;
  
  private String centroOperativo;
  
  private String convenio;
  
  private String usuarioAsignaCita;
  
  private String fechaAsignacion;
  
  public String getNombreCompleto() {
    return this.nombreCompleto;
  }
  
  public void setNombreCompleto(String nombreCompleto) {
    this.nombreCompleto = nombreCompleto;
  }
  
  public String getTipoDocumento() {
    return this.tipoDocumento;
  }
  
  public void setTipoDocumento(String tipoDocumento) {
    this.tipoDocumento = tipoDocumento;
  }
  
  public String getDocumento() {
    return this.documento;
  }
  
  public void setDocumento(String documento) {
    this.documento = documento;
  }
  
  public String getTelefono() {
    return this.telefono;
  }
  
  public void setTelefono(String telefono) {
    this.telefono = telefono;
  }
  
  public String getCorreoElectronico() {
    return this.correoElectronico;
  }
  
  public void setCorreoElectronico(String correoElectronico) {
    this.correoElectronico = correoElectronico;
  }
  
  public String getFechaCita() {
    return this.fechaCita;
  }
  
  public void setFechaCita(String fechaCita) {
    this.fechaCita = fechaCita;
  }
  
  public String getHoraCita() {
    return this.horaCita;
  }
  
  public void setHoraCita(String horaCita) {
    this.horaCita = horaCita;
  }
  
  public String getEspecialidad() {
    return this.especialidad;
  }
  
  public void setEspecialidad(String especialidad) {
    this.especialidad = especialidad;
  }
  
  public String getProfesional() {
    return this.profesional;
  }
  
  public void setProfesional(String profesional) {
    this.profesional = profesional;
  }
  
  public String getConsultorio() {
    return this.consultorio;
  }
  
  public void setConsultorio(String consultorio) {
    this.consultorio = consultorio;
  }
  
  public String getPrestacion() {
    return this.prestacion;
  }
  
  public void setPrestacion(String prestacion) {
    this.prestacion = prestacion;
  }
  
  public String getCentroOperativo() {
    return this.centroOperativo;
  }
  
  public void setCentroOperativo(String centroOperativo) {
    this.centroOperativo = centroOperativo;
  }
  
  public String getConvenio() {
    return this.convenio;
  }
  
  public void setConvenio(String convenio) {
    this.convenio = convenio;
  }
  
  public String getUsuarioAsignaCita() {
    return this.usuarioAsignaCita;
  }
  
  public void setUsuarioAsignaCita(String usuarioAsignaCita) {
    this.usuarioAsignaCita = usuarioAsignaCita;
  }
  
  public String getFechaAsignacion() {
    return this.fechaAsignacion;
  }
  
  public void setFechaAsignacion(String fechaAsignacion) {
    this.fechaAsignacion = fechaAsignacion;
  }
}

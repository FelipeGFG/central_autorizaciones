package co.fsfb.ws.rest.vo;

import java.io.Serializable;
import java.util.ArrayList;

public class PlanAlta implements Serializable {
  private static final long serialVersionUID = 2544966498843873094L;
  
  private String codDiagnostico;
  
  private String descDiagnostico;
  
  private String dieta;
  
  private String actividadFisica;
  
  private String recomEspeciales;
  
  private String profesional;
  
  private String registro;
  
  private String fecha;
  
  private String diasControl;
  
  private String consultorio;
  
  private String indMedicinaPrepagada;
  
  private String indEps;
  
  private String alertas;
  
  private String teleProfesional;
  
  private String direProfesional;
  
  private String contraReferencia;
  
  private String tabEncuentro;
  
  private ArrayList<PlanAltaMedicamento> listaMedicamentos;
  
  private ArrayList<PlanAltaOrden> listaOrdenes;
  
  private ArrayList<PlanAltaRecomendacion> listaRecomendaciones;
  
  public String getCodDiagnostico() {
    return this.codDiagnostico;
  }
  
  public void setCodDiagnostico(String codDiagnostico) {
    this.codDiagnostico = codDiagnostico;
  }
  
  public String getDescDiagnostico() {
    return this.descDiagnostico;
  }
  
  public void setDescDiagnostico(String descDiagnostico) {
    this.descDiagnostico = descDiagnostico;
  }
  
  public String getDieta() {
    return this.dieta;
  }
  
  public void setDieta(String dieta) {
    this.dieta = dieta;
  }
  
  public String getActividadFisica() {
    return this.actividadFisica;
  }
  
  public void setActividadFisica(String actividadFisica) {
    this.actividadFisica = actividadFisica;
  }
  
  public String getRecomEspeciales() {
    return this.recomEspeciales;
  }
  
  public void setRecomEspeciales(String recomEspeciales) {
    this.recomEspeciales = recomEspeciales;
  }
  
  public String getProfesional() {
    return this.profesional;
  }
  
  public void setProfesional(String profesional) {
    this.profesional = profesional;
  }
  
  public String getRegistro() {
    return this.registro;
  }
  
  public void setRegistro(String registro) {
    this.registro = registro;
  }
  
  public String getFecha() {
    return this.fecha;
  }
  
  public void setFecha(String fecha) {
    this.fecha = fecha;
  }
  
  public String getDiasControl() {
    return this.diasControl;
  }
  
  public void setDiasControl(String diasControl) {
    this.diasControl = diasControl;
  }
  
  public String getConsultorio() {
    return this.consultorio;
  }
  
  public void setConsultorio(String consultorio) {
    this.consultorio = consultorio;
  }
  
  public String getIndMedicinaPrepagada() {
    return this.indMedicinaPrepagada;
  }
  
  public void setIndMedicinaPrepagada(String indMedicinaPrepagada) {
    this.indMedicinaPrepagada = indMedicinaPrepagada;
  }
  
  public String getIndEps() {
    return this.indEps;
  }
  
  public void setIndEps(String indEps) {
    this.indEps = indEps;
  }
  
  public String getAlertas() {
    return this.alertas;
  }
  
  public void setAlertas(String alertas) {
    this.alertas = alertas;
  }
  
  public String getTeleProfesional() {
    return this.teleProfesional;
  }
  
  public void setTeleProfesional(String teleProfesional) {
    this.teleProfesional = teleProfesional;
  }
  
  public String getDireProfesional() {
    return this.direProfesional;
  }
  
  public void setDireProfesional(String direProfesional) {
    this.direProfesional = direProfesional;
  }
  
  public String getContraReferencia() {
    return this.contraReferencia;
  }
  
  public void setContraReferencia(String contraReferencia) {
    this.contraReferencia = contraReferencia;
  }
  
  public String getTabEncuentro() {
    return this.tabEncuentro;
  }
  
  public void setTabEncuentro(String tabEncuentro) {
    this.tabEncuentro = tabEncuentro;
  }
  
  public ArrayList<PlanAltaMedicamento> getListaMedicamentos() {
    return this.listaMedicamentos;
  }
  
  public void setListaMedicamentos(ArrayList<PlanAltaMedicamento> listaMedicamentos) {
    this.listaMedicamentos = listaMedicamentos;
  }
  
  public ArrayList<PlanAltaOrden> getListaOrdenes() {
    return this.listaOrdenes;
  }
  
  public void setListaOrdenes(ArrayList<PlanAltaOrden> listaOrdenes) {
    this.listaOrdenes = listaOrdenes;
  }
  
  public ArrayList<PlanAltaRecomendacion> getListaRecomendaciones() {
    return this.listaRecomendaciones;
  }
  
  public void setListaRecomendaciones(ArrayList<PlanAltaRecomendacion> listaRecomendaciones) {
    this.listaRecomendaciones = listaRecomendaciones;
  }
  
  public String obtenerCadenaPlanAlta(int indTipoUsuario) {
    String planDeAlta = "";
    if (indTipoUsuario == 0)
      planDeAlta = planDeAlta + "<br><b><u><i>DIAGNOSTICO PRINCIPAL</i></u></b><br>" + this.codDiagnostico + " - " + this.descDiagnostico + "<br>"; 
    planDeAlta = planDeAlta + "<br><b><u><i>DIETA</i></u></b><br>" + this.dieta + "<br>";
    planDeAlta = planDeAlta + "<br><br><b><u><i>ACTIVIDAD FISICA</i></u></b><br>" + this.actividadFisica + "<br>";
    planDeAlta = planDeAlta + "<br><br><b><u><i>RECOMENDACIONES ESPECIALES</i></u></b><br>" + this.recomEspeciales + "<br>";
    planDeAlta = planDeAlta + "<br><br><b><u><i>PROFESIONAL DE SALUD</i></u></b><br>" + this.profesional + "<br>";
    planDeAlta = planDeAlta + "<br><br><b><u><i>REGISTRO</i></u></b><br>" + this.registro + "<br>";
    planDeAlta = planDeAlta + "<br><br><b><u><i></i>FECHA</u></b><br>" + this.fecha + "<br>";
    planDeAlta = planDeAlta + "<br><br><b><u><i>DIAS DE CONTROL</i></u></b><br>" + this.diasControl + "<br>";
    planDeAlta = planDeAlta + "<br><br><b><u><i>CONSULTORIO MEDICO</i></u></b><br>" + this.consultorio + "<br>";
    planDeAlta = planDeAlta + "<br><br><b><u><i>MEDICINA PREPAGADA</i></u></b><br>" + this.indMedicinaPrepagada + "<br>";
    planDeAlta = planDeAlta + "<br><br><b><u><i>EPS</i></u></b><br>" + this.indEps + "<br>";
    planDeAlta = planDeAlta + "<br><br><b><u><i>ALERTAS</i></u></b><br>" + this.alertas + "<br>";
    planDeAlta = planDeAlta + "<br><br><b><u><i>TELEFONO PROFESIONAL</i></u></b><br>" + this.teleProfesional + "<br>";
    planDeAlta = planDeAlta + "<br><br><b><u><i>DIRECCION PROFESIONAL</i></u></b><br>" + this.direProfesional + "<br>";
    planDeAlta = planDeAlta + "<br><br><b><u><i>CONTRAREFERENCIA</i></u></b><br>" + this.contraReferencia + "<br>";
    planDeAlta = planDeAlta.concat("<br><b><u><i>MEDICAMENTOS ORDENADOS</i></u></b><br>");
    planDeAlta = planDeAlta.concat("<table border='1'>");
    int i;
    for (i = 0; i < this.listaMedicamentos.size(); i++) {
      PlanAltaMedicamento pmedicamento = this.listaMedicamentos.get(i);
      planDeAlta = planDeAlta.concat(pmedicamento.obtenerCadena());
    } 
    planDeAlta = planDeAlta.concat("</table>");
    planDeAlta = planDeAlta.concat("<br><b><u><i>ORDENES DE SERVICIO</i></u></b><br>");
    planDeAlta = planDeAlta.concat("<table border='1'>");
    for (i = 0; i < this.listaOrdenes.size(); i++) {
      PlanAltaOrden porden = this.listaOrdenes.get(i);
      planDeAlta = planDeAlta.concat(porden.obtenerCadena());
    } 
    planDeAlta = planDeAlta.concat("</table>");
    planDeAlta = planDeAlta.concat("<br><b><u><i>RECOMENDACIONES</i></u></b><br>");
    planDeAlta = planDeAlta.concat("<table border='1'>");
    for (i = 0; i < this.listaRecomendaciones.size(); i++) {
      PlanAltaRecomendacion precomendacion = this.listaRecomendaciones.get(i);
      planDeAlta = planDeAlta.concat(precomendacion.obtenerCadena());
    } 
    planDeAlta = planDeAlta.concat("</table>");
    return planDeAlta;
  }
}

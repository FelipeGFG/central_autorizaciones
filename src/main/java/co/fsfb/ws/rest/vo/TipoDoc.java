package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class TipoDoc implements Serializable {
  private static final long serialVersionUID = 1L;
  
  String strTipIdeCod;
  
  String strTipIdeGlosa;
  
  String strTipIdav;
  
  public String getStrTipIdeCod() {
    return this.strTipIdeCod;
  }
  
  public void setStrTipIdeCod(String strTipIdeCod) {
    this.strTipIdeCod = strTipIdeCod;
  }
  
  public String getStrTipIdeGlosa() {
    return this.strTipIdeGlosa;
  }
  
  public void setStrTipIdeGlosa(String strTipIdeGlosa) {
    this.strTipIdeGlosa = strTipIdeGlosa;
  }
  
  public String getStrTipIdav() {
    return this.strTipIdav;
  }
  
  public void setStrTipIdav(String strTipIdav) {
    this.strTipIdav = strTipIdav;
  }
}

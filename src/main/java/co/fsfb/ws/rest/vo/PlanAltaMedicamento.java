package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class PlanAltaMedicamento implements Serializable {
  private static final long serialVersionUID = 6954688035669048144L;
  
  private String codigo;
  
  private String descripcion;
  
  private String numero;
  
  private String dosis;
  
  private String secuencia;
  
  private String numReceta;
  
  public String getCodigo() {
    return this.codigo;
  }
  
  public void setCodigo(String codigo) {
    this.codigo = codigo;
  }
  
  public String getDescripcion() {
    return this.descripcion;
  }
  
  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }
  
  public String getNumero() {
    return this.numero;
  }
  
  public void setNumero(String numero) {
    this.numero = numero;
  }
  
  public String getDosis() {
    return this.dosis;
  }
  
  public void setDosis(String dosis) {
    this.dosis = dosis;
  }
  
  public String getSecuencia() {
    return this.secuencia;
  }
  
  public void setSecuencia(String secuencia) {
    this.secuencia = secuencia;
  }
  
  public String getNumReceta() {
    return this.numReceta;
  }
  
  public void setNumReceta(String numReceta) {
    this.numReceta = numReceta;
  }
  
  public String obtenerCadena() {
    String planDeAlta = "";
    planDeAlta = planDeAlta.concat("<tr><td>" + this.codigo + "</td><td>" + this.descripcion + "</td><td>" + this.numero + "</td><td>" + this.dosis + "</td><td>" + this.secuencia + "</td><td>" + this.numReceta + "</td></tr>");
    return planDeAlta;
  }
}

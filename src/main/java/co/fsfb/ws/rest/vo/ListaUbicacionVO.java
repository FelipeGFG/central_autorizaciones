package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class ListaUbicacionVO implements Serializable {
  private static final long serialVersionUID = 1L;
  
  private String id;
  
  private String descripcion;
  
  private String otro;
  
  private String otros;
  
  public String getId() {
    return this.id;
  }
  
  public void setId(String id) {
    this.id = id;
  }
  
  public String getDescripcion() {
    return this.descripcion;
  }
  
  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }
  
  public String getOtro() {
    return this.otro;
  }
  
  public void setOtro(String otro) {
    this.otro = otro;
  }
  
  public String getOtros() {
    return this.otros;
  }
  
  public void setOtros(String otros) {
    this.otros = otros;
  }
  
  public static long getSerialversionuid() {
    return 1L;
  }
}

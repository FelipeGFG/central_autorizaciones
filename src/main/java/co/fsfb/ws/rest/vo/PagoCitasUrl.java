package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class PagoCitasUrl implements Serializable {
  private static final long serialVersionUID = -8820699724602373914L;
  
  private PersonP2P compradorP2P;
  
  private PersonP2P pagadorP2P;
  
  private String name;
  
  private String surname;
  
  private String email;
  
  private String document;
  
  private String documentType;
  
  private String mobile;
  
  private String reference;
  
  private String description;
  
  private String currency;
  
  private String total;
  
  private String ipAddress;
  
  private String returnUrl;
  
  private String userAgent;
  
  private String expiration;
  
  private String documentTypePlaceToPay;
  
  public PersonP2P getCompradorP2P() {
    return this.compradorP2P;
  }
  
  public void setCompradorP2P(PersonP2P compradorP2P) {
    this.compradorP2P = compradorP2P;
  }
  
  public PersonP2P getPagadorP2P() {
    return this.pagadorP2P;
  }
  
  public void setPagadorP2P(PersonP2P pagadorP2P) {
    this.pagadorP2P = pagadorP2P;
  }
  
  public String getName() {
    return this.name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public String getSurname() {
    return this.surname;
  }
  
  public void setSurname(String surname) {
    this.surname = surname;
  }
  
  public String getEmail() {
    return this.email;
  }
  
  public void setEmail(String email) {
    this.email = email;
  }
  
  public String getDocument() {
    return this.document;
  }
  
  public void setDocument(String document) {
    this.document = document;
  }
  
  public String getDocumentType() {
    return this.documentType;
  }
  
  public void setDocumentType(String documentType) {
    this.documentType = documentType;
  }
  
  public String getMobile() {
    return this.mobile;
  }
  
  public void setMobile(String mobile) {
    this.mobile = mobile;
  }
  
  public String getReference() {
    return this.reference;
  }
  
  public void setReference(String reference) {
    this.reference = reference;
  }
  
  public String getDescription() {
    return this.description;
  }
  
  public void setDescription(String description) {
    this.description = description;
  }
  
  public String getCurrency() {
    return this.currency;
  }
  
  public void setCurrency(String currency) {
    this.currency = currency;
  }
  
  public String getTotal() {
    return this.total;
  }
  
  public void setTotal(String total) {
    this.total = total;
  }
  
  public String getIpAddress() {
    return this.ipAddress;
  }
  
  public void setIpAddress(String ipAddress) {
    this.ipAddress = ipAddress;
  }
  
  public String getReturnUrl() {
    return this.returnUrl;
  }
  
  public void setReturnUrl(String returnUrl) {
    this.returnUrl = returnUrl;
  }
  
  public String getUserAgent() {
    return this.userAgent;
  }
  
  public void setUserAgent(String userAgent) {
    this.userAgent = userAgent;
  }
  
  public String getExpiration() {
    return this.expiration;
  }
  
  public void setExpiration(String expiration) {
    this.expiration = expiration;
  }
  
  public static long getSerialversionuid() {
    return -8820699724602373914L;
  }
  
  public String getDocumentTypePlaceToPay() {
    return this.documentTypePlaceToPay;
  }
  
  public void setDocumentTypePlaceToPay(String documentTypePlaceToPay) {
    this.documentTypePlaceToPay = documentTypePlaceToPay;
  }
}

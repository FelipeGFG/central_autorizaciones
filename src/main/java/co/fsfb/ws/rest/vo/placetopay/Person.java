package co.fsfb.ws.rest.vo.placetopay;

import java.io.Serializable;

public class Person implements Serializable {
  private String documentType;
  
  private String document;
  
  private String name;
  
  private String surname;
  
  private String email;
  
  private String mobile;
  
  public String getDocumentType() {
    return this.documentType;
  }
  
  public void setDocumentType(String documentType) {
    this.documentType = documentType;
  }
  
  public String getDocument() {
    return this.document;
  }
  
  public void setDocument(String document) {
    this.document = document;
  }
  
  public String getName() {
    return this.name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public String getSurname() {
    return this.surname;
  }
  
  public void setSurname(String surname) {
    this.surname = surname;
  }
  
  public String getEmail() {
    return this.email;
  }
  
  public void setEmail(String email) {
    this.email = email;
  }
  
  public String getMobile() {
    return this.mobile;
  }
  
  public void setMobile(String mobile) {
    this.mobile = mobile;
  }
}

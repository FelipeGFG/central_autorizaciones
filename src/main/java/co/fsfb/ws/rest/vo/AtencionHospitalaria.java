package co.fsfb.ws.rest.vo;

import java.io.Serializable;
import java.util.Date;

public class AtencionHospitalaria implements Serializable {
  private static final long serialVersionUID = 1L;
  
  private Date fechaHora;
  
  private String ambito;
  
  private String especialidad;
  
  private String medicoTratante;
  
  private String edadAlEvento;
  
  private String fechaInicial;
  
  private String fechaFinal;
  
  private Usuario usr;
  
  public String getFechaInicial() {
    return this.fechaInicial;
  }
  
  public void setFechaInicial(String fechaInicial) {
    this.fechaInicial = fechaInicial;
  }
  
  public String getFechaFinal() {
    return this.fechaFinal;
  }
  
  public void setFechaFinal(String fechaFinal) {
    this.fechaFinal = fechaFinal;
  }
  
  public Usuario getUsr() {
    return this.usr;
  }
  
  public void setUsr(Usuario usr) {
    this.usr = usr;
  }
  
  public Date getFechaHora() {
    return this.fechaHora;
  }
  
  public void setFechaHora(Date fechaHora) {
    this.fechaHora = fechaHora;
  }
  
  public String getAmbito() {
    return this.ambito;
  }
  
  public void setAmbito(String ambito) {
    this.ambito = ambito;
  }
  
  public String getEspecialidad() {
    return this.especialidad;
  }
  
  public void setEspecialidad(String especialidad) {
    this.especialidad = especialidad;
  }
  
  public String getMedicoTratante() {
    return this.medicoTratante;
  }
  
  public void setMedicoTratante(String medicoTratante) {
    this.medicoTratante = medicoTratante;
  }
  
  public String getEdadAlEvento() {
    return this.edadAlEvento;
  }
  
  public void setEdadAlEvento(String edadAlEvento) {
    this.edadAlEvento = edadAlEvento;
  }
}

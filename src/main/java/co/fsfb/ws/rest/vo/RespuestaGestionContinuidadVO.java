package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class RespuestaGestionContinuidadVO implements Serializable {
  private static final long serialVersionUID = 1L;
  
  private boolean success;
  
  private String respMensaje;
  
  public RespuestaGestionContinuidadVO() {}
  
  public RespuestaGestionContinuidadVO(boolean success, String respMensaje) {
    this.success = success;
    this.respMensaje = respMensaje;
  }
  
  public boolean isSuccess() {
    return this.success;
  }
  
  public void setSuccess(boolean success) {
    this.success = success;
  }
  
  public String getRespMensaje() {
    return this.respMensaje;
  }
  
  public void setRespMensaje(String respMensaje) {
    this.respMensaje = respMensaje;
  }
}

package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class ConsultaSharePointVO implements Serializable {
  private static final long serialVersionUID = 1L;
  
  private String tdsDescripcion;
  
  private String tipIdDav;
  
  private String pacPacRut;
  
  private String tdsSigla;
  
  private Long domShaIdentificador;
  
  private String domShaUrl;
  
  public String getTdsDescripcion() {
    return this.tdsDescripcion;
  }
  
  public void setTdsDescripcion(String tdsDescripcion) {
    this.tdsDescripcion = tdsDescripcion;
  }
  
  public String getTipIdDav() {
    return this.tipIdDav;
  }
  
  public void setTipIdDav(String tipIdDav) {
    this.tipIdDav = tipIdDav;
  }
  
  public String getPacPacRut() {
    return this.pacPacRut;
  }
  
  public void setPacPacRut(String pacPacRut) {
    this.pacPacRut = pacPacRut;
  }
  
  public String getTdsSigla() {
    return this.tdsSigla;
  }
  
  public void setTdsSigla(String tdsSigla) {
    this.tdsSigla = tdsSigla;
  }
  
  public Long getDomShaIdentificador() {
    return this.domShaIdentificador;
  }
  
  public void setDomShaIdentificador(Long domShaIdentificador) {
    this.domShaIdentificador = domShaIdentificador;
  }
  
  public String getDomShaUrl() {
    return this.domShaUrl;
  }
  
  public void setDomShaUrl(String domShaUrl) {
    this.domShaUrl = domShaUrl;
  }
}

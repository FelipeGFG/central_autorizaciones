package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class FiltrosDetalleCitaPacienteVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String pacNumero;

    private String tipoDocumento;

    private String documento;

    private String fechaCita;

    private String ageCodigoServi;

    private String codigoCentroAten;

    private String codigoProfe;

    private String conCodigo;

    private String codigoRecep;

    public String getTipoDocumento() {
        return this.tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getDocumento() {
        return this.documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getAgeCodigoServi() {
        return this.ageCodigoServi;
    }

    public void setAgeCodigoServi(String ageCodigoServi) {
        this.ageCodigoServi = ageCodigoServi;
    }

    public String getCodigoCentroAten() {
        return this.codigoCentroAten;
    }

    public void setCodigoCentroAten(String codigoCentroAten) {
        this.codigoCentroAten = codigoCentroAten;
    }

    public String getCodigoProfe() {
        return this.codigoProfe;
    }

    public void setCodigoProfe(String codigoProfe) {
        this.codigoProfe = codigoProfe;
    }

    public String getConCodigo() {
        return this.conCodigo;
    }

    public void setConCodigo(String conCodigo) {
        this.conCodigo = conCodigo;
    }

    public String getCodigoRecep() {
        return this.codigoRecep;
    }

    public void setCodigoRecep(String codigoRecep) {
        this.codigoRecep = codigoRecep;
    }

    public String getFechaCita() {
        return this.fechaCita;
    }

    public void setFechaCita(String fechaCita) {
        this.fechaCita = fechaCita;
    }

    public String getPacNumero() {
        return this.pacNumero;
    }

    public void setPacNumero(String pacNumero) {
        this.pacNumero = pacNumero;
    }
}

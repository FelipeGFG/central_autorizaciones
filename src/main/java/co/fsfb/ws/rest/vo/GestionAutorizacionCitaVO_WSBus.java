package co.fsfb.ws.rest.vo;

import java.util.Date;

public class GestionAutorizacionCitaVO_WSBus {

    private String horaCita;

    private String fechaCita;

    private String codUsrCita;

    private Long pacNum;

    private String gauNombreAutorizador;

    private String gauTelefonoAutorizador;

    private String gauAutorizaServ;

    private Long mnaIdcodigo;

    private String omnDesc;

    private String gauCodigoAutorizacion;

    private String gauFechaAutorizacion;

    private String gauFechaVencAutorizacion;

    private Long gauVigenciaAutorizacion;

    private Long gauValorPrestacion;

    private Long gauCostoConvenio;

    private Long gauCostoPac;

    private String pcaAgeCodigoRecep;

    private Date cgFechaProceso;

    private boolean enviarCorreo;

    private String gauObservaciones;

    private String numeroPoliza;
    
    private String nombrePaciente;

    private String centroAtencion;
    
    private String idCita;
    private String nroFormulario;
    private String codigoPrestacion;
    private String codConvenio;
    private String nombreConvenio;

    public String getIdCita() {
        return idCita;
    }

    public void setIdCita(String idCita) {
        this.idCita = idCita;
    }

    public String getNroFormulario() {
        return nroFormulario;
    }

    public void setNroFormulario(String nroFormulario) {
        this.nroFormulario = nroFormulario;
    }

    public String getCodigoPrestacion() {
        return codigoPrestacion;
    }

    public void setCodigoPrestacion(String codigoPrestacion) {
        this.codigoPrestacion = codigoPrestacion;
    }

    public String getCodConvenio() {
        return codConvenio;
    }

    public void setCodConvenio(String codConvenio) {
        this.codConvenio = codConvenio;
    }
    
    
    
     
        
    public String getHoraCita() {
        return this.horaCita;
    }

    public void setHoraCita(String horaCita) {
        this.horaCita = horaCita;
    }

    public String getFechaCita() {
        return this.fechaCita;
    }

    public void setFechaCita(String fechaCita) {
        this.fechaCita = fechaCita;
    }

    public String getCodUsrCita() {
        return this.codUsrCita;
    }

    public void setCodUsrCita(String codUsrCita) {
        this.codUsrCita = codUsrCita;
    }

    public Long getPacNum() {
        return this.pacNum;
    }

    public void setPacNum(Long pacNum) {
        this.pacNum = pacNum;
    }

    public String getGauNombreAutorizador() {
        return this.gauNombreAutorizador;
    }

    public void setGauNombreAutorizador(String gauNombreAutorizador) {
        this.gauNombreAutorizador = gauNombreAutorizador;
    }

    public String getGauTelefonoAutorizador() {
        return this.gauTelefonoAutorizador;
    }

    public void setGauTelefonoAutorizador(String gauTelefonoAutorizador) {
        this.gauTelefonoAutorizador = gauTelefonoAutorizador;
    }

    public String getGauAutorizaServ() {
        return this.gauAutorizaServ;
    }

    public void setGauAutorizaServ(String gauAutorizaServ) {
        this.gauAutorizaServ = gauAutorizaServ;
    }

    public Long getMnaIdcodigo() {
        return this.mnaIdcodigo;
    }

    public void setMnaIdcodigo(Long mnaIdcodigo) {
        this.mnaIdcodigo = mnaIdcodigo;
    }

    public String getOmnDesc() {
        return this.omnDesc;
    }

    public void setOmnDesc(String omnDesc) {
        this.omnDesc = omnDesc;
    }

    public String getGauCodigoAutorizacion() {
        return this.gauCodigoAutorizacion;
    }

    public void setGauCodigoAutorizacion(String gauCodigoAutorizacion) {
        this.gauCodigoAutorizacion = gauCodigoAutorizacion;
    }

    public String getGauFechaAutorizacion() {
        return this.gauFechaAutorizacion;
    }

    public void setGauFechaAutorizacion(String gauFechaAutorizacion) {
        this.gauFechaAutorizacion = gauFechaAutorizacion;
    }

    public String getGauFechaVencAutorizacion() {
        return this.gauFechaVencAutorizacion;
    }

    public void setGauFechaVencAutorizacion(String gauFechaVencAutorizacion) {
        this.gauFechaVencAutorizacion = gauFechaVencAutorizacion;
    }

    public Long getGauVigenciaAutorizacion() {
        return this.gauVigenciaAutorizacion;
    }

    public void setGauVigenciaAutorizacion(Long gauVigenciaAutorizacion) {
        this.gauVigenciaAutorizacion = gauVigenciaAutorizacion;
    }

    public Long getGauValorPrestacion() {
        return this.gauValorPrestacion;
    }

    public void setGauValorPrestacion(Long gauValorPrestacion) {
        this.gauValorPrestacion = gauValorPrestacion;
    }

    public Long getGauCostoConvenio() {
        return this.gauCostoConvenio;
    }

    public void setGauCostoConvenio(Long gauCostoConvenio) {
        this.gauCostoConvenio = gauCostoConvenio;
    }

    public Long getGauCostoPac() {
        return this.gauCostoPac;
    }

    public void setGauCostoPac(Long gauCostoPac) {
        this.gauCostoPac = gauCostoPac;
    }

    public String getPcaAgeCodigoRecep() {
        return this.pcaAgeCodigoRecep;
    }

    public void setPcaAgeCodigoRecep(String pcaAgeCodigoRecep) {
        this.pcaAgeCodigoRecep = pcaAgeCodigoRecep;
    }

    public Date getCgFechaProceso() {
        return this.cgFechaProceso;
    }

    public void setCgFechaProceso(Date cgFechaProceso) {
        this.cgFechaProceso = cgFechaProceso;
    }

    public boolean isEnviarCorreo() {
        return this.enviarCorreo;
    }

    public void setEnviarCorreo(boolean enviarCorreo) {
        this.enviarCorreo = enviarCorreo;
    }

    public String getGauObservaciones() {
        return this.gauObservaciones;
    }

    public void setGauObservaciones(String gauObservaciones) {
        this.gauObservaciones = gauObservaciones;
    }

    public String getNumeroPoliza() {
        return this.numeroPoliza;
    }

    public void setNumeroPoliza(String numeroPoliza) {
        this.numeroPoliza = numeroPoliza;
    }

    public String getNombrePaciente() {
        return this.nombrePaciente;
    }

    public void setNombrePaciente(String nombrePaciente) {
        this.nombrePaciente = nombrePaciente;
    }

    public String getCentroAtencion() {
        return this.centroAtencion;
    }

    public void setCentroAtencion(String centroAtencion) {
        this.centroAtencion = centroAtencion;
    }
    
    public String getNombreConvenio() {
        return nombreConvenio;
    }

    public void setNombreConvenio(String nombreConvenio) {
        this.nombreConvenio = nombreConvenio;
    }
}

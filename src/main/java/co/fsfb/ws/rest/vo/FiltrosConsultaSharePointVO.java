package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class FiltrosConsultaSharePointVO implements Serializable {
  private static final long serialVersionUID = 1L;
  
  private Long pacPacNumero;
  
  private Long accIdNumero;
  
  public Long getPacPacNumero() {
    return this.pacPacNumero;
  }
  
  public void setPacPacNumero(Long pacPacNumero) {
    this.pacPacNumero = pacPacNumero;
  }
  
  public Long getAccIdNumero() {
    return this.accIdNumero;
  }
  
  public void setAccIdNumero(Long accIdNumero) {
    this.accIdNumero = accIdNumero;
  }
}

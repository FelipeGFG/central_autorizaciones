package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class Email implements Serializable {
  private static final long serialVersionUID = -1760854111470937910L;
  
  private String entidad;
  
  private String estado;
  
  private String estadoId;
  
  private String medico;
  
  private String consultorio;
  
  private String direccion;
  
  private String ciudad;
  
  private String hora;
  
  private String dia;
  
  private String asunto;
  
  public String getEntidad() {
    return this.entidad;
  }
  
  public void setEntidad(String entidad) {
    this.entidad = entidad;
  }
  
  public String getEstado() {
    return this.estado;
  }
  
  public void setEstado(String estado) {
    this.estado = estado;
  }
  
  public String getMedico() {
    return this.medico;
  }
  
  public void setMedico(String medico) {
    this.medico = medico;
  }
  
  public String getConsultorio() {
    return this.consultorio;
  }
  
  public void setConsultorio(String consultorio) {
    this.consultorio = consultorio;
  }
  
  public String getDireccion() {
    return this.direccion;
  }
  
  public void setDireccion(String direccion) {
    this.direccion = direccion;
  }
  
  public String getCiudad() {
    return this.ciudad;
  }
  
  public void setCiudad(String ciudad) {
    this.ciudad = ciudad;
  }
  
  public String getHora() {
    return this.hora;
  }
  
  public void setHora(String hora) {
    this.hora = hora;
  }
  
  public String getDia() {
    return this.dia;
  }
  
  public void setDia(String dia) {
    this.dia = dia;
  }
  
  public String getAsunto() {
    return this.asunto;
  }
  
  public void setAsunto(String asunto) {
    this.asunto = asunto;
  }
  
  public String getEstadoId() {
    return this.estadoId;
  }
  
  public void setEstadoId(String estadoId) {
    this.estadoId = estadoId;
  }
}

package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class TrazabilidadCitaVO implements Serializable {
  private static final long serialVersionUID = 1L;
  
  private Long pacNum;
  
  private String fechaCita;
  
  private String horaCita;
  
  private String codEspecialidad;
  
  private String codProf;
  
  private String[] codConvenio;
  
  private String codServicio;
  
  public Long getPacNum() {
    return this.pacNum;
  }
  
  public void setPacNum(Long pacNum) {
    this.pacNum = pacNum;
  }
  
  public String getFechaCita() {
    return this.fechaCita;
  }
  
  public void setFechaCita(String fechaCita) {
    this.fechaCita = fechaCita;
  }
  
  public String getHoraCita() {
    return this.horaCita;
  }
  
  public void setHoraCita(String horaCita) {
    this.horaCita = horaCita;
  }
  
  public String getCodEspecialidad() {
    return this.codEspecialidad;
  }
  
  public void setCodEspecialidad(String codEspecialidad) {
    this.codEspecialidad = codEspecialidad;
  }
  
  public String getCodProf() {
    return this.codProf;
  }
  
  public void setCodProf(String codProf) {
    this.codProf = codProf;
  }
  
  public String[] getCodConvenio() {
    return this.codConvenio;
  }
  
  public void setCodConvenio(String[] codConvenio) {
    this.codConvenio = codConvenio;
  }
  
  public String getCodServicio() {
    return this.codServicio;
  }
  
  public void setCodServicio(String codServicio) {
    this.codServicio = codServicio;
  }
}

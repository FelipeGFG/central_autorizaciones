package co.fsfb.ws.rest.vo.placetopay;

import java.io.Serializable;

public class Payment implements Serializable {
  private String reference;
  
  private String description;
  
  private Amount amount;
  
  public String getReference() {
    return this.reference;
  }
  
  public void setReference(String reference) {
    this.reference = reference;
  }
  
  public String getDescription() {
    return this.description;
  }
  
  public void setDescription(String description) {
    this.description = description;
  }
  
  public Amount getAmount() {
    return this.amount;
  }
  
  public void setAmount(Amount amount) {
    this.amount = amount;
  }
}

package co.fsfb.ws.rest.vo;

import co.fsfb.ws.rest.entidades.CaDetalleOrdenesMedicas;
import java.io.Serializable;

public class AdmOrdenesMedicasVO implements Serializable {
  private static final long serialVersionUID = 1L;

    private CaDetalleOrdenesMedicas caDetalleOrdenesMedicas;

    private String dorFechaOrdenmString;

    private Long accIdNumero;

    private String ecPolizaNumero;

    private String codUsrCita;

    private boolean eliminar;

    private Long momId;

    public CaDetalleOrdenesMedicas getCaDetalleOrdenesMedicas() {
        return this.caDetalleOrdenesMedicas;
    }

    public void setCaDetalleOrdenesMedicas(CaDetalleOrdenesMedicas caDetalleOrdenesMedicas) {
        this.caDetalleOrdenesMedicas = caDetalleOrdenesMedicas;
    }

    public String getDorFechaOrdenmString() {
        return this.dorFechaOrdenmString;
    }

    public void setDorFechaOrdenmString(String dorFechaOrdenmString) {
        this.dorFechaOrdenmString = dorFechaOrdenmString;
    }

    public Long getAccIdNumero() {
        return this.accIdNumero;
    }

    public void setAccIdNumero(Long accIdNumero) {
        this.accIdNumero = accIdNumero;
    }

    public String getEcPolizaNumero() {
        return this.ecPolizaNumero;
    }

    public void setEcPolizaNumero(String ecPolizaNumero) {
        this.ecPolizaNumero = ecPolizaNumero;
    }

    public String getCodUsrCita() {
        return this.codUsrCita;
    }

    public void setCodUsrCita(String codUsrCita) {
        this.codUsrCita = codUsrCita;
    }

    public boolean isEliminar() {
        return this.eliminar;
    }

    public void setEliminar(boolean eliminar) {
        this.eliminar = eliminar;
    }

    public Long getMomId() {
        return this.momId;
    }

    public void setMomId(Long momId) {
        this.momId = momId;
    }

}

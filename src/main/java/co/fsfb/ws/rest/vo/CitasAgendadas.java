package co.fsfb.ws.rest.vo;

import java.io.Serializable;
import java.util.List;

public class CitasAgendadas implements Serializable {
  private static final long serialVersionUID = -9002488102721284356L;
  
  public static final String CAMPO_FECHA_HORA = "fechaHora";
  
  public static final String CAMPO_NOMBRE = "nombre";
  
  private String fechaCita;
  
  private String fechaInicial;
  
  private String fechaFinal;
  
  private String fechaInicialHIS;
  
  private String fechaFinalHIS;
  
  private String horaCita;
  
  private String fechaFormateada;
  
  private String descripcionServ;
  
  private String nombreProf;
  
  private String codProf;
  
  private String codigoLugar;
  
  private String indRecepcionado;
  
  private String codCentroAten;
  
  private String nombreCentroAten;
  
  private String codigoPrestacion;
  
  private String nombrePrestacion;
  
  private String codEspecialidad;
  
  private String especialidad;
  
  private String consultorio;
  
  private String prestacion;
  
  private List<String> codConvenio;
  
  private String convenio;
  
  private String usrCita;
  
  private String codUsrCita;
  
  private String fechaAsigna;
  
  private String codServicio;
  
  private List<String> agenciaServicio;
  
  private Paciente paciente;
  
  public List<String> getCodConvenio() {
    return this.codConvenio;
  }
  
  public void setCodConvenio(List<String> codConvenio) {
    this.codConvenio = codConvenio;
  }
  
  public String getCodCentroAten() {
    return this.codCentroAten;
  }
  
  public void setCodCentroAten(String codCentroAten) {
    this.codCentroAten = codCentroAten;
  }
  
  public String getCodEspecialidad() {
    return this.codEspecialidad;
  }
  
  public void setCodEspecialidad(String codEspecialidad) {
    this.codEspecialidad = codEspecialidad;
  }
  
  public List<String> getAgenciaServicio() {
    return this.agenciaServicio;
  }
  
  public void setAgenciaServicio(List<String> agenciaServicio) {
    this.agenciaServicio = agenciaServicio;
  }
  
  public String getNombreCentroAten() {
    return this.nombreCentroAten;
  }
  
  public void setNombreCentroAten(String nombreCentroAten) {
    this.nombreCentroAten = nombreCentroAten;
  }
  
  public String getCodServicio() {
    return this.codServicio;
  }
  
  public void setCodServicio(String codServicio) {
    this.codServicio = codServicio;
  }
  
  public String getFechaInicialHIS() {
    return this.fechaInicialHIS;
  }
  
  public void setFechaInicialHIS(String fechaInicialHIS) {
    this.fechaInicialHIS = fechaInicialHIS;
  }
  
  public String getFechaFinalHIS() {
    return this.fechaFinalHIS;
  }
  
  public void setFechaFinalHIS(String fechaFinalHIS) {
    this.fechaFinalHIS = fechaFinalHIS;
  }
  
  public String getFechaInicial() {
    return this.fechaInicial;
  }
  
  public void setFechaInicial(String fechaInicial) {
    this.fechaInicial = fechaInicial;
  }
  
  public String getCodProf() {
    return this.codProf;
  }
  
  public void setCodProf(String codProf) {
    this.codProf = codProf;
  }
  
  public String getFechaFinal() {
    return this.fechaFinal;
  }
  
  public void setFechaFinal(String fechaFinal) {
    this.fechaFinal = fechaFinal;
  }
  
  public CitasAgendadas() {
    this.paciente = new Paciente();
  }
  
  public String getHoraCita() {
    return this.horaCita;
  }
  
  public void setHoraCita(String horaCita) {
    this.horaCita = horaCita;
  }
  
  public String getEspecialidad() {
    return this.especialidad;
  }
  
  public void setEspecialidad(String especialidad) {
    this.especialidad = especialidad;
  }
  
  public String getConsultorio() {
    return this.consultorio;
  }
  
  public void setConsultorio(String consultorio) {
    this.consultorio = consultorio;
  }
  
  public String getPrestacion() {
    return this.prestacion;
  }
  
  public void setPrestacion(String prestacion) {
    this.prestacion = prestacion;
  }
  
  public String getConvenio() {
    return this.convenio;
  }
  
  public void setConvenio(String convenio) {
    this.convenio = convenio;
  }
  
  public String getUsrCita() {
    return this.usrCita;
  }
  
  public void setUsrCita(String usrCita) {
    this.usrCita = usrCita;
  }
  
  public String getFechaAsigna() {
    return this.fechaAsigna;
  }
  
  public void setFechaAsigna(String fechaAsigna) {
    this.fechaAsigna = fechaAsigna;
  }
  
  public Paciente getPaciente() {
    return this.paciente;
  }
  
  public void setPaciente(Paciente paciente) {
    this.paciente = paciente;
  }
  
  public String getFechaCita() {
    return this.fechaCita;
  }
  
  public String getCodigoPrestacion() {
    return this.codigoPrestacion;
  }
  
  public void setCodigoPrestacion(String codigoPrestacion) {
    this.codigoPrestacion = codigoPrestacion;
  }
  
  public String getNombrePrestacion() {
    return this.nombrePrestacion;
  }
  
  public void setNombrePrestacion(String nombrePrestacion) {
    this.nombrePrestacion = nombrePrestacion;
  }
  
  public void setFechaCita(String fechaCita) {
    this.fechaCita = fechaCita;
  }
  
  public String getFechaFormateada() {
    return this.fechaFormateada;
  }
  
  public void setFechaFormateada(String fechaFormateada) {
    this.fechaFormateada = fechaFormateada;
  }
  
  public String getDescripcionServ() {
    return this.descripcionServ;
  }
  
  public void setDescripcionServ(String descripcionServ) {
    this.descripcionServ = descripcionServ;
  }
  
  public String getNombreProf() {
    return this.nombreProf;
  }
  
  public void setNombreProf(String nombreProf) {
    this.nombreProf = nombreProf;
  }
  
  public String getCodigoLugar() {
    return this.codigoLugar;
  }
  
  public void setCodigoLugar(String codigoLugar) {
    this.codigoLugar = codigoLugar;
  }
  
  public String getIndRecepcionado() {
    return this.indRecepcionado;
  }
  
  public void setIndRecepcionado(String indRecepcionado) {
    this.indRecepcionado = indRecepcionado;
  }
  
  public static String getCAMPO_FECHA_HORA() {
    return "fechaHora";
  }
  
  public static String getCAMPO_NOMBRE() {
    return "nombre";
  }
  
  public int compareTo(CitasAgendadas citasAgendadasDTO) {
    if (this.fechaCita == null)
      return -1; 
    if (citasAgendadasDTO == null || citasAgendadasDTO.getFechaCita() == null)
      return 1; 
    return this.fechaCita.compareTo(citasAgendadasDTO.getFechaCita());
  }
  
  public String getCodUsrCita() {
    return this.codUsrCita;
  }
  
  public void setCodUsrCita(String codUsrCita) {
    this.codUsrCita = codUsrCita;
  }
}

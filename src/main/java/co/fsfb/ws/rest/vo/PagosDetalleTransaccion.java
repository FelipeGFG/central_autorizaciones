package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class PagosDetalleTransaccion implements Serializable {
  private static final long serialVersionUID = -8820699724602373914L;
  
  private Long id;
  
  private String numeroObligacion;
  
  private String ValorObligacion;
  
  public Long getId() {
    return this.id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public String getNumeroObligacion() {
    return this.numeroObligacion;
  }
  
  public void setNumeroObligacion(String numeroObligacion) {
    this.numeroObligacion = numeroObligacion;
  }
  
  public String getValorObligacion() {
    return this.ValorObligacion;
  }
  
  public void setValorObligacion(String valorObligacion) {
    this.ValorObligacion = valorObligacion;
  }
  
  public static long getSerialversionuid() {
    return -8820699724602373914L;
  }
}

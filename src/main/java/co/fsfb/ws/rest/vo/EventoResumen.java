package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class EventoResumen implements Serializable {
  private static final long serialVersionUID = 1368079367805313948L;
  
  private String fechaHora;
  
  private String descFechaHora;
  
  private String centroAtencion;
  
  private String ambito;
  
  private String diagnostico;
  
  private String especialidad;
  
  private String medicoTratante;
  
  private String edadAlEvento;
  
  private String convenio;
  
  private long idHisEvento;
  
  private String noCama;
  
  public String getNoCama() {
    return this.noCama;
  }
  
  public void setNoCama(String noCama) {
    this.noCama = noCama;
  }
  
  public String getCentroAtencion() {
    return this.centroAtencion;
  }
  
  public void setCentroAtencion(String centroAtencion) {
    this.centroAtencion = centroAtencion;
  }
  
  public long getIdHisEvento() {
    return this.idHisEvento;
  }
  
  public void setIdHisEvento(long idHisEvento) {
    this.idHisEvento = idHisEvento;
  }
  
  public String getFechaHora() {
    return this.fechaHora;
  }
  
  public void setFechaHora(String fechaHora) {
    this.fechaHora = fechaHora;
  }
  
  public String getAmbito() {
    return this.ambito;
  }
  
  public void setAmbito(String ambito) {
    this.ambito = ambito;
  }
  
  public String getConvenio() {
    return this.convenio;
  }
  
  public void setConvenio(String convenio) {
    this.convenio = convenio;
  }
  
  public String getEdadAlEvento() {
    return this.edadAlEvento;
  }
  
  public void setEdadAlEvento(String edadAlEvento) {
    this.edadAlEvento = edadAlEvento;
  }
  
  public String getMedicoTratante() {
    return this.medicoTratante;
  }
  
  public void setMedicoTratante(String medicoTratante) {
    this.medicoTratante = medicoTratante;
  }
  
  public String getEspecialidad() {
    return this.especialidad;
  }
  
  public void setEspecialidad(String especialidad) {
    this.especialidad = especialidad;
  }
  
  public String getDiagnostico() {
    return this.diagnostico;
  }
  
  public void setDiagnostico(String diagnostico) {
    this.diagnostico = diagnostico;
  }
  
  public String getDescFechaHora() {
    return this.descFechaHora;
  }
  
  public void setDescFechaHora(String descFechaHora) {
    this.descFechaHora = descFechaHora;
  }
}

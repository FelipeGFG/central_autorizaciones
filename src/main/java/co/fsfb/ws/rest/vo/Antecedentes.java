package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class Antecedentes implements Serializable {
  private static final long serialVersionUID = -1760854111470937910L;
  
  private String codigo;
  
  private String descripcion;
  
  private int secuencia;
  
  public String getCodigo() {
    return this.codigo;
  }
  
  public void setCodigo(String codigo) {
    this.codigo = codigo;
  }
  
  public String getDescripcion() {
    return this.descripcion;
  }
  
  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }
  
  public int getSecuencia() {
    return this.secuencia;
  }
  
  public void setSecuencia(int secuencia) {
    this.secuencia = secuencia;
  }
}

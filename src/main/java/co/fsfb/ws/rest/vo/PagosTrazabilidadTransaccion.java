package co.fsfb.ws.rest.vo;

import java.io.Serializable;
import java.sql.Date;

public class PagosTrazabilidadTransaccion implements Serializable {
  private static final long serialVersionUID = -8820699724602373914L;
  
  private Long id;
  
  private Date fechaInicio;
  
  private String fechaFin;
  
  private String estado;
  
  public String getEstado() {
    return this.estado;
  }
  
  public void setEstado(String estado) {
    this.estado = estado;
  }
  
  public Long getId() {
    return this.id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public Date getFechaInicio() {
    return this.fechaInicio;
  }
  
  public void setFechaInicio(Date fechaInicio) {
    this.fechaInicio = fechaInicio;
  }
  
  public String getFechaFin() {
    return this.fechaFin;
  }
  
  public void setFechaFin(String fechaFin) {
    this.fechaFin = fechaFin;
  }
  
  public static long getSerialversionuid() {
    return -8820699724602373914L;
  }
}

package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class ConsultaCitaPacientePorAutoVO implements Serializable {
  private static final long serialVersionUID = 1L;
  
  private String fechaInicial;
  
  private String fechaFinal;
  
  private Long estado;
  
  public String getFechaInicial() {
    return this.fechaInicial;
  }
  
  public void setFechaInicial(String fechaInicial) {
    this.fechaInicial = fechaInicial;
  }
  
  public String getFechaFinal() {
    return this.fechaFinal;
  }
  
  public void setFechaFinal(String fechaFinal) {
    this.fechaFinal = fechaFinal;
  }
  
  public Long getEstado() {
    return this.estado;
  }
  
  public void setEstado(Long estado) {
    this.estado = estado;
  }
}

package co.fsfb.ws.rest.vo;

import co.fsfb.ws.rest.entidades.CaPrestacionesOrdMed;
import java.util.List;

public class CrearPrestacionesOrdMedVO {
  private Long ormIdOrdmNumero;
  
  private String pcaAgeCodigRecep;
  
  private List<CaPrestacionesOrdMed> caPrestacionesOrdMed;
  
  public Long getOrmIdOrdmNumero() {
    return this.ormIdOrdmNumero;
  }
  
  public void setOrmIdOrdmNumero(Long ormIdOrdmNumero) {
    this.ormIdOrdmNumero = ormIdOrdmNumero;
  }
  
  public String getPcaAgeCodigRecep() {
    return this.pcaAgeCodigRecep;
  }
  
  public void setPcaAgeCodigRecep(String pcaAgeCodigRecep) {
    this.pcaAgeCodigRecep = pcaAgeCodigRecep;
  }
  
  public List<CaPrestacionesOrdMed> getCaPrestacionesOrdMed() {
    return this.caPrestacionesOrdMed;
  }
  
  public void setCaPrestacionesOrdMed(List<CaPrestacionesOrdMed> caPrestacionesOrdMed) {
    this.caPrestacionesOrdMed = caPrestacionesOrdMed;
  }
}

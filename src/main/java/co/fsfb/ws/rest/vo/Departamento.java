package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class Departamento implements Serializable {
  private static final long serialVersionUID = 1L;
  
  String strCodigo;
  
  String strNombre;
  
  String strVigencia;
  
  public String getStrNombre() {
    return this.strNombre;
  }
  
  public void setStrNombre(String strNombre) {
    this.strNombre = strNombre;
  }
  
  public String getStrVigencia() {
    return this.strVigencia;
  }
  
  public void setStrVigencia(String strVigencia) {
    this.strVigencia = strVigencia;
  }
  
  public String getStrCodigo() {
    return this.strCodigo;
  }
  
  public void setStrCodigo(String strCodigo) {
    this.strCodigo = strCodigo;
  }
}

package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class ConsultaPagosVO implements Serializable {
  private static final long serialVersionUID = -8820699724602373914L;
  
  private Integer noPacPaciente;
  
  public Integer getNoPacPaciente() {
    return this.noPacPaciente;
  }
  
  public void setNoPacPaciente(Integer noPacPaciente) {
    this.noPacPaciente = noPacPaciente;
  }
  
  public static long getSerialversionuid() {
    return -8820699724602373914L;
  }
}

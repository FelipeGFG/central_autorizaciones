package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class ConsultaTrazabilidadCitaVO implements Serializable {
  private static final long serialVersionUID = 1L;
  
  private Long cGIdCitaNumero;
  
  private String pcaAgeFechaDigit;
  
  private String cgFechaProceso;
  
  private String cgHoraProceso;
  
  private String cauDescUsuarios;
  
  private Integer ecIdCodigo;
  
  private String ecDescripcion;
  
  private String ecAsistencia;
  
  public Long getcGIdCitaNumero() {
    return this.cGIdCitaNumero;
  }
  
  public void setcGIdCitaNumero(Long cGIdCitaNumero) {
    this.cGIdCitaNumero = cGIdCitaNumero;
  }
  
  public String getPcaAgeFechaDigit() {
    return this.pcaAgeFechaDigit;
  }
  
  public void setPcaAgeFechaDigit(String pcaAgeFechaDigit) {
    this.pcaAgeFechaDigit = pcaAgeFechaDigit;
  }
  
  public String getCgFechaProceso() {
    return this.cgFechaProceso;
  }
  
  public void setCgFechaProceso(String cgFechaProceso) {
    this.cgFechaProceso = cgFechaProceso;
  }
  
  public String getCgHoraProceso() {
    return this.cgHoraProceso;
  }
  
  public void setCgHoraProceso(String cgHoraProceso) {
    this.cgHoraProceso = cgHoraProceso;
  }
  
  public String getCauDescUsuarios() {
    return this.cauDescUsuarios;
  }
  
  public void setCauDescUsuarios(String cauDescUsuarios) {
    this.cauDescUsuarios = cauDescUsuarios;
  }
  
  public Integer getEcIdCodigo() {
    return this.ecIdCodigo;
  }
  
  public void setEcIdCodigo(Integer ecIdCodigo) {
    this.ecIdCodigo = ecIdCodigo;
  }
  
  public String getEcDescripcion() {
    return this.ecDescripcion;
  }
  
  public void setEcDescripcion(String ecDescripcion) {
    this.ecDescripcion = ecDescripcion;
  }

public String getEcAsistencia() {
	return ecAsistencia;
}

public void setEcAsistencia(String ecAsistencia) {
	this.ecAsistencia = ecAsistencia;
}
}

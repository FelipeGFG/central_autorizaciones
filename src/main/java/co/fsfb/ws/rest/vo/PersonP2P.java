package co.fsfb.ws.rest.vo;

public class PersonP2P {
  private String noDocumento;
  
  private String tipoDocumento;
  
  private String email;
  
  private Long celular;
  
  private String nombres;
  
  private String apellidos;
  
  private String tipoDocPlacetoPay;
  
  public String getNoDocumento() {
    return this.noDocumento;
  }
  
  public void setNoDocumento(String noDocumento) {
    this.noDocumento = noDocumento;
  }
  
  public String getTipoDocumento() {
    return this.tipoDocumento;
  }
  
  public void setTipoDocumento(String tipoDocumento) {
    this.tipoDocumento = tipoDocumento;
  }
  
  public String getEmail() {
    return this.email;
  }
  
  public void setEmail(String email) {
    this.email = email;
  }
  
  public Long getCelular() {
    return this.celular;
  }
  
  public void setCelular(Long celular) {
    this.celular = celular;
  }
  
  public String getNombres() {
    return this.nombres;
  }
  
  public void setNombres(String nombres) {
    this.nombres = nombres;
  }
  
  public String getApellidos() {
    return this.apellidos;
  }
  
  public void setApellidos(String apellidos) {
    this.apellidos = apellidos;
  }
  
  public String getTipoDocPlacetoPay() {
    return this.tipoDocPlacetoPay;
  }
  
  public void setTipoDocPlacetoPay(String tipoDocPlacetoPay) {
    this.tipoDocPlacetoPay = tipoDocPlacetoPay;
  }
}

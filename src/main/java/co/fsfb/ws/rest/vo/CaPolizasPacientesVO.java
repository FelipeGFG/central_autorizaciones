package co.fsfb.ws.rest.vo;

import java.util.Date;

public class CaPolizasPacientesVO {
  private Long pacPacNumero;
  
  private Long accIdNumero;
  
  private String ecPolizaNumero;
  
  private String pcaAgeCodigRecep;
  
  private Date cgFechaProceso;
  
  public Long getPacPacNumero() {
    return this.pacPacNumero;
  }
  
  public void setPacPacNumero(Long pacPacNumero) {
    this.pacPacNumero = pacPacNumero;
  }
  
  public Long getAccIdNumero() {
    return this.accIdNumero;
  }
  
  public void setAccIdNumero(Long accIdNumero) {
    this.accIdNumero = accIdNumero;
  }
  
  public String getEcPolizaNumero() {
    return this.ecPolizaNumero;
  }
  
  public void setEcPolizaNumero(String ecPolizaNumero) {
    this.ecPolizaNumero = ecPolizaNumero;
  }
  
  public String getPcaAgeCodigRecep() {
    return this.pcaAgeCodigRecep;
  }
  
  public void setPcaAgeCodigRecep(String pcaAgeCodigRecep) {
    this.pcaAgeCodigRecep = pcaAgeCodigRecep;
  }
  
  public Date getCgFechaProceso() {
    return this.cgFechaProceso;
  }
  
  public void setCgFechaProceso(Date cgFechaProceso) {
    this.cgFechaProceso = cgFechaProceso;
  }
}

package co.fsfb.ws.rest.vo.placetopay;

public class Amount {
  private String currency;
  
  private String total;
  
  public String getCurrency() {
    return this.currency;
  }
  
  public void setCurrency(String currency) {
    this.currency = currency;
  }
  
  public String getTotal() {
    return this.total;
  }
  
  public void setTotal(String total) {
    this.total = total;
  }
}

package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class SubEspecialidad implements Serializable {
  private static final long serialVersionUID = 1L;
  
  String strSubCodigo;
  
  String strNombre;
  
  String strEspCodigo;
  
  public String getStrSubCodigo() {
    return this.strSubCodigo;
  }
  
  public void setStrSubCodigo(String strSubCodigo) {
    this.strSubCodigo = strSubCodigo;
  }
  
  public String getStrNombre() {
    return this.strNombre;
  }
  
  public void setStrNombre(String strNombre) {
    this.strNombre = strNombre;
  }
  
  public String getStrEspCodigo() {
    return this.strEspCodigo;
  }
  
  public void setStrEspCodigo(String strEspCodigo) {
    this.strEspCodigo = strEspCodigo;
  }
}

package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class Medicos implements Serializable {
  private static final long serialVersionUID = 1L;
  
  String strTipoIde;
  
  String strProRut;
  
  String strProApePat;
  
  String strProApeMat;
  
  String strProApeNom;
  
  String strProEst;
  
  String strProTip;
  
  String strProCta;
  
  public String getStrTipoIde() {
    return this.strTipoIde;
  }
  
  public void setStrTipoIde(String strTipoIde) {
    this.strTipoIde = strTipoIde;
  }
  
  public String getStrProRut() {
    return this.strProRut;
  }
  
  public void setStrProRut(String strProRut) {
    this.strProRut = strProRut;
  }
  
  public String getStrProApePat() {
    return this.strProApePat;
  }
  
  public void setStrProApePat(String strProApePat) {
    this.strProApePat = strProApePat;
  }
  
  public String getStrProApeMat() {
    return this.strProApeMat;
  }
  
  public void setStrProApeMat(String strProApeMat) {
    this.strProApeMat = strProApeMat;
  }
  
  public String getStrProApeNom() {
    return this.strProApeNom;
  }
  
  public void setStrProApeNom(String strProApeNom) {
    this.strProApeNom = strProApeNom;
  }
  
  public String getStrProEst() {
    return this.strProEst;
  }
  
  public void setStrProEst(String strProEst) {
    this.strProEst = strProEst;
  }
  
  public String getStrProTip() {
    return this.strProTip;
  }
  
  public void setStrProTip(String strProTip) {
    this.strProTip = strProTip;
  }
  
  public String getStrProCta() {
    return this.strProCta;
  }
  
  public void setStrProCta(String strProCta) {
    this.strProCta = strProCta;
  }
}

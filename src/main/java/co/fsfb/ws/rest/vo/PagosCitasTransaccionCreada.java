package co.fsfb.ws.rest.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class PagosCitasTransaccionCreada implements Serializable {
  private static final long serialVersionUID = -8820699724602373914L;
  
  private Integer idCita;
  
  private String nombreCita;
  
  private BigDecimal valorCita;
  
  public String getNombreCita() {
    return this.nombreCita;
  }
  
  public void setNombreCita(String nombreCita) {
    this.nombreCita = nombreCita;
  }
  
  public Integer getIdCita() {
    return this.idCita;
  }
  
  public void setIdCita(Integer idCita) {
    this.idCita = idCita;
  }
  
  public BigDecimal getValorCita() {
    return this.valorCita;
  }
  
  public void setValorCita(BigDecimal valorCita) {
    this.valorCita = valorCita;
  }
  
  public static long getSerialversionuid() {
    return -8820699724602373914L;
  }
}

package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class Sede implements Serializable {
  private static final long serialVersionUID = 1L;
  
  String strCodIntCen;
  
  String strNomCenAte;
  
  String strCodCenAte;
  
  public String getStrCodIntCen() {
    return this.strCodIntCen;
  }
  
  public void setStrCodIntCen(String strCodIntCen) {
    this.strCodIntCen = strCodIntCen;
  }
  
  public String getStrNomCenAte() {
    return this.strNomCenAte;
  }
  
  public void setStrNomCenAte(String strNomCenAte) {
    this.strNomCenAte = strNomCenAte;
  }
  
  public String getStrCodCenAte() {
    return this.strCodCenAte;
  }
  
  public void setStrCodCenAte(String strCodCenAte) {
    this.strCodCenAte = strCodCenAte;
  }
}

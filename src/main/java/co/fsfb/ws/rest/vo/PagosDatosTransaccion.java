package co.fsfb.ws.rest.vo;

import java.io.Serializable;
import java.sql.Date;

public class PagosDatosTransaccion implements Serializable {
  private static final long serialVersionUID = -8820699724602373914L;
  
  private Long id;
  
  private String numeroTransaccion;
  
  private String valorTransaccion;
  
  private Date fechaRegistro;
  
  private String numeroReferencia;
  
  private String autorizacionCus;
  
  private String pacPacNumero;
  
  private String metodoDePago;
  
  private String estadoTransaccion;
  
  private String franquicia;
  
  public String getFranquicia() {
    return this.franquicia;
  }
  
  public void setFranquicia(String franquicia) {
    this.franquicia = franquicia;
  }
  
  public String getMetodoDePago() {
    return this.metodoDePago;
  }
  
  public void setMetodoDePago(String metodoDePago) {
    this.metodoDePago = metodoDePago;
  }
  
  public String getEstadoTransaccion() {
    return this.estadoTransaccion;
  }
  
  public void setEstadoTransaccion(String estadoTransaccion) {
    this.estadoTransaccion = estadoTransaccion;
  }
  
  public Long getId() {
    return this.id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public String getNumeroTransaccion() {
    return this.numeroTransaccion;
  }
  
  public void setNumeroTransaccion(String numeroTransaccion) {
    this.numeroTransaccion = numeroTransaccion;
  }
  
  public String getValorTransaccion() {
    return this.valorTransaccion;
  }
  
  public void setValorTransaccion(String valorTransaccion) {
    this.valorTransaccion = valorTransaccion;
  }
  
  public Date getFechaRegistro() {
    return this.fechaRegistro;
  }
  
  public void setFechaRegistro(Date fechaRegistro) {
    this.fechaRegistro = fechaRegistro;
  }
  
  public String getNumeroReferencia() {
    return this.numeroReferencia;
  }
  
  public void setNumeroReferencia(String numeroReferencia) {
    this.numeroReferencia = numeroReferencia;
  }
  
  public String getAutorizacionCus() {
    return this.autorizacionCus;
  }
  
  public void setAutorizacionCus(String autorizacionCus) {
    this.autorizacionCus = autorizacionCus;
  }
  
  public String getPacPacNumero() {
    return this.pacPacNumero;
  }
  
  public void setPacPacNumero(String pacPacNumero) {
    this.pacPacNumero = pacPacNumero;
  }
  
  public static long getSerialversionuid() {
    return -8820699724602373914L;
  }
}

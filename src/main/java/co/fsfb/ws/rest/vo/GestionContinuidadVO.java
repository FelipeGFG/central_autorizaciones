package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class GestionContinuidadVO implements Serializable {
  private static final long serialVersionUID = 1L;
  
  private Long pomIdPrestOrdm;
  
  private Long gcoIdCodigoEstado;
  
  private Long gcoIdCodigoMotivo;
  
  private Boolean gcoDirecPaciente;
  
  private Boolean gcoRealizoAgendamiento;
  
  private String gcoObservaciones;
  
  private String pcaAgeCodigRecep;
  
  private Long pacPacNumero;
  
  private Boolean enviarCorreo;
  
  private Long ormIdOrdmNumero;
  
  private String nombrePaciente;
  
  private String prePreDesc;
  
  private String prePreCodigo;
  
  private String serSerCodigo;
  
  private String serSerDesc;
  
  public Long getPomIdPrestOrdm() {
    return this.pomIdPrestOrdm;
  }
  
  public void setPomIdPrestOrdm(Long pomIdPrestOrdm) {
    this.pomIdPrestOrdm = pomIdPrestOrdm;
  }
  
  public Long getGcoIdCodigoEstado() {
    return this.gcoIdCodigoEstado;
  }
  
  public void setGcoIdCodigoEstado(Long gcoIdCodigoEstado) {
    this.gcoIdCodigoEstado = gcoIdCodigoEstado;
  }
  
  public Long getGcoIdCodigoMotivo() {
    return this.gcoIdCodigoMotivo;
  }
  
  public void setGcoIdCodigoMotivo(Long gcoIdCodigoMotivo) {
    this.gcoIdCodigoMotivo = gcoIdCodigoMotivo;
  }
  
  public Boolean isGcoDirecPaciente() {
    return this.gcoDirecPaciente;
  }
  
  public void setGcoDirecPaciente(Boolean gcoDirecPaciente) {
    this.gcoDirecPaciente = gcoDirecPaciente;
  }
  
  public Boolean isGcoRealizoAgendamiento() {
    return this.gcoRealizoAgendamiento;
  }
  
  public void setGcoRealizoAgendamiento(Boolean gcoRealizoAgendamiento) {
    this.gcoRealizoAgendamiento = gcoRealizoAgendamiento;
  }
  
  public String getGcoObservaciones() {
    return this.gcoObservaciones;
  }
  
  public void setGcoObservaciones(String gcoObservaciones) {
    this.gcoObservaciones = gcoObservaciones;
  }
  
  public String getPcaAgeCodigRecep() {
    return this.pcaAgeCodigRecep;
  }
  
  public void setPcaAgeCodigRecep(String pcaAgeCodigRecep) {
    this.pcaAgeCodigRecep = pcaAgeCodigRecep;
  }
  
  public Long getPacPacNumero() {
    return this.pacPacNumero;
  }
  
  public void setPacPacNumero(Long pacPacNumero) {
    this.pacPacNumero = pacPacNumero;
  }
  
  public Boolean isEnviarCorreo() {
    return this.enviarCorreo;
  }
  
  public void setEnviarCorreo(Boolean enviarCorreo) {
    this.enviarCorreo = enviarCorreo;
  }
  
  public Long getOrmIdOrdmNumero() {
    return this.ormIdOrdmNumero;
  }
  
  public void setOrmIdOrdmNumero(Long ormIdOrdmNumero) {
    this.ormIdOrdmNumero = ormIdOrdmNumero;
  }
  
  public String getNombrePaciente() {
    return this.nombrePaciente;
  }
  
  public void setNombrePaciente(String nombrePaciente) {
    this.nombrePaciente = nombrePaciente;
  }
  
  public String getPrePreDesc() {
    return this.prePreDesc;
  }
  
  public void setPrePreDesc(String prePreDesc) {
    this.prePreDesc = prePreDesc;
  }
  
  public String getSerSerCodigo() {
    return this.serSerCodigo;
  }
  
  public void setSerSerCodigo(String serSerCodigo) {
    this.serSerCodigo = serSerCodigo;
  }
  
  public String getSerSerDesc() {
    return this.serSerDesc;
  }
  
  public void setSerSerDesc(String serSerDesc) {
    this.serSerDesc = serSerDesc;
  }
  
  public String getPrePreCodigo() {
    return this.prePreCodigo;
  }
  
  public void setPrePreCodigo(String prePreCodigo) {
    this.prePreCodigo = prePreCodigo;
  }
}

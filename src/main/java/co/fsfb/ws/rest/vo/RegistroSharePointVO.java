package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class RegistroSharePointVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private boolean respuesta;

    private String rutaUrlArchivo;

    private String nombreArchivo;

    private boolean respuestaVer;

    public boolean isRespuesta() {
        return this.respuesta;
    }

    public void setRespuesta(boolean respuesta) {
        this.respuesta = respuesta;
    }

    public String getRutaUrlArchivo() {
        return this.rutaUrlArchivo;
    }

    public void setRutaUrlArchivo(String rutaUrlArchivo) {
        this.rutaUrlArchivo = rutaUrlArchivo;
    }

    public String getNombreArchivo() {
        return this.nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public boolean isRespuestaVer() {
        return this.respuestaVer;
    }

    public void setRespuestaVer(boolean respuestaVer) {
        this.respuestaVer = respuestaVer;
    }
}

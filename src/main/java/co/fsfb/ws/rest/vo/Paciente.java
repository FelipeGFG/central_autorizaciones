package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class Paciente implements Serializable {
  private static final long serialVersionUID = -8820699724602373914L;
  
  private String conTipoConvenio;
  
  private String nombres;
  
  private String numDocId;
  
  private String primerApellido;
  
  private String segundoApellido;
  
  private String tipoDocId;
  
  private long pacNum;
  
  private String pacPacTipoIdentCodigo;
  
  private String tipTipIDav;
  
  private String nombreCompleto;
  
  private String codOrdenAlterna;
  
  private String tipoExamen;
  
  private String idTipoExamen;
  
  public String getConTipoConvenio() {
    return this.conTipoConvenio;
  }
  
  public void setConTipoConvenio(String conTipoConvenio) {
    this.conTipoConvenio = conTipoConvenio;
  }
  
  public String getNombres() {
    return this.nombres;
  }
  
  public void setNombres(String nombres) {
    this.nombres = nombres;
  }
  
  public String getNumDocId() {
    return this.numDocId;
  }
  
  public void setNumDocId(String numDocId) {
    this.numDocId = numDocId;
  }
  
  public String getPrimerApellido() {
    return this.primerApellido;
  }
  
  public void setPrimerApellido(String primerApellido) {
    this.primerApellido = primerApellido;
  }
  
  public String getSegundoApellido() {
    return this.segundoApellido;
  }
  
  public void setSegundoApellido(String segundoApellido) {
    this.segundoApellido = segundoApellido;
  }
  
  public String getTipoDocId() {
    return this.tipoDocId;
  }
  
  public void setTipoDocId(String tipoDocId) {
    this.tipoDocId = tipoDocId;
  }
  
  public String getPacPacTipoIdentCodigo() {
    return this.pacPacTipoIdentCodigo;
  }
  
  public void setPacPacTipoIdentCodigo(String pacPacTipoIdentCodigo) {
    this.pacPacTipoIdentCodigo = pacPacTipoIdentCodigo;
  }
  
  public long getPacNum() {
    return this.pacNum;
  }
  
  public void setPacNum(long pacNum) {
    this.pacNum = pacNum;
  }
  
  public String getTipTipIDav() {
    return this.tipTipIDav;
  }
  
  public void setTipTipIDav(String tipTipIDav) {
    this.tipTipIDav = tipTipIDav;
  }
  
  public String getNombreCompleto() {
    return this.nombreCompleto;
  }
  
  public void setNombreCompleto(String nombreCompleto) {
    this.nombreCompleto = nombreCompleto;
  }
  
  public String getCodOrdenAlterna() {
    return this.codOrdenAlterna;
  }
  
  public void setCodOrdenAlterna(String codOrdenAlterna) {
    this.codOrdenAlterna = codOrdenAlterna;
  }
  
  public String getTipoExamen() {
    return this.tipoExamen;
  }
  
  public void setTipoExamen(String tipoExamen) {
    this.tipoExamen = tipoExamen;
  }
  
  public String getIdTipoExamen() {
    return this.idTipoExamen;
  }
  
  public void setIdTipoExamen(String idTipoExamen) {
    this.idTipoExamen = idTipoExamen;
  }
}

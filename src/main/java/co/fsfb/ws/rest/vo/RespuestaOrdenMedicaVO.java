package co.fsfb.ws.rest.vo;

import java.io.Serializable;
import java.util.Date;

public class RespuestaOrdenMedicaVO implements Serializable {
  private static final long serialVersionUID = 1L;
  
  private Long ormIdOrdmNumero;
  
  private String nombreCompleto;
  
  private String tipoDocumento;
  
  private String documento;
  
  private Long pacPacNumero;
  
  private String tipTipIDav;
  
  private String ecPolizaNumero;
  
  private Long eomIdCodigo;
  
  private Date cgFechaProceso;
  
  private String filename;
  
  private String mensajeError;
  
  private String fechaRegistroFile;
  
  public Long getOrmIdOrdmNumero() {
    return this.ormIdOrdmNumero;
  }
  
  public void setOrmIdOrdmNumero(Long ormIdOrdmNumero) {
    this.ormIdOrdmNumero = ormIdOrdmNumero;
  }
  
  public String getNombreCompleto() {
    return this.nombreCompleto;
  }
  
  public void setNombreCompleto(String nombreCompleto) {
    this.nombreCompleto = nombreCompleto;
  }
  
  public String getTipoDocumento() {
    return this.tipoDocumento;
  }
  
  public void setTipoDocumento(String tipoDocumento) {
    this.tipoDocumento = tipoDocumento;
  }
  
  public String getDocumento() {
    return this.documento;
  }
  
  public void setDocumento(String documento) {
    this.documento = documento;
  }
  
  public Long getPacPacNumero() {
    return this.pacPacNumero;
  }
  
  public void setPacPacNumero(Long pacPacNumero) {
    this.pacPacNumero = pacPacNumero;
  }
  
  public String getTipTipIDav() {
    return this.tipTipIDav;
  }
  
  public void setTipTipIDav(String tipTipIDav) {
    this.tipTipIDav = tipTipIDav;
  }
  
  public String getEcPolizaNumero() {
    return this.ecPolizaNumero;
  }
  
  public void setEcPolizaNumero(String ecPolizaNumero) {
    this.ecPolizaNumero = ecPolizaNumero;
  }
  
  public Long getEomIdCodigo() {
    return this.eomIdCodigo;
  }
  
  public void setEomIdCodigo(Long eomIdCodigo) {
    this.eomIdCodigo = eomIdCodigo;
  }
  
  public Date getCgFechaProceso() {
    return this.cgFechaProceso;
  }
  
  public void setCgFechaProceso(Date cgFechaProceso) {
    this.cgFechaProceso = cgFechaProceso;
  }
  
  public String getFilename() {
    return this.filename;
  }
  
  public void setFilename(String filename) {
    this.filename = filename;
  }
  
  public String getMensajeError() {
    return this.mensajeError;
  }
  
  public void setMensajeError(String mensajeError) {
    this.mensajeError = mensajeError;
  }
  
  public String getFechaRegistroFile() {
    return this.fechaRegistroFile;
  }
  
  public void setFechaRegistroFile(String fechaRegistroFile) {
    this.fechaRegistroFile = fechaRegistroFile;
  }
}

package co.fsfb.ws.rest.vo;

public class CambioEstadoCitaVO extends ResultadoCitaPacienteVO {
  private static final long serialVersionUID = 1L;
  
  private Long ecIdCodigo;
  
  private String pcaAgeCodigRecep;
  
  private String cauDescUsuarios;
  
  private String otcObservacion;
  
  private boolean correoElectronicoPaciente;
  
  public Long getEcIdCodigo() {
    return this.ecIdCodigo;
  }
  
  public void setEcIdCodigo(Long ecIdCodigo) {
    this.ecIdCodigo = ecIdCodigo;
  }
  
  public String getPcaAgeCodigRecep() {
    return this.pcaAgeCodigRecep;
  }
  
  public void setPcaAgeCodigRecep(String pcaAgeCodigRecep) {
    this.pcaAgeCodigRecep = pcaAgeCodigRecep;
  }
  
  public String getCauDescUsuarios() {
    return this.cauDescUsuarios;
  }
  
  public void setCauDescUsuarios(String cauDescUsuarios) {
    this.cauDescUsuarios = cauDescUsuarios;
  }
  
  public String getOtcObservacion() {
    return this.otcObservacion;
  }
  
  public void setOtcObservacion(String otcObservacion) {
    this.otcObservacion = otcObservacion;
  }
  
  public boolean isCorreoElectronicoPaciente() {
    return this.correoElectronicoPaciente;
  }
  
  public void setCorreoElectronicoPaciente(boolean correoElectronicoPaciente) {
    this.correoElectronicoPaciente = correoElectronicoPaciente;
  }
  
  public static long getSerialversionuid() {
    return 1L;
  }
}

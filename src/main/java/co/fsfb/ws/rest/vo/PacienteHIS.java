package co.fsfb.ws.rest.vo;

public class PacienteHIS {
  private String tipoDoc;
  
  private String tipoDocHom;
  
  private String numDoc;
  
  private String nombres;
  
  private String apellidos;
  
  private String correo;
  
  private String celular;
  
  public String getTipoDoc() {
    return this.tipoDoc;
  }
  
  public void setTipoDoc(String tipoDoc) {
    this.tipoDoc = tipoDoc;
  }
  
  public String getTipoDocHom() {
    return this.tipoDocHom;
  }
  
  public void setTipoDocHom(String tipoDocHom) {
    this.tipoDocHom = tipoDocHom;
  }
  
  public String getNumDoc() {
    return this.numDoc;
  }
  
  public void setNumDoc(String numDoc) {
    this.numDoc = numDoc;
  }
  
  public String getNombres() {
    return this.nombres;
  }
  
  public void setNombres(String nombres) {
    this.nombres = nombres;
  }
  
  public String getApellidos() {
    return this.apellidos;
  }
  
  public void setApellidos(String apellidos) {
    this.apellidos = apellidos;
  }
  
  public String getCorreo() {
    return this.correo;
  }
  
  public void setCorreo(String correo) {
    this.correo = correo;
  }
  
  public String getCelular() {
    return this.celular;
  }
  
  public void setCelular(String celular) {
    this.celular = celular;
  }
}

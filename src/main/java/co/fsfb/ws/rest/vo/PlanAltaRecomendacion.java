package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class PlanAltaRecomendacion implements Serializable {
  private static final long serialVersionUID = -3742645223546398331L;
  
  private String tipo;
  
  private String nombre;
  
  private String indicador;
  
  private String desc;
  
  public String getTipo() {
    return this.tipo;
  }
  
  public void setTipo(String tipo) {
    this.tipo = tipo;
  }
  
  public String getNombre() {
    return this.nombre;
  }
  
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }
  
  public String getIndicador() {
    return this.indicador;
  }
  
  public void setIndicador(String indicador) {
    this.indicador = indicador;
  }
  
  public String getDesc() {
    return this.desc;
  }
  
  public void setDesc(String desc) {
    this.desc = desc;
  }
  
  public String obtenerCadena() {
    String planDeAlta = "";
    planDeAlta = planDeAlta.concat("<tr><td>" + this.tipo + "</td><td>" + this.nombre + "</td><td>");
    planDeAlta = planDeAlta.concat(this.indicador + "</td><td>");
    planDeAlta = planDeAlta.concat(this.desc + "</td></tr>");
    return planDeAlta;
  }
}

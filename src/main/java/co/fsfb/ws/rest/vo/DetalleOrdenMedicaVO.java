package co.fsfb.ws.rest.vo;

import java.io.Serializable;
import java.time.LocalDate;

public class DetalleOrdenMedicaVO implements Serializable {
  private static final long serialVersionUID = 1L;
  
  private String serEspCodigo;
  
  private String serSerCodSubEspe;
  
  private String serSerCodigo;
  
  private String pcaAgeLugar;
  
  private String diaAgrCodigo;
  
  private String diaOtroAgr;
  
  private String conConCodigo;
  
  private String pcaAgeCodigProfe;
  
  private String pcaAgeCodigRecep;
  
  private String domShaFechaProceso;
  
  private String ecPolizaNumero;
  
  private LocalDate dorFechaOrdenm;
  
  private String dorFechaOrdenmString;
  
  public String getSerEspCodigo() {
    return this.serEspCodigo;
  }
  
  public void setSerEspCodigo(String serEspCodigo) {
    this.serEspCodigo = serEspCodigo;
  }
  
  public String getSerSerCodSubEspe() {
    return this.serSerCodSubEspe;
  }
  
  public void setSerSerCodSubEspe(String serSerCodSubEspe) {
    this.serSerCodSubEspe = serSerCodSubEspe;
  }
  
  public String getSerSerCodigo() {
    return this.serSerCodigo;
  }
  
  public void setSerSerCodigo(String serSerCodigo) {
    this.serSerCodigo = serSerCodigo;
  }
  
  public String getPcaAgeLugar() {
    return this.pcaAgeLugar;
  }
  
  public void setPcaAgeLugar(String pcaAgeLugar) {
    this.pcaAgeLugar = pcaAgeLugar;
  }
  
  public String getDiaAgrCodigo() {
    return this.diaAgrCodigo;
  }
  
  public void setDiaAgrCodigo(String diaAgrCodigo) {
    this.diaAgrCodigo = diaAgrCodigo;
  }
  
  public String getDiaOtroAgr() {
    return this.diaOtroAgr;
  }
  
  public void setDiaOtroAgr(String diaOtroAgr) {
    this.diaOtroAgr = diaOtroAgr;
  }
  
  public String getConConCodigo() {
    return this.conConCodigo;
  }
  
  public void setConConCodigo(String conConCodigo) {
    this.conConCodigo = conConCodigo;
  }
  
  public String getPcaAgeCodigProfe() {
    return this.pcaAgeCodigProfe;
  }
  
  public void setPcaAgeCodigProfe(String pcaAgeCodigProfe) {
    this.pcaAgeCodigProfe = pcaAgeCodigProfe;
  }
  
  public String getPcaAgeCodigRecep() {
    return this.pcaAgeCodigRecep;
  }
  
  public void setPcaAgeCodigRecep(String pcaAgeCodigRecep) {
    this.pcaAgeCodigRecep = pcaAgeCodigRecep;
  }
  
  public String getDomShaFechaProceso() {
    return this.domShaFechaProceso;
  }
  
  public void setDomShaFechaProceso(String domShaFechaProceso) {
    this.domShaFechaProceso = domShaFechaProceso;
  }
  
  public String getEcPolizaNumero() {
    return this.ecPolizaNumero;
  }
  
  public void setEcPolizaNumero(String ecPolizaNumero) {
    this.ecPolizaNumero = ecPolizaNumero;
  }
  
  public LocalDate getDorFechaOrdenm() {
    return this.dorFechaOrdenm;
  }
  
  public void setDorFechaOrdenm(LocalDate dorFechaOrdenm) {
    this.dorFechaOrdenm = dorFechaOrdenm;
  }
  
  public String getDorFechaOrdenmString() {
    return this.dorFechaOrdenmString;
  }
  
  public void setDorFechaOrdenmString(String dorFechaOrdenmString) {
    this.dorFechaOrdenmString = dorFechaOrdenmString;
  }
}

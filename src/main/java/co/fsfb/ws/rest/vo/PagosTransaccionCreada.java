package co.fsfb.ws.rest.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class PagosTransaccionCreada implements Serializable {
  private static final long serialVersionUID = -8820699724602373914L;
  
  private String noPacPaciente;
  
  private PersonP2P pagadorP2P;
  
  private PersonP2P compradorP2P;
  
  private String userAgent;
  
  private String ipCliente;
  
  private String urlRedireccion;
  
  private List<PagosCitasTransaccionCreada> listaCitas;
  
  public String getNoPacPaciente() {
    return this.noPacPaciente;
  }
  
  public void setNoPacPaciente(String noPacPaciente) {
    this.noPacPaciente = noPacPaciente;
  }
  
  public String getUrlRedireccion() {
    return this.urlRedireccion;
  }
  
  public void setUrlRedireccion(String urlRedireccion) {
    this.urlRedireccion = urlRedireccion;
  }
  
  public String getIpCliente() {
    return this.ipCliente;
  }
  
  public void setIpCliente(String ipCliente) {
    this.ipCliente = ipCliente;
  }
  
  public String getUserAgent() {
    return this.userAgent;
  }
  
  public void setUserAgent(String userAgent) {
    this.userAgent = userAgent;
  }
  
  public List<PagosCitasTransaccionCreada> getListaCitas() {
    return this.listaCitas;
  }
  
  public void setListaCitas(List<PagosCitasTransaccionCreada> listaCitas) {
    this.listaCitas = listaCitas;
  }
  
  public static long getSerialversionuid() {
    return -8820699724602373914L;
  }
  
  public PersonP2P getPagadorP2P() {
    return this.pagadorP2P;
  }
  
  public void setPagadorP2P(PersonP2P pagadorP2P) {
    this.pagadorP2P = pagadorP2P;
  }
  
  public PersonP2P getCompradorP2P() {
    return this.compradorP2P;
  }
  
  public void setCompradorP2P(PersonP2P personP2P) {
    this.compradorP2P = personP2P;
  }
  
  public void addCita(PagosCitasTransaccionCreada pagosCitasTransaccionCreada) {
    if (null == this.listaCitas)
      this.listaCitas = new ArrayList<>(); 
    this.listaCitas.add(pagosCitasTransaccionCreada);
  }
  
  public BigDecimal valorTotal() {
    BigDecimal response = new BigDecimal(0);
    if (null != this.listaCitas)
      for (PagosCitasTransaccionCreada pagosCitasTransaccionCreada : this.listaCitas)
        response = response.add(pagosCitasTransaccionCreada.getValorCita());  
    return response;
  }
}

package co.fsfb.ws.rest.vo;

public class DiagnosticosVO {
  private String diaAgrCodigo;
  
  private String diaDiaDescripcion;
  
  public String getDiaAgrCodigo() {
    return this.diaAgrCodigo;
  }
  
  public void setDiaAgrCodigo(String diaAgrCodigo) {
    this.diaAgrCodigo = diaAgrCodigo;
  }
  
  public String getDiaDiaDescripcion() {
    return this.diaDiaDescripcion;
  }
  
  public void setDiaDiaDescripcion(String diaDiaDescripcion) {
    this.diaDiaDescripcion = diaDiaDescripcion;
  }
}

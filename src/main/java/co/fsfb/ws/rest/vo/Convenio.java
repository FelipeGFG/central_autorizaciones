package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class Convenio implements Serializable {
  private static final long serialVersionUID = 1L;
  
  String strCodigo;
  
  String strDesc;
  
  String strVigencia;
  
  String strTipCon;
  
  String strTipConCod;
  
  String strModTarCod;
  
  public String getStrCodigo() {
    return this.strCodigo;
  }
  
  public void setStrCodigo(String strCodigo) {
    this.strCodigo = strCodigo;
  }
  
  public String getStrDesc() {
    return this.strDesc;
  }
  
  public void setStrDesc(String strDesc) {
    this.strDesc = strDesc;
  }
  
  public String getStrVigencia() {
    return this.strVigencia;
  }
  
  public void setStrVigencia(String strVigencia) {
    this.strVigencia = strVigencia;
  }
  
  public String getStrTipCon() {
    return this.strTipCon;
  }
  
  public void setStrTipCon(String strTipCon) {
    this.strTipCon = strTipCon;
  }
  
  public String getStrTipConCod() {
    return this.strTipConCod;
  }
  
  public void setStrTipConCod(String strTipConCod) {
    this.strTipConCod = strTipConCod;
  }
  
  public String getStrModTarCod() {
    return this.strModTarCod;
  }
  
  public void setStrModTarCod(String strModTarCod) {
    this.strModTarCod = strModTarCod;
  }
}

package co.fsfb.ws.rest.vo;

import java.io.Serializable;
import java.util.List;

public class ResultadoCitaPacienteVO implements Serializable {
  private static final long serialVersionUID = 1L;
  
  private long pacNum;
  
  private String nombreCompleto;
  
  private String pacPacTipoIdentCodigo;
  
  private String tipoDocId;
  
  private String tipTipIDav;
  
  private String numDocId;
  
  private String telefono;
  
  private String email;
  
  private Long idCita;
  
  private String horaCita;
  
  private String fechaCita;
  
  private String codEspecialidad;
  
  private String especialidad;
  
  private String subEspecialidad;
  
  private String sede;
  
  private String codProf;
  
  private String nombreProf;
  
  private String consultorio;
  
  private String codigoPrestacion;
  
  private String prestacion;
  
  private String codCentroAten;
  
  private String nombreCentroAten;
  
  private List<String> codConvenio;
  
  private String convenio;
  
  private String codUsrCita;
  
  private String usrCita;
  
  private String fechaAsigna;
  
  private String indRecepcionado;
  
  private String codEstadoCita;
  
  private String estadoCita;
  
  private Boolean asistencia;
  
  private String codServicio;
  
  private String direccionCentroOperativo;
  
  private String telefonoCentroOperativo;
  
  private String ecPolizaNumero;
  
  public long getPacNum() {
    return this.pacNum;
  }
  
  public void setPacNum(long pacNum) {
    this.pacNum = pacNum;
  }
  
  public String getNombreCompleto() {
    return this.nombreCompleto;
  }
  
  public void setNombreCompleto(String nombreCompleto) {
    this.nombreCompleto = nombreCompleto;
  }
  
  public String getPacPacTipoIdentCodigo() {
    return this.pacPacTipoIdentCodigo;
  }
  
  public void setPacPacTipoIdentCodigo(String pacPacTipoIdentCodigo) {
    this.pacPacTipoIdentCodigo = pacPacTipoIdentCodigo;
  }
  
  public String getTipoDocId() {
    return this.tipoDocId;
  }
  
  public void setTipoDocId(String tipoDocId) {
    this.tipoDocId = tipoDocId;
  }
  
  public String getTipTipIDav() {
    return this.tipTipIDav;
  }
  
  public void setTipTipIDav(String tipTipIDav) {
    this.tipTipIDav = tipTipIDav;
  }
  
  public String getNumDocId() {
    return this.numDocId;
  }
  
  public void setNumDocId(String numDocId) {
    this.numDocId = numDocId;
  }
  
  public String getTelefono() {
    return this.telefono;
  }
  
  public void setTelefono(String telefono) {
    this.telefono = telefono;
  }
  
  public String getEmail() {
    return this.email;
  }
  
  public void setEmail(String email) {
    this.email = email;
  }
  
  public Long getIdCita() {
    return this.idCita;
  }
  
  public void setIdCita(Long idCita) {
    this.idCita = idCita;
  }
  
  public String getHoraCita() {
    return this.horaCita;
  }
  
  public void setHoraCita(String horaCita) {
    this.horaCita = horaCita;
  }
  
  public String getFechaCita() {
    return this.fechaCita;
  }
  
  public void setFechaCita(String fechaCita) {
    this.fechaCita = fechaCita;
  }
  
  public String getCodEspecialidad() {
    return this.codEspecialidad;
  }
  
  public void setCodEspecialidad(String codEspecialidad) {
    this.codEspecialidad = codEspecialidad;
  }
  
  public String getEspecialidad() {
    return this.especialidad;
  }
  
  public void setEspecialidad(String especialidad) {
    this.especialidad = especialidad;
  }
  
  public String getSubEspecialidad() {
    return this.subEspecialidad;
  }
  
  public void setSubEspecialidad(String subEspecialidad) {
    this.subEspecialidad = subEspecialidad;
  }
  
  public String getSede() {
    return this.sede;
  }
  
  public void setSede(String sede) {
    this.sede = sede;
  }
  
  public String getCodProf() {
    return this.codProf;
  }
  
  public void setCodProf(String codProf) {
    this.codProf = codProf;
  }
  
  public String getNombreProf() {
    return this.nombreProf;
  }
  
  public void setNombreProf(String nombreProf) {
    this.nombreProf = nombreProf;
  }
  
  public String getConsultorio() {
    return this.consultorio;
  }
  
  public void setConsultorio(String consultorio) {
    this.consultorio = consultorio;
  }
  
  public String getCodigoPrestacion() {
    return this.codigoPrestacion;
  }
  
  public void setCodigoPrestacion(String codigoPrestacion) {
    this.codigoPrestacion = codigoPrestacion;
  }
  
  public String getPrestacion() {
    return this.prestacion;
  }
  
  public void setPrestacion(String prestacion) {
    this.prestacion = prestacion;
  }
  
  public String getCodCentroAten() {
    return this.codCentroAten;
  }
  
  public void setCodCentroAten(String codCentroAten) {
    this.codCentroAten = codCentroAten;
  }
  
  public String getNombreCentroAten() {
    return this.nombreCentroAten;
  }
  
  public void setNombreCentroAten(String nombreCentroAten) {
    this.nombreCentroAten = nombreCentroAten;
  }
  
  public List<String> getCodConvenio() {
    return this.codConvenio;
  }
  
  public void setCodConvenio(List<String> codConvenio) {
    this.codConvenio = codConvenio;
  }
  
  public String getConvenio() {
    return this.convenio;
  }
  
  public void setConvenio(String convenio) {
    this.convenio = convenio;
  }
  
  public String getCodUsrCita() {
    return this.codUsrCita;
  }
  
  public void setCodUsrCita(String codUsrCita) {
    this.codUsrCita = codUsrCita;
  }
  
  public String getUsrCita() {
    return this.usrCita;
  }
  
  public void setUsrCita(String usrCita) {
    this.usrCita = usrCita;
  }
  
  public String getFechaAsigna() {
    return this.fechaAsigna;
  }
  
  public void setFechaAsigna(String fechaAsigna) {
    this.fechaAsigna = fechaAsigna;
  }
  
  public String getIndRecepcionado() {
    return this.indRecepcionado;
  }
  
  public void setIndRecepcionado(String indRecepcionado) {
    this.indRecepcionado = indRecepcionado;
  }
  
  public String getCodEstadoCita() {
    return this.codEstadoCita;
  }
  
  public void setCodEstadoCita(String codEstadoCita) {
    this.codEstadoCita = codEstadoCita;
  }
  
  public String getEstadoCita() {
    return this.estadoCita;
  }
  
  public void setEstadoCita(String estadoCita) {
    this.estadoCita = estadoCita;
  }
  
  public String getDireccionCentroOperativo() {
    return this.direccionCentroOperativo;
  }
  
  public void setDireccionCentroOperativo(String direccionCentroOperativo) {
    this.direccionCentroOperativo = direccionCentroOperativo;
  }
  
  public String getTelefonoCentroOperativo() {
    return this.telefonoCentroOperativo;
  }
  
  public void setTelefonoCentroOperativo(String telefonoCentroOperativo) {
    this.telefonoCentroOperativo = telefonoCentroOperativo;
  }
  
  public String getEcPolizaNumero() {
    return this.ecPolizaNumero;
  }
  
  public void setEcPolizaNumero(String ecPolizaNumero) {
    this.ecPolizaNumero = ecPolizaNumero;
  }
  
  public Boolean getAsistencia() {
    return this.asistencia;
  }
  
  public void setAsistencia(Boolean asistencia) {
    this.asistencia = asistencia;
  }
  
  public String getCodServicio() {
    return this.codServicio;
  }
  
  public void setCodServicio(String codServicio) {
    this.codServicio = codServicio;
  }
}

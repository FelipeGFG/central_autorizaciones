package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class PlanAltaOrden implements Serializable {
  private static final long serialVersionUID = 3971590976564748760L;
  
  private String tipo;
  
  private String numero;
  
  private String ordCat;
  
  private String codPres;
  
  private String descPres;
  
  private String cantOrden;
  
  private String ordenPrev;
  
  private String ladoCuerpo;
  
  private String codVia;
  
  private String volumen;
  
  public String getTipo() {
    return this.tipo;
  }
  
  public void setTipo(String tipo) {
    this.tipo = tipo;
  }
  
  public String getNumero() {
    return this.numero;
  }
  
  public void setNumero(String numero) {
    this.numero = numero;
  }
  
  public String getOrdCat() {
    return this.ordCat;
  }
  
  public void setOrdCat(String ordCat) {
    this.ordCat = ordCat;
  }
  
  public String getCodPres() {
    return this.codPres;
  }
  
  public void setCodPres(String codPres) {
    this.codPres = codPres;
  }
  
  public String getDescPres() {
    return this.descPres;
  }
  
  public void setDescPres(String descPres) {
    this.descPres = descPres;
  }
  
  public String getCantOrden() {
    return this.cantOrden;
  }
  
  public void setCantOrden(String cantOrden) {
    this.cantOrden = cantOrden;
  }
  
  public String getOrdenPrev() {
    return this.ordenPrev;
  }
  
  public void setOrdenPrev(String ordenPrev) {
    this.ordenPrev = ordenPrev;
  }
  
  public String getLadoCuerpo() {
    return this.ladoCuerpo;
  }
  
  public void setLadoCuerpo(String ladoCuerpo) {
    this.ladoCuerpo = ladoCuerpo;
  }
  
  public String getCodVia() {
    return this.codVia;
  }
  
  public void setCodVia(String codVia) {
    this.codVia = codVia;
  }
  
  public String getVolumen() {
    return this.volumen;
  }
  
  public void setVolumen(String volumen) {
    this.volumen = volumen;
  }
  
  public String obtenerCadena() {
    String planDeAlta = "";
    planDeAlta = planDeAlta.concat("<tr><td>" + this.tipo + "</td><td>" + this.numero + "</td><td>" + this.ordCat + "</td><td>" + this.codPres + "</td><td>" + this.descPres + "</td><td>" + this.cantOrden + "</td><td>" + this.ordenPrev + "</td><td>" + this.ladoCuerpo + "</td><td>" + this.codVia + "</td><td>" + this.volumen + "</td></tr>");
    return planDeAlta;
  }
}

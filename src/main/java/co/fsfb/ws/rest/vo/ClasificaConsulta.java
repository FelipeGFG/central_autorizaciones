package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class ClasificaConsulta implements Serializable {
  private static final long serialVersionUID = 1L;
  
  String codigoEspec;
  
  String codSubEspec;
  
  String codServicio;
  
  String servicio;
  
  public String getCodigoEspec() {
    return this.codigoEspec;
  }
  
  public void setCodigoEspec(String codigoEspec) {
    this.codigoEspec = codigoEspec;
  }
  
  public String getCodSubEspec() {
    return this.codSubEspec;
  }
  
  public void setCodSubEspec(String codSubEspec) {
    this.codSubEspec = codSubEspec;
  }
  
  public String getCodServicio() {
    return this.codServicio;
  }
  
  public void setCodServicio(String codServicio) {
    this.codServicio = codServicio;
  }
  
  public String getServicio() {
    return this.servicio;
  }
  
  public void setServicio(String servicio) {
    this.servicio = servicio;
  }
}

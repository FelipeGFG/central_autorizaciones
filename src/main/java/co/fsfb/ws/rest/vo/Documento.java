package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class Documento implements Serializable {
  private static final long serialVersionUID = 1L;
  
  private Paciente pac;
  
  private String archivo;
  
  private String nombreArchivo;
  
  private String fecha;
  
  private Long ormIdOrdmNumero;
  
  private boolean archivoGral;
  
  public boolean isArchivoGral() {
    return this.archivoGral;
  }
  
  public void setArchivoGral(boolean archivoGral) {
    this.archivoGral = archivoGral;
  }
  
  public Paciente getPac() {
    return this.pac;
  }
  
  public void setPac(Paciente pac) {
    this.pac = pac;
  }
  
  public String getArchivo() {
    return this.archivo;
  }
  
  public void setArchivo(String archivo) {
    this.archivo = archivo;
  }
  
  public String getNombreArchivo() {
    return this.nombreArchivo;
  }
  
  public void setNombreArchivo(String nombreArchivo) {
    this.nombreArchivo = nombreArchivo;
  }
  
  public String getFecha() {
    return this.fecha;
  }
  
  public void setFecha(String fecha) {
    this.fecha = fecha;
  }
  
  public Long getOrmIdOrdmNumero() {
    return this.ormIdOrdmNumero;
  }
  
  public void setOrmIdOrdmNumero(Long ormIdOrdmNumero) {
    this.ormIdOrdmNumero = ormIdOrdmNumero;
  }
}

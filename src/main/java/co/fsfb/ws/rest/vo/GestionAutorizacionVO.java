package co.fsfb.ws.rest.vo;

import java.util.Date;

public class GestionAutorizacionVO {
  private Long pomIdPrestOrdm;
  
  private Long pacPacNumero;
  
  private String gauNombreAutorizador;
  
  private String gauTelefonoAutorizador;
  
  private String gauAutorizaServ;
  
  private Long mnaIdcodigo;
  
  private String omnDesc;
  
  private String gauCodigoAutorizacion;
  
  private String gauFechaAutorizacion;
  
  private String gauFechaVencAutorizacion;
  
  private Long gauVigenciaAutorizacion;
  
  private Long gauValorPrestacion;
  
  private Long gauCostoConvenio;
  
  private Long gauCostoPac;
  
  private String pcaAgeCodigoRecep;
  
  private Date cgFechaProceso;
  
  private boolean enviarCorreo;
  
  private String gauObservaciones;
  
  private String numeroPoliza;
  
  private String ogaDescripcion;
  
  private String nombrePaciente;
  
  private String centroAtencion;
  
  public Long getPomIdPrestOrdm() {
    return this.pomIdPrestOrdm;
  }
  
  public void setPomIdPrestOrdm(Long pomIdPrestOrdm) {
    this.pomIdPrestOrdm = pomIdPrestOrdm;
  }
  
  public Long getPacPacNumero() {
    return this.pacPacNumero;
  }
  
  public void setPacPacNumero(Long pacPacNumero) {
    this.pacPacNumero = pacPacNumero;
  }
  
  public String getGauNombreAutorizador() {
    return this.gauNombreAutorizador;
  }
  
  public void setGauNombreAutorizador(String gauNombreAutorizador) {
    this.gauNombreAutorizador = gauNombreAutorizador;
  }
  
  public String getGauTelefonoAutorizador() {
    return this.gauTelefonoAutorizador;
  }
  
  public void setGauTelefonoAutorizador(String gauTelefonoAutorizador) {
    this.gauTelefonoAutorizador = gauTelefonoAutorizador;
  }
  
  public String getGauAutorizaServ() {
    return this.gauAutorizaServ;
  }
  
  public void setGauAutorizaServ(String gauAutorizaServ) {
    this.gauAutorizaServ = gauAutorizaServ;
  }
  
  public Long getMnaIdcodigo() {
    return this.mnaIdcodigo;
  }
  
  public void setMnaIdcodigo(Long mnaIdcodigo) {
    this.mnaIdcodigo = mnaIdcodigo;
  }
  
  public String getOmnDesc() {
    return this.omnDesc;
  }
  
  public void setOmnDesc(String omnDesc) {
    this.omnDesc = omnDesc;
  }
  
  public String getGauCodigoAutorizacion() {
    return this.gauCodigoAutorizacion;
  }
  
  public void setGauCodigoAutorizacion(String gauCodigoAutorizacion) {
    this.gauCodigoAutorizacion = gauCodigoAutorizacion;
  }
  
  public String getGauFechaAutorizacion() {
    return this.gauFechaAutorizacion;
  }
  
  public void setGauFechaAutorizacion(String gauFechaAutorizacion) {
    this.gauFechaAutorizacion = gauFechaAutorizacion;
  }
  
  public String getGauFechaVencAutorizacion() {
    return this.gauFechaVencAutorizacion;
  }
  
  public void setGauFechaVencAutorizacion(String gauFechaVencAutorizacion) {
    this.gauFechaVencAutorizacion = gauFechaVencAutorizacion;
  }
  
  public Long getGauVigenciaAutorizacion() {
    return this.gauVigenciaAutorizacion;
  }
  
  public void setGauVigenciaAutorizacion(Long gauVigenciaAutorizacion) {
    this.gauVigenciaAutorizacion = gauVigenciaAutorizacion;
  }
  
  public Long getGauValorPrestacion() {
    return this.gauValorPrestacion;
  }
  
  public void setGauValorPrestacion(Long gauValorPrestacion) {
    this.gauValorPrestacion = gauValorPrestacion;
  }
  
  public Long getGauCostoConvenio() {
    return this.gauCostoConvenio;
  }
  
  public void setGauCostoConvenio(Long gauCostoConvenio) {
    this.gauCostoConvenio = gauCostoConvenio;
  }
  
  public Long getGauCostoPac() {
    return this.gauCostoPac;
  }
  
  public void setGauCostoPac(Long gauCostoPac) {
    this.gauCostoPac = gauCostoPac;
  }
  
  public String getPcaAgeCodigoRecep() {
    return this.pcaAgeCodigoRecep;
  }
  
  public void setPcaAgeCodigoRecep(String pcaAgeCodigoRecep) {
    this.pcaAgeCodigoRecep = pcaAgeCodigoRecep;
  }
  
  public Date getCgFechaProceso() {
    return this.cgFechaProceso;
  }
  
  public void setCgFechaProceso(Date cgFechaProceso) {
    this.cgFechaProceso = cgFechaProceso;
  }
  
  public boolean isEnviarCorreo() {
    return this.enviarCorreo;
  }
  
  public void setEnviarCorreo(boolean enviarCorreo) {
    this.enviarCorreo = enviarCorreo;
  }
  
  public String getGauObservaciones() {
    return this.gauObservaciones;
  }
  
  public void setGauObservaciones(String gauObservaciones) {
    this.gauObservaciones = gauObservaciones;
  }
  
  public String getNumeroPoliza() {
    return this.numeroPoliza;
  }
  
  public void setNumeroPoliza(String numeroPoliza) {
    this.numeroPoliza = numeroPoliza;
  }
  
  public String getOgaDescripcion() {
    return this.ogaDescripcion;
  }
  
  public void setOgaDescripcion(String ogaDescripcion) {
    this.ogaDescripcion = ogaDescripcion;
  }
  
  public String getNombrePaciente() {
    return this.nombrePaciente;
  }
  
  public void setNombrePaciente(String nombrePaciente) {
    this.nombrePaciente = nombrePaciente;
  }
  
  public String getCentroAtencion() {
    return this.centroAtencion;
  }
  
  public void setCentroAtencion(String centroAtencion) {
    this.centroAtencion = centroAtencion;
  }
}

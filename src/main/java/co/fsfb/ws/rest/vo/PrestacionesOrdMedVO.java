package co.fsfb.ws.rest.vo;

import java.util.Date;

public class PrestacionesOrdMedVO {
  private Long pomIdPrestOrdm;
  
  private Long ormIdOrdmNumero;
  
  private String serSerCodigo;
  
  private String serSerDesc;
  
  private String prePreCodigo;
  
  private String prePreDesc;
  
  private String pcaAgeCodigRecep;
  
  private Date cgFechaProceso;
  
  private GestionContinuidadVO caTrazaGestContinuidad;
  
  private GestionAutorizacionVO caGestionAutorizacion;
  
  public Long getPomIdPrestOrdm() {
    return this.pomIdPrestOrdm;
  }
  
  public void setPomIdPrestOrdm(Long pomIdPrestOrdm) {
    this.pomIdPrestOrdm = pomIdPrestOrdm;
  }
  
  public Long getOrmIdOrdmNumero() {
    return this.ormIdOrdmNumero;
  }
  
  public void setOrmIdOrdmNumero(Long ormIdOrdmNumero) {
    this.ormIdOrdmNumero = ormIdOrdmNumero;
  }
  
  public String getSerSerCodigo() {
    return this.serSerCodigo;
  }
  
  public void setSerSerCodigo(String serSerCodigo) {
    this.serSerCodigo = serSerCodigo;
  }
  
  public String getSerSerDesc() {
    return this.serSerDesc;
  }
  
  public void setSerSerDesc(String serSerDesc) {
    this.serSerDesc = serSerDesc;
  }
  
  public String getPrePreCodigo() {
    return this.prePreCodigo;
  }
  
  public void setPrePreCodigo(String prePreCodigo) {
    this.prePreCodigo = prePreCodigo;
  }
  
  public String getPrePreDesc() {
    return this.prePreDesc;
  }
  
  public void setPrePreDesc(String prePreDesc) {
    this.prePreDesc = prePreDesc;
  }
  
  public String getPcaAgeCodigRecep() {
    return this.pcaAgeCodigRecep;
  }
  
  public void setPcaAgeCodigRecep(String pcaAgeCodigRecep) {
    this.pcaAgeCodigRecep = pcaAgeCodigRecep;
  }
  
  public Date getCgFechaProceso() {
    return this.cgFechaProceso;
  }
  
  public void setCgFechaProceso(Date cgFechaProceso) {
    this.cgFechaProceso = cgFechaProceso;
  }
  
  public GestionContinuidadVO getCaTrazaGestContinuidad() {
    return this.caTrazaGestContinuidad;
  }
  
  public void setCaTrazaGestContinuidad(GestionContinuidadVO caTrazaGestContinuidad) {
    this.caTrazaGestContinuidad = caTrazaGestContinuidad;
  }
  
  public GestionAutorizacionVO getCaGestionAutorizacion() {
    return this.caGestionAutorizacion;
  }
  
  public void setCaGestionAutorizacion(GestionAutorizacionVO caGestionAutorizacion) {
    this.caGestionAutorizacion = caGestionAutorizacion;
  }
}

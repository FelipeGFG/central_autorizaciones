package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class ConsultaCitaPorPacienteAutoVO implements Serializable {
  private static final long serialVersionUID = 1L;
  
  private String pacId;
  
  private String tipoDcumento;
  
  public String getPacId() {
    return this.pacId;
  }
  
  public void setPacId(String pacId) {
    this.pacId = pacId;
  }
  
  public static long getSerialversionuid() {
    return 1L;
  }
  
  public String getTipoDcumento() {
    return this.tipoDcumento;
  }
  
  public void setTipoDcumento(String tipoDcumento) {
    this.tipoDcumento = tipoDcumento;
  }
}

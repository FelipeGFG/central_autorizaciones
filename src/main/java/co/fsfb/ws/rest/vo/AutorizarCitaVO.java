package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class AutorizarCitaVO implements Serializable {
  private static final long serialVersionUID = 1L;
  
  private Long pomIdPrestOrdm;
  
  private Long gcoIdCodigoEstado;
  
  private Long gcoIdCodigoMotivo;
  
  private boolean gcoDirecPaciente;
  
  private boolean gcoRealizoAgendamiento;
  
  private String gcoObservaciones;
  
  private String pcaAgeCodigRecep;
  
  private Long pacPacNumero;
  
  private boolean enviarCorreo;
  
  public Long getPomIdPrestOrdm() {
    return this.pomIdPrestOrdm;
  }
  
  public void setPomIdPrestOrdm(Long pomIdPrestOrdm) {
    this.pomIdPrestOrdm = pomIdPrestOrdm;
  }
  
  public Long getGcoIdCodigoEstado() {
    return this.gcoIdCodigoEstado;
  }
  
  public void setGcoIdCodigoEstado(Long gcoIdCodigoEstado) {
    this.gcoIdCodigoEstado = gcoIdCodigoEstado;
  }
  
  public Long getGcoIdCodigoMotivo() {
    return this.gcoIdCodigoMotivo;
  }
  
  public void setGcoIdCodigoMotivo(Long gcoIdCodigoMotivo) {
    this.gcoIdCodigoMotivo = gcoIdCodigoMotivo;
  }
  
  public boolean isGcoDirecPaciente() {
    return this.gcoDirecPaciente;
  }
  
  public void setGcoDirecPaciente(boolean gcoDirecPaciente) {
    this.gcoDirecPaciente = gcoDirecPaciente;
  }
  
  public boolean isGcoRealizoAgendamiento() {
    return this.gcoRealizoAgendamiento;
  }
  
  public void setGcoRealizoAgendamiento(boolean gcoRealizoAgendamiento) {
    this.gcoRealizoAgendamiento = gcoRealizoAgendamiento;
  }
  
  public String getGcoObservaciones() {
    return this.gcoObservaciones;
  }
  
  public void setGcoObservaciones(String gcoObservaciones) {
    this.gcoObservaciones = gcoObservaciones;
  }
  
  public String getPcaAgeCodigRecep() {
    return this.pcaAgeCodigRecep;
  }
  
  public void setPcaAgeCodigRecep(String pcaAgeCodigRecep) {
    this.pcaAgeCodigRecep = pcaAgeCodigRecep;
  }
  
  public Long getPacPacNumero() {
    return this.pacPacNumero;
  }
  
  public void setPacPacNumero(Long pacPacNumero) {
    this.pacPacNumero = pacPacNumero;
  }
  
  public boolean isEnviarCorreo() {
    return this.enviarCorreo;
  }
  
  public void setEnviarCorreo(boolean enviarCorreo) {
    this.enviarCorreo = enviarCorreo;
  }
}

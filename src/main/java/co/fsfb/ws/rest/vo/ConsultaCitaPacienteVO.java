package co.fsfb.ws.rest.vo;

import java.io.Serializable;
import java.util.List;

public class ConsultaCitaPacienteVO implements Serializable {
  private static final long serialVersionUID = 1L;
  
  private Paciente paciente;
  
  private String codCentroAten;
  
  private String codEspecialidad;
  
  private String codSubEspecialidad;
  
  private String codProf;
  
  private String convenio;
  
  private String fechaInicialHIS;
  
  private String fechaFinalHIS;
  
  private String indRecepcionado;
  
  private String codServicio;
  
  private List<String> agenciaServicio;
  
  private String fechaInicial;
  
  private String fechaFinal;
  
  private List<String> codConvenio;
  
  private Integer estado;
  
  public String getCodCentroAten() {
    return this.codCentroAten;
  }
  
  public void setCodCentroAten(String codCentroAten) {
    this.codCentroAten = codCentroAten;
  }
  
  public String getCodProf() {
    return this.codProf;
  }
  
  public String getCodEspecialidad() {
    return this.codEspecialidad;
  }
  
  public void setCodEspecialidad(String codEspecialidad) {
    this.codEspecialidad = codEspecialidad;
  }
  
  public String getCodSubEspecialidad() {
    return this.codSubEspecialidad;
  }
  
  public void setCodSubEspecialidad(String codSubEspecialidad) {
    this.codSubEspecialidad = codSubEspecialidad;
  }
  
  public void setCodProf(String codProf) {
    this.codProf = codProf;
  }
  
  public String getConvenio() {
    return this.convenio;
  }
  
  public void setConvenio(String convenio) {
    this.convenio = convenio;
  }
  
  public String getFechaFinalHIS() {
    return this.fechaFinalHIS;
  }
  
  public void setFechaFinalHIS(String fechaFinalHIS) {
    this.fechaFinalHIS = fechaFinalHIS;
  }
  
  public String getIndRecepcionado() {
    return this.indRecepcionado;
  }
  
  public void setIndRecepcionado(String indRecepcionado) {
    this.indRecepcionado = indRecepcionado;
  }
  
  public String getCodServicio() {
    return this.codServicio;
  }
  
  public void setCodServicio(String codServicio) {
    this.codServicio = codServicio;
  }
  
  public List<String> getAgenciaServicio() {
    return this.agenciaServicio;
  }
  
  public void setAgenciaServicio(List<String> agenciaServicio) {
    this.agenciaServicio = agenciaServicio;
  }
  
  public String getFechaInicial() {
    return this.fechaInicial;
  }
  
  public void setFechaInicial(String fechaInicial) {
    this.fechaInicial = fechaInicial;
  }
  
  public String getFechaFinal() {
    return this.fechaFinal;
  }
  
  public void setFechaFinal(String fechaFinal) {
    this.fechaFinal = fechaFinal;
  }
  
  public List<String> getCodConvenio() {
    return this.codConvenio;
  }
  
  public void setCodConvenio(List<String> codConvenio) {
    this.codConvenio = codConvenio;
  }
  
  public String getFechaInicialHIS() {
    return this.fechaInicialHIS;
  }
  
  public void setFechaInicialHIS(String fechaInicialHIS) {
    this.fechaInicialHIS = fechaInicialHIS;
  }
  
  public Paciente getPaciente() {
    return this.paciente;
  }
  
  public void setPaciente(Paciente paciente) {
    this.paciente = paciente;
  }
  
  public Integer getEstado() {
    return this.estado;
  }
  
  public void setEstado(Integer estado) {
    this.estado = estado;
  }
}

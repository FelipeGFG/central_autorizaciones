package co.fsfb.ws.rest.vo;

import java.io.Serializable;
import java.util.List;

public class FiltrosOrdenMedicaVO implements Serializable {
  private static final long serialVersionUID = 1L;
  
  private String pacPacTipoIdentCodigo;
  
  private String pacPacRut;
  
  private String cauDescUsuario;
  
  private String codUsrCita;
  
  private String fechaInicial;
  
  private String fechaFinal;
  
  private List<Long> estados;
  
  private String nombres;
  
  private String primerApellido;
  
  private String segundoApellido;
  
  public String getPacPacTipoIdentCodigo() {
    return this.pacPacTipoIdentCodigo;
  }
  
  public void setPacPacTipoIdentCodigo(String pacPacTipoIdentCodigo) {
    this.pacPacTipoIdentCodigo = pacPacTipoIdentCodigo;
  }
  
  public String getPacPacRut() {
    return this.pacPacRut;
  }
  
  public void setPacPacRut(String pacPacRut) {
    this.pacPacRut = pacPacRut;
  }
  
  public String getCauDescUsurio() {
    return this.cauDescUsuario;
  }
  
  public void setCauDescUsuario(String cauDescUsuario) {
    this.cauDescUsuario = cauDescUsuario;
  }
  
  public String getFechaInicial() {
    return this.fechaInicial;
  }
  
  public void setFechaInicial(String fechaInicial) {
    this.fechaInicial = fechaInicial;
  }
  
  public String getFechaFinal() {
    return this.fechaFinal;
  }
  
  public void setFechaFinal(String fechaFinal) {
    this.fechaFinal = fechaFinal;
  }
  
  public List<Long> getEstados() {
    return this.estados;
  }
  
  public void setEstados(List<Long> estados) {
    this.estados = estados;
  }
  
  public String getNombres() {
    return this.nombres;
  }
  
  public void setNombres(String nombres) {
    this.nombres = nombres;
  }
  
  public String getPrimerApellido() {
    return this.primerApellido;
  }
  
  public void setPrimerApellido(String primerApellido) {
    this.primerApellido = primerApellido;
  }
  
  public String getSegundoApellido() {
    return this.segundoApellido;
  }
  
  public void setSegundoApellido(String segundoApellido) {
    this.segundoApellido = segundoApellido;
  }
  
  public String getCauDescUsuario() {
    return this.cauDescUsuario;
  }
  
  public String getCodUsrCita() {
    return this.codUsrCita;
  }
  
  public void setCodUsrCita(String codUsrCita) {
    this.codUsrCita = codUsrCita;
  }
}

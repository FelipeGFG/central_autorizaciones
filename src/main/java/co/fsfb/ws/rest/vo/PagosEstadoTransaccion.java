package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class PagosEstadoTransaccion implements Serializable {
  private static final long serialVersionUID = -8820699724602373914L;
  
  private Long id;
  
  private String idEstadoTransaccion;
  
  private String nombre;
  
  private String estado;
  
  public Long getId() {
    return this.id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public String getIdEstadoTransaccion() {
    return this.idEstadoTransaccion;
  }
  
  public void setIdEstadoTransaccion(String idEstadoTransaccion) {
    this.idEstadoTransaccion = idEstadoTransaccion;
  }
  
  public String getNombre() {
    return this.nombre;
  }
  
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }
  
  public String getEstado() {
    return this.estado;
  }
  
  public void setEstado(String estado) {
    this.estado = estado;
  }
  
  public static long getSerialversionuid() {
    return -8820699724602373914L;
  }
}

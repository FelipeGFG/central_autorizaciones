package co.fsfb.ws.rest.vo;

import java.io.Serializable;

public class PagosPersona implements Serializable {
  private static final long serialVersionUID = -8820699724602373914L;
  
  private String id;
  
  private String pacPac;
  
  private Integer numeroDocumento;
  
  public String getId() {
    return this.id;
  }
  
  public void setId(String id) {
    this.id = id;
  }
  
  public String getPacPac() {
    return this.pacPac;
  }
  
  public void setPacPac(String pacPac) {
    this.pacPac = pacPac;
  }
  
  public Integer getNumeroDocumento() {
    return this.numeroDocumento;
  }
  
  public void setNumeroDocumento(Integer numeroDocumento) {
    this.numeroDocumento = numeroDocumento;
  }
  
  public static long getSerialversionuid() {
    return -8820699724602373914L;
  }
}

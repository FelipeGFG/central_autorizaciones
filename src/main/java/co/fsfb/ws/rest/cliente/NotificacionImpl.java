package co.fsfb.ws.rest.cliente;

import co.fsfb.ws.rest.entidades.CaGestionNotificaciones;
import co.fsfb.ws.rest.util.EntityManagerUtil;
import java.util.Calendar;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class NotificacionImpl {
  private static Logger logger = LoggerFactory.getLogger(NotificacionImpl.class);
  
  private EntityManager em = null;
  
  public void registrarCaGestionNotificaciones(CaGestionNotificaciones caGestionNotificaciones) {
    logger.info("Creando cambiarEstadoCita(CambioEstadoCitaVO)::::");
    this.em = EntityManagerUtil.getEntityManager();
    if (caGestionNotificaciones != null)
      try {
        this.em.getTransaction().begin();
        caGestionNotificaciones.setCgFechaProceso(Calendar.getInstance().getTime());
        this.em.persist(caGestionNotificaciones);
        this.em.getTransaction().commit();
      } catch (Exception e) {
        this.em.getTransaction().rollback();
        this.em.close();
      } finally {
        if (this.em.isOpen())
          this.em.close(); 
      }  
    this.em.close();
  }
}

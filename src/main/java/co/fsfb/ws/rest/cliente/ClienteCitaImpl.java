package co.fsfb.ws.rest.cliente;

import co.fsfb.ws.rest.dao.CaCitasGestionadasDAO;
import co.fsfb.ws.rest.dao.CaCorreoPacienteDAO;
import co.fsfb.ws.rest.dao.CaGestionAutorizacionDAO;
import co.fsfb.ws.rest.dao.CaPacientesDAO;
import co.fsfb.ws.rest.dao.CaPolizasPacientesDAO;
import co.fsfb.ws.rest.dao.CaTelefonosPacienteDAO;
import co.fsfb.ws.rest.dao.CaUsuariosDAO;
import co.fsfb.ws.rest.entidades.CaCitasGestionadas;
import co.fsfb.ws.rest.entidades.CaCorreoPaciente;
import co.fsfb.ws.rest.entidades.CaGestionAutorizacionCita;
import co.fsfb.ws.rest.entidades.CaObsTrazaCitas;
import co.fsfb.ws.rest.entidades.CaPacientes;
import co.fsfb.ws.rest.entidades.CaPolizasPacientes;
import co.fsfb.ws.rest.entidades.CaTelefonosPaciente;
import co.fsfb.ws.rest.entidades.CaTrazaCitas;
import co.fsfb.ws.rest.entidades.CaTrazaPolizasPacientes;
import co.fsfb.ws.rest.entidades.CaUsuarios;
import co.fsfb.ws.rest.exception.DataException;
import co.fsfb.ws.rest.mapper.GestionAutorizacionCitaMapper;
import co.fsfb.ws.rest.util.EntityManagerUtil;
import co.fsfb.ws.rest.util.EnviarEmail;
import co.fsfb.ws.rest.util.PropertiesManager;
import co.fsfb.ws.rest.vo.CaPolizasPacientesVO;
import co.fsfb.ws.rest.vo.CambioEstadoCitaVO;
import co.fsfb.ws.rest.vo.ConsultaDetalleCitaPacienteVO;
import co.fsfb.ws.rest.vo.ConsultaTrazabilidadCitaVO;
import co.fsfb.ws.rest.vo.FiltrosDetalleCitaPacienteVO;
import co.fsfb.ws.rest.vo.GestionAutorizacionCitaVO;
import co.fsfb.ws.rest.vo.ResCambioEstadoCitaVO;
import co.fsfb.ws.rest.vo.TrazabilidadCitaVO;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import org.apache.commons.lang3.text.StrSubstitutor;
import org.apache.commons.mail.HtmlEmail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class ClienteCitaImpl {
  private static Logger logger = LoggerFactory.getLogger(ClienteCitaImpl.class);
  
  private EntityManager em = null;
  
  private CaPacientesDAO caPacientesDAO = new CaPacientesDAO();
  
  private CaUsuariosDAO caUsuariosDAO = new CaUsuariosDAO();
  
  private CaTelefonosPacienteDAO caTelefonosPacienteDAO = new CaTelefonosPacienteDAO();
  
  private CaCorreoPacienteDAO caCorreoPacienteDAO = new CaCorreoPacienteDAO();
  
  private CaCitasGestionadasDAO caCitasGestionadasDAO = new CaCitasGestionadasDAO();
  
  private CaPolizasPacientesDAO caPolizasPacientesDAO = new CaPolizasPacientesDAO();
  
  private CaGestionAutorizacionDAO caGestionAutorizacionDAO = new CaGestionAutorizacionDAO();
  
  public ResCambioEstadoCitaVO cambiarEstadoCita(CambioEstadoCitaVO cambioEstadoCitaVO) throws Exception {
    logger.info("Creando cambiarEstadoCita(CambioEstadoCitaVO)::::");
    ResCambioEstadoCitaVO resCambioEstadoCitaVO = new ResCambioEstadoCitaVO();
    this.em = EntityManagerUtil.getEntityManager();
    if (cambioEstadoCitaVO != null)
      try {
        this.em.getTransaction().begin();
        LocalDate localDate = null;
        DateTimeFormatter formatter = null;
        CaPacientes caPacientes = this.caPacientesDAO.consultarCaPacientes(Long.valueOf(cambioEstadoCitaVO.getPacNum()));
        if (caPacientes == null) {
          caPacientes = new CaPacientes();
          caPacientes.setPacPacNumero(Long.valueOf(cambioEstadoCitaVO.getPacNum()));
          caPacientes.setPacPacRut(cambioEstadoCitaVO.getNumDocId());
          caPacientes.setPacPacTipoIdentCodigo(cambioEstadoCitaVO.getPacPacTipoIdentCodigo());
          caPacientes.setTipIdav(cambioEstadoCitaVO.getTipTipIDav());
          this.em.persist(caPacientes);
        } 
        CaUsuarios caUsuarios = this.caUsuariosDAO.consultarCaUsuarios(cambioEstadoCitaVO.getCodUsrCita());
        if (caUsuarios == null) {
          caUsuarios = new CaUsuarios();
          caUsuarios.setPcaAgeCodigRecep(cambioEstadoCitaVO.getCodUsrCita());
          caUsuarios.setCauDescUsuarios(cambioEstadoCitaVO.getCauDescUsuarios());
          caUsuarios.setCauEstado("A");
          this.em.persist(caUsuarios);
        } 
        CaUsuarios caUsuariosBD = this.caUsuariosDAO.consultarCaUsuarios(cambioEstadoCitaVO.getPcaAgeCodigRecep());
        if (caUsuariosBD == null) {
          caUsuarios = new CaUsuarios();
          caUsuarios.setPcaAgeCodigRecep(cambioEstadoCitaVO.getPcaAgeCodigRecep());
          caUsuarios.setCauDescUsuarios(cambioEstadoCitaVO.getCauDescUsuarios());
          caUsuarios.setCauEstado("A");
          this.em.persist(caUsuarios);
        } 
        CaTelefonosPaciente caTelefonosPaciente = this.caTelefonosPacienteDAO.consultarCaTelefonosPaciente(Long.valueOf(cambioEstadoCitaVO.getPacNum()), Long.valueOf(4L));
        if (caTelefonosPaciente == null) {
          caTelefonosPaciente = new CaTelefonosPaciente();
          caTelefonosPaciente.setPacPacNumero(Long.valueOf(cambioEstadoCitaVO.getPacNum()));
          caTelefonosPaciente.setTcIdCodigo(Long.valueOf(4L));
          caTelefonosPaciente.setTpTelefono(cambioEstadoCitaVO.getTelefono());
          caTelefonosPaciente.setPcaAgeCodigRecep(cambioEstadoCitaVO.getCodUsrCita());
          caTelefonosPaciente.setCgFechaProceso(Calendar.getInstance().getTime());
          this.em.persist(caTelefonosPaciente);
        } 
        CaCorreoPaciente caCorreoPaciente = this.caCorreoPacienteDAO.consultarCaCorreoPaciente(Long.valueOf(cambioEstadoCitaVO.getPacNum()), Long.valueOf(4L));
        if (caCorreoPaciente == null) {
          caCorreoPaciente = new CaCorreoPaciente();
          caCorreoPaciente.setPacPacNumero(Long.valueOf(cambioEstadoCitaVO.getPacNum()));
          caCorreoPaciente.setTcIdCodigo(Long.valueOf(4L));
          caCorreoPaciente.setTpCorreoE(cambioEstadoCitaVO.getEmail());
          caCorreoPaciente.setPcaAgeCodigRecep(cambioEstadoCitaVO.getCodUsrCita());
          caCorreoPaciente.setCgFechaProceso(Calendar.getInstance().getTime());
          this.em.persist(caCorreoPaciente);
        } 
        formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        localDate = LocalDate.parse(cambioEstadoCitaVO.getFechaCita(), formatter);
        CaCitasGestionadas caCitasGestionadas = this.caCitasGestionadasDAO.consultarCaCitasGestionadas(cambioEstadoCitaVO
            .getHoraCita(), 
            Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant()), cambioEstadoCitaVO
            .getCodUsrCita(), Long.valueOf(cambioEstadoCitaVO.getPacNum()));
        if (caCitasGestionadas == null) {
          caCitasGestionadas = new CaCitasGestionadas();
          caCitasGestionadas.setPacPacNumero(Long.valueOf(cambioEstadoCitaVO.getPacNum()));
          caCitasGestionadas
            .setPcaAgeFechaCitac(Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));
          caCitasGestionadas.setPcaAgeHoraCitac(cambioEstadoCitaVO.getHoraCita());
          caCitasGestionadas.setPcaAgeCodigServi(cambioEstadoCitaVO.getCodServicio());
          caCitasGestionadas.setPcaAgeCodigProfe(cambioEstadoCitaVO.getCodProf());
          caCitasGestionadas.setPcaAgeObjeto(cambioEstadoCitaVO.getConsultorio());
          caCitasGestionadas.setPrePreCodigo(cambioEstadoCitaVO.getCodigoPrestacion());
          if (cambioEstadoCitaVO.getCodCentroAten() != null)
            caCitasGestionadas.setPcaAgeLugar(cambioEstadoCitaVO.getCodCentroAten()); 
          if (cambioEstadoCitaVO.getCodConvenio() != null)
            caCitasGestionadas.setConConCodigo(cambioEstadoCitaVO.getCodConvenio().get(0)); 
          caCitasGestionadas.setPcaAgeCodigRecep(cambioEstadoCitaVO.getCodUsrCita());
          localDate = LocalDate.parse(cambioEstadoCitaVO.getFechaAsigna(), formatter);
          caCitasGestionadas
            .setPcaAgeFechaDigit(Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));
          caCitasGestionadas.setPcaAgeRecepcionado(cambioEstadoCitaVO.getIndRecepcionado());
          caCitasGestionadas.setEcIdCodigo(cambioEstadoCitaVO.getEcIdCodigo());
          caCitasGestionadas.setCgFechaProceso(Calendar.getInstance().getTime());
          this.em.persist(caCitasGestionadas);
          resCambioEstadoCitaVO.setcGIdCitaNumero(caCitasGestionadas.getcGIdCitaNumero());
          CaTrazaCitas caTrazaCitas = new CaTrazaCitas();
          Calendar fechaProceso = Calendar.getInstance();
          fechaProceso.setTime(caCitasGestionadas.getPcaAgeFechaCitac());
          fechaProceso.set(10, 
              Integer.parseInt(caCitasGestionadas.getPcaAgeHoraCitac().split(":")[0]));
          fechaProceso.set(12, 
              Integer.parseInt(caCitasGestionadas.getPcaAgeHoraCitac().split(":")[1]));
          fechaProceso.set(13, 0);
          caTrazaCitas.setCgFechaProceso(fechaProceso.getTime());
          caTrazaCitas.setCgIdCitaNumero(caCitasGestionadas.getcGIdCitaNumero());
          caTrazaCitas.setEcIdCodigo(Long.valueOf(1L));
//          if(cambioEstadoCitaVO.getCodUsrCita() == null){
//        	  caTrazaCitas.setPcaAgeCodigRecep("FSFBIMG");
//          }
          caTrazaCitas.setPcaAgeCodigRecep(cambioEstadoCitaVO.getCodUsrCita());
          this.em.persist(caTrazaCitas);
          CaTrazaCitas caTrazaCitasCambioEstado = new CaTrazaCitas();
          caTrazaCitasCambioEstado.setCgFechaProceso(Calendar.getInstance().getTime());
          caTrazaCitasCambioEstado.setCgIdCitaNumero(caCitasGestionadas.getcGIdCitaNumero());
          caTrazaCitasCambioEstado.setEcIdCodigo(cambioEstadoCitaVO.getEcIdCodigo());
          caTrazaCitasCambioEstado.setPcaAgeCodigRecep(cambioEstadoCitaVO.getPcaAgeCodigRecep());
          this.em.persist(caTrazaCitasCambioEstado);
        } else {
          resCambioEstadoCitaVO.setcGIdCitaNumero(caCitasGestionadas.getcGIdCitaNumero());
          caCitasGestionadas.setEcIdCodigo(cambioEstadoCitaVO.getEcIdCodigo());
          caCitasGestionadas.setCgFechaProceso(Calendar.getInstance().getTime());
          this.em.merge(caCitasGestionadas);
          CaTrazaCitas caTrazaCitas = new CaTrazaCitas();
          caTrazaCitas.setCgFechaProceso(Calendar.getInstance().getTime());
          caTrazaCitas.setCgIdCitaNumero(caCitasGestionadas.getcGIdCitaNumero());
          caTrazaCitas.setEcIdCodigo(cambioEstadoCitaVO.getEcIdCodigo());
          caTrazaCitas.setPcaAgeCodigRecep(caUsuariosBD.getPcaAgeCodigRecep());
          this.em.persist(caTrazaCitas);
        } 
        CaObsTrazaCitas caObsTrazaCitas = new CaObsTrazaCitas();
        caObsTrazaCitas.setCgIdCitaNumero(caCitasGestionadas.getcGIdCitaNumero());
        caObsTrazaCitas.setOtcObservacion(cambioEstadoCitaVO.getOtcObservacion());
        caObsTrazaCitas.setEcIdCodigo(cambioEstadoCitaVO.getEcIdCodigo());
        this.em.persist(caObsTrazaCitas);
        if (cambioEstadoCitaVO.getEcPolizaNumero() != null && 
          !cambioEstadoCitaVO.getEcPolizaNumero().isEmpty()) {
          CaPolizasPacientes caPolizasPacientes = this.caPolizasPacientesDAO.consultarCaPolizasPacientes(Long.valueOf(cambioEstadoCitaVO.getPacNum()));
          if (caPolizasPacientes == null) {
            caPolizasPacientes = new CaPolizasPacientes();
            caPolizasPacientes.setCgFechaProceso(Calendar.getInstance().getTime());
            caPolizasPacientes.setPacPacNumero(Long.valueOf(cambioEstadoCitaVO.getPacNum()));
            caPolizasPacientes.setPcaAgeCodigRecep(cambioEstadoCitaVO.getPcaAgeCodigRecep());
            caPolizasPacientes.setEcPolizaNumero(cambioEstadoCitaVO.getEcPolizaNumero());
            caPolizasPacientes.setCgFechaProceso(Calendar.getInstance().getTime());
            caPolizasPacientes.setAccIdNumero(Long.valueOf(1L));
            this.em.persist(caPolizasPacientes);
          } else if (!caPolizasPacientes.getEcPolizaNumero().equals(cambioEstadoCitaVO.getEcPolizaNumero())) {
            caPolizasPacientes.setEcPolizaNumero(cambioEstadoCitaVO.getEcPolizaNumero());
            caPolizasPacientes.setCgFechaProceso(Calendar.getInstance().getTime());
            caPolizasPacientes.setPcaAgeCodigRecep(cambioEstadoCitaVO.getPcaAgeCodigRecep());
            this.em.merge(caPolizasPacientes);
          } 
          CaTrazaPolizasPacientes caTrazaPolizasPacientes = new CaTrazaPolizasPacientes();
          caTrazaPolizasPacientes.setAccIdNumero(caPolizasPacientes.getAccIdNumero());
          caTrazaPolizasPacientes.setCgFechaProceso(Calendar.getInstance().getTime());
          caTrazaPolizasPacientes.setEcPolizaNumero(caPolizasPacientes.getEcPolizaNumero());
          caTrazaPolizasPacientes.setPacPacNumero(caPolizasPacientes.getPacPacNumero());
          caTrazaPolizasPacientes.setPcaAgeCodigRecep(caPolizasPacientes.getPcaAgeCodigRecep());
          this.em.persist(caTrazaPolizasPacientes);
        } 
        if (cambioEstadoCitaVO.isCorreoElectronicoPaciente());
        this.em.getTransaction().commit();
      } catch (Exception e) {
        StringWriter errors = new StringWriter();
        e.printStackTrace(new PrintWriter(errors));
        logger.info("error cambio estado cita " + errors.toString());
        this.em.getTransaction().rollback();
        this.em.close();
        throw e;
      } finally {
        if (this.em.isOpen())
          this.em.close(); 
      }  
    return resCambioEstadoCitaVO;
  }
  
  public List<ConsultaTrazabilidadCitaVO> consultarTrazabilidadCita(Long cGIdCitaNumero) {
    logger.info("Creando consultarTrazabilidadCita::::");
    return this.caCitasGestionadasDAO.consultarTrazabilidadCita(cGIdCitaNumero);
  }
  
  public List<ConsultaTrazabilidadCitaVO> consultarTrazabilidadCita(TrazabilidadCitaVO trazabilidadCitaVO) {
    logger.info("Creando consultarTrazabilidadCita::::");
    return this.caCitasGestionadasDAO.consultarTrazabilidadCita(trazabilidadCitaVO);
  }
  
  public ConsultaDetalleCitaPacienteVO consultarDetalleCitaPaciente(FiltrosDetalleCitaPacienteVO filtrosDetalleCitaPacienteVO) throws DataException {
    logger.info("Creando consultarDetalleCitaPaciente::::");
    return this.caCitasGestionadasDAO.consultarDetalleCitaPaciente(filtrosDetalleCitaPacienteVO);
  }
  
  public boolean registrarAutorizacion(GestionAutorizacionCitaVO gestionAutorizacionCitaVO) throws DataException {
    logger.info("Iniciando registrarAutorizacion(gestionAutorizacionVO)::::");
    this.em = EntityManagerUtil.getEntityManager();
    try {
      this.em.getTransaction().begin();
      DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
      LocalDate localDate = LocalDate.parse(gestionAutorizacionCitaVO.getFechaCita(), formatter);
      CaCitasGestionadas caCitasGestionadas = this.caCitasGestionadasDAO.consultarCaCitasGestionadas(gestionAutorizacionCitaVO
          .getHoraCita(), 
          Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant()), gestionAutorizacionCitaVO
          .getCodUsrCita(), gestionAutorizacionCitaVO.getPacNum());
      CaGestionAutorizacionCita caGestionAutorizacionCita = this.caGestionAutorizacionDAO.consultarCaAutorizacionCita(caCitasGestionadas.getPacPacNumero().longValue());
      caGestionAutorizacionCita = GestionAutorizacionCitaMapper.INSTANCE.voToEntity(gestionAutorizacionCitaVO);
      caGestionAutorizacionCita.setCgIdCitaNumero(caCitasGestionadas.getcGIdCitaNumero());
      caGestionAutorizacionCita.setCgFechaProceso(Calendar.getInstance().getTime());
      caGestionAutorizacionCita.setPacPacNumero(caCitasGestionadas.getPacPacNumero());
      caGestionAutorizacionCita.setOgaDescripcion(gestionAutorizacionCitaVO.getGauObservaciones());
      this.em.merge(caGestionAutorizacionCita);
      CaPolizasPacientes caPolizasPacientes = this.caPolizasPacientesDAO.consultarCaPolizasPacientes(caCitasGestionadas.getPacPacNumero());
      if (caPolizasPacientes == null) {
        caPolizasPacientes = new CaPolizasPacientes();
        caPolizasPacientes.setAccIdNumero(Long.valueOf(2L));
        caPolizasPacientes.setEcPolizaNumero(gestionAutorizacionCitaVO.getNumeroPoliza());
        caPolizasPacientes.setPcaAgeCodigRecep(gestionAutorizacionCitaVO.getPcaAgeCodigoRecep());
        caPolizasPacientes.setPacPacNumero(caCitasGestionadas.getPacPacNumero());
        caPolizasPacientes.setCgFechaProceso(new Date());
        this.em.persist(caPolizasPacientes);
      } else {
        caPolizasPacientes.setEcPolizaNumero(gestionAutorizacionCitaVO.getNumeroPoliza());
        caPolizasPacientes.setPcaAgeCodigRecep(gestionAutorizacionCitaVO.getPcaAgeCodigoRecep());
        this.em.merge(caPolizasPacientes);
      } 
      CaTrazaCitas caTrazaCitasCambioEstado = new CaTrazaCitas();
      caTrazaCitasCambioEstado.setCgFechaProceso(Calendar.getInstance().getTime());
      caTrazaCitasCambioEstado.setCgIdCitaNumero(caCitasGestionadas.getcGIdCitaNumero());
      if (gestionAutorizacionCitaVO.getGauAutorizaServ().equals("1")) {
        caCitasGestionadas.setEcIdCodigo(Long.valueOf(3L));
        caTrazaCitasCambioEstado.setEcIdCodigo(Long.valueOf(3L));
      } else {
        caCitasGestionadas.setEcIdCodigo(Long.valueOf(7L));
        caTrazaCitasCambioEstado.setEcIdCodigo(Long.valueOf(7L));
      } 
      caTrazaCitasCambioEstado.setPcaAgeCodigRecep(gestionAutorizacionCitaVO.getPcaAgeCodigoRecep());
      this.em.persist(caTrazaCitasCambioEstado);
      this.em.merge(caCitasGestionadas);
      if (gestionAutorizacionCitaVO.isEnviarCorreo())
        enviarCorreoAutorizacion(gestionAutorizacionCitaVO); 
      this.em.getTransaction().commit();
    } catch (Exception e) {
      logger.error("Error creando la gestion de autorizacion de la cita: ", e);
      this.em.getTransaction().rollback();
      this.em.close();
      throw new DataException(0);
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
    this.em.close();
    return true;
  }
  
  private void enviarCorreoAutorizacion(GestionAutorizacionCitaVO gestionAutorizacionCitaVO) throws DataException {
    (new Thread(() -> {
          String html = null;
          StrSubstitutor strSub = null;
          String asunto = "";
          String estado = "";
          HtmlEmail htmlEmail = new HtmlEmail();
          try {
            String email = "centralautorizacionesfsfb@gmail.com";
            CaCorreoPaciente caCorreoPaciente = this.caCorreoPacienteDAO.consultarCaCorreoPaciente(gestionAutorizacionCitaVO.getPacNum());
            if (caCorreoPaciente != null && caCorreoPaciente.getTpCorreoE() != null && !caCorreoPaciente.getTpCorreoE().trim().isEmpty())
              email = caCorreoPaciente.getTpCorreoE().trim(); 
            if (gestionAutorizacionCitaVO.getGauAutorizaServ().equals("1")) {
              asunto = String.format(PropertiesManager.CONFIG.getString("AUTORIZACION_ASEG_EMAIL_SUBJECT"), new Object[] { "AUTORIZADA" });
              estado = "AUTORIZADA POR TU ASEGURADOR.";
            } else if (gestionAutorizacionCitaVO.getGauAutorizaServ().equals("2")) {
              asunto = String.format(PropertiesManager.CONFIG.getString("AUTORIZACION_ASEG_EMAIL_SUBJECT"), new Object[] { "NO AUTORIZADA" });
              estado = "NO AUTORIZADA POR TU ASEGURADOR.";
            } 
            Map<String, String> emailDatos = new HashMap<>();
            emailDatos.put("nombre", gestionAutorizacionCitaVO.getNombrePaciente().split(" ")[0]);
            emailDatos.put("estado", estado);
            emailDatos.put("entidad", gestionAutorizacionCitaVO.getCentroAtencion());
            html = fileToString(OrdenesMedicasImpl.class.getResourceAsStream("/mail/email-auth.html"), "utf-8");
            String[] images = { "CitaProgramada-01.png", "CitaProgramada-02.png", "CitaProgramada-03.png", "facebook-icon.png", "instagram-icon.png", "twitter-icon.png" };
            for (int i = 0; i < images.length; i++)
              emailDatos.put(images[i], PropertiesManager.CONFIG.getString("MAIL_IMAGES").concat(images[i])); 
            strSub = new StrSubstitutor(emailDatos);
            html = strSub.replace(html);
            (new EnviarEmail()).postMail(new String[] { email }, asunto, html);
          } catch (DataException|IOException ex) {
            logger.info("Error enviando correo continuidad", ex);
          } 
        })).start();
  }
  
  public CaPolizasPacientesVO consultarPoliza(long pacNum) {
    CaPolizasPacientes caPolizasPacientes = this.caPolizasPacientesDAO.consultarCaPolizasPacientes(Long.valueOf(pacNum));
    if (caPolizasPacientes != null) {
      CaPolizasPacientesVO poliza = new CaPolizasPacientesVO();
      poliza.setAccIdNumero(caPolizasPacientes.getAccIdNumero());
      poliza.setEcPolizaNumero(caPolizasPacientes.getEcPolizaNumero());
      return poliza;
    } 
    return null;
  }
  
  private static String fileToString(InputStream input, String encoding) throws IOException {
    StringWriter sw = new StringWriter();
    InputStreamReader in = new InputStreamReader(input, encoding);
    char[] buffer = new char[2048];
    int n = 0;
    while (-1 != (n = in.read(buffer)))
      sw.write(buffer, 0, n); 
    return sw.toString();
  }
}

package co.fsfb.ws.rest.cliente;

import co.fsfb.ws.rest.dao.CaCorreoPacienteDAO;
import co.fsfb.ws.rest.dao.CaOrdenesMedicasDAO;
import co.fsfb.ws.rest.dao.CaPacientesDAO;
import co.fsfb.ws.rest.dao.CaPolizasPacientesDAO;
import co.fsfb.ws.rest.dao.CaUsuariosDAO;
import co.fsfb.ws.rest.entidades.CaCorreoPaciente;
import co.fsfb.ws.rest.entidades.CaMotivosEliminarOm;
import co.fsfb.ws.rest.entidades.CaOrdenesMedicas;
import co.fsfb.ws.rest.entidades.CaPacientes;
import co.fsfb.ws.rest.entidades.CaPolizasPacientes;
import co.fsfb.ws.rest.entidades.CaPrestacionesOrdMed;
import co.fsfb.ws.rest.entidades.CaTrazaEliminarOm;
import co.fsfb.ws.rest.entidades.CaTrazaGestContinuidad;
import co.fsfb.ws.rest.entidades.CaTrazaGestionAutorizacion;
import co.fsfb.ws.rest.entidades.CaTrazaOrdenMedicas;
import co.fsfb.ws.rest.entidades.CaTrazaPolizasPacientes;
import co.fsfb.ws.rest.entidades.CaUsuarios;
import co.fsfb.ws.rest.enumeraciones.EnumTipoIdentDocumento;
import co.fsfb.ws.rest.exception.DataException;
import co.fsfb.ws.rest.mapper.GestionAutorizacionMapper;
import co.fsfb.ws.rest.mapper.OrdenMedicaMapper;
import co.fsfb.ws.rest.mapper.PrestacionOrdenMedicaMapper;
import co.fsfb.ws.rest.util.EntityManagerUtil;
import co.fsfb.ws.rest.util.EnviarEmail;
import co.fsfb.ws.rest.util.PropertiesManager;
import co.fsfb.ws.rest.vo.AdmOrdenesMedicasVO;
import co.fsfb.ws.rest.vo.ConsultaTrazabilidadCitaVO;
import co.fsfb.ws.rest.vo.CrearPrestacionesOrdMedVO;
import co.fsfb.ws.rest.vo.DetalleOrdenMedicaVO;
import co.fsfb.ws.rest.vo.FiltrosOrdenMedicaVO;
import co.fsfb.ws.rest.vo.FinalizarOrdenMedicaVO;
import co.fsfb.ws.rest.vo.GestionAutorizacionVO;
import co.fsfb.ws.rest.vo.GestionContinuidadVO;
import co.fsfb.ws.rest.vo.OrdenMedicaVO;
import co.fsfb.ws.rest.vo.Paciente;
import co.fsfb.ws.rest.vo.PrestacionesOrdMedVO;
import co.fsfb.ws.rest.vo.RespuestaGestionContinuidadVO;
import co.fsfb.ws.rest.vo.RespuestaOrdenMedicaVO;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import org.apache.commons.lang3.text.StrSubstitutor;
import org.apache.commons.mail.HtmlEmail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class OrdenesMedicasImpl {
  private static Logger logger = LoggerFactory.getLogger(OrdenesMedicasImpl.class);
  
  private EntityManager em = null;
  
  private CaOrdenesMedicasDAO caOrdenesMedicasDAO = new CaOrdenesMedicasDAO();
  
  private CaPolizasPacientesDAO caPolizasPacientesDAO = new CaPolizasPacientesDAO();
  
  private CaPacientesDAO caPacientesDAO = new CaPacientesDAO();
  
  private CaUsuariosDAO caUsuariosDAO = new CaUsuariosDAO();
  
  private CaCorreoPacienteDAO caCorreoPacienteDAO = new CaCorreoPacienteDAO();
  
  public List<RespuestaOrdenMedicaVO> consultarOrdenMedica(FiltrosOrdenMedicaVO filtrosOrdenMedicaVO) throws DataException {
    logger.info("Creando consultarOrdenMedica(FiltrosOrdenMedicaVO)::::");
    RespuestaOrdenMedicaVO respuestaOrdenMedicaVO = new RespuestaOrdenMedicaVO();
    this.em = EntityManagerUtil.getEntityManager();
    List<RespuestaOrdenMedicaVO> oldOrden = consultarOrdenExistente(filtrosOrdenMedicaVO);
    try {
      this.em.getTransaction().begin();
      if (oldOrden != null && !oldOrden.isEmpty()) {
        this.em.getTransaction().commit();
        return oldOrden;
      } 
      Paciente paciente = null;
      if (filtrosOrdenMedicaVO.getPacPacTipoIdentCodigo()
        .equals(EnumTipoIdentDocumento.ADULTO_SIN_IDENTIFICACION.getId()) || filtrosOrdenMedicaVO
        .getPacPacTipoIdentCodigo()
        .equals(EnumTipoIdentDocumento.MENOR_SIN_IDENTIFICACION.getId())) {
        Paciente filPaciente = new Paciente();
        filPaciente.setNombres(filtrosOrdenMedicaVO.getNombres());
        filPaciente.setPrimerApellido(filtrosOrdenMedicaVO.getPrimerApellido());
        filPaciente.setSegundoApellido(filtrosOrdenMedicaVO.getSegundoApellido());
        filPaciente.setPacPacTipoIdentCodigo(filtrosOrdenMedicaVO.getPacPacTipoIdentCodigo());
        filPaciente.setTipoDocId(filtrosOrdenMedicaVO.getPacPacRut());
        paciente = this.caPacientesDAO.consultarPacientesFSFBHIS(filPaciente);
        if (paciente != null) {
          CaPacientes caPacientes = this.caPacientesDAO.consultarCaPacientes(paciente.getPacPacTipoIdentCodigo(), paciente
              .getTipoDocId());
          if (caPacientes == null) {
            CaPacientes caPacientesReg = new CaPacientes();
            caPacientesReg.setPacPacNumero(Long.valueOf(paciente.getPacNum()));
            caPacientesReg.setPacPacRut(paciente.getNumDocId());
            caPacientesReg.setPacPacTipoIdentCodigo(paciente.getPacPacTipoIdentCodigo());
            caPacientesReg.setTipIdav(paciente.getTipTipIDav());
            this.em.persist(caPacientesReg);
          } 
        } 
      } else {
        paciente = this.caPacientesDAO.consultarPacientesFSFBHIS(filtrosOrdenMedicaVO.getPacPacTipoIdentCodigo(), filtrosOrdenMedicaVO
            .getPacPacRut());
        if (paciente != null) {
          CaPacientes caPacientes = this.caPacientesDAO.consultarCaPacientes(filtrosOrdenMedicaVO
              .getPacPacTipoIdentCodigo(), filtrosOrdenMedicaVO.getPacPacRut());
          if (caPacientes == null) {
            CaPacientes caPacientesReg = new CaPacientes();
            caPacientesReg.setPacPacNumero(Long.valueOf(paciente.getPacNum()));
            caPacientesReg.setPacPacRut(paciente.getNumDocId());
            caPacientesReg.setPacPacTipoIdentCodigo(paciente.getPacPacTipoIdentCodigo());
            caPacientesReg.setTipIdav(paciente.getTipTipIDav());
            this.em.persist(caPacientesReg);
          } 
        } 
        if (paciente == null)
          throw new Exception(); 
        respuestaOrdenMedicaVO.setTipoDocumento(paciente.getPacPacTipoIdentCodigo());
        respuestaOrdenMedicaVO.setDocumento(paciente.getNumDocId());
        respuestaOrdenMedicaVO.setNombreCompleto(paciente.getNombreCompleto());
        respuestaOrdenMedicaVO.setTipTipIDav(paciente.getTipTipIDav());
        respuestaOrdenMedicaVO.setPacPacNumero(Long.valueOf(paciente.getPacNum()));
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        respuestaOrdenMedicaVO.setFechaRegistroFile(simpleDateFormat.format(Calendar.getInstance().getTime()));
        CaUsuarios caUsuarios = this.caUsuariosDAO.consultarCaUsuarios(filtrosOrdenMedicaVO.getCodUsrCita());
        if (caUsuarios == null) {
          caUsuarios = new CaUsuarios();
          caUsuarios.setPcaAgeCodigRecep(filtrosOrdenMedicaVO.getCodUsrCita());
          caUsuarios.setCauDescUsuarios(filtrosOrdenMedicaVO.getCauDescUsurio());
          caUsuarios.setCauEstado("A");
          this.em.persist(caUsuarios);
        } 
      } 
      CaPolizasPacientes caPolizasPacientes = this.caPolizasPacientesDAO.consultarCaPolizasPacientes(respuestaOrdenMedicaVO.getPacPacNumero());
      if (caPolizasPacientes != null)
        respuestaOrdenMedicaVO.setEcPolizaNumero(caPolizasPacientes.getEcPolizaNumero()); 
      if (paciente == null)
        throw new Exception(); 
      oldOrden.add(respuestaOrdenMedicaVO);
      this.em.getTransaction().commit();
    } catch (Exception e) {
      logger.error("Error consultando orden medica:", e);
      this.em.getTransaction().rollback();
      this.em.close();
      throw new DataException(e.getMessage(), new Date(), e.getMessage().toString());
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
    this.em.close();
    return oldOrden;
  }
  
  public RespuestaGestionContinuidadVO crearGestionContinuidad(GestionContinuidadVO gestionContinuidadVO) throws DataException {
    logger.info("Creando consultarOrdenMedica(FiltrosOrdenMedicaVO)::::");
    RespuestaGestionContinuidadVO respuestaGestionContinuidadVO = new RespuestaGestionContinuidadVO();
    this.em = EntityManagerUtil.getEntityManager();
    try {
      this.em.getTransaction().begin();
      CaPrestacionesOrdMed caPrestacionesOrdMed = (CaPrestacionesOrdMed)this.em.find(CaPrestacionesOrdMed.class, gestionContinuidadVO
          .getPomIdPrestOrdm());
      CaTrazaGestContinuidad caTrazaGestContinuidad = new CaTrazaGestContinuidad();
      caTrazaGestContinuidad.setPomIdPrestOrdm(gestionContinuidadVO.getPomIdPrestOrdm());
      caTrazaGestContinuidad.setGcoIdCodigoEstado(gestionContinuidadVO.getGcoIdCodigoEstado());
      caTrazaGestContinuidad.setGcoIdCodigoMotivo(gestionContinuidadVO.getGcoIdCodigoMotivo());
      caTrazaGestContinuidad.setGcoDirecPaciente(gestionContinuidadVO.isGcoDirecPaciente());
      caTrazaGestContinuidad.setGcoRealizoAgendamiento(gestionContinuidadVO.isGcoRealizoAgendamiento());
      caTrazaGestContinuidad.setGcoObservaciones(gestionContinuidadVO.getGcoObservaciones());
      caTrazaGestContinuidad.setPcaAgeCodigRecep(gestionContinuidadVO.getPcaAgeCodigRecep());
      caTrazaGestContinuidad.setCgFechaProceso(Calendar.getInstance().getTime());
      caPrestacionesOrdMed.setCaTrazaGestContinuidad(caTrazaGestContinuidad);
      caTrazaGestContinuidad.setCaPrestacionesOrdMed(caPrestacionesOrdMed);
      this.em.persist(caTrazaGestContinuidad);
      if (gestionContinuidadVO.isEnviarCorreo().booleanValue()) {
        gestionContinuidadVO
          .setOrmIdOrdmNumero(caPrestacionesOrdMed.getCaOrdenesMedicas().getOrmIdOrdmNumero());
        enviarCorreoGestionContiniudad(gestionContinuidadVO);
      } 
      this.em.getTransaction().commit();
      respuestaGestionContinuidadVO.setSuccess(true);
      respuestaGestionContinuidadVO
        .setRespMensaje(PropertiesManager.CONFIG.getString("GESTION_CONTINUIDAD_SUCCESS"));
    } catch (Exception e) {
      this.em.getTransaction().rollback();
      this.em.close();
      throw new DataException(e.getMessage(), new Date(), e.getMessage());
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
    this.em.close();
    return respuestaGestionContinuidadVO;
  }
  
  private void enviarCorreoGestionContiniudad(final GestionContinuidadVO filtrosGestionContinuidadVO) throws DataException {
    (new Thread(new Runnable() {
          public void run() {
            String html = null;
            StrSubstitutor strSub = null;
            HtmlEmail htmlEmail = new HtmlEmail();
            try {
              CaOrdenesMedicas caOrdenesMedicas = OrdenesMedicasImpl.this.caOrdenesMedicasDAO.consultarCaOrdenesMedicas(filtrosGestionContinuidadVO.getOrmIdOrdmNumero());
              String email = "centralautorizacionesfsfb@gmail.com";
              if (caOrdenesMedicas != null) {
                CaCorreoPaciente caCorreoPaciente = OrdenesMedicasImpl.this.caCorreoPacienteDAO.consultarCaCorreoPaciente(caOrdenesMedicas
                    .getPacPacNumero(), 
                    Long.valueOf(caOrdenesMedicas.getPacPacTipoIdentCodigo()));
                if (caCorreoPaciente != null && caCorreoPaciente.getTpCorreoE() != null && 
                  !caCorreoPaciente.getTpCorreoE().trim().isEmpty())
                  email = caCorreoPaciente.getTpCorreoE().trim(); 
              } 
              Map<String, String> emailDatos = new HashMap<>();
              emailDatos.put("nombre", filtrosGestionContinuidadVO.getNombrePaciente());
              emailDatos.put("prestacion", filtrosGestionContinuidadVO.getPrePreCodigo().concat(" - ")
                  .concat(filtrosGestionContinuidadVO.getPrePreDesc()));
              emailDatos.put("servicio", filtrosGestionContinuidadVO.getSerSerCodigo().concat(" - ")
                  .concat(filtrosGestionContinuidadVO.getSerSerDesc()));
              if (filtrosGestionContinuidadVO.getGcoIdCodigoEstado().longValue() == 1L) {
                html = OrdenesMedicasImpl.fileToString(OrdenesMedicasImpl.class.getResourceAsStream("/mail/email-continuity.html"), "utf-8");
                String[] images = { "CitaProgramada-01.png", "CitaProgramada-02.png", "CitaProgramada-03.png", "facebook-icon.png", "instagram-icon.png", "twitter-icon.png" };
                for (int i = 0; i < images.length; i++)
                  emailDatos.put(images[i], PropertiesManager.CONFIG.getString("MAIL_IMAGES").concat(images[i])); 
                strSub = new StrSubstitutor(emailDatos);
                html = strSub.replace(html);
                (new EnviarEmail()).postMail(new String[] { email }, PropertiesManager.CONFIG
                    .getString("GESTION_CONTINUIDAD_EMAIL_SUBJECT"), html);
              } 
            } catch (DataException|IOException ex) {
              OrdenesMedicasImpl.logger.info("Error enviando correo continuidad", ex);
            } 
          }
        })).start();
  }
  
  public List<RespuestaOrdenMedicaVO> consultarOrdenesMedicas(FiltrosOrdenMedicaVO filtrosOrdenMedicaVO) throws DataException {
    logger.info("Creando consultarOrdenesMedicas(FiltrosOrdenMedicaVO)::::");
    List<RespuestaOrdenMedicaVO> listRespuestaOrdenMedicaVO = new ArrayList<>();
    this.em = EntityManagerUtil.getEntityManager();
    try {
      this.em.getTransaction().begin();
      List<CaOrdenesMedicas> listCaOrdenesMedicas = this.caOrdenesMedicasDAO.consultarCaOrdenesMedicas(filtrosOrdenMedicaVO);
      if (listCaOrdenesMedicas.isEmpty())
        return listRespuestaOrdenMedicaVO; 
      for (CaOrdenesMedicas caOrdenMedica : listCaOrdenesMedicas) {
        Paciente paciente = this.caPacientesDAO.consultarPacientesFSFBHIS(caOrdenMedica.getPacPacTipoIdentCodigo(), caOrdenMedica
            .getPacPacRut());
        RespuestaOrdenMedicaVO respuestaOrdenMedicaVO = new RespuestaOrdenMedicaVO();
        respuestaOrdenMedicaVO.setOrmIdOrdmNumero(caOrdenMedica.getOrmIdOrdmNumero());
        respuestaOrdenMedicaVO.setTipoDocumento(caOrdenMedica.getPacPacTipoIdentCodigo());
        respuestaOrdenMedicaVO.setDocumento(paciente.getNumDocId());
        respuestaOrdenMedicaVO.setNombreCompleto(paciente.getNombreCompleto());
        respuestaOrdenMedicaVO.setPacPacNumero(Long.valueOf(paciente.getPacNum()));
        respuestaOrdenMedicaVO.setTipTipIDav(paciente.getTipTipIDav());
        respuestaOrdenMedicaVO.setEomIdCodigo(Long.valueOf(1L));
        respuestaOrdenMedicaVO.setCgFechaProceso(caOrdenMedica.getCgFechaProceso());
        listRespuestaOrdenMedicaVO.add(respuestaOrdenMedicaVO);
      } 
      this.em.getTransaction().commit();
    } catch (Exception e) {
      logger.error("Error consultando orden medica:", e);
      this.em.getTransaction().rollback();
      this.em.close();
      throw new DataException(e.getMessage(), new Date(), e.getMessage());
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
    this.em.close();
    return listRespuestaOrdenMedicaVO;
  }
  
  private List<RespuestaOrdenMedicaVO> consultarOrdenExistente(FiltrosOrdenMedicaVO filtrosOrdenMedicaVO) throws DataException {
    List<RespuestaOrdenMedicaVO> listRespuestaOrdenMedicaVO = new ArrayList<>();
    List<CaOrdenesMedicas> caOrdenesMedicasOld = this.caOrdenesMedicasDAO.consultarCaOrdenMedica(filtrosOrdenMedicaVO
        .getPacPacTipoIdentCodigo(), filtrosOrdenMedicaVO.getPacPacRut());
    Paciente paciente = null;
    if (caOrdenesMedicasOld != null)
      for (CaOrdenesMedicas caOrdenesMedicas : caOrdenesMedicasOld) {
        List<CaTrazaOrdenMedicas> traza = this.caOrdenesMedicasDAO.consultarCaTrazaOrdenesMedicas(caOrdenesMedicas.getOrmIdOrdmNumero(), new Long[0]);
        if (!traza.isEmpty() && ((CaTrazaOrdenMedicas)traza.get(0)).getEomIdCodigo().longValue() != 4L && ((CaTrazaOrdenMedicas)traza.get(0)).getEomIdCodigo().longValue() != 6L) {
          paciente = this.caPacientesDAO.consultarPacientesFSFBHIS(filtrosOrdenMedicaVO.getPacPacTipoIdentCodigo(), filtrosOrdenMedicaVO
              .getPacPacRut());
          RespuestaOrdenMedicaVO respuestaOrdenMedicaVO = new RespuestaOrdenMedicaVO();
          respuestaOrdenMedicaVO.setOrmIdOrdmNumero(caOrdenesMedicas.getOrmIdOrdmNumero());
          respuestaOrdenMedicaVO.setTipoDocumento(caOrdenesMedicas.getPacPacTipoIdentCodigo());
          respuestaOrdenMedicaVO.setDocumento(paciente.getNumDocId());
          respuestaOrdenMedicaVO.setNombreCompleto(paciente.getNombreCompleto());
          respuestaOrdenMedicaVO.setPacPacNumero(Long.valueOf(paciente.getPacNum()));
          respuestaOrdenMedicaVO.setTipTipIDav(paciente.getTipTipIDav());
          respuestaOrdenMedicaVO.setEomIdCodigo(((CaTrazaOrdenMedicas)traza.get(0)).getEomIdCodigo());
          respuestaOrdenMedicaVO.setFilename(caOrdenesMedicas.getOrmFilename());
          if (caOrdenesMedicas.getFechaRegistroFile() != null) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
            respuestaOrdenMedicaVO.setFechaRegistroFile(simpleDateFormat
                .format(caOrdenesMedicas.getFechaRegistroFile()));
          } 
          CaPolizasPacientes caPolizasPacientes = this.caPolizasPacientesDAO.consultarCaPolizasPacientes(respuestaOrdenMedicaVO.getPacPacNumero());
          if (caPolizasPacientes != null)
            respuestaOrdenMedicaVO.setEcPolizaNumero(caPolizasPacientes.getEcPolizaNumero()); 
          listRespuestaOrdenMedicaVO.add(respuestaOrdenMedicaVO);
        } 
      }  
    return listRespuestaOrdenMedicaVO;
  }
  
  public OrdenMedicaVO detalleOrdenMedica(Long ormIdOrdmNumero) throws DataException {
    logger.info("Iniciando detalleOrdenMedica(ormIdOrdmNumero)::::");
    OrdenMedicaVO ordenMedicaVO = new OrdenMedicaVO();
    this.em = EntityManagerUtil.getEntityManager();
    try {
      this.em.getTransaction().begin();
      CaOrdenesMedicas caOrdenesMedicas = (CaOrdenesMedicas)this.em.find(CaOrdenesMedicas.class, ormIdOrdmNumero);
      if (caOrdenesMedicas != null) {
        ordenMedicaVO = OrdenMedicaMapper.INSTANCE.entityToVO(caOrdenesMedicas);
        Paciente paciente = this.caPacientesDAO.consultarPacientesFSFBHIS(caOrdenesMedicas
            .getPacPacTipoIdentCodigo(), caOrdenesMedicas.getPacPacRut());
        ordenMedicaVO.setNombreCompletoPaciente(paciente.getNombreCompleto());
        ordenMedicaVO.setTipTipIDav(paciente.getTipTipIDav());
        List<CaTrazaOrdenMedicas> traza = this.caOrdenesMedicasDAO.consultarCaTrazaOrdenesMedicas(caOrdenesMedicas.getOrmIdOrdmNumero(), new Long[0]);
        ordenMedicaVO.setEomIdCodigo(((CaTrazaOrdenMedicas)traza.get(0)).getEomIdCodigo());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        ordenMedicaVO.setFechaRegistroFile(simpleDateFormat.format(Calendar.getInstance().getTime()));
        CaPolizasPacientes caPolizasPacientes = this.caPolizasPacientesDAO.consultarCaPolizasPacientes(caOrdenesMedicas.getPacPacNumero());
        if (caPolizasPacientes != null) {
          if (ordenMedicaVO.getCaDetalleOrdenesMedicas() == null)
            ordenMedicaVO.setCaDetalleOrdenesMedicas(new DetalleOrdenMedicaVO()); 
          ordenMedicaVO.getCaDetalleOrdenesMedicas()
            .setEcPolizaNumero(caPolizasPacientes.getEcPolizaNumero());
        } 
      } 
    } catch (Exception e) {
      logger.error("Error consultando orden", e);
      this.em.getTransaction().rollback();
      this.em.close();
      throw new DataException(0);
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
    this.em.close();
    return ordenMedicaVO;
  }
  
  public void administrarOrdenMedica(AdmOrdenesMedicasVO admOrdenesMedicasVO) throws DataException {
    logger.info("Iniciando administrarOrdenMedica(AdmOrdenesMedicasVO)::::");
    this.em = EntityManagerUtil.getEntityManager();
    try {
      this.em.getTransaction().begin();
      LocalDate localDate = null;
      DateTimeFormatter formatter = null;
      CaOrdenesMedicas caOrdenesMedicas = (CaOrdenesMedicas)this.em.find(CaOrdenesMedicas.class, admOrdenesMedicasVO
          .getCaDetalleOrdenesMedicas().getOrmIdOrdmNumero());
      if (!admOrdenesMedicasVO.isEliminar()) {
        if (caOrdenesMedicas != null) {
          admOrdenesMedicasVO.getCaDetalleOrdenesMedicas()
            .setDomShaFechaProceso(Calendar.getInstance().getTime());
          formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
          localDate = LocalDate.parse(admOrdenesMedicasVO.getDorFechaOrdenmString(), formatter);
          admOrdenesMedicasVO.getCaDetalleOrdenesMedicas()
            .setDorFechaOrdenm(Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));
          admOrdenesMedicasVO.getCaDetalleOrdenesMedicas().setCaOrdenesMedicas(caOrdenesMedicas);
          caOrdenesMedicas.setCaDetalleOrdenesMedicas(admOrdenesMedicasVO.getCaDetalleOrdenesMedicas());
          this.em.persist(caOrdenesMedicas.getCaDetalleOrdenesMedicas());
        } else {
          throw new DataException(0);
        } 
        CaPolizasPacientes caPolizasPacientes = this.caPolizasPacientesDAO.consultarCaPolizasPacientes(admOrdenesMedicasVO
            .getCaDetalleOrdenesMedicas().getPacPacNumero());
        if (caPolizasPacientes == null) {
          caPolizasPacientes = new CaPolizasPacientes();
          caPolizasPacientes.setCgFechaProceso(Calendar.getInstance().getTime());
          caPolizasPacientes
            .setPacPacNumero(admOrdenesMedicasVO.getCaDetalleOrdenesMedicas().getPacPacNumero());
          caPolizasPacientes.setPcaAgeCodigRecep(admOrdenesMedicasVO
              .getCaDetalleOrdenesMedicas().getPcaAgeCodigRecep());
          caPolizasPacientes.setEcPolizaNumero(admOrdenesMedicasVO.getEcPolizaNumero());
          caPolizasPacientes.setCgFechaProceso(Calendar.getInstance().getTime());
          caPolizasPacientes.setAccIdNumero(Long.valueOf(123L));
          this.em.persist(caPolizasPacientes);
        } else if (!caPolizasPacientes.getEcPolizaNumero().equals(admOrdenesMedicasVO.getEcPolizaNumero())) {
          caPolizasPacientes.setEcPolizaNumero(admOrdenesMedicasVO.getEcPolizaNumero());
          caPolizasPacientes.setCgFechaProceso(Calendar.getInstance().getTime());
          caPolizasPacientes.setPcaAgeCodigRecep(admOrdenesMedicasVO
              .getCaDetalleOrdenesMedicas().getPcaAgeCodigRecep());
          this.em.merge(caPolizasPacientes);
        } 
        CaTrazaPolizasPacientes caTrazaPolizasPacientes = new CaTrazaPolizasPacientes();
        caTrazaPolizasPacientes.setAccIdNumero(caPolizasPacientes.getAccIdNumero());
        caTrazaPolizasPacientes.setCgFechaProceso(Calendar.getInstance().getTime());
        caTrazaPolizasPacientes.setEcPolizaNumero(caPolizasPacientes.getEcPolizaNumero());
        caTrazaPolizasPacientes.setPacPacNumero(caPolizasPacientes.getPacPacNumero());
        caTrazaPolizasPacientes.setPcaAgeCodigRecep(caPolizasPacientes.getPcaAgeCodigRecep());
        this.em.persist(caTrazaPolizasPacientes);
        CaTrazaOrdenMedicas caTrazaOrdenMedicas = new CaTrazaOrdenMedicas();
        caTrazaOrdenMedicas.setCgFechaProceso(Calendar.getInstance().getTime());
        caTrazaOrdenMedicas.setEomIdCodigo(Long.valueOf(2L));
        caTrazaOrdenMedicas.setCaOrdenesMedicas(caOrdenesMedicas);
        caTrazaOrdenMedicas.setPcaAgeCodigRecep(admOrdenesMedicasVO.getCodUsrCita());
        this.em.persist(caTrazaOrdenMedicas);
      } else {
        boolean ordenMedica = this.caOrdenesMedicasDAO.validarCaTrazaOrdenesMedicas(caOrdenesMedicas.getOrmIdOrdmNumero());
        if (!ordenMedica) {
          CaTrazaOrdenMedicas caTrazaOrdenMedicasfinal = new CaTrazaOrdenMedicas();
          caTrazaOrdenMedicasfinal.setCgFechaProceso(Calendar.getInstance().getTime());
          caTrazaOrdenMedicasfinal.setEomIdCodigo(Long.valueOf(6L));
          caTrazaOrdenMedicasfinal.setCaOrdenesMedicas(caOrdenesMedicas);
          caTrazaOrdenMedicasfinal.setPcaAgeCodigRecep(admOrdenesMedicasVO.getCodUsrCita());
          this.em.persist(caTrazaOrdenMedicasfinal);
          CaTrazaEliminarOm caTrazaEliminarOm = new CaTrazaEliminarOm();
          caTrazaEliminarOm.setOrmIdOrdmNumero(caOrdenesMedicas.getOrmIdOrdmNumero());
          caTrazaEliminarOm.setCgFechaProceso(Calendar.getInstance().getTime());
          caTrazaEliminarOm.setPcaAgeCodigRecep(admOrdenesMedicasVO
              .getCaDetalleOrdenesMedicas().getPcaAgeCodigRecep());
          caTrazaEliminarOm.setTomIdTrazaOrdmNum(caTrazaOrdenMedicasfinal.getTomIdTrazaOrdmNum());
          caTrazaEliminarOm.setMomId(admOrdenesMedicasVO.getMomId());
          this.em.persist(caTrazaEliminarOm);
        } 
      } 
      this.em.getTransaction().commit();
    } catch (Exception e) {
      this.em.getTransaction().rollback();
      this.em.close();
      throw new DataException(0);
    } finally {
      if (this.em.isOpen()) 
        this.em.close(); 
    } 
    this.em.close();
  }
  
  public List<CaPrestacionesOrdMed> consultaPrestaciones(CaPrestacionesOrdMed consultaPrestacionesVO) throws DataException {
    logger.info("Iniciando consultaPrestacionesVO(consultaPrestacionesVO)::::");
    List<CaPrestacionesOrdMed> consultaPrestaciones = new ArrayList<>();
    this.em = EntityManagerUtil.getEntityManager();
    try {
      this.em.getTransaction().begin();
      consultaPrestaciones = this.caOrdenesMedicasDAO.consultarPrestaciones(consultaPrestacionesVO);
      this.em.getTransaction().commit();
    } catch (Exception e) {
      this.em.getTransaction().rollback();
      this.em.close();
      throw new DataException(0);
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
    this.em.close();
    return consultaPrestaciones;
  }
  
  public void saveFile(Long ormIdOrdmNumero, String filename) throws DataException {
    logger.info("Guardando file orden(filename)::::");
    this.em = EntityManagerUtil.getEntityManager();
    try {
      this.em.getTransaction().begin();
      CaOrdenesMedicas caOrdenesMedicas = (CaOrdenesMedicas)this.em.find(CaOrdenesMedicas.class, ormIdOrdmNumero);
      caOrdenesMedicas.setOrmFilename(filename);
      caOrdenesMedicas.setFechaRegistroFile(Calendar.getInstance().getTime());
      this.em.merge(caOrdenesMedicas);
      this.em.getTransaction().commit();
    } catch (Exception e) {
      this.em.getTransaction().rollback();
      this.em.close();
      throw new DataException(0);
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
    this.em.close();
  }
  
  public List<PrestacionesOrdMedVO> crearPrestaciones(CrearPrestacionesOrdMedVO crearPrestacionesOrdMedVO) throws DataException {
    logger.info("Iniciando crearPrestaciones(crearPrestaciones)::::");
    List<PrestacionesOrdMedVO> prestacionesOrdMedVOList = new ArrayList<>();
    this.em = EntityManagerUtil.getEntityManager();
    try {
      this.em.getTransaction().begin();
      CaOrdenesMedicas caOrdenesMedicas = (CaOrdenesMedicas)this.em.find(CaOrdenesMedicas.class, crearPrestacionesOrdMedVO
          .getOrmIdOrdmNumero());
      caOrdenesMedicas.setCaPrestacionesOrdMed(crearPrestacionesOrdMedVO.getCaPrestacionesOrdMed());
      for (CaPrestacionesOrdMed caPrestacionesOrdMed : crearPrestacionesOrdMedVO.getCaPrestacionesOrdMed()) {
        caPrestacionesOrdMed.setCgFechaProceso(Calendar.getInstance().getTime());
        caPrestacionesOrdMed.setPcaAgeCodigRecep(crearPrestacionesOrdMedVO.getPcaAgeCodigRecep());
        caPrestacionesOrdMed.setCaOrdenesMedicas(caOrdenesMedicas);
        this.em.persist(caPrestacionesOrdMed);
      } 
      CaTrazaOrdenMedicas caTrazaOrdenMedicas = new CaTrazaOrdenMedicas();
      caTrazaOrdenMedicas.setEomIdCodigo(Long.valueOf(3L));
      caTrazaOrdenMedicas.setCaOrdenesMedicas(caOrdenesMedicas);
      caTrazaOrdenMedicas.setPcaAgeCodigRecep(crearPrestacionesOrdMedVO.getPcaAgeCodigRecep());
      caTrazaOrdenMedicas.setCgFechaProceso(Calendar.getInstance().getTime());
      this.em.persist(caTrazaOrdenMedicas);
      this.em.getTransaction().commit();
      prestacionesOrdMedVOList = PrestacionOrdenMedicaMapper.INSTANCE.entityToVO(crearPrestacionesOrdMedVO.getCaPrestacionesOrdMed());
    } catch (Exception e) {
      logger.error("Error creando prestaciones: ", e);
      this.em.getTransaction().rollback();
      this.em.close();
      throw new DataException(0);
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
    this.em.close();
    return prestacionesOrdMedVOList;
  }
  
  public boolean registrarAutorizacion(GestionAutorizacionVO gestionAutorizacionVO) throws DataException {
    logger.info("Iniciando registrarAutorizacion(gestionAutorizacionVO)::::");
    this.em = EntityManagerUtil.getEntityManager();
    try {
      this.em.getTransaction().begin();
      CaPrestacionesOrdMed caPrestacionesOrdMed = (CaPrestacionesOrdMed)this.em.find(CaPrestacionesOrdMed.class, gestionAutorizacionVO
          .getPomIdPrestOrdm());
      boolean tieneAutorizacion = true;
      if (caPrestacionesOrdMed.getCaGestionAutorizacion() == null)
        tieneAutorizacion = false; 
      caPrestacionesOrdMed
        .setCaGestionAutorizacion(GestionAutorizacionMapper.INSTANCE.voToEntity(gestionAutorizacionVO));
      caPrestacionesOrdMed.getCaGestionAutorizacion().setCaPrestacionesOrdMed(caPrestacionesOrdMed);
      caPrestacionesOrdMed.getCaGestionAutorizacion().setCgFechaProceso(Calendar.getInstance().getTime());
      caPrestacionesOrdMed.getCaGestionAutorizacion()
        .setPomIdPrestOrdm(gestionAutorizacionVO.getPomIdPrestOrdm());
      caPrestacionesOrdMed.getCaGestionAutorizacion().setPacPacNumero(gestionAutorizacionVO.getPacPacNumero());
      caPrestacionesOrdMed.getCaGestionAutorizacion()
        .setOgaDescripcion(gestionAutorizacionVO.getGauObservaciones());
      if (tieneAutorizacion) {
        this.em.merge(caPrestacionesOrdMed);
      } else {
        this.em.persist(caPrestacionesOrdMed);
      } 
      CaTrazaGestionAutorizacion caTrazaGestionAutorizacion = new CaTrazaGestionAutorizacion();
      caTrazaGestionAutorizacion.setEgaIdcodigo(Long.valueOf(1L));
      caTrazaGestionAutorizacion.setPcaAgeCodigRecep(gestionAutorizacionVO.getPcaAgeCodigoRecep());
      caTrazaGestionAutorizacion.setCgFechaProceso(Calendar.getInstance().getTime());
      this.em.persist(caTrazaGestionAutorizacion);
      CaPolizasPacientes caPolizasPacientes = this.caPolizasPacientesDAO.consultarCaPolizasPacientes(gestionAutorizacionVO.getPacPacNumero());
      if (caPolizasPacientes == null) {
        caPolizasPacientes = new CaPolizasPacientes();
        caPolizasPacientes.setAccIdNumero(Long.valueOf(2L));
        caPolizasPacientes.setEcPolizaNumero(gestionAutorizacionVO.getNumeroPoliza());
        caPolizasPacientes.setPcaAgeCodigRecep(gestionAutorizacionVO.getPcaAgeCodigoRecep());
        caPolizasPacientes.setPacPacNumero(gestionAutorizacionVO.getPacPacNumero());
        caPolizasPacientes.setCgFechaProceso(new Date());
        this.em.persist(caPolizasPacientes);
      } else {
        caPolizasPacientes.setEcPolizaNumero(gestionAutorizacionVO.getNumeroPoliza());
        caPolizasPacientes.setPcaAgeCodigRecep(gestionAutorizacionVO.getPcaAgeCodigoRecep());
        this.em.merge(caPolizasPacientes);
      } 
      enviarCorreoEstadoAutorizaicion(gestionAutorizacionVO);
      this.em.getTransaction().commit();
    } catch (Exception e) {
      logger.error("Error creando prestaciones: ", e);
      this.em.getTransaction().rollback();
      this.em.close();
      throw new DataException(0);
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
    this.em.close();
    return true;
  }
  
  public boolean finalizarOrden(FinalizarOrdenMedicaVO finalizarOrdenMedicaVO) throws DataException {
    logger.info("Iniciando crearPrestaciones(crearPrestaciones)::::");
    this.em = EntityManagerUtil.getEntityManager();
    try {
      this.em.getTransaction().begin();
      CaOrdenesMedicas caOrdenesMedicas = (CaOrdenesMedicas)this.em.find(CaOrdenesMedicas.class, finalizarOrdenMedicaVO
          .getOrmIdOrdmNumero());
      CaTrazaOrdenMedicas caTrazaOrdenMedicas = new CaTrazaOrdenMedicas();
      caTrazaOrdenMedicas.setEomIdCodigo(Long.valueOf(4L));
      caTrazaOrdenMedicas.setCaOrdenesMedicas(caOrdenesMedicas);
      caTrazaOrdenMedicas.setPcaAgeCodigRecep(finalizarOrdenMedicaVO.getPcaAgeCodigRecep());
      caTrazaOrdenMedicas.setCgFechaProceso(Calendar.getInstance().getTime());
      this.em.persist(caTrazaOrdenMedicas);
      this.em.getTransaction().commit();
    } catch (Exception e) {
      logger.error("Error creando prestaciones: ", e);
      this.em.getTransaction().rollback();
      this.em.close();
      throw new DataException(0);
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
    this.em.close();
    return true;
  }
  
  public boolean eliminarOrden(FinalizarOrdenMedicaVO finalizarOrdenMedicaVO) throws DataException {
    if (null == finalizarOrdenMedicaVO.getRazon() || null == finalizarOrdenMedicaVO.getOrmIdOrdmNumero() || null == finalizarOrdenMedicaVO.getPcaAgeCodigRecep())
      throw new DataException(0); 
    logger.info("Iniciando crearPrestaciones(crearPrestaciones)::::");
    this.em = EntityManagerUtil.getEntityManager();
    try {
      this.em.getTransaction().begin();
      CaOrdenesMedicas caOrdenesMedicas = (CaOrdenesMedicas)this.em.find(CaOrdenesMedicas.class, finalizarOrdenMedicaVO
          .getOrmIdOrdmNumero());
      CaTrazaOrdenMedicas caTrazaOrdenMedicas = new CaTrazaOrdenMedicas();
      caTrazaOrdenMedicas.setEomIdCodigo(Long.valueOf(6L));
      caTrazaOrdenMedicas.setCaOrdenesMedicas(caOrdenesMedicas);
      caTrazaOrdenMedicas.setPcaAgeCodigRecep(finalizarOrdenMedicaVO.getPcaAgeCodigRecep());
      caTrazaOrdenMedicas.setCgFechaProceso(Calendar.getInstance().getTime());
      this.em.persist(caTrazaOrdenMedicas);
      this.em.flush();
      CaTrazaEliminarOm caTrazaEliminarOm = new CaTrazaEliminarOm();
      caTrazaEliminarOm.setTomIdTrazaOrdmNum(caTrazaOrdenMedicas.getTomIdTrazaOrdmNum());
      caTrazaEliminarOm.setOrmIdOrdmNumero(caOrdenesMedicas.getOrmIdOrdmNumero());
      caTrazaEliminarOm.setMomId(finalizarOrdenMedicaVO.getRazon());
      caTrazaEliminarOm.setPcaAgeCodigRecep(finalizarOrdenMedicaVO.getPcaAgeCodigRecep());
      caTrazaEliminarOm.setCgFechaProceso(Calendar.getInstance().getTime());
      this.em.persist(caTrazaEliminarOm);
      this.em.getTransaction().commit();
    } catch (Exception e) {
      logger.error("Error creando prestaciones: ", e);
      this.em.getTransaction().rollback();
      this.em.close();
      throw new DataException(0, e);
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
    this.em.close();
    return true;
  }
  
  public List<ConsultaTrazabilidadCitaVO> consultarTrazabilidadOrden(Long ormIdOrdmNumero) {
    logger.info("Creando consultarTrazabilidadOrden::::");
    return this.caOrdenesMedicasDAO.consultarTrazabilidadOrden(ormIdOrdmNumero);
  }
  
  public PrestacionesOrdMedVO consultaPrestacionPorPomIdPrestOrdm(Long pomIdPrestOrdm) {
    PrestacionesOrdMedVO prestacionesOrdMedVO = null;
    this.em = EntityManagerUtil.getEntityManager();
    try {
      this.em.getTransaction().begin();
      CaPrestacionesOrdMed caPrestacionesOrdMed = (CaPrestacionesOrdMed)this.em.find(CaPrestacionesOrdMed.class, pomIdPrestOrdm);
      prestacionesOrdMedVO = PrestacionOrdenMedicaMapper.INSTANCE.entityToVO(caPrestacionesOrdMed);
    } catch (Exception e) {
      if (this.em.isOpen())
        this.em.close(); 
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
    this.em.close();
    return prestacionesOrdMedVO;
  }
  
  public List<CaMotivosEliminarOm> motivoEliminacion() throws DataException {
    this.em = EntityManagerUtil.getEntityManager();
    try {
      this.em.getTransaction().begin();
      return this.em.createQuery("select om from CaMotivosEliminarOm om").getResultList();
    } catch (Exception e) {
      throw new DataException("Error al obtener la lista de motivos de eliminacion", Calendar.getInstance().getTime(), e.getMessage());
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
  }
  
  public RespuestaOrdenMedicaVO registrarOrdenMedica(FiltrosOrdenMedicaVO filtrosOrdenMedicaVO) throws DataException {
    RespuestaOrdenMedicaVO respuestaOrdenMedicaVO = new RespuestaOrdenMedicaVO();
    this.em = EntityManagerUtil.getEntityManager();
    try {
      this.em.getTransaction().begin();
      Paciente paciente = null;
      if (filtrosOrdenMedicaVO.getPacPacTipoIdentCodigo()
        .equals(EnumTipoIdentDocumento.ADULTO_SIN_IDENTIFICACION.getId()) || filtrosOrdenMedicaVO
        .getPacPacTipoIdentCodigo()
        .equals(EnumTipoIdentDocumento.MENOR_SIN_IDENTIFICACION.getId())) {
        Paciente filPaciente = new Paciente();
        filPaciente.setNombres(filtrosOrdenMedicaVO.getNombres());
        filPaciente.setPrimerApellido(filtrosOrdenMedicaVO.getPrimerApellido());
        filPaciente.setSegundoApellido(filtrosOrdenMedicaVO.getSegundoApellido());
        filPaciente.setPacPacTipoIdentCodigo(filtrosOrdenMedicaVO.getPacPacTipoIdentCodigo());
        filPaciente.setTipoDocId(filtrosOrdenMedicaVO.getPacPacRut());
        paciente = this.caPacientesDAO.consultarPacientesFSFBHIS(filPaciente);
        if (paciente != null) {
          CaPacientes caPacientes = this.caPacientesDAO.consultarCaPacientes(paciente.getPacPacTipoIdentCodigo(), paciente
              .getTipoDocId());
          if (caPacientes == null) {
            CaPacientes caPacientesReg = new CaPacientes();
            caPacientesReg.setPacPacNumero(Long.valueOf(paciente.getPacNum()));
            caPacientesReg.setPacPacRut(paciente.getNumDocId());
            caPacientesReg.setPacPacTipoIdentCodigo(paciente.getPacPacTipoIdentCodigo());
            caPacientesReg.setTipIdav(paciente.getTipTipIDav());
            this.em.persist(caPacientesReg);
          } 
        } else {
          respuestaOrdenMedicaVO
            .setMensajeError(PropertiesManager.CONFIG.getString("ORDEN_MEDICA_NOT_FOUND_PACIENTE"));
          return respuestaOrdenMedicaVO;
        } 
      } else {
        paciente = this.caPacientesDAO.consultarPacientesFSFBHIS(filtrosOrdenMedicaVO.getPacPacTipoIdentCodigo(), filtrosOrdenMedicaVO
            .getPacPacRut());
        if (paciente != null) {
          CaPacientes caPacientes = this.caPacientesDAO.consultarCaPacientes(filtrosOrdenMedicaVO
              .getPacPacTipoIdentCodigo(), filtrosOrdenMedicaVO.getPacPacRut());
          if (caPacientes == null) {
            CaPacientes caPacientesReg = new CaPacientes();
            caPacientesReg.setPacPacNumero(Long.valueOf(paciente.getPacNum()));
            caPacientesReg.setPacPacRut(paciente.getNumDocId());
            caPacientesReg.setPacPacTipoIdentCodigo(paciente.getPacPacTipoIdentCodigo());
            caPacientesReg.setTipIdav(paciente.getTipTipIDav());
            this.em.persist(caPacientesReg);
          } 
        } else {
          respuestaOrdenMedicaVO
            .setMensajeError(PropertiesManager.CONFIG.getString("ORDEN_MEDICA_NOT_FOUND_PACIENTE"));
          return respuestaOrdenMedicaVO;
        } 
        respuestaOrdenMedicaVO.setTipoDocumento(paciente.getPacPacTipoIdentCodigo());
        respuestaOrdenMedicaVO.setDocumento(paciente.getNumDocId());
        respuestaOrdenMedicaVO.setNombreCompleto(paciente.getNombreCompleto());
        respuestaOrdenMedicaVO.setTipTipIDav(paciente.getTipTipIDav());
        respuestaOrdenMedicaVO.setPacPacNumero(Long.valueOf(paciente.getPacNum()));
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        respuestaOrdenMedicaVO.setFechaRegistroFile(simpleDateFormat.format(Calendar.getInstance().getTime()));
        CaOrdenesMedicas caOrdenesMedicas = new CaOrdenesMedicas();
        caOrdenesMedicas.setCgFechaProceso(Calendar.getInstance().getTime());
        caOrdenesMedicas.setPacPacNumero(Long.valueOf(paciente.getPacNum()));
        caOrdenesMedicas.setPacPacRut(filtrosOrdenMedicaVO.getPacPacRut());
        caOrdenesMedicas.setPacPacTipoIdentCodigo(filtrosOrdenMedicaVO.getPacPacTipoIdentCodigo());
        caOrdenesMedicas.setPcaAgeCodigoRecep(filtrosOrdenMedicaVO.getCodUsrCita());
        this.em.persist(caOrdenesMedicas);
        respuestaOrdenMedicaVO.setOrmIdOrdmNumero(caOrdenesMedicas.getOrmIdOrdmNumero());
        CaUsuarios caUsuarios = this.caUsuariosDAO.consultarCaUsuarios(filtrosOrdenMedicaVO.getCodUsrCita());
        if (caUsuarios == null) {
          caUsuarios = new CaUsuarios();
          caUsuarios.setPcaAgeCodigRecep(filtrosOrdenMedicaVO.getCodUsrCita());
          caUsuarios.setCauDescUsuarios(filtrosOrdenMedicaVO.getCauDescUsurio());
          caUsuarios.setCauEstado("A");
          this.em.persist(caUsuarios);
        } 
        CaTrazaOrdenMedicas caTrazaOrdenMedicas = new CaTrazaOrdenMedicas();
        caTrazaOrdenMedicas.setCgFechaProceso(Calendar.getInstance().getTime());
        caTrazaOrdenMedicas.setEomIdCodigo(Long.valueOf(1L));
        caTrazaOrdenMedicas.setCaOrdenesMedicas(caOrdenesMedicas);
        caTrazaOrdenMedicas.setPcaAgeCodigRecep(filtrosOrdenMedicaVO.getCodUsrCita());
        this.em.persist(caTrazaOrdenMedicas);
        respuestaOrdenMedicaVO.setEomIdCodigo(caTrazaOrdenMedicas.getEomIdCodigo());
      } 
      CaPolizasPacientes caPolizasPacientes = this.caPolizasPacientesDAO.consultarCaPolizasPacientes(respuestaOrdenMedicaVO.getPacPacNumero());
      if (caPolizasPacientes != null)
        respuestaOrdenMedicaVO.setEcPolizaNumero(caPolizasPacientes.getEcPolizaNumero()); 
      this.em.getTransaction().commit();
    } catch (Exception e) {
      logger.error("Error consultando orden medica:", e);
      this.em.getTransaction().rollback();
      this.em.close();
      throw new DataException(e.getMessage(), new Date(), e.getMessage().toString());
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
    this.em.close();
    return respuestaOrdenMedicaVO;
  }
  
  private static String fileToString(InputStream input, String encoding) throws IOException {
    StringWriter sw = new StringWriter();
    InputStreamReader in = new InputStreamReader(input, encoding);
    char[] buffer = new char[2048];
    int n = 0;
    while (-1 != (n = in.read(buffer)))
      sw.write(buffer, 0, n); 
    return sw.toString();
  }
  
  public void enviarCorreoEstadoAutorizaicion(GestionAutorizacionVO gestionAutorizacionVO) {
    (new Thread(() -> {
          String html = null;
          StrSubstitutor strSub = null;
          String asunto = "";
          String estado = "";
          try {
            String email = "centralautorizacionesfsfb@gmail.com";
            CaCorreoPaciente caCorreoPaciente = this.caCorreoPacienteDAO.consultarCaCorreoPaciente(gestionAutorizacionVO.getPacPacNumero());
            if (caCorreoPaciente != null && caCorreoPaciente.getTpCorreoE() != null && !caCorreoPaciente.getTpCorreoE().trim().isEmpty())
              email = caCorreoPaciente.getTpCorreoE().trim(); 
            if (gestionAutorizacionVO.getGauAutorizaServ().equals("1")) {
              asunto = String.format(PropertiesManager.CONFIG.getString("AUTORIZACION_ASEG_EMAIL_SUBJECT"), new Object[] { "AUTORIZADA" });
              estado = "AUTORIZADA POR TU ASEGURADOR.";
            } else if (gestionAutorizacionVO.getGauAutorizaServ().equals("2")) {
              asunto = String.format(PropertiesManager.CONFIG.getString("AUTORIZACION_ASEG_EMAIL_SUBJECT"), new Object[] { "NO AUTORIZADA" });
              estado = "NO AUTORIZADA POR TU ASEGURADOR.";
            } 
            Map<String, String> emailDatos = new HashMap<>();
            emailDatos.put("nombre", gestionAutorizacionVO.getNombrePaciente().split(" ")[0]);
            emailDatos.put("estado", estado);
            emailDatos.put("entidad", gestionAutorizacionVO.getCentroAtencion());
            html = fileToString(OrdenesMedicasImpl.class.getResourceAsStream("/mail/email-auth.html"), "utf-8");
            String[] images = { "CitaProgramada-01.png", "CitaProgramada-02.png", "CitaProgramada-03.png", "facebook-icon.png", "instagram-icon.png", "twitter-icon.png" };
            for (int i = 0; i < images.length; i++)
              emailDatos.put(images[i], PropertiesManager.CONFIG.getString("MAIL_IMAGES").concat(images[i])); 
            strSub = new StrSubstitutor(emailDatos);
            html = strSub.replace(html);
            (new EnviarEmail()).postMail(new String[] { email }, asunto, html);
          } catch (DataException|IOException ex) {
            logger.info("Error enviando correo continuidad", ex);
          } 
        })).start();
  }
}

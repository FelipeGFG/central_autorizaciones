package co.fsfb.ws.rest.cliente;

import co.fsfb.ws.rest.dao.CaDocumentosSharePointDAO;
import co.fsfb.ws.rest.dto.SharePointDTO;
import co.fsfb.ws.rest.entidades.CaDocumentosSharePoint;
import co.fsfb.ws.rest.exception.CredencialesFallidasException;
import co.fsfb.ws.rest.util.EntityManagerUtil;
import co.fsfb.ws.rest.vo.ConsultaSharePointVO;
import co.fsfb.ws.rest.vo.Documento;
import co.fsfb.ws.rest.vo.DocumentoVO;
import co.fsfb.ws.rest.vo.FiltrosConsultaSharePointVO;
import co.fsfb.ws.rest.vo.RegistroSharePointVO;
import java.util.Calendar;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class SharePointImpl {
  private static Logger logger = LoggerFactory.getLogger(SharePointImpl.class);
  
  private EntityManager em = null;
  
  private SharePointDTO sharePointDTO = new SharePointDTO();
  
  CaDocumentosSharePointDAO caDocumentosSharePointDAO = new CaDocumentosSharePointDAO();
  
  public void administrarDocumento(DocumentoVO documentoVO) throws CredencialesFallidasException {
    logger.info("Iniciando administrarDocumento(DocumentoVO)::::");
    this.em = EntityManagerUtil.getEntityManager();
    try {
      this.em.getTransaction().begin();
      RegistroSharePointVO registroSharePointVO = this.sharePointDTO.uploadDocument((Documento)documentoVO);
      registroSharePointVO.isRespuestaVer();
      CaDocumentosSharePoint caDocumentosSharePoint = new CaDocumentosSharePoint();
      caDocumentosSharePoint.setAccIdNumero(documentoVO.getAccIdNumero());
      caDocumentosSharePoint.setDomShaFechaProceso(Calendar.getInstance().getTime());
      caDocumentosSharePoint.setDomShaIdentificador(documentoVO.getDomShaIdentificador());
      caDocumentosSharePoint.setDomShaUrl(registroSharePointVO.getRutaUrlArchivo());
      caDocumentosSharePoint.setEomIdCodigo(documentoVO.getEomIdCodigo());
      caDocumentosSharePoint.setPacPacNumero(documentoVO.getPacPacNumero());
      caDocumentosSharePoint.setPacPacRut(documentoVO.getDocumento());
      caDocumentosSharePoint.setPacPacTipoIdentCodigo(documentoVO.getPacPacTipoIdentCodigo());
      caDocumentosSharePoint.setPcaAgeCodigRecep(documentoVO.getPcaAgeCodigRecep());
      caDocumentosSharePoint.setTdsSigla(documentoVO.getTdsSigla());
      caDocumentosSharePoint.setTipIdDav(documentoVO.getTipTipIDav());
      this.em.persist(caDocumentosSharePoint);
    } catch (Exception e) {
      this.em.getTransaction().rollback();
      this.em.close();
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
    this.em.getTransaction().commit();
    this.em.close();
  }
  
  public ConsultaSharePointVO consultarDocumento(FiltrosConsultaSharePointVO filtrosConsultaSharepointVO) {
    logger.info("Iniciando consultarDocumento(FiltrosConsultaSharePointVO)::::");
    ConsultaSharePointVO consultaSharePointVO = this.caDocumentosSharePointDAO.consultarCaDocumentosSharePoint(filtrosConsultaSharepointVO);
    return consultaSharePointVO;
  }
}

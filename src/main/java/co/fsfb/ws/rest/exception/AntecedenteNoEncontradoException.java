package co.fsfb.ws.rest.exception;

import java.util.Date;

public class AntecedenteNoEncontradoException extends RespondException {
  private static final long serialVersionUID = 1L;
  
  public AntecedenteNoEncontradoException(String mensaje, Date fechaExcepcion, String detalleExcepcion) {
    super(mensaje, fechaExcepcion, detalleExcepcion);
  }
}

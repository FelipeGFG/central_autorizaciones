package co.fsfb.ws.rest.exception;

import java.util.Date;

public class DataException extends RespondException {
  private static final long serialVersionUID = 1L;
  
  public DataException(String mensaje, Date fechaExcepcion, String detalleExcepcion) {
    super(mensaje, fechaExcepcion, detalleExcepcion);
  }
  
  public DataException(int intCodigoError) {
    super(intCodigoError);
  }
  
  public DataException(int intCodigoError, Exception e) {
    super(intCodigoError, e);
  }
}

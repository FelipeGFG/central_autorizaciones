package co.fsfb.ws.rest.exception;

import java.util.Date;

public class CredencialesFallidasException extends RespondException {
  private static final long serialVersionUID = 1L;
  
  public CredencialesFallidasException(String mensaje, Date fechaExcepcion, String detalleExcepcion) {
    super(mensaje, fechaExcepcion, detalleExcepcion);
  }
}

package co.fsfb.ws.rest.exception;

import co.fsfb.ws.rest.util.PropertiesManager;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class LogicException extends Exception {
  private static final long serialVersionUID = -4989349945402497708L;
  
  private static Logger logger = LoggerFactory.getLogger(LogicException.class);
  
  private int errorCode;
  
  private String customMessage;
  
  private ArrayList<String> errorMessegesList;
  
  private Exception originalException;
  
  private String getCustomMessage(int newErrorCode) throws LogicException {
    try {
      String customMessage = PropertiesManager.CONFIG.getString("ERROR_" + newErrorCode);
      return customMessage;
    } catch (Exception ex) {
      throw getLogicException(ex);
    } 
  }
  
  public LogicException(int newErrorCode) throws LogicException {
    this.errorCode = newErrorCode;
    this.customMessage = getCustomMessage(newErrorCode);
    logger.error(getCustomMessage());
  }
  
  protected LogicException(int newErrorCode, String newCustomMessage, Exception newOriginalException) {
    this.errorCode = newErrorCode;
    this.customMessage = newCustomMessage;
    this.originalException = newOriginalException;
    logger.info(Level.SEVERE.toString(), getCustomMessage(), this.originalException);
  }
  
  protected LogicException(int newErrorCode, String newCustomMessage) {
    this.errorCode = newErrorCode;
    this.customMessage = newCustomMessage;
    logger.info(Level.SEVERE.toString(), getCustomMessage(), this.originalException);
  }
  
  public LogicException(int newErrorCode, Exception newOriginalException) throws LogicException {
    this.errorCode = newErrorCode;
    this.customMessage = getCustomMessage(newErrorCode);
    this.originalException = newOriginalException;
    logger.info(Level.SEVERE.toString(), getCustomMessage(), this.originalException);
  }
  
  public LogicException(int newErrorCode, Exception newOriginalException, String[] newParamsMessage) throws LogicException {
    this.errorCode = newErrorCode;
    this.customMessage = getCustomMessage(newErrorCode);
    this.originalException = newOriginalException;
    this.customMessage = MessageFormat.format(this.customMessage, (Object[])newParamsMessage);
    logger.info(Level.SEVERE.toString(), getCustomMessage(), this.originalException);
  }
  
  public LogicException(int newErrorCode, String[] newParamsMessage) throws LogicException {
    this.errorCode = newErrorCode;
    this.customMessage = getCustomMessage(newErrorCode);
    this.customMessage = MessageFormat.format(this.customMessage, (Object[])newParamsMessage);
    logger.error(getCustomMessage());
  }
  
  public LogicException(int newErrorCode, String[] newParamsMessage, Exception newOriginalException) throws LogicException {
    this.errorCode = newErrorCode;
    this.customMessage = getCustomMessage(newErrorCode);
    this.originalException = newOriginalException;
    this.customMessage = MessageFormat.format(this.customMessage, (Object[])newParamsMessage);
    logger.info(Level.SEVERE.toString(), getCustomMessage(), this.originalException);
  }
  
  public LogicException(int newErrorCode, Exception newOriginalException, ArrayList<String> newErrorMessagesList) throws LogicException {
    this.errorCode = newErrorCode;
    this.customMessage = getCustomMessage(newErrorCode);
    this.originalException = newOriginalException;
    this.errorMessegesList = newErrorMessagesList;
    StringBuffer message = new StringBuffer();
    message.append(getCustomMessage());
    if (newErrorMessagesList != null) {
      Iterator<String> i = newErrorMessagesList.iterator();
      while (i.hasNext()) {
        message.append(' ');
        message.append(i.next());
      } 
    } 
    logger.info(Level.SEVERE.toString(), getCustomMessage(), this.originalException);
  }
  
  public LogicException(int newErrorCode, ArrayList<String> newErrorMessagesList) throws LogicException {
    this.errorCode = newErrorCode;
    this.customMessage = getCustomMessage(newErrorCode);
    this.errorMessegesList = newErrorMessagesList;
    StringBuffer message = new StringBuffer();
    message.append(getCustomMessage());
    if (newErrorMessagesList != null) {
      Iterator<String> i = newErrorMessagesList.iterator();
      while (i.hasNext()) {
        message.append(' ');
        message.append(i.next());
      } 
    } 
    logger.info(Level.SEVERE.toString(), getCustomMessage(), this.originalException);
  }
  
  public String getCustomMessage() {
    return this.customMessage;
  }
  
  public String getClientMessage() {
    String clientMessage = null;
    if (this.customMessage != null) {
      int finalIndex = this.customMessage.indexOf("]]_");
      if (finalIndex != -1)
        clientMessage = this.customMessage.substring(finalIndex + 3); 
    } 
    return clientMessage;
  }
  
  public Exception getOriginalException() {
    return this.originalException;
  }
  
  public ArrayList<String> getErrorMessegesList() {
    return this.errorMessegesList;
  }
  
  public int getErrorCode() {
    return this.errorCode;
  }
  
  public static LogicException getLogicException(Exception r) {
    int code;
    String errorCode = null;
    if (r.getMessage() != null) {
      int initIndex = r.getMessage().indexOf("_[[");
      int finalIndex = r.getMessage().indexOf("]]_");
      if (initIndex != -1 && finalIndex != -1 && initIndex < finalIndex)
        errorCode = r.getMessage().substring(initIndex + 3, finalIndex); 
    } 
    try {
      code = Integer.parseInt(errorCode);
    } catch (NumberFormatException nF) {
      return new LogicException(-3, "Error de invocacide mremoto.", r);
    } 
    return new LogicException(code, r.getMessage(), r);
  }
}

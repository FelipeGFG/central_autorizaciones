package co.fsfb.ws.rest.exception;

import java.io.Serializable;
import java.util.Date;

public class RespondException extends Exception implements Serializable {
  private static final long serialVersionUID = 1L;
  
  private String mensaje;
  
  private String detalleExcepcion;
  
  private int errorCode;
  
  private Exception exception;
  
  public Exception getException() {
    return this.exception;
  }
  
  public void setException(Exception exception) {
    this.exception = exception;
  }
  
  public RespondException(String mensaje, Date fechaExcepcion, String detalleExcepcion) {
    this.mensaje = mensaje;
    this.detalleExcepcion = detalleExcepcion;
  }
  
  public RespondException(int intErroCode) {
    this.errorCode = intErroCode;
  }
  
  public RespondException(int intErroCode, Exception e) {
    this.errorCode = intErroCode;
    this.exception = e;
  }
  
  public int getErrorCode() {
    return this.errorCode;
  }
  
  public void setErrorCode(int errorCode) {
    this.errorCode = errorCode;
  }
  
  public String getMensaje() {
    return this.mensaje;
  }
  
  public void setMensaje(String mensaje) {
    this.mensaje = mensaje;
  }
  
  public String getDetalleExcepcion() {
    return this.detalleExcepcion;
  }
  
  public void setDetalleExcepcion(String detalleExcepcion) {
    this.detalleExcepcion = detalleExcepcion;
  }
}

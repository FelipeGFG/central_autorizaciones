package co.fsfb.ws.rest.exception;

import java.util.Date;

public class ValorException extends RespondException {
  private static final long serialVersionUID = 1L;
  
  public ValorException(String mensaje, Date fechaExcepcion, String detalleExcepcion) {
    super(mensaje, fechaExcepcion, detalleExcepcion);
  }
}

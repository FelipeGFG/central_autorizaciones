package co.fsfb.ws.rest.exception;

import java.util.Date;

public class CreacionFallidaException extends RespondException {
  private static final long serialVersionUID = 1L;
  
  public CreacionFallidaException(String mensaje, Date fechaExcepcion, String detalleExcepcion) {
    super(mensaje, fechaExcepcion, detalleExcepcion);
  }
}

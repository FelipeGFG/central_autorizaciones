package co.fsfb.ws.rest.dao;

import co.fsfb.ws.rest.entidades.PlDatosTransaccion;
import co.fsfb.ws.rest.entidades.PlDetalleTransaccion;
import co.fsfb.ws.rest.entidades.PlHistoricoPersona;
import co.fsfb.ws.rest.entidades.PlPersona;
import co.fsfb.ws.rest.entidades.PlTrazabilidadTransaccion;
import co.fsfb.ws.rest.exception.DataException;
import co.fsfb.ws.rest.util.EntityManagerUtil;
import co.fsfb.ws.rest.vo.PagosCitasTransaccionCreada;
import co.fsfb.ws.rest.vo.PagosTransaccionCreada;
import co.fsfb.ws.rest.vo.PersonP2P;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Transactional
@Service
public class PagosDAO {
  private static Logger logger = LoggerFactory.getLogger(PagosDAO.class);
  
  private EntityManager em = null;
  
  public Long guardarPago(PagosTransaccionCreada pagosTransaccionCreada) throws DataException {
    this.em = EntityManagerUtil.getEntityManager();
    this.em.getTransaction().begin();
    PlPersona plPersona = null;
    PersonP2P person = null;
    try {
      if (pagosTransaccionCreada.getPagadorP2P() != null) {
        person = pagosTransaccionCreada.getPagadorP2P();
      } else {
        person = pagosTransaccionCreada.getCompradorP2P();
      } 
      plPersona = buscarPersona(person.getNoDocumento());
      if (plPersona == null)
        plPersona = new PlPersona(); 
      plPersona.setNumeroDocumento(person.getNoDocumento());
      plPersona.setPacPacTipoIdentCodigo(person.getTipoDocumento());
      this.em.persist(plPersona);
      PlHistoricoPersona plHistoricoPersona = new PlHistoricoPersona();
      plHistoricoPersona.setPlPersona(plPersona);
      plHistoricoPersona.setApellidos(person.getApellidos());
      plHistoricoPersona.setCelular(person.getCelular());
      plHistoricoPersona.setEmail(person.getEmail());
      plHistoricoPersona.setFechaRegistro(new Date());
      plHistoricoPersona.setNonbres(person.getNombres());
      this.em.persist(plHistoricoPersona);
      PlDatosTransaccion plDatosTransaccion = new PlDatosTransaccion();
      plDatosTransaccion.setEstadoTransaccion("CREATED");
      plDatosTransaccion.setFechaRegistro(new Date());
      plDatosTransaccion.setPacPacNumero(Integer.valueOf(Integer.parseInt(pagosTransaccionCreada.getNoPacPaciente())));
      plDatosTransaccion.setPlPersona(plPersona);
      plDatosTransaccion.setValorTransacion(pagosTransaccionCreada.valorTotal());
      this.em.persist(plDatosTransaccion);
      PlTrazabilidadTransaccion plTrazabilidadTransaccion = new PlTrazabilidadTransaccion();
      plTrazabilidadTransaccion.setEstadoTransaccion("CREATED");
      plTrazabilidadTransaccion.setFechaInicio(new Date());
      plTrazabilidadTransaccion.setPlDatosTransaccion(plDatosTransaccion);
      this.em.persist(plTrazabilidadTransaccion);
      Iterator<PagosCitasTransaccionCreada> var7 = pagosTransaccionCreada.getListaCitas().iterator();
      while (var7.hasNext()) {
        PagosCitasTransaccionCreada pagosCitasTransaccionCreada = var7.next();
        PlDetalleTransaccion plDetalleTransaccion = new PlDetalleTransaccion();
        plDetalleTransaccion.setNumeroObligacion(pagosCitasTransaccionCreada.getIdCita().toString());
        plDetalleTransaccion.setPlDatosTransaccion(plDatosTransaccion);
        plDetalleTransaccion.setValorObligacion(pagosCitasTransaccionCreada.getValorCita());
        this.em.persist(plDetalleTransaccion);
      } 
      this.em.flush();
      this.em.getTransaction().commit();
      Long var19 = plDatosTransaccion.getIdDatosTransaccion();
      return var19;
    } catch (Exception var17) {
      throw new DataException(var17.getMessage(), new Date(), var17.getMessage().toString());
    } finally {
      try {
        this.em.close();
      } catch (Exception var16) {
        throw new DataException(var16.getMessage(), new Date(), var16.getMessage().toString());
      } 
    } 
  }
  
  private PlPersona buscarPersona(String idDocumento) {
    try {
      logger.info("Documento a buscar: '" + idDocumento + "'");
      Query query = this.em.createNamedQuery("PL_PERSONA.findByNumeroDocumento").setParameter("idDocumento", idDocumento);
      return (PlPersona) query.getResultList().get(0);
    } catch (Exception var3) {
      logger.error("Persona no encontrada : " + var3.getMessage());
      return null;
    } 
  }
  
  public List<PlDatosTransaccion> buscarPagosPorCliente(Integer pacNumero) {
    ArrayList<PlDatosTransaccion> var3;
    this.em = EntityManagerUtil.getEntityManager();
    try {
      Query query = this.em.createNamedQuery("PL_DATOS_TRANSACCION.findByPacNumberAndState").setParameter("pacPacNumero", pacNumero);
      List<PlDatosTransaccion> var9 = query.getResultList();
      return var9;
    } catch (Exception var7) {
      logger.error("Pagos no encontrados " + var7.getMessage());
      var3 = new ArrayList();
    } finally {
      this.em.close();
    } 
    return var3;
  }
  
  public List<PlDetalleTransaccion> buscarCitasPorTransaccion(PlDatosTransaccion plDatosTransaccion) {
    ArrayList<PlDetalleTransaccion> var3;
    this.em = EntityManagerUtil.getEntityManager();
    try {
      Query query = this.em.createNamedQuery("PL_DETALLE_TRANSACCION.findByTransactionData").setParameter("plDatosTransaccion", plDatosTransaccion);
      List<PlDetalleTransaccion> var9 = query.getResultList();
      return var9;
    } catch (Exception var7) {
      logger.error("Citas no encontradas en la transaccion " + var7.getMessage());
      var3 = new ArrayList();
    } finally {
      this.em.close();
    } 
    return var3;
  }
  
  public void persistReference(Long datosId, String idPlaceToPay) throws DataException {
    this.em = EntityManagerUtil.getEntityManager();
    this.em.getTransaction().begin();
    System.out.println("Inicio transaccion " + datosId + " datos palcetopau: " + idPlaceToPay);
    try {
      PlDatosTransaccion plDatosTransaccion = (PlDatosTransaccion)this.em.find(PlDatosTransaccion.class, datosId);
      System.out.println("Obtuvo datos transaccion exitosamente " + plDatosTransaccion.getNumeroTransaccion());
      plDatosTransaccion.setNumeroTransaccion(idPlaceToPay);
      plDatosTransaccion.setEstadoTransaccion("PENDING");
      this.em.persist(plDatosTransaccion);
      this.em.flush();
      this.em.getTransaction().commit();
    } catch (Exception var7) {
      var7.printStackTrace();
      throw new DataException(var7.getMessage(), new Date(), var7.getMessage().toString());
    } finally {
      this.em.close();
    } 
  }
  
  public PlDatosTransaccion getPlace2PayReference(Long datosId) throws DataException {
    PlDatosTransaccion var3;
    this.em = EntityManagerUtil.getEntityManager();
    try {
      PlDatosTransaccion plDatosTransaccion = (PlDatosTransaccion)this.em.find(PlDatosTransaccion.class, datosId);
      var3 = plDatosTransaccion;
    } catch (Exception var7) {
      throw new DataException(var7.getMessage(), new Date(), var7.getMessage().toString());
    } finally {
      this.em.close();
    } 
    return var3;
  }
  
  public String getTipoDocCA(String tipoDoc) {
    byte var3 = -1;
    switch (tipoDoc.hashCode()) {
      case 2144:
        if (tipoDoc.equals("CC"))
          var3 = 2; 
        break;
      case 2146:
        if (tipoDoc.equals("CE"))
          var3 = 3; 
        break;
      case 2609:
        if (tipoDoc.equals("RC"))
          var3 = 0; 
        break;
      case 2677:
        if (tipoDoc.equals("TI"))
          var3 = 1; 
        break;
      case 77305:
        if (tipoDoc.equals("NIT"))
          var3 = 4; 
        break;
      case 79438:
        if (tipoDoc.equals("PPN"))
          var3 = 5; 
        break;
    } 
    switch (var3) {
      case 0:
        return "2";
      case 1:
        return "3";
      case 2:
        return "4";
      case 3:
        return "5";
      case 4:
        return "6";
      case 5:
        return "P";
    } 
    return tipoDoc;
  }
}

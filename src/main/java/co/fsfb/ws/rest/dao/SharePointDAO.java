package co.fsfb.ws.rest.dao;

import co.fsfb.ws.rest.exception.DataException;
import co.fsfb.ws.rest.util.PropertiesManager;
import co.fsfb.ws.rest.util.SPOnline;
import co.fsfb.ws.rest.vo.RegistroSharePointVO;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.transaction.Transactional;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Transactional
@Service
public class SharePointDAO {
  private static Logger logger = LoggerFactory.getLogger(SharePointDAO.class);
  
  public SharePointDAO() {
    try {
      SPOnline.SPOnlineInit();
    } catch (DataException e) {
      logger.info("Error Iniciando Sharepoint ::::" + e.getMessage());
    } 
  }
  
  public boolean createFolderGral(String id, String File) throws Exception {
    logger.info("Iniciando createFolderGral DAO::::");
    String jsonString = SPOnline.post("/_api/web/GetFolderByServerRelativeUrl('/" + PropertiesManager.CONFIG.getString("sharepoint.folder") + "')/folders", "{ '__metadata': { 'type': 'SP.Folder' }, 'ServerRelativeUrl': '" + id + "'}", false);
    if (jsonString != null) {
      SPOnline.post("/_api/web/GetFolderByServerRelativeUrl('/" + PropertiesManager.CONFIG.getString("sharepoint.folder") + "/" + id + "')/folders", "{ '__metadata': { 'type': 'SP.Folder' }, 'ServerRelativeUrl': 'generales'}", false);
      SPOnline.post("/_api/web/GetFolderByServerRelativeUrl('/" + PropertiesManager.CONFIG.getString("sharepoint.folder") + "/" + id + "')/folders", "{ '__metadata': { 'type': 'SP.Folder' }, 'ServerRelativeUrl': 'eventos'}", false);
    } else {
      return false;
    } 
    return true;
  }
  
  public RegistroSharePointVO createArchivoEventos(String id, String servicio, String file, String nombreArchivo) throws Exception {
    logger.info("Iniciando createArchivoEventos DAO::::");
    RegistroSharePointVO registroSharePointVO = new RegistroSharePointVO();
    registroSharePointVO.setNombreArchivo(nombreArchivo);
    SimpleDateFormat d = new SimpleDateFormat("yyyy/MM/dd");
    String strfecha = d.format(new Date());
    String fecha = strfecha.replace("/", "");
    String urlRelative = "/" + PropertiesManager.CONFIG.getString("sharepoint.folder") + "/" + id + "/eventos/";
    String jsonString = SPOnline.post("/_api/web/GetFolderByServerRelativeUrl('" + urlRelative + "')/folders", "{ '__metadata': { 'type': 'SP.Folder' }, 'ServerRelativeUrl': '" + fecha + "'}", false);
    if (jsonString != null) {
      urlRelative = urlRelative + fecha + "/";
      if (servicio != null && !servicio.isEmpty()) {
        jsonString = SPOnline.post("/_api/web/GetFolderByServerRelativeUrl('" + urlRelative + "')/folders", "{ '__metadata': { 'type': 'SP.Folder' }, 'ServerRelativeUrl': '" + servicio + "'}", false);
        if (jsonString != null) {
          urlRelative = urlRelative + servicio + "/";
          byte[] decodedBytes = Base64.decodeBase64(file);
          jsonString = SPOnline.uploadFile("/_api/web/GetFolderByServerRelativeUrl('" + urlRelative + "')/Files/add(url='" + nombreArchivo + "',overwrite=true)", decodedBytes);
        } 
      } else {
        byte[] decodedBytes = Base64.decodeBase64(file);
        jsonString = SPOnline.uploadFile("/_api/web/GetFolderByServerRelativeUrl('" + urlRelative + "')/Files/add(url='" + nombreArchivo + "',overwrite=true)", decodedBytes);
      } 
    } else {
      registroSharePointVO.setRespuesta(false);
      return registroSharePointVO;
    } 
    registroSharePointVO.setRutaUrlArchivo(urlRelative);
    registroSharePointVO.setRespuesta(true);
    return registroSharePointVO;
  }
  
  public RegistroSharePointVO createArchivoGeneral(String id, String file, String nombreArchivo) throws Exception {
    logger.info("Iniciando createArchivoGeneral DAO::::");
    RegistroSharePointVO registroSharePointVO = new RegistroSharePointVO();
    registroSharePointVO.setNombreArchivo(nombreArchivo);
    String urlRelative = "/" + PropertiesManager.CONFIG.getString("sharepoint.folder") + "/" + id + "/generales";
    byte[] decodedBytes = Base64.decodeBase64(file);
    String jsonString = SPOnline.uploadFile("/_api/web/GetFolderByServerRelativeUrl('" + urlRelative + "')/Files/add(url='" + nombreArchivo + "',overwrite=true)", decodedBytes);
    if (jsonString != null) {
      SPOnline.post("/_api/web/GetFolderByServerRelativeUrl('/" + PropertiesManager.CONFIG.getString("sharepoint.folder") + "/" + id + "')/folders", "{ '__metadata': { 'type': 'SP.Folder' }, 'ServerRelativeUrl': 'generales'}", false);
      SPOnline.post("/_api/web/GetFolderByServerRelativeUrl('/" + PropertiesManager.CONFIG.getString("sharepoint.folder") + "/" + id + "')/folders", "{ '__metadata': { 'type': 'SP.Folder' }, 'ServerRelativeUrl': 'eventos'}", false);
    } 
    registroSharePointVO.setRutaUrlArchivo(urlRelative);
    registroSharePointVO.setRespuesta(true);
    return registroSharePointVO;
  }
  
  public boolean updateArchivo(String id, String file, String nombreArchivo, boolean general, String fecha, String servicio) throws Exception {
    logger.info("Iniciando updateArchivo DAO::::");
    byte[] decodedBytes = Base64.decodeBase64(file);
    String fechaReplace = fecha.replace("/", "");
    if (general) {
      SPOnline.uploadFile("/_api/web/GetFolderByServerRelativeUrl('/" + PropertiesManager.CONFIG.getString("sharepoint.folder") + "/" + id + "/generales')/Files/add(url='" + nombreArchivo + "',overwrite=true)", decodedBytes);
    } else {
      SPOnline.uploadFile("/_api/web/GetFolderByServerRelativeUrl('/" + PropertiesManager.CONFIG
          .getString("sharepoint.folder") + "/" + id + "/eventos/" + fechaReplace + "/" + servicio + "')/Files/add(url='" + nombreArchivo + "',overwrite=true)", decodedBytes);
    } 
    return true;
  }
  
  public boolean deleteArchivo(String id, String nombreArchivo, boolean general, String fecha, String servicio) throws Exception {
    logger.info("Iniciando deleteArchivo DAO::::");
    String fechaReplace = fecha.replace("/", "");
    if (general) {
      SPOnline.delete("/_api/web/GetFolderByServerRelativeUrl('/" + PropertiesManager.CONFIG.getString("sharepoint.folder") + "/" + id + "/generales/" + nombreArchivo + "')");
    } else if (servicio != null) {
      String conser = SPOnline.delete("/_api/web/GetFolderByServerRelativeUrl('/" + PropertiesManager.CONFIG.getString("sharepoint.folder") + "/" + id + "/eventos/" + fechaReplace + "/" + servicio + "/" + nombreArchivo + "')");
      System.out.println(conser);
    } else {
      String sinser = SPOnline.delete("/_api/web/GetFolderByServerRelativeUrl('/" + PropertiesManager.CONFIG.getString("sharepoint.folder") + "/" + id + "/eventos/" + fechaReplace + "/" + nombreArchivo + "')");
      System.out.println(sinser);
    } 
    return true;
  }
  
  public byte[] findArchivo(String id, String nombreArchivo, boolean general, String fecha, String servicio) throws Exception {
    logger.info("Iniciando buscarArchivo DAO::::");
    if (general)
      return SPOnline.get("/_api/web/GetFileByServerRelativeUrl('/" + PropertiesManager.CONFIG.getString("sharepoint.folder") + "/" + id + "/generales/" + nombreArchivo + "')/$value"); 
    String fechaReplace = fecha.replace("/", "");
    if (servicio != null)
      return SPOnline.get("/_api/web/GetFileByServerRelativeUrl('/" + PropertiesManager.CONFIG.getString("sharepoint.folder") + "/" + id + "/eventos/" + fechaReplace + "/" + servicio + "/" + nombreArchivo + "')/$value"); 
    return SPOnline.get("/_api/web/GetFileByServerRelativeUrl('/" + PropertiesManager.CONFIG.getString("sharepoint.folder") + "/" + id + "/eventos/" + fechaReplace + "/" + nombreArchivo + "')/$value");
  }
}

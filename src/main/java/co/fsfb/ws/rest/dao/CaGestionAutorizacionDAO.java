package co.fsfb.ws.rest.dao;

import co.fsfb.ws.rest.entidades.CaGestionAutorizacionCita;
import co.fsfb.ws.rest.util.EntityManagerUtil;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class CaGestionAutorizacionDAO {
  private static Logger logger = LoggerFactory.getLogger(CaGestionAutorizacionDAO.class);
  
  private EntityManager em = null;
  
  public CaGestionAutorizacionCita consultarCaAutorizacionCita(long pacNum) {
    CaGestionAutorizacionCita var6 = null;
    logger.info("Iniciando consultarCaAutorizacionCita(pacNum) ::::");
    this.em = EntityManagerUtil.getEntityManager();
    try {
      String jpql = "SELECT ca FROM CaGestionAutorizacionCita ca WHERE ca.pacPacNumero = :pacNum";
      Query query = this.em.createQuery(jpql);
      query.setParameter("pacNum", Long.valueOf(pacNum));
      List<CaGestionAutorizacionCita> listCaGestion = query.getResultList();
      if (listCaGestion == null || listCaGestion.isEmpty()) {
        CaGestionAutorizacionCita caGestionAutorizacionCita = null;
        return caGestionAutorizacionCita;
      } 
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
    if (this.em.isOpen())
      this.em.close(); 
    return var6;
  }
}

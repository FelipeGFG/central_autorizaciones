package co.fsfb.ws.rest.dao;

import co.fsfb.ws.rest.entidades.CaTelefonosPaciente;
import co.fsfb.ws.rest.util.EntityManagerUtil;
import java.util.List;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Transactional
@Service
public class CaTelefonosPacienteDAO {
  private static Logger logger = LoggerFactory.getLogger(CaTelefonosPacienteDAO.class);
  
  private EntityManager em = null;
  
  public CaTelefonosPaciente consultarCaTelefonosPaciente(Long pacPacNumero, Long tcIdCodigo) {
    logger.info("Iniciando consultarCaTelefonosPaciente - CaTelefonosPacienteDAO ::::");
    this.em = EntityManagerUtil.getEntityManager();
    CaTelefonosPaciente caTelefonosPaciente = null;
    try {
      StringBuilder jpql = new StringBuilder();
      jpql.append("SELECT catel FROM CaTelefonosPaciente catel");
      jpql.append(" WHERE catel.pacPacNumero = ?1");
      jpql.append(" AND catel.tcIdCodigo = ?2");
      List<CaTelefonosPaciente> caTelefonosPacientes = this.em.createQuery(jpql.toString()).setParameter(1, pacPacNumero).setParameter(2, tcIdCodigo).getResultList();
      if (caTelefonosPacientes != null && !caTelefonosPacientes.isEmpty())
        caTelefonosPaciente = caTelefonosPacientes.get(0); 
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
    this.em.close();
    return caTelefonosPaciente;
  }
}

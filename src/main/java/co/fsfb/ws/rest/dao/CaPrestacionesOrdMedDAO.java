package co.fsfb.ws.rest.dao;

import co.fsfb.ws.rest.entidades.CaPrestacionesOrdMed;
import co.fsfb.ws.rest.util.EntityManagerUtil;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class CaPrestacionesOrdMedDAO {
  private static Logger logger = LoggerFactory.getLogger(CaPrestacionesOrdMedDAO.class);
  
  private EntityManager em = null;
  
  public CaPrestacionesOrdMed consultarCaPrestacionesOrdMed(long pomIdPrestOrdm) {
    logger.info("Iniciando consultarCaPrestacionesOrdMed(pomIdPrestOrdm) ::::");
    this.em = EntityManagerUtil.getEntityManager();
    try {
      String jpql = "SELECT ca FROM CaPrestacionesOrdMed ca WHERE ca.pomIdPrestOrdm = :pomIdPrestOrdm";
      Query query = this.em.createQuery(jpql);
      query.setParameter("pomIdPrestOrdm", Long.valueOf(pomIdPrestOrdm));
      List<CaPrestacionesOrdMed> listCaPrestacionesOrdMed = query.getResultList();
      if (listCaPrestacionesOrdMed == null || listCaPrestacionesOrdMed.isEmpty())
        return null; 
      return listCaPrestacionesOrdMed.get(0);
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
  }
}

package co.fsfb.ws.rest.dao;

import co.fsfb.ws.rest.entidades.CaCitasGestionadas;
import co.fsfb.ws.rest.exception.DataException;
import co.fsfb.ws.rest.util.DataBaseUtil;
import co.fsfb.ws.rest.util.EntityManagerUtil;
import co.fsfb.ws.rest.util.PropertiesManager;
import co.fsfb.ws.rest.vo.CitaPorPagarVO;
import co.fsfb.ws.rest.vo.ConsultaCitaPacientePorAutoVO;
import co.fsfb.ws.rest.vo.ConsultaCitaPacienteVO;
import co.fsfb.ws.rest.vo.ConsultaCitaPorPacienteAutoVO;
import co.fsfb.ws.rest.vo.ListaUbicacionVO;
import co.fsfb.ws.rest.vo.Paciente;
import co.fsfb.ws.rest.vo.ResultadoCitaPacienteVO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Transactional
@Service
public class CitasDAO {
  private static Logger logger = LoggerFactory.getLogger(CitasDAO.class);
  
  public static Connection conn;
  
  private CaPolizasPacientesDAO caPolizasPacientesDAO = new CaPolizasPacientesDAO();
  
  private CaCitasGestionadasDAO caCitasGestionadasDAO = new CaCitasGestionadasDAO();
  
  private CaPacientesDAO caPacientesDAO = new CaPacientesDAO();
  
  private EntityManager em = null;
  
  public List<ResultadoCitaPacienteVO> cCitas(ConsultaCitaPacienteVO consultaCitaPacienteVO) throws DataException {
    logger.info("Iniciando cCitas() DAO::::");
    conn = DataBaseUtil.inicializarDesportal();
    ArrayList<ResultadoCitaPacienteVO> arrLst = new ArrayList<>();
    int contadorConsulta = 2;
    ResultSet set = null;
    try {
      String sentencia = PropertiesManager.CONFIG.getString("CONSULTCITAS_DEFAULT_RESULTADO");
      sentencia = String.valueOf(sentencia) + PropertiesManager.CONFIG.getString("CONSULTCITAS_DEFAULT_RELACION");
      if (consultaCitaPacienteVO.getPaciente().getNombres() != null && !consultaCitaPacienteVO.getPaciente().getNombres().isEmpty())
        sentencia = String.valueOf(sentencia) + PropertiesManager.CONFIG.getString("CONSULTCITAS_NOMBRES"); 
      if ((consultaCitaPacienteVO.getPaciente().getNombres() == null || consultaCitaPacienteVO.getPaciente().getNombres().isEmpty()) && consultaCitaPacienteVO.getPaciente().getPrimerApellido() != null && !consultaCitaPacienteVO.getPaciente().getPrimerApellido().isEmpty())
        sentencia = String.valueOf(sentencia) + PropertiesManager.CONFIG.getString("CONSULTCITAS_PRIMER_APELLIDO"); 
      if ((consultaCitaPacienteVO.getPaciente().getNombres() == null || consultaCitaPacienteVO.getPaciente().getNombres().isEmpty()) && consultaCitaPacienteVO.getPaciente().getSegundoApellido() != null && !consultaCitaPacienteVO.getPaciente().getSegundoApellido().isEmpty())
        sentencia = String.valueOf(sentencia) + PropertiesManager.CONFIG.getString("CONSULTCITAS_SEGUNDO_APELLIDO"); 
      if (consultaCitaPacienteVO.getPaciente().getNumDocId() != null && !consultaCitaPacienteVO.getPaciente().getNumDocId().isEmpty())
        sentencia = String.valueOf(sentencia) + PropertiesManager.CONFIG.getString("CONSULTCITAS_DOC"); 
      if (consultaCitaPacienteVO.getPaciente().getTipoDocId() != null && !consultaCitaPacienteVO.getPaciente().getTipoDocId().isEmpty())
        sentencia = String.valueOf(sentencia) + PropertiesManager.CONFIG.getString("CONSULTCITAS_TIPDOC"); 
      if (consultaCitaPacienteVO.getCodCentroAten() != null && !consultaCitaPacienteVO.getCodCentroAten().isEmpty())
        sentencia = String.valueOf(sentencia) + PropertiesManager.CONFIG.getString("CONSULTCITAS_CENATE"); 
      if (consultaCitaPacienteVO.getCodEspecialidad() != null && !consultaCitaPacienteVO.getCodEspecialidad().isEmpty())
        sentencia = String.valueOf(sentencia) + PropertiesManager.CONFIG.getString("CONSULTCITAS_COD_ESP"); 
      if (consultaCitaPacienteVO.getCodSubEspecialidad() != null && !consultaCitaPacienteVO.getCodSubEspecialidad().isEmpty())
        sentencia = String.valueOf(sentencia) + PropertiesManager.CONFIG.getString("CONSULTCITAS_COD_SUBESP"); 
      if (consultaCitaPacienteVO.getCodServicio() != null && !consultaCitaPacienteVO.getCodServicio().isEmpty())
        sentencia = String.valueOf(sentencia) + PropertiesManager.CONFIG.getString("CONSULTCITAS_SER"); 
      if (consultaCitaPacienteVO.getCodProf() != null && !consultaCitaPacienteVO.getCodProf().isEmpty())
        sentencia = String.valueOf(sentencia) + PropertiesManager.CONFIG.getString("CONSULTCITAS_PROF"); 
      if (consultaCitaPacienteVO.getConvenio() != null && !consultaCitaPacienteVO.getConvenio().isEmpty())
        sentencia = String.valueOf(sentencia) + PropertiesManager.CONFIG.getString("CONSULTCITAS_CONV"); 
      if (consultaCitaPacienteVO.getFechaFinalHIS() != null && !consultaCitaPacienteVO.getFechaFinalHIS().isEmpty())
        sentencia = String.valueOf(sentencia) + PropertiesManager.CONFIG.getString("CONSULTCITAS_DIGHIS"); 
      if (consultaCitaPacienteVO.getIndRecepcionado() != null && !consultaCitaPacienteVO.getIndRecepcionado().isEmpty())
        sentencia = String.valueOf(sentencia) + PropertiesManager.CONFIG.getString("CONSULTCITAS_RECEP"); 
      if (consultaCitaPacienteVO.getCodServicio() != null && !consultaCitaPacienteVO.getCodServicio().isEmpty())
        sentencia = String.valueOf(sentencia) + PropertiesManager.CONFIG.getString("CONSULTCITAS_COD_SER"); 
      if (consultaCitaPacienteVO.getAgenciaServicio() != null && !consultaCitaPacienteVO.getAgenciaServicio().isEmpty())
        sentencia = String.valueOf(sentencia) + PropertiesManager.CONFIG.getString("CONSULTCITAS_AGE_SER"); 
      if (consultaCitaPacienteVO.getEstado() != null)
        sentencia = String.valueOf(sentencia) + PropertiesManager.CONFIG.getString("CONSULTCITAS_EC_IDCODIGO"); 
      sentencia = String.valueOf(sentencia) + PropertiesManager.CONFIG.getString("CONSULTCITAS_ORDER");
      PreparedStatement stmt = conn.prepareStatement(sentencia);
      stmt.setMaxRows(20);
      stmt.setFetchSize(20);
      stmt.setString(1, consultaCitaPacienteVO.getFechaInicial());
      stmt.setString(2, consultaCitaPacienteVO.getFechaFinal());
      if (consultaCitaPacienteVO.getPaciente().getNombres() != null && !consultaCitaPacienteVO.getPaciente().getNombres().isEmpty()) {
        contadorConsulta++;
        stmt.setString(contadorConsulta, consultaCitaPacienteVO.getPaciente().getNombres());
        contadorConsulta++;
        stmt.setString(contadorConsulta, consultaCitaPacienteVO.getPaciente().getPrimerApellido());
        contadorConsulta++;
        stmt.setString(contadorConsulta, consultaCitaPacienteVO.getPaciente().getSegundoApellido());
      } 
      if ((consultaCitaPacienteVO.getPaciente().getNombres() == null || consultaCitaPacienteVO.getPaciente().getNombres().isEmpty()) && consultaCitaPacienteVO.getPaciente().getPrimerApellido() != null && !consultaCitaPacienteVO.getPaciente().getPrimerApellido().isEmpty()) {
        contadorConsulta++;
        stmt.setString(contadorConsulta, consultaCitaPacienteVO.getPaciente().getPrimerApellido());
      } 
      if ((consultaCitaPacienteVO.getPaciente().getNombres() == null || consultaCitaPacienteVO.getPaciente().getNombres().isEmpty()) && consultaCitaPacienteVO.getPaciente().getSegundoApellido() != null && !consultaCitaPacienteVO.getPaciente().getSegundoApellido().isEmpty()) {
        contadorConsulta++;
        stmt.setString(contadorConsulta, consultaCitaPacienteVO.getPaciente().getSegundoApellido());
      } 
      if (consultaCitaPacienteVO.getPaciente().getNumDocId() != null && !consultaCitaPacienteVO.getPaciente().getNumDocId().isEmpty()) {
        contadorConsulta++;
        stmt.setString(contadorConsulta, consultaCitaPacienteVO.getPaciente().getNumDocId());
      } 
      if (consultaCitaPacienteVO.getPaciente().getTipoDocId() != null && !consultaCitaPacienteVO.getPaciente().getTipoDocId().isEmpty()) {
        contadorConsulta++;
        stmt.setString(contadorConsulta, consultaCitaPacienteVO.getPaciente().getTipoDocId());
      } 
      if (consultaCitaPacienteVO.getCodCentroAten() != null && !consultaCitaPacienteVO.getCodCentroAten().isEmpty()) {
        contadorConsulta++;
        stmt.setString(contadorConsulta, consultaCitaPacienteVO.getCodCentroAten());
      } 
      if (consultaCitaPacienteVO.getCodEspecialidad() != null && !consultaCitaPacienteVO.getCodEspecialidad().isEmpty()) {
        contadorConsulta++;
        stmt.setString(contadorConsulta, consultaCitaPacienteVO.getCodEspecialidad());
      } 
      if (consultaCitaPacienteVO.getCodSubEspecialidad() != null && !consultaCitaPacienteVO.getCodSubEspecialidad().isEmpty()) {
        contadorConsulta++;
        stmt.setString(contadorConsulta, consultaCitaPacienteVO.getCodSubEspecialidad());
      } 
      if (consultaCitaPacienteVO.getCodServicio() != null && !consultaCitaPacienteVO.getCodServicio().isEmpty()) {
        contadorConsulta++;
        stmt.setString(contadorConsulta, consultaCitaPacienteVO.getCodServicio());
      } 
      if (consultaCitaPacienteVO.getCodProf() != null && !consultaCitaPacienteVO.getCodProf().isEmpty()) {
        contadorConsulta++;
        stmt.setString(contadorConsulta, consultaCitaPacienteVO.getCodProf());
      } 
      if (consultaCitaPacienteVO.getConvenio() != null && !consultaCitaPacienteVO.getConvenio().isEmpty()) {
        contadorConsulta++;
        stmt.setString(contadorConsulta, consultaCitaPacienteVO.getConvenio());
      } 
      if (consultaCitaPacienteVO.getFechaFinalHIS() != null && !consultaCitaPacienteVO.getFechaFinalHIS().isEmpty()) {
        contadorConsulta++;
        stmt.setString(contadorConsulta, consultaCitaPacienteVO.getFechaInicialHIS());
        contadorConsulta++;
        stmt.setString(contadorConsulta, consultaCitaPacienteVO.getFechaFinalHIS());
      } 
      if (consultaCitaPacienteVO.getCodServicio() != null && !consultaCitaPacienteVO.getCodServicio().isEmpty()) {
        contadorConsulta++;
        stmt.setString(contadorConsulta, consultaCitaPacienteVO.getCodServicio());
      } 
      if (consultaCitaPacienteVO.getAgenciaServicio() != null && !consultaCitaPacienteVO.getAgenciaServicio().isEmpty()) {
        contadorConsulta++;
        stmt.setString(contadorConsulta, consultaCitaPacienteVO.getAgenciaServicio().toString());
      } 
      if (consultaCitaPacienteVO.getEstado() != null) {
        contadorConsulta++;
        stmt.setInt(contadorConsulta, consultaCitaPacienteVO.getEstado().intValue());
      } 
      set = stmt.executeQuery();
      while (set.next()) {
        ResultadoCitaPacienteVO resultadoCitaPacienteVO = new ResultadoCitaPacienteVO();
        resultadoCitaPacienteVO.setIdCita(Long.valueOf(set.getLong(0)));
        resultadoCitaPacienteVO.setEstadoCita(set.getString(1));
        resultadoCitaPacienteVO.setAsistencia(Boolean.valueOf(set.getBoolean(2)));
        resultadoCitaPacienteVO.setPacNum(set.getLong(3));
        resultadoCitaPacienteVO.setNombreCompleto(set.getString(4).trim());
        resultadoCitaPacienteVO.setTipoDocId(set.getString(5).trim());
        resultadoCitaPacienteVO.setTipTipIDav(set.getString(6).trim());
        resultadoCitaPacienteVO.setNumDocId(set.getString(7).trim());
        resultadoCitaPacienteVO.setTelefono(set.getString(8).trim());
        resultadoCitaPacienteVO.setEmail(set.getString(9).trim());
        resultadoCitaPacienteVO.setHoraCita(set.getString(10).trim());
        resultadoCitaPacienteVO.setFechaCita(set.getString(11).trim());
        resultadoCitaPacienteVO.setCodProf(set.getString(12).trim());
        resultadoCitaPacienteVO.setNombreProf(set.getString(13).trim());
        resultadoCitaPacienteVO.setCodProf(set.getString(14).trim());
        resultadoCitaPacienteVO.setConsultorio(set.getString(15).trim());
        resultadoCitaPacienteVO.setCodigoPrestacion(set.getString(16).trim());
        resultadoCitaPacienteVO.setPrestacion(set.getString(17).trim());
        resultadoCitaPacienteVO.setCodCentroAten(set.getString(18).trim());
        resultadoCitaPacienteVO.setNombreCentroAten(set.getString(19).trim());
        resultadoCitaPacienteVO.setCodConvenio(Arrays.asList(new String[] { set.getString(20).trim() }));
        resultadoCitaPacienteVO.setConvenio(set.getString(21).trim());
        resultadoCitaPacienteVO.setCodUsrCita(set.getString(22).trim());
        resultadoCitaPacienteVO.setFechaAsigna(set.getString(23));
        resultadoCitaPacienteVO.setDireccionCentroOperativo(set.getString(24).trim());
        resultadoCitaPacienteVO.setTelefonoCentroOperativo(set.getString(25).trim());
        resultadoCitaPacienteVO.setCodEspecialidad(set.getString(27).trim());
        resultadoCitaPacienteVO.setEspecialidad(set.getString(28).trim());
        resultadoCitaPacienteVO.setSubEspecialidad(set.getString(30).trim());
        arrLst.add(resultadoCitaPacienteVO);
      } 
    } catch (SQLException var16) {
      logger.error("Error consultando citas", var16);
      throw new DataException(var16.getMessage(), new Date(), var16.getMessage());
    } finally {
      try {
        if (set != null)
          set.close(); 
        conn.close();
      } catch (Exception var15) {
        throw new DataException(var15.getMessage(), new Date(), var15.getMessage());
      } 
    } 
    return arrLst;
  }
  
  public List<ResultadoCitaPacienteVO> cCitasPorAutorizar(ConsultaCitaPacientePorAutoVO consultaCitaPacientePorAutoVO) throws DataException {
    try {
      List<ResultadoCitaPacienteVO> list = new ArrayList<>();
      List<CaCitasGestionadas> caCitasGestionadas = this.caCitasGestionadasDAO.consultarCaCitasGestionadas((new SimpleDateFormat("yyyy-MM-dd")).parse(consultaCitaPacientePorAutoVO.getFechaInicial()), (new SimpleDateFormat("yyyy-MM-dd")).parse(consultaCitaPacientePorAutoVO.getFechaFinal()), consultaCitaPacientePorAutoVO.getEstado());
      Iterator<CaCitasGestionadas> var5 = caCitasGestionadas.iterator();
      while (var5.hasNext()) {
        CaCitasGestionadas caCitasGestionada = var5.next();
        Paciente paciente = this.caPacientesDAO.consultarPacientesFSFBHIS(caCitasGestionada.getPacPacNumero());
        ResultadoCitaPacienteVO resultadoCitaPacienteVO = new ResultadoCitaPacienteVO();
        resultadoCitaPacienteVO.setIdCita(caCitasGestionada.getcGIdCitaNumero());
        resultadoCitaPacienteVO.setPacNum(paciente.getPacNum());
        resultadoCitaPacienteVO.setNombreCompleto(paciente.getNombreCompleto());
        resultadoCitaPacienteVO.setCodEstadoCita(caCitasGestionada.getEcIdCodigo().toString());
        resultadoCitaPacienteVO.setFechaCita((new SimpleDateFormat("yyyy/MM/dd")).format(caCitasGestionada.getPcaAgeFechaCitac()));
        resultadoCitaPacienteVO.setHoraCita(caCitasGestionada.getPcaAgeHoraCitac());
        resultadoCitaPacienteVO.setPacPacTipoIdentCodigo(paciente.getTipTipIDav());
        resultadoCitaPacienteVO.setNumDocId(paciente.getNumDocId());
        resultadoCitaPacienteVO.setCodUsrCita(caCitasGestionada.getPcaAgeCodigRecep());
        list.add(resultadoCitaPacienteVO);
      } 
      return list;
    } catch (Exception var8) {
      logger.error("Error consultando citas por autorizar", var8);
      throw new DataException(var8.getMessage(), new Date(), var8.getMessage().toString());
    } 
  }
  
  public List<CitaPorPagarVO> citasParaPago(ConsultaCitaPorPacienteAutoVO consultaCitaPorPacienteAutoVO) throws DataException {
    logger.info("Iniciando consultarPacientesFSFB(String) -CaPacientesDAO ::::");
    List<CitaPorPagarVO> citaPorPagarVOList = new ArrayList<>();
    conn = DataBaseUtil.inicializarDesportal();
    PreparedStatement stmt = null;
    Calendar fecha = Calendar.getInstance();
    try {
      String sql = "SELECT \r\nTRIM(PAC.DONURL) \"correo\",\r\nPAC.PAC_PAC_TIPOIDENTCODIGO \"tipo doc\",\r\nTRIM(TIP.TIPIDAV) \"Tipo doc homologado\",\r\nPAC.PAC_PAC_FECHANACIM \"fecha nacimiento\",\r\nPAC.PAC_PAC_SEXO \"sexo\",\r\nCIT.CG_ID_CITA_NUMERO \"id_cita Central\",\r\nPAC.PAC_PAC_NUMERO \"Pac numero\",\r\nTRIM(PAC.PAC_PAC_NOMBRE)||' '||TRIM(PAC.PAC_PAC_APELLPATER)||' '||TRIM(PAC.PAC_PAC_APELLMATER) \"Nombre Completo\",\r\nTIP.TIPIDAV \"Tipo de Documento homologado\",\r\nPAC.PAC_PAC_RUT \"Documento\",\r\nCIT.PCA_AGE_FECHACITAC \"Fecha Cita\",\r\nCIT.PCA_AGE_HORACITAC \"Hora Cita\",\r\nCIT.CG_FECHA_PROCESO \"Fecha Cita CA\",\r\nCIT.PCA_AGE_CODIGRECEP \"Cod user\", \r\nCIT.CON_CON_CODIGO \"codigo convenio\",\r\nCIT.PRE_PRE_CODIGO \"codigo prestacion\", \r\nAUTCIT.GAU_AUTORIZA_SERV \"estadoProceso\", \r\nAUTCIT.GAU_COSTO_PAC \"Costo paciente\",\r\nAUTCIT.GAU_FECHA_PROCESO \"Fecha y hora autorizacion\",\r\nTRIM(PREST.PRE_PRE_CODIGO)||' '||TRIM(PREST.PRE_PRE_DESCRIPCIO) \"descripcion\"\r\n\r\nFROM CA_CITAS_GESTIONADAS CIT\r\n\r\nINNER JOIN      ADMSALUD.PAC_PACIENTE@ISIS PAC\r\nON              TRIM(CIT.PAC_PAC_NUMERO)  = TRIM(PAC.PAC_PAC_NUMERO)\r\n            \r\nLEFT JOIN       ADMSALUD.TAB_TIPOIDENT@ISIS TIP\r\nON              TRIM(PAC.PAC_PAC_TIPOIDENTCODIGO) = TRIM(TIP.TAB_TIPOIDENTCODIGO)\r\n \r\nLEFT JOIN       ADMSALUD.PRE_PRESTACION@ISIS PREST\r\nON              TRIM(CIT.PRE_PRE_CODIGO) = TRIM(PREST.PRE_PRE_CODIGO) \r\n            \r\nINNER JOIN      CA_ESTADOS_CITAS ESCIT\r\nON \t\tCIT.EC_IDCODIGO = ESCIT.EC_IDCODIGO               \r\n\r\nINNER JOIN        CA_GESTION_AUTORIZACION_CITA AUTCIT\r\nON              AUTCIT.CG_ID_CITA_NUMERO = CIT.CG_ID_CITA_NUMERO \r\n\r\nWHERE           \r\n\r\nAUTCIT.GAU_AUTORIZA_SERV = 1  \r\nAND TIP.TAB_TIPOIDENTCODIGO =  ? \r\nAND TRIM(PAC.PAC_PAC_RUT) = ? \r\nAND AUTCIT.GAU_COSTO_PAC >= 3200\r\nAND CIT.PCA_AGE_FECHACITAC >= TO_DATE('" + 
        
        pintDateToFormatYYYYMMDD(fecha) + "','YYYY-MM-DD') \r\n" + 
        "AND CIT.CG_ID_CITA_NUMERO NOT IN(SELECT DISTINCT DETRAN.NUMERO_OBLIGACION\r\n\r\n" + 
        "FROM PL_DETALLE_TRANSACCION DETRAN \r\n\r\n" + 
        "LEFT JOIN  PL_DATOS_TRANSACCION DTRAN \r\n" + 
        "ON \t\t" + 
        "DTRAN.ID_DATOS_TRANSACCION = DETRAN.ID_DATOS_TRANSACCION \r\n" + 
        "WHERE DTRAN.ESTADO_TRANSACCION IN ('APPROVED','PENDING'))";
      stmt = conn.prepareStatement(sql.toString());
      stmt.setString(1, consultaCitaPorPacienteAutoVO.getTipoDcumento());
      stmt.setString(2, consultaCitaPorPacienteAutoVO.getPacId());
      ResultSet set = stmt.executeQuery();
      while (set.next()) {
        Calendar fecCita = Calendar.getInstance();
        fecCita.setTime(set.getDate(11));
        LocalTime horaCita = LocalTime.parse(set.getString(12));
        LocalTime horaActual = LocalTime.now();
        if (pintDateToFormatYYYYMMDD(fecha).equals(pintDateToFormatYYYYMMDD(fecCita)) && horaActual.isAfter(horaCita))
          continue; 
        CitaPorPagarVO citaPorPagarVO = new CitaPorPagarVO();
        citaPorPagarVO.setIdCita(set.getString(6));
        citaPorPagarVO.setPacNumero(set.getString(7));
        citaPorPagarVO.setNombreCompleto(set.getString(8));
        citaPorPagarVO.setTipoDocumento(set.getString(2));
        citaPorPagarVO.setDocumento(set.getString(10));
        citaPorPagarVO.setFechaCita(set.getString(11));
        citaPorPagarVO.setHoraCita(set.getString(12));
        citaPorPagarVO.setFechaCitaCa(String.valueOf(set.getString(11).split(" ")[0]) + " " + set.getString(12));
        citaPorPagarVO.setCodUser(set.getString(14));
        citaPorPagarVO.setCodigoConvenio(set.getString(15));
        citaPorPagarVO.setCodigoPrestacion(set.getString(16));
        citaPorPagarVO.setEstadoProceso(set.getString(17));
        citaPorPagarVO.setCostoPaciente(set.getString(18));
        citaPorPagarVO.setFechaHoraAutorizacion(set.getString(19));
        citaPorPagarVO.setDescripcion(set.getString(20));
        citaPorPagarVOList.add(citaPorPagarVO);
      } 
      return citaPorPagarVOList;
    } catch (SQLException var15) {
      logger.error("Error conultando paciente HIS", var15);
      throw new DataException(var15.getMessage(), new Date(), var15.getMessage());
    } finally {
      try {
        conn.close();
        if (stmt != null)
          stmt.close(); 
      } catch (Exception var14) {
        throw new DataException(var14.getMessage(), new Date(), var14.getMessage());
      } 
    } 
  }
  
  private String pintDateToFormatYYYYMMDD(Calendar fecha) {
    String retorno = "";
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    retorno = dateFormat.format(fecha.getTime());
    System.out.println(retorno);
    return retorno;
  }
  
  public List<ListaUbicacionVO> listaUbicacion() throws DataException {
    List<ListaUbicacionVO> response = new ArrayList<>();
    this.em = EntityManagerUtil.getEntityManager();
    try {
      this.em.getTransaction().begin();
      List<String> listaUbicaciones = this.em.createNativeQuery("select distinct ubicacion as  from ca_ubicacion_sedes where ubicacion is not null").getResultList();
      Iterator<String> var3 = listaUbicaciones.iterator();
      while (var3.hasNext()) {
        String ubicacion = var3.next();
        ListaUbicacionVO listaUbicacionVO = new ListaUbicacionVO();
        listaUbicacionVO.setId(ubicacion);
        listaUbicacionVO.setDescripcion(ubicacion);
        response.add(listaUbicacionVO);
      } 
      return response;
    } catch (Exception var9) {
      throw new DataException("Error al obtener la lista ubicaciones", Calendar.getInstance().getTime(), var9.getMessage());
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
  }
}

package co.fsfb.ws.rest.dao;

import co.fsfb.ws.rest.entidades.CaPacientes;
import co.fsfb.ws.rest.exception.DataException;
import co.fsfb.ws.rest.util.ConexionDirectaDesportal;
import co.fsfb.ws.rest.util.DataBaseUtil;
import co.fsfb.ws.rest.util.EntityManagerUtil;
import co.fsfb.ws.rest.vo.Paciente;
import co.fsfb.ws.rest.vo.PacienteHIS;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Transactional
@Service
public class CaPacientesDAO {

    private static Logger logger = LoggerFactory.getLogger(CaPacientesDAO.class);

    private EntityManager em = null;

    public static Connection conn;

    public CaPacientes consultarCaPacientes(Long pacPacNumero) {
        logger.info("Iniciando consultarCaPacientes -CaPacientesDAO ::::");
        logger.info("Consulta1");
        this.em = EntityManagerUtil.getEntityManager();
        try {
            try {
                return (CaPacientes) this.em.find(CaPacientes.class, pacPacNumero);
            } catch (Exception e) {
                logger.error("error",e);
            }

        } finally {
            if (this.em.isOpen()) {
                this.em.close();
            }
        }
        return null;
    }

    public CaPacientes consultarCaPacientes(String pacPacTipoIdentCodigo, String pacPacRut) {
        logger.info("Iniciando consultarCaPacientes(String,String) -CaPacientesDAO ::::");
        this.em = EntityManagerUtil.getEntityManager();
        CaPacientes caPacientes = null;
        try {
            StringBuilder jpql = new StringBuilder();
            jpql.append("SELECT cp FROM CaPacientes cp");
            jpql.append(" WHERE cp.pacPacTipoIdentCodigo = ?1");
            jpql.append(" AND cp.pacPacRut = ?2");
            List<CaPacientes> caPacientess = this.em.createQuery(jpql.toString()).setParameter(1, pacPacTipoIdentCodigo).setParameter(2, pacPacRut).getResultList();
            if (caPacientess != null && !caPacientess.isEmpty()) {
                caPacientes = caPacientess.get(0);
            }
        } finally {
            if (this.em.isOpen()) {
                this.em.close();
            }
        }
        this.em.close();
        return caPacientes;
    }

    public synchronized Paciente consultarPacientesFSFBHIS(String pacPacTipoIdentCodigo, String pacPacRut) throws DataException {
        logger.info("Iniciando consultarPacientesFSFB(String,String) -CaPacientesDAO ::::");
        conn = DataBaseUtil.inicializarDesportal();
        Paciente paciente = null;
        ResultSet set = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT");
            sql.append(" PAC.PAC_PAC_NUMERO,");
            sql.append(" PAC.PAC_PAC_TIPOIDENTCODIGO,");
            sql.append(" CONV.CON_TIPOCONVECOD,");
            sql.append(" TRIM(TIPD.TIPIDAV),");
            sql.append(" TRIM(PAC.PAC_PAC_RUT),");
            sql.append(" PAC.PAC_PAC_NOMBRE || ' ' || PAC.PAC_PAC_APELLPATER || ' ' || PAC.PAC_PAC_APELLMATER");
            sql.append(" FROM PAC_PACIENTE@ISIS PAC");
            sql.append(" INNER JOIN ADMSALUD.TAB_TIPOIDENT@ISIS TIPD ON PAC.PAC_PAC_TIPOIDENTCODIGO = TIPD.TAB_TIPOIDENTCODIGO");
            sql.append(" INNER JOIN ADMSALUD.CON_CONVENIO@ISIS CONV ON PAC.PAC_PAC_CODIGO = CONV.CON_CON_CODIGO");
            sql.append(" WHERE PAC.PAC_PAC_TIPOIDENTCODIGO = ?");
            sql.append(" AND TRIM(PAC.PAC_PAC_RUT) = ?");
            PreparedStatement stmt = conn.prepareStatement(sql.toString());
            stmt.setString(1, pacPacTipoIdentCodigo);
            stmt.setString(2, pacPacRut);
            set = stmt.executeQuery();
            while (set.next()) {
                paciente = new Paciente();
                paciente.setPacNum(Long.parseLong(set.getString(1)));
                paciente.setPacPacTipoIdentCodigo(set.getString(2));
                paciente.setConTipoConvenio(set.getString(3));
                paciente.setTipTipIDav(set.getString(4));
                paciente.setNumDocId(set.getString(5));
                paciente.setNombreCompleto(set.getString(6));
            }
        } catch (Exception ex) {
            logger.error("Error conultando paciente HIS", ex);
            throw new DataException(ex.getMessage(), new Date(), ex.getMessage());
        } finally {
            try {
                if (set != null) {
                    set.close();
                }
                conn.close();
            } catch (Exception e) {
                throw new DataException(e.getMessage(), new Date(), e.getMessage());
            }
        }
        return paciente;
    }

    public Paciente consultarPacientesFSFBHIS(Long pacPacNum) throws DataException {
        logger.info("Iniciando consultarPacientesFSFB(String) -CaPacientesDAO ::::");
        conn = DataBaseUtil.inicializarDesportal();
        Paciente paciente = null;
        ResultSet set = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT");
            sql.append(" PAC.PAC_PAC_NUMERO,");
            sql.append(" PAC.PAC_PAC_TIPOIDENTCODIGO,");
            sql.append(" TRIM(TIPD.TIPIDAV),");
            sql.append(" TRIM(PAC.PAC_PAC_RUT),");
            sql.append(" PAC.PAC_PAC_NOMBRE || ' ' || PAC.PAC_PAC_APELLPATER || ' ' || PAC.PAC_PAC_APELLMATER,");
            sql.append(" CONV.CON_TIPOCONVECOD");
            sql.append(" FROM PAC_PACIENTE PAC");
            sql.append(" INNER JOIN ADMSALUD.TAB_TIPOIDENT TIPD ON PAC.PAC_PAC_TIPOIDENTCODIGO = TIPD.TAB_TIPOIDENTCODIGO");
            sql.append(" INNER JOIN ADMSALUD.CON_CONVENIO@ISIS CONV ON PAC.PAC_PAC_CODIGO = CONV.CON_CON_CODIGO");
            sql.append(" AND TRIM(PAC.PAC_PAC_NUMERO) = ?");
            PreparedStatement stmt = conn.prepareStatement(sql.toString());
            stmt.setLong(1, pacPacNum.longValue());
            set = stmt.executeQuery();
            if (set.next()) {
                paciente = new Paciente();
                paciente.setPacNum(Long.parseLong(set.getString(1)));
                paciente.setPacPacTipoIdentCodigo(set.getString(2));
                paciente.setTipTipIDav(set.getString(3));
                paciente.setNumDocId(set.getString(4));
                paciente.setNombreCompleto(set.getString(5));
                paciente.setConTipoConvenio(set.getString(6));
            }
        } catch (SQLException ex) {
            logger.error("Error conultando paciente HIS", ex);
            throw new DataException(ex.getMessage(), new Date(), ex.getMessage());
        } finally {
            try {
                if (set != null) {
                    set.close();
                }
                conn.close();
            } catch (Exception e) {
                throw new DataException(e.getMessage(), new Date(), e.getMessage());
            }
        }
        return paciente;
    }

    public Paciente consultarPacientesFSFBHIS(Paciente filPaciente) throws DataException {
        logger.info("Iniciando consultarPacientesFSFB(String,String) -CaPacientesDAO ::::");
        conn = DataBaseUtil.inicializarDesportal();
        Paciente paciente = null;
        ResultSet set = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT");
            sql.append(" PAC.PAC_PAC_NUMERO \"Pac numero\",");
            sql.append(" PAC.PAC_PAC_TIPOIDENTCODIGO\" Tipo Documento\",");
            sql.append(" TRIM(PAC.PAC_PAC_RUT) \"Documento\",");
            sql.append(" PAC.PAC_PAC_NOMBRE || ' ' || PAC.PAC_PAC_APELLPATER || ' ' || PAC.PAC_PAC_APELLMATER,");
            sql.append(" CONV.CON_TIPOCONVECOD");
            sql.append(" FROM ADMSALUD.PAC_PACIENTE PAC");
            sql.append(" INNER JOIN ADMSALUD.CON_CONVENIO@ISIS CONV ON PAC.PAC_PAC_CODIGO = CONV.CON_CON_CODIGO");
            sql.append(" WHERE PAC.PAC_PAC_NOMBRE LIKE ('%?1%')");
            if (filPaciente.getPrimerApellido() != null) {
                sql.append(" AND PAC.PAC_PAC_APELLPATER LIKE ('%?2%')");
            }
            if (filPaciente.getSegundoApellido() != null) {
                sql.append(" AND PAC.PAC_PAC_APELLMATER LIKE ('%?3%')");
            }
            if (filPaciente.getPacPacTipoIdentCodigo() != null) {
                sql.append(" AND TIP.TAB_TIPOIDENTCODIGO =  ?4");
            }
            if (filPaciente.getTipoDocId() != null) {
                sql.append(" AND TRIM(PAC.PAC_PAC_RUT) = ?5");
            }
            PreparedStatement stmt = conn.prepareStatement(sql.toString());
            stmt.setString(1, filPaciente.getNombres());
            if (filPaciente.getPrimerApellido() != null) {
                stmt.setString(2, filPaciente.getPrimerApellido());
            }
            if (filPaciente.getSegundoApellido() != null) {
                stmt.setString(3, filPaciente.getSegundoApellido());
            }
            if (filPaciente.getPacPacTipoIdentCodigo() != null) {
                stmt.setString(4, filPaciente.getPacPacTipoIdentCodigo());
            }
            if (filPaciente.getTipoDocId() != null) {
                stmt.setString(5, filPaciente.getTipoDocId());
            }
            set = stmt.executeQuery();
            if (set.next()) {
                paciente = new Paciente();
                paciente.setPacNum(Long.parseLong(set.getString(1)));
                paciente.setPacPacTipoIdentCodigo(set.getString(2));
                paciente.setNumDocId(set.getString(3));
                paciente.setNombreCompleto(set.getString(4));
                paciente.setConTipoConvenio(set.getString(5));
            }
        } catch (SQLException ex) {
            throw new DataException(ex.getMessage(), new Date(), ex.getMessage().toString());
        } finally {
            try {
                if (set != null) {
                    set.close();
                }
                conn.close();
            } catch (Exception e) {
                throw new DataException(e.getMessage(), new Date(), e.getMessage().toString());
            }
        }
        return paciente;
    }

    public PacienteHIS consultarPacienteHIS(String tipoDoc, String numDoc) throws DataException {
        logger.info("Iniciando consultarPacienteHIS(String,String) -CaPacientesDAO ::::");
        conn = DataBaseUtil.inicializarDesportal();
        PacienteHIS pacienteHIS = null;
        ResultSet set = null;
        try {
            String sql = "SELECT PAC.PAC_PAC_TIPOIDENTCODIGO, \nTRIM(TIP.TIPIDAV), \nTRIM(PAC.PAC_PAC_RUT), \nTRIM(PAC.PAC_PAC_NOMBRE), \nTRIM(PAC.PAC_PAC_APELLPATER) || ' ' || TRIM(PAC.PAC_PAC_APELLMATER), \nTRIM(PAC.DONURL), \nTRIM(PAC.PAC_PAC_CELULAR) \nFROM ADMSALUD.PAC_PACIENTE@ISIS PAC \nLEFT JOIN ADMSALUD.TAB_TIPOIDENT@ISIS TIP \nON TRIM(PAC.PAC_PAC_TIPOIDENTCODIGO) = TRIM(TIP.TAB_TIPOIDENTCODIGO) \nWHERE TIP.TAB_TIPOIDENTCODIGO = ? AND TRIM(PAC.PAC_PAC_RUT) = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, tipoDoc);
            stmt.setString(2, numDoc);
            set = stmt.executeQuery();
            if (set.next()) {
                pacienteHIS = new PacienteHIS();
                pacienteHIS.setTipoDoc(set.getString(1));
                pacienteHIS.setTipoDocHom(set.getString(2));
                pacienteHIS.setNumDoc(set.getString(3));
                pacienteHIS.setNombres(set.getString(4));
                pacienteHIS.setApellidos(set.getString(5));
                pacienteHIS.setCorreo(set.getString(6));
                pacienteHIS.setCelular(set.getString(7));
            }
        } catch (SQLException ex) {
            throw new DataException(ex.getMessage(), new Date(), ex.getMessage().toString());
        } finally {
            try {
                if (set != null) {
                    set.close();
                }
                conn.close();
            } catch (Exception e) {
                throw new DataException(e.getMessage(), new Date(), e.getMessage().toString());
            }
        }
        return pacienteHIS;
    }
}

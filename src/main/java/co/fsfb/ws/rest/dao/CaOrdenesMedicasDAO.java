package co.fsfb.ws.rest.dao;

import co.fsfb.ws.rest.entidades.CaOrdenesMedicas;
import co.fsfb.ws.rest.entidades.CaPrestacionesOrdMed;
import co.fsfb.ws.rest.entidades.CaTrazaOrdenMedicas;
import co.fsfb.ws.rest.exception.DataException;
import co.fsfb.ws.rest.util.EntityManagerUtil;
import co.fsfb.ws.rest.vo.ConsultaTrazabilidadCitaVO;
import co.fsfb.ws.rest.vo.FiltrosOrdenMedicaVO;
import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Transactional
@Service
public class CaOrdenesMedicasDAO {
  private static Logger logger = LoggerFactory.getLogger(CaOrdenesMedicasDAO.class);
  
  private EntityManager em = null;
  
  public static Connection conn;
  
  public CaOrdenesMedicas consultarCaOrdenesMedicas(Long ormIdOrdmNumero, String pacPacTipoIdentCodigo, String pacPacRut) {
    logger.info("Iniciando consultarCaOrdenesMedicas(Long,String,String) - CaOrdenesMedicasDAO ::::");
    this.em = EntityManagerUtil.getEntityManager();
    CaOrdenesMedicas caOrdenesMedicas = null;
    try {
      StringBuilder jpql = new StringBuilder();
      jpql.append("SELECT com FROM CaOrdenesMedicas com");
      jpql.append(" WHERE com.ormIdOrdmNumero = ?1");
      jpql.append(" AND com.pacPacTipoIdentCodigo = ?2");
      jpql.append(" AND com.pacPacRut = ?3");
      List<CaOrdenesMedicas> casOrdenesMedicas = this.em.createQuery(jpql.toString()).setParameter(1, ormIdOrdmNumero).setParameter(2, pacPacTipoIdentCodigo).setParameter(3, pacPacRut).getResultList();
      if (casOrdenesMedicas != null && !casOrdenesMedicas.isEmpty())
        caOrdenesMedicas = casOrdenesMedicas.get(0); 
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
    this.em.close();
    return caOrdenesMedicas;
  }
  
  public List<CaOrdenesMedicas> consultarCaOrdenMedica(String pacPacTipoIdentCodigo, String pacPacRut) {
    logger.info("Iniciando consultarCaOrdenMedica(Long,String,String) - CaOrdenesMedicasDAO ::::");
    this.em = EntityManagerUtil.getEntityManager();
    try {
      StringBuilder jpql = new StringBuilder();
      jpql.append("SELECT com FROM CaOrdenesMedicas com");
      jpql.append(" WHERE com.pacPacTipoIdentCodigo = ?1");
      jpql.append(" AND com.pacPacRut = ?2");
      jpql.append(" order by ormIdOrdmNumero desc");
      List<CaOrdenesMedicas> casOrdenesMedicas = this.em.createQuery(jpql.toString()).setParameter(1, pacPacTipoIdentCodigo).setParameter(2, pacPacRut).getResultList();
      if (casOrdenesMedicas != null && !casOrdenesMedicas.isEmpty())
        return casOrdenesMedicas; 
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
    this.em.close();
    return null;
  }
  
  public List<CaOrdenesMedicas> consultarCaOrdenesMedicas(FiltrosOrdenMedicaVO filtrosOrdenMedicaVO) throws ParseException {
    logger.info("Iniciando consultarCaOrdenesMedicas(Long,String,String) - CaOrdenesMedicasDAO ::::");
    this.em = EntityManagerUtil.getEntityManager();
    try {
      StringBuilder jpql = new StringBuilder();
      jpql.append("SELECT com FROM CaOrdenesMedicas com");
      jpql.append(" LEFT JOIN FETCH com.caPrestacionesOrdMed p ");
      jpql.append(" LEFT JOIN FETCH p.caTrazaGestContinuidad t ");
      jpql.append(" WHERE com.cgFechaProceso between ?1 and ?2");
      if (filtrosOrdenMedicaVO.getPacPacTipoIdentCodigo() != null && 
        !filtrosOrdenMedicaVO.getPacPacTipoIdentCodigo().isEmpty())
        jpql.append(" AND com.pacPacTipoIdentCodigo = :pacPacTipoIdentCodigo"); 
      if (filtrosOrdenMedicaVO.getPacPacRut() != null && !filtrosOrdenMedicaVO.getPacPacRut().isEmpty())
        jpql.append(" AND com.pacPacRut = :pacPacRut"); 
      if (filtrosOrdenMedicaVO.getEstados() != null)
        jpql.append(" AND (select MAX(cto.eomIdCodigo) from CaTrazaOrdenMedicas cto where cto.caOrdenesMedicas.ormIdOrdmNumero = com.ormIdOrdmNumero) IN (:eomIdCodigo)"); 
      jpql.append(" order by com.ormIdOrdmNumero desc");
      Query query = this.em.createQuery(jpql.toString()).setParameter(1, (new SimpleDateFormat("yyyy-MM-dd")).parse(filtrosOrdenMedicaVO.getFechaInicial().split("T")[0])).setParameter(2, (new SimpleDateFormat("yyyy-MM-dd"))
          .parse(filtrosOrdenMedicaVO.getFechaFinal().split("T")[0]));
      if (filtrosOrdenMedicaVO.getPacPacTipoIdentCodigo() != null && 
        !filtrosOrdenMedicaVO.getPacPacTipoIdentCodigo().isEmpty())
        query.setParameter("pacPacTipoIdentCodigo", filtrosOrdenMedicaVO.getPacPacTipoIdentCodigo()); 
      if (filtrosOrdenMedicaVO.getPacPacRut() != null && !filtrosOrdenMedicaVO.getPacPacRut().isEmpty())
        query.setParameter("pacPacRut", filtrosOrdenMedicaVO.getPacPacRut()); 
      if (filtrosOrdenMedicaVO.getEstados() != null)
        query.setParameter("eomIdCodigo", filtrosOrdenMedicaVO.getEstados()); 
      List<CaOrdenesMedicas> casOrdenesMedicas = query.getResultList();
      if (casOrdenesMedicas == null)
        casOrdenesMedicas = new ArrayList<>(); 
      return casOrdenesMedicas;
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
  }
  
  public CaOrdenesMedicas consultarCaOrdenesMedicas(Long ormIdOrdmNumero) {
    logger.info("Iniciando consultarCaOrdenesMedicas(Long) - CaOrdenesMedicasDAO ::::");
    this.em = EntityManagerUtil.getEntityManager();
    try {
      return (CaOrdenesMedicas)this.em.find(CaOrdenesMedicas.class, ormIdOrdmNumero);
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
  }
  
  public boolean validarCaTrazaOrdenesMedicas(Long ormIdOrdmNumero) {
    logger.info("Iniciando consultarCaTrazaOrdenesMedicas(Long) - CaOrdenesMedicasDAO ::::");
    this.em = EntityManagerUtil.getEntityManager();
    boolean existe = false;
    try {
      StringBuilder sql = new StringBuilder();
      sql.append("SELECT");
      sql.append(" TOM.ORM_ID_ORDM_NUMERO OM_RADICADA");
      sql.append(" FROM CA_ORDENES_MEDICAS OM");
      sql.append(" INNER JOIN CA_TRAZA_ORDENMEDICAS TOM ON OM.ORM_ID_ORDM_NUMERO = TOM.ORM_ID_ORDM_NUMERO");
      sql.append(" WHERE TOM.ORM_ID_ORDM_NUMERO = ?1");
      sql.append(" AND TOM.EOM_IDCODIGO IN (3,4,5,6)");
      List<Object[]> objects = this.em.createNativeQuery(sql.toString()).setParameter(1, ormIdOrdmNumero).getResultList();
      if (objects != null && !objects.isEmpty())
        existe = true; 
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
    this.em.close();
    return existe;
  }
  
  public List<CaTrazaOrdenMedicas> consultarCaTrazaOrdenesMedicas(Long ormIdOrdmNumero, Long... estados) {
    logger.info("Iniciando consultarCaTrazaOrdenesMedicas(Long) - CaOrdenesMedicasDAO ::::");
    this.em = EntityManagerUtil.getEntityManager();
    try {
      StringBuilder jpql = new StringBuilder();
      jpql.append("SELECT traza FROM CaTrazaOrdenMedicas traza");
      jpql.append(" WHERE traza.caOrdenesMedicas.ormIdOrdmNumero = ?1");
      if (estados.length > 0)
        jpql.append(" AND traza.eomIdCodigo = ?2"); 
      jpql.append(" ORDER BY CG_FECHA_PROCESO DESC");
      Query query = this.em.createQuery(jpql.toString()).setParameter(1, ormIdOrdmNumero);
      if (estados.length > 0)
        query.setParameter(2, estados); 
      List<CaTrazaOrdenMedicas> traza = query.getResultList();
      if (traza == null)
        traza = new ArrayList<>(); 
      return traza;
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
  }
  
  public List<CaPrestacionesOrdMed> consultarPrestaciones(CaPrestacionesOrdMed consultaPrestacionesVO) throws DataException {
    logger.info("Iniciando consultaPrestacionesVO(consultaPrestacionesVO) - CaOrdenesMedicasDAO ::::");
    this.em = EntityManagerUtil.getEntityManager();
    List<CaPrestacionesOrdMed> consultaPrestaciones = new ArrayList<>();
    try {
      StringBuilder sql = new StringBuilder();
      sql.append("SELECT");
      sql.append(" SER.SER_SER_CODIGO COD_SERVICIO,");
      sql.append(" TRIM(SER.SER_SER_DESCRIPCIO) SERVICIO,");
      sql.append(" PRESSERV.PRE_PRE_CODIGO COD_PRESTACION,");
      sql.append(" TRIM(PREST.PRE_PRE_DESCRIPCIO) PRESTACION");
      sql.append(" FROM ADMSALUD.SER_SERVICIOS@ISIS SER, ADMSALUD.NET_PREPRESERSER@ISIS PRESSERV, ADMSALUD.PRE_PRESTACION@ISIS PREST");
      sql.append(" WHERE SER.SER_SER_CODIGO = PRESSERV.SER_SER_CODIGO");
      sql.append(" AND PRESSERV.PRE_PRE_CODIGO = PREST.PRE_PRE_CODIGO");
      if (consultaPrestacionesVO.getSerSerCodigo() != null && 
        !consultaPrestacionesVO.getSerSerCodigo().isEmpty())
        sql.append(" AND SER.SER_SER_CODIGO LIKE ('%" + consultaPrestacionesVO.getSerSerCodigo() + "%')"); 
      if (consultaPrestacionesVO.getSerSerDesc() != null && !consultaPrestacionesVO.getSerSerDesc().isEmpty())
        sql.append(" AND TRIM(SER.SER_SER_DESCRIPCIO) LIKE ('%" + consultaPrestacionesVO
            .getSerSerDesc() + "%')"); 
      if (consultaPrestacionesVO.getPrePreCodigo() != null && 
        !consultaPrestacionesVO.getPrePreCodigo().isEmpty())
        sql.append(" AND PRESSERV.PRE_PRE_CODIGO LIKE ('%" + consultaPrestacionesVO.getPrePreCodigo() + "%')"); 
      if (consultaPrestacionesVO.getPrePreDesc() != null && consultaPrestacionesVO.getPrePreDesc().isEmpty())
        sql.append(" AND TRIM(PREST.PRE_PRE_DESCRIPCIO) LIKE ('%" + consultaPrestacionesVO.getPrePreDesc() + "%')"); 
      sql.append(" UNION ALL ");
      sql.append("SELECT  DISTINCT ");
      sql.append("TIPO.PRE_TIP_TIPO COD_TIPO,");
      sql.append("PRE_TIP_DESCRIPCIO TIPO,");
      sql.append("PREST.PRE_PRE_CODIGO COD_PRESTACION,");
      sql.append("TRIM(PREST.PRE_PRE_DESCRIPCIO) PRESTACION ");
      sql.append("FROM ADMSALUD.PRE_PRESTACION@ISIS PREST, ADMSALUD.PRE_TIPO@ISIS TIPO, CA_SERVICIOS SERV ");
      sql.append("WHERE  PREST.PRE_PRE_TIPO=TIPO.PRE_TIP_TIPO ");
      sql.append("AND PRE_PRE_CODIGO NOT IN (SELECT PRE_PRE_CODIGO FROM ADMSALUD.NET_PREPRESERSER@ISIS PRESSERV) ");
      sql.append("AND PRE_PRE_VIGENCIA='S' ");
      sql.append("AND TIPO.PRE_TIP_TIPO = SERV.CODIGO_SERVICIO ");
      if (consultaPrestacionesVO.getSerSerCodigo() != null && 
        !consultaPrestacionesVO.getSerSerCodigo().isEmpty())
        sql.append(" AND TIPO.PRE_TIP_TIPO LIKE ('%" + consultaPrestacionesVO.getSerSerCodigo() + "%')"); 
      if (consultaPrestacionesVO.getSerSerDesc() != null && !consultaPrestacionesVO.getSerSerDesc().isEmpty())
        sql.append(" AND  TRIM(PRE_TIP_DESCRIPCIO) LIKE ('%" + consultaPrestacionesVO.getSerSerDesc() + "%')"); 
      if (consultaPrestacionesVO.getPrePreCodigo() != null && 
        !consultaPrestacionesVO.getPrePreCodigo().isEmpty())
        sql.append(" AND PREST.PRE_PRE_CODIGO LIKE ('%" + consultaPrestacionesVO.getPrePreCodigo() + "%')"); 
      if (consultaPrestacionesVO.getPrePreDesc() != null && consultaPrestacionesVO.getPrePreDesc().isEmpty())
        sql.append(" AND  TRIM(PREST.PRE_PRE_DESCRIPCIO) LIKE ('%" + consultaPrestacionesVO.getPrePreDesc() + "%')"); 
      sql.append(" ORDER BY 1,3");
      Query query = this.em.createNativeQuery(sql.toString());
      List<Object[]> objects = query.getResultList();
      if (objects != null && !objects.isEmpty())
        for (Object[] object : objects) {
          CaPrestacionesOrdMed caPrestacionesOrdMed = new CaPrestacionesOrdMed();
          caPrestacionesOrdMed.setSerSerCodigo(object[0].toString());
          caPrestacionesOrdMed.setSerSerDesc(object[1].toString());
          caPrestacionesOrdMed.setPrePreCodigo(object[2].toString());
          caPrestacionesOrdMed.setPrePreDesc(object[3].toString());
          consultaPrestaciones.add(caPrestacionesOrdMed);
        }  
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
    this.em.close();
    return consultaPrestaciones;
  }
  
  public List<ConsultaTrazabilidadCitaVO> consultarTrazabilidadOrden(Long ormIdOrdmNumero) {
    logger.info("Iniciando consultarTrazabilidadOrden(Long) - ormIdOrdmNumero ::::");
    this.em = EntityManagerUtil.getEntityManager();
    ConsultaTrazabilidadCitaVO consultaTrazabilidadCitaVO = null;
    try {
      StringBuilder sql = new StringBuilder();
      sql.append("SELECT DISTINCT");
      sql.append(" CG.ORM_ID_ORDM_NUMERO \"ID Orden\",");
      sql.append(" TO_CHAR(CG.CG_FECHA_PROCESO,'DD/MM/YYYY') \"Fecha Traza orden\",");
      sql.append(" TO_CHAR(CG.CG_FECHA_PROCESO,'HH24:MI:SS') \"Hora Traza orden\",");
      sql.append(" TRIM(USU.CAU_DESC_USUARIOS) \"Usuario Traza orden\",");
      sql.append(" TRIM(ESCIT.EOM_DESCRIPCION) \"Estado Orden\"");
      sql.append(" FROM CA_TRAZA_ORDENMEDICAS CG");
      sql.append(" LEFT JOIN CA_USUARIOS USU ON USU.PCA_AGE_CODIGRECEP = CG.PCA_AGE_CODIGRECEP");
      sql.append(" INNER JOIN CA_ESTADOS_OM ESCIT ON CG.EOM_IDCODIGO = ESCIT.EOM_IDCODIGO");
      sql.append(" WHERE CG.ORM_ID_ORDM_NUMERO = ?1");
      sql.append(" ORDER BY \"Fecha Traza orden\" DESC, \"Hora Traza orden\" DESC");
      List<Object[]> objects = this.em.createNativeQuery(sql.toString()).setParameter(1, ormIdOrdmNumero).getResultList();
      List<ConsultaTrazabilidadCitaVO> listTraza = new ArrayList<>();
      if (objects != null && !objects.isEmpty())
        for (Object[] object : objects) {
          consultaTrazabilidadCitaVO = new ConsultaTrazabilidadCitaVO();
          consultaTrazabilidadCitaVO.setcGIdCitaNumero(Long.valueOf(object[0].toString()));
          consultaTrazabilidadCitaVO.setCgFechaProceso(object[1].toString());
          consultaTrazabilidadCitaVO.setCgHoraProceso(object[2].toString());
          consultaTrazabilidadCitaVO.setCauDescUsuarios(object[3].toString());
          consultaTrazabilidadCitaVO.setEcDescripcion(object[4].toString());
          listTraza.add(consultaTrazabilidadCitaVO);
        }  
      return listTraza;
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
  }
}

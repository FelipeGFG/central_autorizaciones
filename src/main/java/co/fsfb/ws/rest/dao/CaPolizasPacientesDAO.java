package co.fsfb.ws.rest.dao;

import co.fsfb.ws.rest.entidades.CaPolizasPacientes;
import co.fsfb.ws.rest.util.EntityManagerUtil;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class CaPolizasPacientesDAO {

    private static final java.util.logging.Logger LOG = java.util.logging.Logger.getLogger(CaPolizasPacientesDAO.class.getName());

    private EntityManager em = null;

    public CaPolizasPacientes consultarCaPolizasPacientes(Long pacPacNumero) {
        LOG.info("Iniciando CaPolizasPacientes - CaPolizasPacientesDAO ::::");
        this.em = EntityManagerUtil.getEntityManager();
        CaPolizasPacientes caPolizasPaciente = null;
        try {
            StringBuilder jpql = new StringBuilder();
            jpql.append("SELECT cpp FROM CaPolizasPacientes cpp");
            jpql.append(" WHERE cpp.pacPacNumero = ?1");
            LOG.info("Numero de pacien " + pacPacNumero);

            List<CaPolizasPacientes> caPolizasPacientes = this.em.createQuery(jpql.toString()).setParameter(1, pacPacNumero).getResultList();
            if (caPolizasPacientes != null && !caPolizasPacientes.isEmpty()) {
                caPolizasPaciente = caPolizasPacientes.get(0);
            }
        } finally {
            if (this.em.isOpen()) {
                this.em.close();
            }
        }
        this.em.close();
        return caPolizasPaciente;
    }
}

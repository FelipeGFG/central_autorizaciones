package co.fsfb.ws.rest.dao;

import co.fsfb.ws.rest.exception.DataException;
import co.fsfb.ws.rest.exception.UsuarioNoEncontradoException;
import co.fsfb.ws.rest.util.DataBaseUtil;
import co.fsfb.ws.rest.util.PropertiesManager;
import co.fsfb.ws.rest.vo.Usuario;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Transactional
@Service
public class ValidarUsuarioDAO {
  private static Logger logger = LoggerFactory.getLogger(ValidarUsuarioDAO.class);
  
  private static CallableStatement cstmt;
  
  private static Connection connection;
  
  public Usuario validaUsr(Usuario usr) throws DataException, UsuarioNoEncontradoException {
   
	  logger.info("consultaPaciente DTO::::");

	    connection = DataBaseUtil.inicializarDesportal();

	    String maxRowsProp = PropertiesManager.CONFIG.getString("JDBC_MAX_ROWS_SP_GENERAL");

	    String retorno = "";

	    try {

	      cstmt = connection.prepareCall("{call ADMSALUD.SVCIBMVALDATPAC_PKG.SVCIBMVALDATPAC(?,?,?,?,?,?,?,?,?,?)}");

	      cstmt.setString(1, maxRowsProp);

	      cstmt.setString(2, usr.getTipoDoc());

	      cstmt.setString(3, String.valueOf(usr.getUsuario()));

	      cstmt.setString(4, usr.getCn().toUpperCase());

	      cstmt.setString(5, usr.getApellidoPaterno().toUpperCase());

	      cstmt.setString(6, usr.getSn().toUpperCase());

	      cstmt.setString(7, usr.getSexo());

	      cstmt.setString(8, usr.getFechaNacimiento());

	      cstmt.setString(9, usr.getMail());

	      cstmt.registerOutParameter(1, 12);

	      cstmt.registerOutParameter(10, 2);

	      cstmt.execute();

	      if (!cstmt.getObject(1).toString().contains(PropertiesManager.CONFIG.getString("USER_CONSISTENT"))) {

	        retorno = cstmt.getObject(1).toString();

	        usr.setPacnumero(retorno);

	        if (cstmt.getObject(10) != null) {

	          retorno = retorno + " " + cstmt.getObject(10).toString();

	          usr.setPacnumero(retorno);

	        } 

	      } else {

	        retorno = cstmt.getObject(10).toString();

	        usr.setPacnumero(retorno);

	      } 

	    } catch (SQLException e) {

	      throw new UsuarioNoEncontradoException(e.getMessage(), new Date(), e.getMessage().toString());

	    } finally {

	      try {

	        connection.close();

	      } catch (Exception e) {

	        throw new DataException(e.getMessage(), new Date(), e.getMessage().toString());

	      } 

	    } 

	    return usr;

	  }
	    
}

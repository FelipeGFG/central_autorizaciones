package co.fsfb.ws.rest.dao;

import co.fsfb.ws.rest.util.EntityManagerUtil;
import co.fsfb.ws.rest.vo.ConsultaSharePointVO;
import co.fsfb.ws.rest.vo.FiltrosConsultaSharePointVO;
import java.util.List;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Transactional
@Service
public class CaDocumentosSharePointDAO {
  private static Logger logger = LoggerFactory.getLogger(CaDocumentosSharePointDAO.class);
  
  private EntityManager em = null;
  
  public ConsultaSharePointVO consultarCaDocumentosSharePoint(FiltrosConsultaSharePointVO filtrosConsultaSharePointVO) {
    logger.info("Iniciando consultarCaDocumentosSharePoint(FiltrosConsultaSharepointVO) ::::");
    this.em = EntityManagerUtil.getEntityManager();
    ConsultaSharePointVO consultaSharePointVO = null;
    try {
      StringBuilder sql = new StringBuilder();
      sql.append("SELECT");
      sql.append("TDOC.TDS_DESCRIPCION \"Descripcion documento\",");
      sql.append("SHA.TIPIDAV \"Tipo Documento homologado\",");
      sql.append("TRIM(SHA.PAC_PAC_RUT) \"Numero Documento Paciente\",");
      sql.append("TRIM(SHA.TDS_SIGLA) \"Sigla documento\",");
      sql.append("SHA.DOM_SHA_IDENTIFICADOR \"ID negocio documento\",");
      sql.append("TRIM(SHA.TIPIDAV)||''||TRIM(SHA.PAC_PAC_RUT)||' - '||TRIM(SHA.TDS_SIGLA)||' - '|| SHA.DOM_SHA_IDENTIFICADOR \"Nombre Documento\",");
      sql.append("TRIM(SHA.DOM_SHA_URL)");
      sql.append(" FROM CA_DOCUMENTOS_SHAREPOINT SHA");
      sql.append(" INNER JOIN CA_ACCIONES ACC ON SHA.ACC_ID_NUMERO = ACC.ACC_ID_NUMERO");
      sql.append(" INNER JOIN CA_TIPO_DOCUMENTAL_SHAREPOINT TDOC ON SHA.TDS_SIGLA = TDOC.TDS_SIGLA");
      sql.append(" WHERE SHA.PAC_PAC_NUMERO = ?1");
      sql.append(" WHERE AND SHA.ACC_ID_NUMERO = ?2");
      List<Object[]> objects = this.em.createNativeQuery(sql.toString()).setParameter(1, filtrosConsultaSharePointVO.getPacPacNumero()).setParameter(2, filtrosConsultaSharePointVO.getAccIdNumero()).getResultList();
      if (objects != null && !objects.isEmpty()) {
        consultaSharePointVO = new ConsultaSharePointVO();
        consultaSharePointVO.setTdsDescripcion(((Object[])objects.get(0))[0].toString());
        consultaSharePointVO.setTipIdDav(((Object[])objects.get(0))[1].toString());
        consultaSharePointVO.setPacPacRut(((Object[])objects.get(0))[2].toString());
        consultaSharePointVO.setTdsSigla(((Object[])objects.get(0))[3].toString());
        consultaSharePointVO.setDomShaIdentificador(Long.valueOf(((Object[])objects.get(0))[4].toString()));
        consultaSharePointVO.setDomShaUrl(((Object[])objects.get(0))[6].toString());
      } 
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
    this.em.close();
    return consultaSharePointVO;
  }
}

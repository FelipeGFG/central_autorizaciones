package co.fsfb.ws.rest.dao;

import co.fsfb.ws.rest.entidades.CaUsuarios;
import co.fsfb.ws.rest.util.EntityManagerUtil;
import java.util.List;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Transactional
@Service
public class CaUsuariosDAO {
  private static Logger logger = LoggerFactory.getLogger(CaUsuariosDAO.class);
  
  private EntityManager em = null;
  
  public CaUsuarios consultarCaUsuarios(String pcaAgeCodigRecep) {
    logger.info("Iniciando consultarCaUsuarios -CaUsuariosDAO ::::");
    this.em = EntityManagerUtil.getEntityManager();
    CaUsuarios caUsuario = null;
    try {
      StringBuilder jpql = new StringBuilder();
      jpql.append("SELECT cau FROM CaUsuarios cau");
      jpql.append(" WHERE cau.pcaAgeCodigRecep = ?1");
      List<CaUsuarios> caUsuarios = this.em.createQuery(jpql.toString()).setParameter(1, pcaAgeCodigRecep).getResultList();
      if (caUsuarios != null && !caUsuarios.isEmpty())
        caUsuario = caUsuarios.get(0); 
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
    this.em.close();
    return caUsuario;
  }
}

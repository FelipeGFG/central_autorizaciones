package co.fsfb.ws.rest.dao;

import co.fsfb.ws.rest.entidades.CaGestionAutorizacion;
import co.fsfb.ws.rest.util.EntityManagerUtil;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class CaGestionAutorizacionPrestacionDAO {
  private static Logger logger = LoggerFactory.getLogger(CaGestionAutorizacionPrestacionDAO.class);
  
  private EntityManager em = null;
  
  public CaGestionAutorizacion consultarCaAutorizacionPrestacion(long pacNum) {
    CaGestionAutorizacion var6 = null;
    logger.info("Iniciando consultarCaAutorizacionPrestacion(pacNum) ::::");
    this.em = EntityManagerUtil.getEntityManager();
    try {
      String jpql = "SELECT ca FROM CaGestionAutorizacion ca WHERE ca.pacPacNumero = :pacNum";
      Query query = this.em.createQuery(jpql);
      query.setParameter("pacNum", Long.valueOf(pacNum));
      List<CaGestionAutorizacion> listCaGestion = query.getResultList();
      if (listCaGestion == null || listCaGestion.isEmpty()) {
        CaGestionAutorizacion caGestionAutorizacion = null;
        return caGestionAutorizacion;
      } 
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
    if (this.em.isOpen())
      this.em.close(); 
    return var6;
  }
}

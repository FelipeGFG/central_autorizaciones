package co.fsfb.ws.rest.dao;

import co.fsfb.ws.rest.exception.DataException;
import co.fsfb.ws.rest.util.DataBaseUtil;
import co.fsfb.ws.rest.util.PropertiesManager;
import co.fsfb.ws.rest.vo.CIE;
import co.fsfb.ws.rest.vo.CitasAgendadas;
import co.fsfb.ws.rest.vo.ClasificaConsulta;
import co.fsfb.ws.rest.vo.Convenio;
import co.fsfb.ws.rest.vo.Departamento;
import co.fsfb.ws.rest.vo.DiagnosticosVO;
import co.fsfb.ws.rest.vo.Medicos;
import co.fsfb.ws.rest.vo.Sede;
import co.fsfb.ws.rest.vo.Servicio;
import co.fsfb.ws.rest.vo.SubEspecialidad;
import co.fsfb.ws.rest.vo.TipoDoc;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Transactional
@Service
public class CatalogDAO {
  private static Logger logger = LoggerFactory.getLogger(CatalogDAO.class);
  
  public static Connection conn;
  
  public List<Departamento> departamento() throws DataException {
    logger.info("Iniciando departamento DAO::::");
    conn = DataBaseUtil.inicializarDesportal();
    ArrayList<Departamento> arrLst = new ArrayList<>();
    ResultSet set = null;
    try {
      Statement stmt = conn.createStatement();
      set = stmt.executeQuery(PropertiesManager.CONFIG.getString("CONSULTDEPTO"));
      while (set.next()) {
        Departamento depto = new Departamento();
        depto.setStrCodigo(set.getString(1));
        depto.setStrNombre(set.getString(2));
        arrLst.add(depto);
      } 
    } catch (SQLException ex) {
      throw new DataException(ex.getMessage(), new Date(), ex.getMessage().toString());
    } finally {
      try {
        if (set != null)
          set.close(); 
        conn.close();
      } catch (Exception e) {
        throw new DataException(e.getMessage(), new Date(), e.getMessage().toString());
      } 
    } 
    return arrLst;
  }
  
  public List<SubEspecialidad> subEspecialidades() throws DataException {
    logger.info("Iniciando subEspecialidades DAO::::");
    conn = DataBaseUtil.inicializarDesportal();
    ArrayList<SubEspecialidad> arrLst = new ArrayList<>();
    ResultSet set = null;
    try {
      Statement stmt = conn.createStatement();
      set = stmt.executeQuery(PropertiesManager.CONFIG.getString("CONSULTSUBESP"));
      while (set.next()) {
        SubEspecialidad subEspecialidad = new SubEspecialidad();
        subEspecialidad.setStrSubCodigo(set.getString(1));
        subEspecialidad.setStrNombre(set.getString(2));
        subEspecialidad.setStrEspCodigo(set.getString(3));
        arrLst.add(subEspecialidad);
      } 
    } catch (SQLException ex) {
      throw new DataException(ex.getMessage(), new Date(), ex.getMessage().toString());
    } finally {
      try {
        if (set != null)
          set.close(); 
        conn.close();
      } catch (Exception e) {
        throw new DataException(e.getMessage(), new Date(), e.getMessage().toString());
      } 
    } 
    return arrLst;
  }
  
  public List<Servicio> servicios() throws DataException {
    logger.info("Iniciando servicios DAO::::");
    conn = DataBaseUtil.inicializarDesportal();
    ArrayList<Servicio> arrLst = new ArrayList<>();
    ResultSet set = null;
    try {
      Statement stmt = conn.createStatement();
      set = stmt.executeQuery(PropertiesManager.CONFIG.getString("CONSULTSERV"));
      while (set.next()) {
        Servicio servicio = new Servicio();
        servicio.setStrSerCodigo(set.getString(1));
        servicio.setStrSerDescripcion(set.getString(2));
        servicio.setStrSerCodSubEsp(set.getString(3));
        arrLst.add(servicio);
      } 
    } catch (SQLException ex) {
      throw new DataException(ex.getMessage(), new Date(), ex.getMessage().toString());
    } finally {
      try {
        if (set != null)
          set.close(); 
        conn.close();
      } catch (Exception e) {
        throw new DataException(e.getMessage(), new Date(), e.getMessage().toString());
      } 
    } 
    return arrLst;
  }
  
  public List<Servicio> servicio() throws DataException {
    logger.info("Iniciando departamento DAO::::");
    conn = DataBaseUtil.inicializarDesportal();
    ResultSet set = null;
    ArrayList<Servicio> arrLst = new ArrayList<>();
    try {
      Statement stmt = conn.createStatement();
      set = stmt.executeQuery(PropertiesManager.CONFIG.getString("CONSULTSERV"));
      while (set.next()) {
        Servicio servicio = new Servicio();
        servicio.setStrCodigoPrestacion(set.getString(1));
        servicio.setStrNombrePrestacion(set.getString(2));
        arrLst.add(servicio);
      } 
    } catch (SQLException ex) {
      throw new DataException(ex.getMessage(), new Date(), ex.getMessage().toString());
    } finally {
      try {
        if (set != null)
          set.close(); 
        conn.close();
      } catch (Exception e) {
        throw new DataException(e.getMessage(), new Date(), e.getMessage().toString());
      } 
    } 
    return arrLst;
  }
  
  public List<ClasificaConsulta> clasificaConsul(ClasificaConsulta consul) throws DataException {
    logger.info("Iniciando clasificaConsul DAO::::");
    conn = DataBaseUtil.inicializarDesportal();
    ResultSet set = null;
    ArrayList<ClasificaConsulta> arrLst = new ArrayList<>();
    try {
      Statement stmt = conn.createStatement();
      String consulta = PropertiesManager.CONFIG.getString("CONSULTCLASIFI") + "SER.SER_SER_CODIGESPEC IN('" + consul.getCodigoEspec() + "') AND SER.SER_SER_CODSUBESPE IN('" + consul.getCodSubEspec() + "') ORDER BY SER.SER_SER_DESCRIPCIO";
      set = stmt.executeQuery(consulta);
      while (set.next()) {
        ClasificaConsulta clasiCons = new ClasificaConsulta();
        clasiCons.setCodServicio(set.getString(1));
        clasiCons.setServicio(set.getString(2));
        arrLst.add(clasiCons);
      } 
    } catch (SQLException ex) {
      throw new DataException(ex.getMessage(), new Date(), ex.getMessage().toString());
    } finally {
      try {
        if (set != null)
          set.close(); 
        conn.close();
      } catch (Exception e) {
        throw new DataException(e.getMessage(), new Date(), e.getMessage().toString());
      } 
    } 
    return arrLst;
  }
  
  public List<CIE> cie(CIE cie) throws DataException {
    logger.info("Iniciando cie DAO::::");
    conn = DataBaseUtil.inicializarDesportal();
    ResultSet set = null;
    ArrayList<CIE> arrLst = new ArrayList<>();
    try {
      String consulta = PropertiesManager.CONFIG.getString("CONSULTCIE") + "PREST.PRE_PRE_CODIGO AND SER.SER_SER_CODIGO IN ('" + cie.getCodPrestacion() + "') ORDER BY 1, 3";
      Statement stmt = conn.createStatement();
      set = stmt.executeQuery(consulta);
      while (set.next()) {
        CIE nCie = new CIE();
        nCie.setCodServicio(set.getString(1));
        nCie.setServicio(set.getString(2));
        nCie.setCodPrestacion(set.getString(3));
        nCie.setPrestacion(set.getString(4));
        arrLst.add(nCie);
      } 
    } catch (SQLException ex) {
      throw new DataException(ex.getMessage(), new Date(), ex.getMessage().toString());
    } finally {
      try {
        if (set != null)
          set.close(); 
        conn.close();
      } catch (Exception e) {
        throw new DataException(e.getMessage(), new Date(), e.getMessage().toString());
      } 
    } 
    return arrLst;
  }
  
  public List<Sede> sede() throws DataException {
    logger.info("Iniciando sede DAO::::");
    conn = DataBaseUtil.inicializarDesportal();
    ResultSet set = null;
    ArrayList<Sede> arrLst = new ArrayList<>();
    try {
      Statement stmt = conn.createStatement();
      set = stmt.executeQuery(PropertiesManager.CONFIG.getString("CONSULTSEDE"));
      while (set.next()) {
        Sede sede = new Sede();
        sede.setStrCodIntCen(set.getString(1));
        sede.setStrNomCenAte(set.getString(2));
        sede.setStrCodCenAte(set.getString(3));
        arrLst.add(sede);
      } 
    } catch (SQLException ex) {
      throw new DataException(ex.getMessage(), new Date(), ex.getMessage().toString());
    } finally {
      try {
        if (set != null)
          set.close(); 
        conn.close();
      } catch (Exception e) {
        throw new DataException(e.getMessage(), new Date(), e.getMessage().toString());
      } 
    } 
    return arrLst;
  }
  
  public List<TipoDoc> tipoDoc() throws DataException {
    logger.info("Iniciando tipoDoc DAO::::");
    conn = DataBaseUtil.inicializarDesportal();
    ResultSet set = null;
    ArrayList<TipoDoc> arrLst = new ArrayList<>();
    try {
      Statement stmt = conn.createStatement();
      set = stmt.executeQuery(PropertiesManager.CONFIG.getString("CONSULTTIPODOC"));
      while (set.next()) {
        TipoDoc tipDoc = new TipoDoc();
        tipDoc.setStrTipIdeCod(set.getString(1));
        tipDoc.setStrTipIdeGlosa(set.getString(2));
        tipDoc.setStrTipIdav(set.getString(3));
        arrLst.add(tipDoc);
      } 
    } catch (SQLException ex) {
      throw new DataException(ex.getMessage(), new Date(), ex.getMessage().toString());
    } finally {
      try {
        if (set != null)
          set.close(); 
        conn.close();
      } catch (Exception e) {
        throw new DataException(e.getMessage(), new Date(), e.getMessage().toString());
      } 
    } 
    return arrLst;
  }
  
  public List<Convenio> convenio() throws DataException {
    logger.info("Iniciando convenio DAO::::");
    conn = DataBaseUtil.inicializarDesportal();
    ResultSet set = null;
    ArrayList<Convenio> arrLst = new ArrayList<>();
    try {
      Statement stmt = conn.createStatement();
      set = stmt.executeQuery(PropertiesManager.CONFIG.getString("CONSULTCONVENIO"));
      while (set.next()) {
        Convenio convenio = new Convenio();
        convenio.setStrCodigo(set.getString(1));
        convenio.setStrDesc(set.getString(2));
        convenio.setStrVigencia(set.getString(3));
        convenio.setStrTipCon(set.getString(4));
        convenio.setStrTipConCod(set.getString(5));
        convenio.setStrModTarCod(set.getString(6));
        arrLst.add(convenio);
      } 
    } catch (SQLException ex) {
      throw new DataException(ex.getMessage(), new Date(), ex.getMessage().toString());
    } finally {
      try {
        if (set != null)
          set.close(); 
        conn.close();
      } catch (Exception e) {
        throw new DataException(e.getMessage(), new Date(), e.getMessage().toString());
      } 
    } 
    return arrLst;
  }
  
  public List<Medicos> medicos() throws DataException {
    logger.info("Iniciando medicos DAO::::");
    conn = DataBaseUtil.inicializarDesportal();
    ResultSet set = null;
    ArrayList<Medicos> arrLst = new ArrayList<>();
    try {
      Statement stmt = conn.createStatement();
      set = stmt.executeQuery(PropertiesManager.CONFIG.getString("CONSULTMEDICOS"));
      while (set.next()) {
        Medicos medico = new Medicos();
        medico.setStrTipoIde(set.getString(1));
        medico.setStrProRut(set.getString(2));
        medico.setStrProApePat(set.getString(3));
        medico.setStrProApeMat(set.getString(4));
        medico.setStrProApeNom(set.getString(5) + " " + set.getString(3) + " " + set.getString(4));
        medico.setStrProEst(set.getString(6));
        medico.setStrTipoIde(set.getString(7));
        medico.setStrProCta(set.getString(8));
        arrLst.add(medico);
      } 
    } catch (SQLException ex) {
      throw new DataException(ex.getMessage(), new Date(), ex.getMessage().toString());
    } finally {
      try {
        if (set != null)
          set.close(); 
        conn.close();
      } catch (Exception e) {
        throw new DataException(e.getMessage(), new Date(), e.getMessage().toString());
      } 
    } 
    return arrLst;
  }
  
  public List<CitasAgendadas> prestacion() throws DataException {
    logger.info("Iniciando pretacion DAO::::");
    conn = DataBaseUtil.inicializarDesportal();
    ResultSet set = null;
    ArrayList<CitasAgendadas> arrLst = new ArrayList<>();
    try {
      Statement stmt = conn.createStatement();
      stmt.setMaxRows(100);
      stmt.setFetchSize(100);
      set = stmt.executeQuery(PropertiesManager.CONFIG.getString("CONSULTPRESTACION"));
      while (set.next()) {
        CitasAgendadas citas = new CitasAgendadas();
        citas.setCodigoPrestacion(set.getString(1));
        citas.setNombrePrestacion(set.getString(2));
        arrLst.add(citas);
      } 
    } catch (SQLException ex) {
      throw new DataException(ex.getMessage(), new Date(), ex.getMessage().toString());
    } finally {
      try {
        if (set != null)
          set.close(); 
        conn.close();
      } catch (Exception e) {
        throw new DataException(e.getMessage(), new Date(), e.getMessage().toString());
      } 
    } 
    return arrLst;
  }
  
    public List<DiagnosticosVO> getDiagnosticos() throws DataException {
        logger.info("Iniciando diagnosticos DAO::::");
        conn = DataBaseUtil.inicializarDesportal();
        ResultSet set = null;
        ArrayList<DiagnosticosVO> arrLst = new ArrayList<>();
        try {
            String consulta = PropertiesManager.CONFIG.getString("CONSULTDIAGNOSTICOS");
            Statement stmt = conn.createStatement();
            set = stmt.executeQuery(consulta);
            while (set.next()) {
                DiagnosticosVO dg = new DiagnosticosVO();
                dg.setDiaAgrCodigo(set.getString(2));
                dg.setDiaDiaDescripcion((set.getString(3) != null) ? set.getString(3).trim() : "");
                arrLst.add(dg);
            }
        } catch (SQLException ex) {
            throw new DataException(ex.getMessage(), new Date(), ex.getMessage().toString());
        } finally {
            try {
                if (set != null) {
                    set.close();
                }
                conn.close();
            } catch (Exception e) {
                throw new DataException(e.getMessage(), new Date(), e.getMessage().toString());
            }
        }
        return arrLst;
    }
  
  public List<DiagnosticosVO> getDiagnosticosLike(String valor) throws DataException {
    logger.info("Iniciando diagnosticos DAO::::");
    conn = DataBaseUtil.inicializarDesportal();
    ResultSet set = null;
    ArrayList<DiagnosticosVO> arrLst = new ArrayList<>();
    try {
      String consulta = PropertiesManager.CONFIG.getString("CONSULTDIAGNOSTICOSLIKE")+valor+"%' ORDER BY DIA_DIA_DESCRIPCIO";
      Statement stmt = conn.createStatement();
      set = stmt.executeQuery(consulta);
      while (set.next()) {
        DiagnosticosVO dg = new DiagnosticosVO();
        dg.setDiaAgrCodigo(set.getString(2));
        dg.setDiaDiaDescripcion((set.getString(3) != null) ? set.getString(3).trim() : "");
        arrLst.add(dg);
      } 
    } catch (SQLException ex) {
      throw new DataException(ex.getMessage(), new Date(), ex.getMessage().toString());
    } finally {
      try {
        if (set != null)
          set.close(); 
        conn.close();
      } catch (Exception e) {
        throw new DataException(e.getMessage(), new Date(), e.getMessage().toString());
      } 
    } 
    return arrLst;
  }
}

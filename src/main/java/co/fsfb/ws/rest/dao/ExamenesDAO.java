package co.fsfb.ws.rest.dao;

import co.fsfb.ws.rest.exception.DataException;
import co.fsfb.ws.rest.util.DataBaseUtil;
import co.fsfb.ws.rest.vo.Paciente;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Transactional
@Service
public class ExamenesDAO {
  private static Logger logger = LoggerFactory.getLogger(ExamenesDAO.class);
  
  public static Connection conn;
  
  public byte[] consultarExamenPDFDePaciente(Paciente p) throws DataException {
    logger.info("Iniciando consultarExamenPDFDePaciente::::");
    conn = DataBaseUtil.inicializarSQL();
    CallableStatement cstmt = null;
    byte[] examenpdf = new byte[5000000];
    try {
      cstmt = conn.prepareCall("{call dbo.Genera_Plano(?,?)}");
      cstmt.setString(1, "" + p.getCodOrdenAlterna());
      cstmt.setBytes(2, examenpdf);
      cstmt.registerOutParameter(2, -3);
      cstmt.execute();
      examenpdf = cstmt.getBytes(2);
    } catch (SQLException ex) {
      throw new DataException(ex.getMessage(), new Date(), ex.getMessage().toString());
    } finally {
      try {
        conn.close();
      } catch (Exception e) {
        throw new DataException(e.getMessage(), new Date(), e.getMessage().toString());
      } 
    } 
    return examenpdf;
  }
}

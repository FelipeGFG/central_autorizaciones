package co.fsfb.ws.rest.dao;

import co.fsfb.ws.rest.entidades.CaCorreoPaciente;
import co.fsfb.ws.rest.util.EntityManagerUtil;
import java.util.List;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Transactional
@Service
public class CaCorreoPacienteDAO {
  private static Logger logger = LoggerFactory.getLogger(CaCorreoPacienteDAO.class);
  
  private EntityManager em = null;
  
  public CaCorreoPaciente consultarCaCorreoPaciente(Long pacPacNumero, Long pacPacTipoIdentCodigo) {
    logger.info("Iniciando consultarCaCorreoPaciente - CaCorreoPacienteDAO ::::");
    this.em = EntityManagerUtil.getEntityManager();
    CaCorreoPaciente caCorreoPaciente = null;
    try {
      StringBuilder jpql = new StringBuilder();
      jpql.append("SELECT cacp FROM CaCorreoPaciente cacp");
      jpql.append(" WHERE cacp.pacPacNumero = ?1");
      jpql.append(" AND cacp.tcIdCodigo = ?2");
      List<CaCorreoPaciente> caCorreoPacientes = this.em.createQuery(jpql.toString()).setParameter(1, pacPacNumero).setParameter(2, pacPacTipoIdentCodigo).getResultList();
      if (caCorreoPacientes != null && !caCorreoPacientes.isEmpty())
        caCorreoPaciente = caCorreoPacientes.get(0); 
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
    this.em.close();
    return caCorreoPaciente;
  }
  
  public CaCorreoPaciente consultarCaCorreoPaciente(Long pacPacNumero) {
    CaCorreoPaciente var5;
    logger.info("Iniciando consultarCaCorreoPaciente - CaCorreoPacienteDAO ::::");
    this.em = EntityManagerUtil.getEntityManager();
    try {
      CaCorreoPaciente caCorreoPaciente = null;
      StringBuilder jpql = new StringBuilder();
      jpql.append("SELECT cacp FROM CaCorreoPaciente cacp");
      jpql.append(" WHERE cacp.pacPacNumero = ?1");
      List<CaCorreoPaciente> caCorreoPacientes = this.em.createQuery(jpql.toString()).setParameter(1, pacPacNumero).getResultList();
      if (caCorreoPacientes != null && !caCorreoPacientes.isEmpty())
        caCorreoPaciente = caCorreoPacientes.get(0); 
      var5 = caCorreoPaciente;
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
    return var5;
  }
}

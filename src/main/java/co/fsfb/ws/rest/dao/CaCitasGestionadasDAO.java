package co.fsfb.ws.rest.dao;

import co.fsfb.ws.rest.entidades.CaCitasGestionadas;
import co.fsfb.ws.rest.exception.DataException;
import co.fsfb.ws.rest.util.DataBaseUtil;
import co.fsfb.ws.rest.util.EntityManagerUtil;
import co.fsfb.ws.rest.vo.ConsultaDetalleCitaPacienteVO;
import co.fsfb.ws.rest.vo.ConsultaTrazabilidadCitaVO;
import co.fsfb.ws.rest.vo.FiltrosDetalleCitaPacienteVO;
import co.fsfb.ws.rest.vo.TrazabilidadCitaVO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Transactional
@Service
public class CaCitasGestionadasDAO {
  private static Logger logger = LoggerFactory.getLogger(CaCitasGestionadasDAO.class);
  
  private EntityManager em = null;
  
  public static Connection conn;
  
  public CaCitasGestionadas consultarCaCitasGestionadas(String pcaAgeHoraCitac, Date pcaAgeFechaCitac, String pcaAgeCodigRecep, Long pacPacNumero) {
    logger.info("Iniciando consultarCaCitasGestionadas(String,Date,String) - CaCitasGestionadasDAO ::::");
    this.em = EntityManagerUtil.getEntityManager();
    CaCitasGestionadas caCitasGestionada = null;
    try {
      StringBuilder jpql = new StringBuilder();
      jpql.append("SELECT ccg FROM CaCitasGestionadas ccg");
      jpql.append(" WHERE ccg.pcaAgeHoraCitac = ?1");
      jpql.append(" AND ccg.pcaAgeFechaCitac = ?2");
      jpql.append(" AND ccg.pcaAgeCodigRecep = ?3");
      jpql.append(" AND ccg.pacPacNumero = ?4");
      List<CaCitasGestionadas> caCitasGestionadas = this.em.createQuery(jpql.toString()).setParameter(1, pcaAgeHoraCitac).setParameter(2, pcaAgeFechaCitac).setParameter(3, pcaAgeCodigRecep).setParameter(4, pacPacNumero).getResultList();
      if (caCitasGestionadas != null && !caCitasGestionadas.isEmpty())
        caCitasGestionada = caCitasGestionadas.get(0); 
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
    this.em.close();
    return caCitasGestionada;
  }
  
  public List<CaCitasGestionadas> consultarCaCitasGestionadas(Date pcaAgeFechaCitacInicial, Date pcaAgeFechaCitacFinal, Long estado) {
    List<CaCitasGestionadas> var6;
    logger.info("Iniciando consultarCaCitasGestionadas(String,Date,Strin,Date,String) - CaCitasGestionadasDAO ::::");
    this.em = EntityManagerUtil.getEntityManager();
    try {
      StringBuilder jpql = new StringBuilder();
      jpql.append("SELECT ccg FROM CaCitasGestionadas ccg");
      jpql.append(" WHERE ccg.pcaAgeFechaCitac >= ?1");
      jpql.append(" AND ccg.pcaAgeFechaCitac <= ?2");
      jpql.append(" AND ccg.ecIdCodigo = ?3");
      List<CaCitasGestionadas> caCitasGestionadas = this.em.createQuery(jpql.toString()).setParameter(1, pcaAgeFechaCitacInicial).setParameter(2, pcaAgeFechaCitacFinal).setParameter(3, estado).getResultList();
      var6 = caCitasGestionadas;
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
    return var6;
  }
  
  public List<ConsultaTrazabilidadCitaVO> consultarTrazabilidadCita(Long cGIdCitaNumero) {
    List<ConsultaTrazabilidadCitaVO> listTraza;
    logger.info("Iniciando consultarTrazabilidadCita(Long) - CaCitasGestionadasDAO ::::");
    this.em = EntityManagerUtil.getEntityManager();
    ConsultaTrazabilidadCitaVO consultaTrazabilidadCitaVO = null;
    try {
      StringBuilder sql = new StringBuilder();
      sql.append("SELECT DISTINCT");
      sql.append(" CG.CG_ID_CITA_NUMERO \"ID cita\",");
      sql.append(" TO_CHAR(CG.PCA_AGE_FECHADIGIT,'DD/MM/YYYY') \"Fecha Asignacion cita\",");
      sql.append(" TO_CHAR(TC.CG_FECHA_PROCESO,'DD/MM/YYYY') \"Fecha Traza cita\",");
      sql.append(" TO_CHAR(TC.CG_FECHA_PROCESO,'HH24:MI:SS') \"Hora Traza cita\",");
      sql.append(" TRIM(USU.CAU_DESC_USUARIOS) \"Usuario Traza Cita\",");
      sql.append(" TRIM(ESCIT.EC_DESCRIPCION) \"Estado Cita\",");
      sql.append(" CG_ASISTENCIA");
      sql.append(" FROM CA_CITAS_GESTIONADAS CG");
      sql.append(" INNER JOIN CA_TRAZA_CITAS TC ON CG.CG_ID_CITA_NUMERO = TC.CG_ID_CITA_NUMERO");
      sql.append(" LEFT JOIN CA_OBS_TRAZA_CITAS OTC ON TC.CG_ID_CITA_NUMERO = OTC.CG_ID_CITA_NUMERO AND TC.EC_IDCODIGO = OTC.EC_IDCODIGO");
      sql.append(" LEFT JOIN CA_USUARIOS USU ON USU.PCA_AGE_CODIGRECEP = TC.PCA_AGE_CODIGRECEP");
      sql.append(" INNER JOIN CA_ESTADOS_CITAS ESCIT ON TC.EC_IDCODIGO = ESCIT.EC_IDCODIGO");
      sql.append(" WHERE CG.CG_ID_CITA_NUMERO = ?1");
      sql.append(" ORDER BY \"Fecha Traza cita\" DESC, \"Hora Traza cita\" DESC");
      List<Object[]> objects = this.em.createNativeQuery(sql.toString()).setParameter(1, cGIdCitaNumero).getResultList();
      listTraza = new ArrayList<>();
      if (objects != null && !objects.isEmpty()) {
        Iterator<Object[]> var6 = objects.iterator();
        while (var6.hasNext()) {
          Object[] object = var6.next();
          consultaTrazabilidadCitaVO = new ConsultaTrazabilidadCitaVO();
          consultaTrazabilidadCitaVO.setcGIdCitaNumero(Long.valueOf(object[0].toString()));
          consultaTrazabilidadCitaVO.setPcaAgeFechaDigit(object[1].toString());
          consultaTrazabilidadCitaVO.setCgFechaProceso(object[2].toString());
          consultaTrazabilidadCitaVO.setCgHoraProceso(object[3].toString());
          consultaTrazabilidadCitaVO.setCauDescUsuarios(object[4].toString());
          consultaTrazabilidadCitaVO.setEcDescripcion(object[5].toString());
          consultaTrazabilidadCitaVO.setEcAsistencia((object[6] != null) ? object[6].toString() : null);
          listTraza.add(consultaTrazabilidadCitaVO);
        } 
      } 
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
    return listTraza;
  }
  
  public List<ConsultaTrazabilidadCitaVO> consultarTrazabilidadCita(TrazabilidadCitaVO trazabilidadCitaVO) {
    logger.info("Iniciando consultarTrazabilidadCita(Long) - CaCitasGestionadasDAO ::::");
    this.em = EntityManagerUtil.getEntityManager();
    ConsultaTrazabilidadCitaVO consultaTrazabilidadCitaVO = null;
    ArrayList<ConsultaTrazabilidadCitaVO> listTraza = null;
    try {
      StringBuilder sql = new StringBuilder();
      sql.append("SELECT DISTINCT");
      sql.append(" CG.CG_ID_CITA_NUMERO \"ID cita\",");
      sql.append(" TO_CHAR(CG.PCA_AGE_FECHADIGIT,'DD/MM/YYYY') \"Fecha Asignacion cita\",");
      sql.append(" TO_CHAR(TC.CG_FECHA_PROCESO,'DD/MM/YYYY') \"Fecha Traza cita\",");
      sql.append(" TO_CHAR(TC.CG_FECHA_PROCESO,'HH24:MI:SS') \"Hora Traza cita\",");
      sql.append(" TRIM(USU.CAU_DESC_USUARIOS) \"Usuario Traza Cita\",");
      sql.append(" TRIM(ESCIT.EC_DESCRIPCION) \"Estado Cita\",");
      sql.append(" ESCIT.EC_IDCODIGO \"Estado Cita ID\",");
      sql.append(" TC.CG_FECHA_PROCESO\"Fecha proceso\",");
      sql.append(" CG_ASISTENCIA");
      sql.append(" FROM CA_CITAS_GESTIONADAS CG");
      sql.append(" INNER JOIN CA_TRAZA_CITAS TC ON CG.CG_ID_CITA_NUMERO = TC.CG_ID_CITA_NUMERO");
      sql.append(" LEFT JOIN CA_OBS_TRAZA_CITAS OTC ON TC.CG_ID_CITA_NUMERO = OTC.CG_ID_CITA_NUMERO AND TC.EC_IDCODIGO = OTC.EC_IDCODIGO");
      sql.append(" LEFT JOIN CA_USUARIOS USU ON USU.PCA_AGE_CODIGRECEP = TC.PCA_AGE_CODIGRECEP");
      sql.append(" INNER JOIN CA_ESTADOS_CITAS ESCIT ON TC.EC_IDCODIGO = ESCIT.EC_IDCODIGO");
      sql.append(" WHERE CG.PAC_PAC_NUMERO = ?1");
      sql.append(" AND CG.PCA_AGE_FECHACITAC = ?2");
      sql.append(" AND CG.PCA_AGE_HORACITAC = ?3");
      sql.append(" AND TRIM(CG.PCA_AGE_CODIGSERVI) = ?4");
      sql.append(" AND TRIM(CG.PCA_AGE_CODIGPROFE) = ?5");
      if (trazabilidadCitaVO.getCodConvenio()[0] != null)
        sql.append(" AND TRIM(CG.CON_CON_CODIGO) = ?6"); 
      sql.append(" ORDER BY TC.CG_FECHA_PROCESO DESC");
      Query query = null;
      query = this.em.createNativeQuery(sql.toString()).setParameter(1, trazabilidadCitaVO.getPacNum()).setParameter(2, (new SimpleDateFormat("yyyy-MM-dd")).parse(trazabilidadCitaVO.getFechaCita().trim())).setParameter(3, trazabilidadCitaVO.getHoraCita().trim()).setParameter(4, trazabilidadCitaVO.getCodServicio().trim()).setParameter(5, trazabilidadCitaVO.getCodProf().trim());
      if (trazabilidadCitaVO.getCodConvenio()[0] != null)
        query.setParameter(6, trazabilidadCitaVO.getCodConvenio()[0].trim()); 
      List<Object[]> objects = query.getResultList();
      listTraza = new ArrayList();
      if (objects != null && !objects.isEmpty()) {
        Iterator<Object[]> var7 = objects.iterator();
        while (var7.hasNext()) {
          Object[] object = var7.next();
          consultaTrazabilidadCitaVO = new ConsultaTrazabilidadCitaVO();
          consultaTrazabilidadCitaVO.setcGIdCitaNumero(Long.valueOf(object[0].toString()));
          consultaTrazabilidadCitaVO.setPcaAgeFechaDigit(object[1].toString());
          consultaTrazabilidadCitaVO.setCgFechaProceso(object[2].toString());
          consultaTrazabilidadCitaVO.setCgHoraProceso(object[3].toString());
          consultaTrazabilidadCitaVO.setCauDescUsuarios((object[4] != null) ? object[4].toString() : null);
          consultaTrazabilidadCitaVO.setEcDescripcion((object[5] != null) ? object[5].toString() : null);
          consultaTrazabilidadCitaVO.setEcIdCodigo((object[6] != null) ? Integer.valueOf(object[6].toString()) : null);
          consultaTrazabilidadCitaVO.setEcAsistencia((object[8] != null) ? object[8].toString() : null);
          listTraza.add(consultaTrazabilidadCitaVO);
        } 
      } 
    } catch (Exception var12) {
      logger.error("Error consultando traza", var12);
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
    this.em.close();
    return listTraza;
  }
  
  public ConsultaTrazabilidadCitaVO getEstado(TrazabilidadCitaVO trazabilidadCitaVO) {
    logger.info("Iniciando getEstado(Long) - CaCitasGestionadasDAO ::::");
    this.em = EntityManagerUtil.getEntityManager();
    ConsultaTrazabilidadCitaVO consultaTrazabilidadCitaVO = null;
    try {
      StringBuilder sql = new StringBuilder();
      sql.append(" SELECT DISTINCT CEC.EC_IDCODIGO, CEC.EC_DESCRIPCION ");
      sql.append(" FROM CA_CITAS_GESTIONADAS CG");
      sql.append(" INNER JOIN CA_TRAZA_CITAS CTC ON CG.CG_ID_CITA_NUMERO = CTC.CG_ID_CITA_NUMERO");
      sql.append(" INNER JOIN CA_ESTADOS_CITAS CEC ON CTC.EC_IDCODIGO = CEC.EC_IDCODIGO");
      sql.append(" WHERE CEC.EC_IDCODIGO = (SELECT MAX(EC_IDCODIGO) FROM CA_TRAZA_CITAS WHERE CG_ID_CITA_NUMERO = CTC.CG_ID_CITA_NUMERO)");
      sql.append(" AND CG.PAC_PAC_NUMERO = ?1");
      sql.append(" AND CG.PCA_AGE_FECHACITAC = ?2");
      sql.append(" AND CG.PCA_AGE_HORACITAC = ?3");
      sql.append(" AND TRIM(CG.PCA_AGE_CODIGSERVI) = ?4");
      sql.append(" AND TRIM(CG.PCA_AGE_CODIGPROFE) = ?5");
      Query query = this.em.createNativeQuery(sql.toString()).setParameter(1, trazabilidadCitaVO.getPacNum()).setParameter(2, trazabilidadCitaVO.getFechaCita().trim()).setParameter(3, trazabilidadCitaVO.getHoraCita().trim()).setParameter(4, trazabilidadCitaVO.getCodEspecialidad().trim()).setParameter(5, trazabilidadCitaVO.getCodProf().trim());
      List<Object[]> objects = query.getResultList();
      if (objects != null && !objects.isEmpty()) {
        consultaTrazabilidadCitaVO = new ConsultaTrazabilidadCitaVO();
        consultaTrazabilidadCitaVO.setEcIdCodigo((((Object[])objects.get(0))[0] != null) ? Integer.valueOf(((Object[])objects.get(0))[0].toString()) : null);
        consultaTrazabilidadCitaVO.setEcDescripcion((((Object[])objects.get(0))[1] != null) ? ((Object[])objects.get(0))[1].toString() : null);
      } 
    } finally {
      if (this.em.isOpen())
        this.em.close(); 
    } 
    this.em.close();
    return consultaTrazabilidadCitaVO;
  }
  
  public ConsultaDetalleCitaPacienteVO consultarDetalleCitaPaciente(FiltrosDetalleCitaPacienteVO filtrosDetalleCitaPacienteVO) throws DataException {
    logger.info("Iniciando consultarDetalleCitaPaciente(Long) - CaCitasGestionadasDAO ::::");
    conn = DataBaseUtil.inicializarDesportal();
    ResultSet set = null;
    ConsultaDetalleCitaPacienteVO consultaDetalleCitaPacienteVO = null;
    try {
      StringBuilder sql = new StringBuilder();
      sql.append("SELECT DISTINCT");
      sql.append(" TRIM(PAC.PAC_PAC_NOMBRE)||' '||TRIM(PAC.PAC_PAC_APELLPATER)||' '||TRIM(PAC.PAC_PAC_APELLMATER) \"Nombre Completo\",");
      sql.append(" TRIM(TIP.TIPIDAV)  \"Tipo de Documento\",");
      sql.append(" TRIM(PAC.PAC_PAC_RUT) \"Documento\",");
      sql.append(" TRIM(PAC.PAC_PAC_FONO) \"Telefono\",");
      sql.append(" LOWER(TRIM(PAC.DONURL)) \"Correo Electronico\",");
      sql.append(" TO_CHAR(AGEND.PCA_AGE_FECHACITAC,'DD/MM/YYYY') \"Fecha Cita\",");
      sql.append(" TRIM(PCA_AGE_HORACITAC) \"Hora Cita\",");
      sql.append(" TRIM(SER.SER_SER_DESCRIPCIO) \"Especialidad\",");
      sql.append(" TRIM(FUNCHSPGETPROF(1,AGEND.PCA_AGE_CODIGPROFE)) \"Profesional\",");
      sql.append(" TRIM(AGEND.PCA_AGE_OBJETO) \"Consultorio\",");
      sql.append(" TRIM(PREST.PRE_PRE_DESCRIPCIO) \"Prestacion\",");
      sql.append(" TRIM(CA.NOMBRECENTROATEN) \"Centro Operativo\",");
      sql.append(" UPPER(TRIM(CONV.CON_CON_DESCRIPCIO)) \"Convenio\",");
      sql.append(" ADMSALUD.GLBGETNOMUSR(AGEND.PCA_AGE_CODIGRECEP) \"Usuario que asigna cita\",");
      sql.append(" TO_CHAR(AGEND.PCA_AGE_FECHADIGIT,'DD/MM/YYYY') \"Fecha de Asignacion\"");
      sql.append(" FROM ADMSALUD.PCA_AGENDA AGEND");
      sql.append(" INNER JOIN ADMSALUD.PAC_PACIENTE PAC ON TRIM(AGEND.PCA_AGE_NUMERPACIE) = TRIM(PAC.PAC_PAC_NUMERO)");
      sql.append(" LEFT JOIN ADMSALUD.TAB_TIPOIDENT TIP ON TRIM(PAC.PAC_PAC_TIPOIDENTCODIGO) = TRIM(TIP.TAB_TIPOIDENTCODIGO)");
      sql.append(" INNER JOIN ADMSALUD.SER_SERVICIOS SER ON TRIM(AGEND.PCA_AGE_CODIGSERVI) = TRIM(SER.SER_SER_CODIGO)");
      sql.append(" INNER JOIN ADMSALUD.RPA_FORCIT FORCIT ON TRIM(AGEND.PCA_AGE_TIPOFORMU) = TRIM(FORCIT.RPA_FOR_TIPOFORMU) AND TRIM(AGEND.PCA_AGE_NUMERFORMU) = TRIM(FORCIT.RPA_FOR_NUMERFORMU)");
      sql.append(" LEFT JOIN ADMSALUD.CON_CONVENIO CONV ON TRIM(FORCIT.CON_CON_CODIGO) = TRIM(CONV.CON_CON_CODIGO)");
      sql.append(" LEFT JOIN ADMSALUD.PRE_PRESTACION PREST ON TRIM(FORCIT.PRE_PRE_CODIGO) = TRIM(PREST.PRE_PRE_CODIGO)");
      sql.append(" LEFT JOIN ADMSALUD.TAB_CENTROSATENC CA ON TRIM(AGEND.PCA_AGE_LUGAR) = TRIM(CA.CODIGOCENTROATEN)");
      sql.append(" INNER JOIN ADMSALUD.SER_OBJETOS OBJ ON TRIM(AGEND.PCA_AGE_TIPOOBJET) = TRIM(OBJ.SER_REC_TIPO) AND TRIM(AGEND.PCA_AGE_OBJETO) = TRIM(OBJ.SER_OBJ_CODIGO)");
      sql.append(" WHERE PAC.PAC_PAC_NUMERO = :PAC_PAC_NUMERO");
      if (filtrosDetalleCitaPacienteVO.getTipoDocumento() != null)
        sql.append(" AND TIP.TAB_TIPOIDENTCODIGO = :TAB_TIPOIDENTCODIGO"); 
      if (filtrosDetalleCitaPacienteVO.getDocumento() != null)
        sql.append(" AND TRIM(PAC.PAC_PAC_RUT) = :PAC_PAC_RUT"); 
      if (filtrosDetalleCitaPacienteVO.getFechaCita() != null)
        sql.append(" AND AGEND.PCA_AGE_FECHACITAC = :PCA_AGE_FECHACITAC"); 
      if (filtrosDetalleCitaPacienteVO.getAgeCodigoServi() != null)
        sql.append(" AND TRIM(AGEND.PCA_AGE_CODIGSERVI) = :PCA_AGE_CODIGSERVI"); 
      if (filtrosDetalleCitaPacienteVO.getCodigoCentroAten() != null)
        sql.append(" AND TRIM(CA.CODIGOCENTROATEN) = :CODIGOCENTROATEN"); 
      if (filtrosDetalleCitaPacienteVO.getCodigoProfe() != null)
        sql.append(" AND TRIM(AGEND.PCA_AGE_CODIGPROFE) = :PCA_AGE_CODIGPROFE"); 
      if (filtrosDetalleCitaPacienteVO.getConCodigo() != null)
        sql.append(" AND TRIM(CONV.CON_CON_CODIGO) = :CON_CON_CODIGO"); 
      if (filtrosDetalleCitaPacienteVO.getCodigoRecep() != null)
        sql.append(" AND AGEND.PCA_AGE_CODIGRECEP = :PCA_AGE_CODIGRECEP"); 
      System.out.println(sql.toString());
      PreparedStatement stmt = conn.prepareStatement(sql.toString());
      stmt.setString(1, filtrosDetalleCitaPacienteVO.getPacNumero());
      if (filtrosDetalleCitaPacienteVO.getTipoDocumento() != null)
        stmt.setString(2, filtrosDetalleCitaPacienteVO.getTipoDocumento()); 
      if (filtrosDetalleCitaPacienteVO.getDocumento() != null)
        stmt.setString(3, filtrosDetalleCitaPacienteVO.getDocumento()); 
      if (filtrosDetalleCitaPacienteVO.getFechaCita() != null)
        stmt.setString(4, filtrosDetalleCitaPacienteVO.getFechaCita()); 
      if (filtrosDetalleCitaPacienteVO.getAgeCodigoServi() != null)
        stmt.setString(5, filtrosDetalleCitaPacienteVO.getAgeCodigoServi()); 
      if (filtrosDetalleCitaPacienteVO.getCodigoCentroAten() != null)
        stmt.setString(6, filtrosDetalleCitaPacienteVO.getCodigoCentroAten()); 
      if (filtrosDetalleCitaPacienteVO.getCodigoProfe() != null)
        stmt.setString(7, filtrosDetalleCitaPacienteVO.getCodigoProfe()); 
      if (filtrosDetalleCitaPacienteVO.getConCodigo() != null)
        stmt.setString(8, filtrosDetalleCitaPacienteVO.getConCodigo()); 
      if (filtrosDetalleCitaPacienteVO.getCodigoRecep() != null)
        stmt.setString(9, filtrosDetalleCitaPacienteVO.getCodigoRecep()); 
      set = stmt.executeQuery();
      ArrayList<ConsultaDetalleCitaPacienteVO> consultaDetalleCitaPacienteVOs = new ArrayList();
      while (set.next()) {
        ConsultaDetalleCitaPacienteVO detalleCitaPacienteVO = new ConsultaDetalleCitaPacienteVO();
        detalleCitaPacienteVO.setNombreCompleto(set.getString(1));
        detalleCitaPacienteVO.setTipoDocumento(set.getString(2));
        detalleCitaPacienteVO.setDocumento(set.getString(3));
        detalleCitaPacienteVO.setTelefono(set.getString(4));
        detalleCitaPacienteVO.setCorreoElectronico(set.getString(5));
        detalleCitaPacienteVO.setFechaCita(set.getString(6));
        detalleCitaPacienteVO.setHoraCita(set.getString(7));
        detalleCitaPacienteVO.setEspecialidad(set.getString(8));
        detalleCitaPacienteVO.setProfesional(set.getString(9));
        detalleCitaPacienteVO.setConsultorio(set.getString(10));
        detalleCitaPacienteVO.setPrestacion(set.getString(11));
        detalleCitaPacienteVO.setCentroOperativo(set.getString(12));
        detalleCitaPacienteVO.setConvenio(set.getString(13));
        detalleCitaPacienteVO.setUsuarioAsignaCita(set.getString(14));
        detalleCitaPacienteVO.setFechaAsignacion(set.getString(15));
        consultaDetalleCitaPacienteVOs.add(detalleCitaPacienteVO);
      } 
      consultaDetalleCitaPacienteVO = consultaDetalleCitaPacienteVOs.get(0);
      return consultaDetalleCitaPacienteVO;
    } catch (Exception var15) {
      logger.error("Error consultando citas", var15);
      throw new DataException(var15.getMessage(), new Date(), var15.getMessage().toString());
    } finally {
      try {
        if (set != null)
          set.close(); 
        conn.close();
      } catch (Exception var14) {
        throw new DataException(var14.getMessage(), new Date(), var14.getMessage().toString());
      } 
    } 
  }
}

package co.fsfb.ws.rest.mapper;

import co.fsfb.ws.rest.entidades.CaGestionAutorizacionCita;
import co.fsfb.ws.rest.vo.GestionAutorizacionCitaVO;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class GestionAutorizacionCitaMapperImpl implements GestionAutorizacionCitaMapper {
  public CaGestionAutorizacionCita voToEntity(GestionAutorizacionCitaVO gestionAutorizacionCitaVO) {
    if (gestionAutorizacionCitaVO == null)
      return null; 
    CaGestionAutorizacionCita caGestionAutorizacionCita = new CaGestionAutorizacionCita();
    try {
      if (gestionAutorizacionCitaVO.getGauFechaAutorizacion() != null)
        caGestionAutorizacionCita.setGauFechaAutorizacion((new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")).parse(gestionAutorizacionCitaVO.getGauFechaAutorizacion())); 
    } catch (ParseException e) {
      throw new RuntimeException(e);
    } 
    try {
      if (gestionAutorizacionCitaVO.getGauFechaVencAutorizacion() != null)
        caGestionAutorizacionCita.setGauFechaVencAutorizacion((new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")).parse(gestionAutorizacionCitaVO.getGauFechaVencAutorizacion())); 
    } catch (ParseException e) {
      throw new RuntimeException(e);
    } 
    caGestionAutorizacionCita.setGauNombreAutorizador(gestionAutorizacionCitaVO.getGauNombreAutorizador());
    caGestionAutorizacionCita.setGauTelefonoAutorizador(gestionAutorizacionCitaVO.getGauTelefonoAutorizador());
    caGestionAutorizacionCita.setGauAutorizaServ(gestionAutorizacionCitaVO.getGauAutorizaServ());
    caGestionAutorizacionCita.setMnaIdcodigo(gestionAutorizacionCitaVO.getMnaIdcodigo());
    caGestionAutorizacionCita.setOmnDesc(gestionAutorizacionCitaVO.getOmnDesc());
    caGestionAutorizacionCita.setGauCodigoAutorizacion(gestionAutorizacionCitaVO.getGauCodigoAutorizacion());
    caGestionAutorizacionCita.setGauVigenciaAutorizacion(gestionAutorizacionCitaVO.getGauVigenciaAutorizacion());
    caGestionAutorizacionCita.setGauValorPrestacion(gestionAutorizacionCitaVO.getGauValorPrestacion());
    caGestionAutorizacionCita.setGauCostoConvenio(gestionAutorizacionCitaVO.getGauCostoConvenio());
    caGestionAutorizacionCita.setGauCostoPac(gestionAutorizacionCitaVO.getGauCostoPac());
    caGestionAutorizacionCita.setPcaAgeCodigoRecep(gestionAutorizacionCitaVO.getPcaAgeCodigoRecep());
    caGestionAutorizacionCita.setCgFechaProceso(gestionAutorizacionCitaVO.getCgFechaProceso());
    return caGestionAutorizacionCita;
  }
}

package co.fsfb.ws.rest.mapper;

import co.fsfb.ws.rest.entidades.CaDetalleOrdenesMedicas;
import co.fsfb.ws.rest.entidades.CaGestionAutorizacion;
import co.fsfb.ws.rest.entidades.CaOrdenesMedicas;
import co.fsfb.ws.rest.entidades.CaPrestacionesOrdMed;
import co.fsfb.ws.rest.entidades.CaTrazaGestContinuidad;
import co.fsfb.ws.rest.vo.DetalleOrdenMedicaVO;
import co.fsfb.ws.rest.vo.GestionAutorizacionVO;
import co.fsfb.ws.rest.vo.GestionContinuidadVO;
import co.fsfb.ws.rest.vo.OrdenMedicaVO;
import co.fsfb.ws.rest.vo.PrestacionesOrdMedVO;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

public class OrdenMedicaMapperImpl implements OrdenMedicaMapper {
  public OrdenMedicaVO entityToVO(CaOrdenesMedicas caOrdenesMedicas) {
    if (caOrdenesMedicas == null)
      return null; 
    OrdenMedicaVO ordenMedicaVO = new OrdenMedicaVO();
    ordenMedicaVO.setOrmIdOrdmNumero(caOrdenesMedicas.getOrmIdOrdmNumero());
    ordenMedicaVO.setPacPacNumero(caOrdenesMedicas.getPacPacNumero());
    ordenMedicaVO.setPacPacTipoIdentCodigo(caOrdenesMedicas.getPacPacTipoIdentCodigo());
    ordenMedicaVO.setPacPacRut(caOrdenesMedicas.getPacPacRut());
    ordenMedicaVO.setPcaAgeCodigoRecep(caOrdenesMedicas.getPcaAgeCodigoRecep());
    ordenMedicaVO.setCgFechaProceso(caOrdenesMedicas.getCgFechaProceso());
    ordenMedicaVO.setOrmFilename(caOrdenesMedicas.getOrmFilename());
    ordenMedicaVO.setCaDetalleOrdenesMedicas(caDetalleOrdenesMedicasToDetalleOrdenMedicaVO(caOrdenesMedicas.getCaDetalleOrdenesMedicas()));
    ordenMedicaVO.setCaPrestacionesOrdMed(caPrestacionesOrdMedListToPrestacionesOrdMedVOList(caOrdenesMedicas.getCaPrestacionesOrdMed()));
    if (caOrdenesMedicas.getFechaRegistroFile() != null)
      ordenMedicaVO.setFechaRegistroFile((new SimpleDateFormat()).format(caOrdenesMedicas.getFechaRegistroFile())); 
    return ordenMedicaVO;
  }
  
  protected DetalleOrdenMedicaVO caDetalleOrdenesMedicasToDetalleOrdenMedicaVO(CaDetalleOrdenesMedicas caDetalleOrdenesMedicas) {
    if (caDetalleOrdenesMedicas == null)
      return null; 
    DetalleOrdenMedicaVO detalleOrdenMedicaVO = new DetalleOrdenMedicaVO();
    detalleOrdenMedicaVO.setSerEspCodigo(caDetalleOrdenesMedicas.getSerEspCodigo());
    detalleOrdenMedicaVO.setSerSerCodSubEspe(caDetalleOrdenesMedicas.getSerSerCodSubEspe());
    detalleOrdenMedicaVO.setSerSerCodigo(caDetalleOrdenesMedicas.getSerSerCodigo());
    detalleOrdenMedicaVO.setPcaAgeLugar(caDetalleOrdenesMedicas.getPcaAgeLugar());
    detalleOrdenMedicaVO.setDiaAgrCodigo(caDetalleOrdenesMedicas.getDiaAgrCodigo());
    detalleOrdenMedicaVO.setDiaOtroAgr(caDetalleOrdenesMedicas.getDiaOtroAgr());
    detalleOrdenMedicaVO.setConConCodigo(caDetalleOrdenesMedicas.getConConCodigo());
    detalleOrdenMedicaVO.setPcaAgeCodigProfe(caDetalleOrdenesMedicas.getPcaAgeCodigProfe());
    detalleOrdenMedicaVO.setPcaAgeCodigRecep(caDetalleOrdenesMedicas.getPcaAgeCodigRecep());
    if (caDetalleOrdenesMedicas.getDomShaFechaProceso() != null)
      detalleOrdenMedicaVO.setDomShaFechaProceso((new SimpleDateFormat()).format(caDetalleOrdenesMedicas.getDomShaFechaProceso())); 
    if (caDetalleOrdenesMedicas.getDorFechaOrdenm() != null)
      detalleOrdenMedicaVO.setDorFechaOrdenm(LocalDateTime.ofInstant(caDetalleOrdenesMedicas.getDorFechaOrdenm().toInstant(), ZoneOffset.UTC).toLocalDate()); 
    return detalleOrdenMedicaVO;
  }
  
  protected GestionContinuidadVO caTrazaGestContinuidadToGestionContinuidadVO(CaTrazaGestContinuidad caTrazaGestContinuidad) {
    if (caTrazaGestContinuidad == null)
      return null; 
    GestionContinuidadVO gestionContinuidadVO = new GestionContinuidadVO();
    gestionContinuidadVO.setPomIdPrestOrdm(caTrazaGestContinuidad.getPomIdPrestOrdm());
    gestionContinuidadVO.setGcoIdCodigoEstado(caTrazaGestContinuidad.getGcoIdCodigoEstado());
    gestionContinuidadVO.setGcoIdCodigoMotivo(caTrazaGestContinuidad.getGcoIdCodigoMotivo());
    gestionContinuidadVO.setGcoDirecPaciente(caTrazaGestContinuidad.isGcoDirecPaciente());
    gestionContinuidadVO.setGcoRealizoAgendamiento(caTrazaGestContinuidad.isGcoRealizoAgendamiento());
    gestionContinuidadVO.setGcoObservaciones(caTrazaGestContinuidad.getGcoObservaciones());
    gestionContinuidadVO.setPcaAgeCodigRecep(caTrazaGestContinuidad.getPcaAgeCodigRecep());
    return gestionContinuidadVO;
  }
  
  protected GestionAutorizacionVO caGestionAutorizacionToGestionAutorizacionVO(CaGestionAutorizacion caGestionAutorizacion) {
    if (caGestionAutorizacion == null)
      return null; 
    GestionAutorizacionVO gestionAutorizacionVO = new GestionAutorizacionVO();
    gestionAutorizacionVO.setPomIdPrestOrdm(caGestionAutorizacion.getPomIdPrestOrdm());
    gestionAutorizacionVO.setPacPacNumero(caGestionAutorizacion.getPacPacNumero());
    gestionAutorizacionVO.setGauNombreAutorizador(caGestionAutorizacion.getGauNombreAutorizador());
    gestionAutorizacionVO.setGauTelefonoAutorizador(caGestionAutorizacion.getGauTelefonoAutorizador());
    gestionAutorizacionVO.setGauAutorizaServ(caGestionAutorizacion.getGauAutorizaServ());
    gestionAutorizacionVO.setMnaIdcodigo(caGestionAutorizacion.getMnaIdcodigo());
    gestionAutorizacionVO.setOmnDesc(caGestionAutorizacion.getOmnDesc());
    gestionAutorizacionVO.setGauCodigoAutorizacion(caGestionAutorizacion.getGauCodigoAutorizacion());
    if (caGestionAutorizacion.getGauFechaAutorizacion() != null)
      gestionAutorizacionVO.setGauFechaAutorizacion((new SimpleDateFormat()).format(caGestionAutorizacion.getGauFechaAutorizacion())); 
    if (caGestionAutorizacion.getGauFechaVencAutorizacion() != null)
      gestionAutorizacionVO.setGauFechaVencAutorizacion((new SimpleDateFormat()).format(caGestionAutorizacion.getGauFechaVencAutorizacion())); 
    gestionAutorizacionVO.setGauVigenciaAutorizacion(caGestionAutorizacion.getGauVigenciaAutorizacion());
    gestionAutorizacionVO.setGauValorPrestacion(caGestionAutorizacion.getGauValorPrestacion());
    gestionAutorizacionVO.setGauCostoConvenio(caGestionAutorizacion.getGauCostoConvenio());
    gestionAutorizacionVO.setGauCostoPac(caGestionAutorizacion.getGauCostoPac());
    gestionAutorizacionVO.setPcaAgeCodigoRecep(caGestionAutorizacion.getPcaAgeCodigoRecep());
    gestionAutorizacionVO.setCgFechaProceso(caGestionAutorizacion.getCgFechaProceso());
    gestionAutorizacionVO.setOgaDescripcion(caGestionAutorizacion.getOgaDescripcion());
    return gestionAutorizacionVO;
  }
  
  protected PrestacionesOrdMedVO caPrestacionesOrdMedToPrestacionesOrdMedVO(CaPrestacionesOrdMed caPrestacionesOrdMed) {
    if (caPrestacionesOrdMed == null)
      return null; 
    PrestacionesOrdMedVO prestacionesOrdMedVO = new PrestacionesOrdMedVO();
    prestacionesOrdMedVO.setPomIdPrestOrdm(caPrestacionesOrdMed.getPomIdPrestOrdm());
    prestacionesOrdMedVO.setSerSerCodigo(caPrestacionesOrdMed.getSerSerCodigo());
    prestacionesOrdMedVO.setSerSerDesc(caPrestacionesOrdMed.getSerSerDesc());
    prestacionesOrdMedVO.setPrePreCodigo(caPrestacionesOrdMed.getPrePreCodigo());
    prestacionesOrdMedVO.setPrePreDesc(caPrestacionesOrdMed.getPrePreDesc());
    prestacionesOrdMedVO.setPcaAgeCodigRecep(caPrestacionesOrdMed.getPcaAgeCodigRecep());
    prestacionesOrdMedVO.setCgFechaProceso(caPrestacionesOrdMed.getCgFechaProceso());
    prestacionesOrdMedVO.setCaTrazaGestContinuidad(caTrazaGestContinuidadToGestionContinuidadVO(caPrestacionesOrdMed.getCaTrazaGestContinuidad()));
    prestacionesOrdMedVO.setCaGestionAutorizacion(caGestionAutorizacionToGestionAutorizacionVO(caPrestacionesOrdMed.getCaGestionAutorizacion()));
    return prestacionesOrdMedVO;
  }
  
  protected List<PrestacionesOrdMedVO> caPrestacionesOrdMedListToPrestacionesOrdMedVOList(List<CaPrestacionesOrdMed> list) {
    if (list == null)
      return null; 
    List<PrestacionesOrdMedVO> list1 = new ArrayList<>(list.size());
    for (CaPrestacionesOrdMed caPrestacionesOrdMed : list)
      list1.add(caPrestacionesOrdMedToPrestacionesOrdMedVO(caPrestacionesOrdMed)); 
    return list1;
  }
}

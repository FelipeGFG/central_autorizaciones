package co.fsfb.ws.rest.mapper;

import co.fsfb.ws.rest.entidades.CaGestionAutorizacion;
import co.fsfb.ws.rest.vo.GestionAutorizacionVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.NullValueMappingStrategy;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL)
public interface GestionAutorizacionMapper {
  public static final GestionAutorizacionMapper INSTANCE = (GestionAutorizacionMapper)Mappers.getMapper(GestionAutorizacionMapper.class);
  
  @Mappings({@Mapping(source = "gauFechaAutorizacion", target = "gauFechaAutorizacion", dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"), @Mapping(source = "gauFechaVencAutorizacion", target = "gauFechaVencAutorizacion", dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"), @Mapping(source = "cgFechaProceso", target = "cgFechaProceso", dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")})
  CaGestionAutorizacion voToEntity(GestionAutorizacionVO paramGestionAutorizacionVO);
}

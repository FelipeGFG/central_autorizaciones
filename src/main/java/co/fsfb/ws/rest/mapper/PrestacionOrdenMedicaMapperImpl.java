package co.fsfb.ws.rest.mapper;

import co.fsfb.ws.rest.entidades.CaGestionAutorizacion;
import co.fsfb.ws.rest.entidades.CaPrestacionesOrdMed;
import co.fsfb.ws.rest.entidades.CaTrazaGestContinuidad;
import co.fsfb.ws.rest.vo.GestionAutorizacionVO;
import co.fsfb.ws.rest.vo.GestionContinuidadVO;
import co.fsfb.ws.rest.vo.PrestacionesOrdMedVO;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class PrestacionOrdenMedicaMapperImpl implements PrestacionOrdenMedicaMapper {
  public List<PrestacionesOrdMedVO> entityToVO(List<CaPrestacionesOrdMed> prestacionesOrdMedVO) {
    if (prestacionesOrdMedVO == null)
      return null; 
    List<PrestacionesOrdMedVO> list = new ArrayList<>(prestacionesOrdMedVO.size());
    for (CaPrestacionesOrdMed caPrestacionesOrdMed : prestacionesOrdMedVO)
      list.add(entityToVO(caPrestacionesOrdMed)); 
    return list;
  }
  
  public PrestacionesOrdMedVO entityToVO(CaPrestacionesOrdMed prestacionesOrdMedVO) {
    if (prestacionesOrdMedVO == null)
      return null; 
    PrestacionesOrdMedVO prestacionesOrdMedVO1 = new PrestacionesOrdMedVO();
    prestacionesOrdMedVO1.setPomIdPrestOrdm(prestacionesOrdMedVO.getPomIdPrestOrdm());
    prestacionesOrdMedVO1.setSerSerCodigo(prestacionesOrdMedVO.getSerSerCodigo());
    prestacionesOrdMedVO1.setSerSerDesc(prestacionesOrdMedVO.getSerSerDesc());
    prestacionesOrdMedVO1.setPrePreCodigo(prestacionesOrdMedVO.getPrePreCodigo());
    prestacionesOrdMedVO1.setPrePreDesc(prestacionesOrdMedVO.getPrePreDesc());
    prestacionesOrdMedVO1.setPcaAgeCodigRecep(prestacionesOrdMedVO.getPcaAgeCodigRecep());
    prestacionesOrdMedVO1.setCgFechaProceso(prestacionesOrdMedVO.getCgFechaProceso());
    prestacionesOrdMedVO1.setCaTrazaGestContinuidad(caTrazaGestContinuidadToGestionContinuidadVO(prestacionesOrdMedVO.getCaTrazaGestContinuidad()));
    prestacionesOrdMedVO1.setCaGestionAutorizacion(caGestionAutorizacionToGestionAutorizacionVO(prestacionesOrdMedVO.getCaGestionAutorizacion()));
    return prestacionesOrdMedVO1;
  }
  
  protected GestionContinuidadVO caTrazaGestContinuidadToGestionContinuidadVO(CaTrazaGestContinuidad caTrazaGestContinuidad) {
    if (caTrazaGestContinuidad == null)
      return null; 
    GestionContinuidadVO gestionContinuidadVO = new GestionContinuidadVO();
    gestionContinuidadVO.setPomIdPrestOrdm(caTrazaGestContinuidad.getPomIdPrestOrdm());
    gestionContinuidadVO.setGcoIdCodigoEstado(caTrazaGestContinuidad.getGcoIdCodigoEstado());
    gestionContinuidadVO.setGcoIdCodigoMotivo(caTrazaGestContinuidad.getGcoIdCodigoMotivo());
    gestionContinuidadVO.setGcoDirecPaciente(caTrazaGestContinuidad.isGcoDirecPaciente());
    gestionContinuidadVO.setGcoRealizoAgendamiento(caTrazaGestContinuidad.isGcoRealizoAgendamiento());
    gestionContinuidadVO.setGcoObservaciones(caTrazaGestContinuidad.getGcoObservaciones());
    gestionContinuidadVO.setPcaAgeCodigRecep(caTrazaGestContinuidad.getPcaAgeCodigRecep());
    return gestionContinuidadVO;
  }
  
  protected GestionAutorizacionVO caGestionAutorizacionToGestionAutorizacionVO(CaGestionAutorizacion caGestionAutorizacion) {
    if (caGestionAutorizacion == null)
      return null; 
    GestionAutorizacionVO gestionAutorizacionVO = new GestionAutorizacionVO();
    gestionAutorizacionVO.setPomIdPrestOrdm(caGestionAutorizacion.getPomIdPrestOrdm());
    gestionAutorizacionVO.setPacPacNumero(caGestionAutorizacion.getPacPacNumero());
    gestionAutorizacionVO.setGauNombreAutorizador(caGestionAutorizacion.getGauNombreAutorizador());
    gestionAutorizacionVO.setGauTelefonoAutorizador(caGestionAutorizacion.getGauTelefonoAutorizador());
    gestionAutorizacionVO.setGauAutorizaServ(caGestionAutorizacion.getGauAutorizaServ());
    gestionAutorizacionVO.setMnaIdcodigo(caGestionAutorizacion.getMnaIdcodigo());
    gestionAutorizacionVO.setOmnDesc(caGestionAutorizacion.getOmnDesc());
    gestionAutorizacionVO.setGauCodigoAutorizacion(caGestionAutorizacion.getGauCodigoAutorizacion());
    if (caGestionAutorizacion.getGauFechaAutorizacion() != null)
      gestionAutorizacionVO.setGauFechaAutorizacion((new SimpleDateFormat()).format(caGestionAutorizacion.getGauFechaAutorizacion())); 
    if (caGestionAutorizacion.getGauFechaVencAutorizacion() != null)
      gestionAutorizacionVO.setGauFechaVencAutorizacion((new SimpleDateFormat()).format(caGestionAutorizacion.getGauFechaVencAutorizacion())); 
    gestionAutorizacionVO.setGauVigenciaAutorizacion(caGestionAutorizacion.getGauVigenciaAutorizacion());
    gestionAutorizacionVO.setGauValorPrestacion(caGestionAutorizacion.getGauValorPrestacion());
    gestionAutorizacionVO.setGauCostoConvenio(caGestionAutorizacion.getGauCostoConvenio());
    gestionAutorizacionVO.setGauCostoPac(caGestionAutorizacion.getGauCostoPac());
    gestionAutorizacionVO.setPcaAgeCodigoRecep(caGestionAutorizacion.getPcaAgeCodigoRecep());
    gestionAutorizacionVO.setCgFechaProceso(caGestionAutorizacion.getCgFechaProceso());
    gestionAutorizacionVO.setOgaDescripcion(caGestionAutorizacion.getOgaDescripcion());
    return gestionAutorizacionVO;
  }
}

package co.fsfb.ws.rest.mapper;

import co.fsfb.ws.rest.entidades.CaPrestacionesOrdMed;
import co.fsfb.ws.rest.vo.PrestacionesOrdMedVO;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueMappingStrategy;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL)
public interface PrestacionOrdenMedicaMapper {
  public static final PrestacionOrdenMedicaMapper INSTANCE = (PrestacionOrdenMedicaMapper)Mappers.getMapper(PrestacionOrdenMedicaMapper.class);
  
  List<PrestacionesOrdMedVO> entityToVO(List<CaPrestacionesOrdMed> paramList);
  
  PrestacionesOrdMedVO entityToVO(CaPrestacionesOrdMed paramCaPrestacionesOrdMed);
}

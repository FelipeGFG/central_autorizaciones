package co.fsfb.ws.rest.mapper;

import co.fsfb.ws.rest.entidades.CaGestionAutorizacionCita;
import co.fsfb.ws.rest.vo.GestionAutorizacionCitaVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.NullValueMappingStrategy;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL)
public interface GestionAutorizacionCitaMapper {
  public static final GestionAutorizacionCitaMapper INSTANCE = (GestionAutorizacionCitaMapper)Mappers.getMapper(GestionAutorizacionCitaMapper.class);
  
  @Mappings({@Mapping(source = "gauFechaAutorizacion", target = "gauFechaAutorizacion", dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"), @Mapping(source = "gauFechaVencAutorizacion", target = "gauFechaVencAutorizacion", dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")})
  CaGestionAutorizacionCita voToEntity(GestionAutorizacionCitaVO paramGestionAutorizacionCitaVO);
}

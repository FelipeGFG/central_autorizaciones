package co.fsfb.ws.rest.mapper;

import co.fsfb.ws.rest.entidades.CaGestionAutorizacion;
import co.fsfb.ws.rest.vo.GestionAutorizacionVO;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class GestionAutorizacionMapperImpl implements GestionAutorizacionMapper {
  public CaGestionAutorizacion voToEntity(GestionAutorizacionVO gestionAutorizacionVO) {
    if (gestionAutorizacionVO == null)
      return null; 
    CaGestionAutorizacion caGestionAutorizacion = new CaGestionAutorizacion();
    try {
      if (gestionAutorizacionVO.getGauFechaAutorizacion() != null)
        caGestionAutorizacion.setGauFechaAutorizacion((new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")).parse(gestionAutorizacionVO.getGauFechaAutorizacion())); 
    } catch (ParseException e) {
      throw new RuntimeException(e);
    } 
    try {
      if (gestionAutorizacionVO.getGauFechaVencAutorizacion() != null)
        caGestionAutorizacion.setGauFechaVencAutorizacion((new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")).parse(gestionAutorizacionVO.getGauFechaVencAutorizacion())); 
    } catch (ParseException e) {
      throw new RuntimeException(e);
    } 
    caGestionAutorizacion.setCgFechaProceso(gestionAutorizacionVO.getCgFechaProceso());
    caGestionAutorizacion.setPomIdPrestOrdm(gestionAutorizacionVO.getPomIdPrestOrdm());
    caGestionAutorizacion.setPacPacNumero(gestionAutorizacionVO.getPacPacNumero());
    caGestionAutorizacion.setGauNombreAutorizador(gestionAutorizacionVO.getGauNombreAutorizador());
    caGestionAutorizacion.setGauTelefonoAutorizador(gestionAutorizacionVO.getGauTelefonoAutorizador());
    caGestionAutorizacion.setGauAutorizaServ(gestionAutorizacionVO.getGauAutorizaServ());
    caGestionAutorizacion.setMnaIdcodigo(gestionAutorizacionVO.getMnaIdcodigo());
    caGestionAutorizacion.setOmnDesc(gestionAutorizacionVO.getOmnDesc());
    caGestionAutorizacion.setGauCodigoAutorizacion(gestionAutorizacionVO.getGauCodigoAutorizacion());
    caGestionAutorizacion.setGauVigenciaAutorizacion(gestionAutorizacionVO.getGauVigenciaAutorizacion());
    caGestionAutorizacion.setGauValorPrestacion(gestionAutorizacionVO.getGauValorPrestacion());
    caGestionAutorizacion.setGauCostoConvenio(gestionAutorizacionVO.getGauCostoConvenio());
    caGestionAutorizacion.setGauCostoPac(gestionAutorizacionVO.getGauCostoPac());
    caGestionAutorizacion.setPcaAgeCodigoRecep(gestionAutorizacionVO.getPcaAgeCodigoRecep());
    caGestionAutorizacion.setOgaDescripcion(gestionAutorizacionVO.getOgaDescripcion());
    return caGestionAutorizacion;
  }
}

package co.fsfb.ws.rest.mapper;

import co.fsfb.ws.rest.entidades.CaOrdenesMedicas;
import co.fsfb.ws.rest.vo.OrdenMedicaVO;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueMappingStrategy;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL)
public interface OrdenMedicaMapper {
  public static final OrdenMedicaMapper INSTANCE = (OrdenMedicaMapper)Mappers.getMapper(OrdenMedicaMapper.class);
  
  OrdenMedicaVO entityToVO(CaOrdenesMedicas paramCaOrdenesMedicas);
}

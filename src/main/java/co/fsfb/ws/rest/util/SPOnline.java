package co.fsfb.ws.rest.util;

import co.fsfb.ws.rest.exception.DataException;
import co.fsfb.ws.rest.exception.RespondException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.Writer;
import java.net.CookieHandler;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class SPOnline {

    private static Logger logger = LoggerFactory.getLogger(SPOnline.class);

    private static String username;

    private static String password;

    private static String domain;

    private static Pair<String, String> tokens;

    private static String token;

    private static String formDigestValue;

    public static void SPOnlineInit() throws DataException {
        logger.info("Iniciando SPOnlineInit::::");
        username = PropertiesManager.CONFIG.getString("sharepoint.user");
        password = PropertiesManager.CONFIG.getString("sharepoint.password");
        domain = PropertiesManager.CONFIG.getString("sharepoint.domain");
        username = StringEscapeUtils.escapeXml11(username);
        password = StringEscapeUtils.escapeXml11(password);
        try {
            token = requestToken(domain, username, password);
        } catch (Exception e1) {
            logger.error("Error en la carga del SharePoint: " + e1.getMessage());
        }
        if (token != null)
      try {
            tokens = submitToken(domain, token);
            String jsonString = post("/_api/contextinfo", null, false);
            JSONObject json = new JSONObject(jsonString);
            formDigestValue = json.getJSONObject("d").getJSONObject("GetContextWebInformation").getString("FormDigestValue");
        } catch (Exception e) {
            throw new DataException(e.getMessage(), new Date(), e.getCause().toString());
        }
    }

    private static String requestToken(String domain, String username, String password) throws Exception {
        logger.info("Iniciando requestToken::::");
        String saml = generateSAML(domain, username, password);
        String sts = PropertiesManager.CONFIG.getString("sharepoint.sts");
        URL u = new URL(sts);
        URLConnection uc = u.openConnection();
        HttpURLConnection connection = (HttpURLConnection) uc;
        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.setRequestMethod("POST");
        connection.addRequestProperty("Content-Type", "text/xml; charset=utf-8");
        OutputStream out = connection.getOutputStream();
        Writer writer = new OutputStreamWriter(out);
        writer.write(saml);
        writer.flush();
        writer.close();
        InputStream in = connection.getInputStream();
        StringBuilder sb = new StringBuilder("");
        int c;
        while ((c = in.read()) != -1) {
            sb.append((char) c);
        }
        in.close();
        String result = sb.toString();
        String token = extractToken(result);
        if (token == null || token.equals("")) {
            throw new Exception(result.toString());
        }
        return token;
    }

    private static String generateSAML(String domain, String username, String password) {
        logger.info("Iniciando generateSAML::::");
        String reqXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<s:Envelope xmlns:s=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:a=\"http://www.w3.org/2005/08/addressing\" xmlns:u=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">\n   <s:Header>\n      <a:Action s:mustUnderstand=\"1\">http://schemas.xmlsoap.org/ws/2005/02/trust/RST/Issue</a:Action>\n      <a:ReplyTo>\n         <a:Address>http://www.w3.org/2005/08/addressing/anonymous</a:Address>\n      </a:ReplyTo>\n      <a:To s:mustUnderstand=\"1\">https://login.microsoftonline.com/extSTS.srf</a:To>\n      <o:Security xmlns:o=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" s:mustUnderstand=\"1\">\n         <o:UsernameToken>\n            <o:Username>[[username]]</o:Username>\n            <o:Password>[[password]]</o:Password>\n         </o:UsernameToken>\n      </o:Security>\n   </s:Header>\n   <s:Body>\n      <t:RequestSecurityToken xmlns:t=\"http://schemas.xmlsoap.org/ws/2005/02/trust\">\n         <wsp:AppliesTo xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\">\n            <a:EndpointReference>\n               <a:Address>[[endpoint]]</a:Address>\n            </a:EndpointReference>\n         </wsp:AppliesTo>\n         <t:KeyType>http://schemas.xmlsoap.org/ws/2005/05/identity/NoProofKey</t:KeyType>\n         <t:RequestType>http://schemas.xmlsoap.org/ws/2005/02/trust/Issue</t:RequestType>\n         <t:TokenType>urn:oasis:names:tc:SAML:1.0:assertion</t:TokenType>\n      </t:RequestSecurityToken>\n   </s:Body>\n</s:Envelope>";
        String saml = reqXML.replace("[[username]]", username);
        saml = saml.replace("[[password]]", password);
        saml = saml.replace("[[endpoint]]", String.format("%s/_forms/default.aspx?wa=wsignin1.0", new Object[]{domain}));
        return saml;
    }

    private static String extractToken(String result) throws SAXException, IOException, ParserConfigurationException, XPathExpressionException {
        logger.info("Iniciando extractToken::::");
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document document = db.parse(new InputSource(new StringReader(result)));
        XPathFactory xpf = XPathFactory.newInstance();
        XPath xp = xpf.newXPath();
        String token = xp.evaluate("//BinarySecurityToken/text()", document.getDocumentElement());
        return token;
    }

    private static Pair<String, String> submitToken(String domain, String token) throws IOException {
        logger.info("Iniciando submitToken::::");
        String loginContextPath = "/_forms/default.aspx?wa=wsignin1.0";
        String url = String.format("%s%s", new Object[]{domain, loginContextPath});
        CookieHandler.setDefault(null);
        URL u = new URL(url);
        URLConnection uc = u.openConnection();
        HttpURLConnection connection = (HttpURLConnection) uc;
        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.setRequestMethod("POST");
        connection.addRequestProperty("Accept", "application/x-www-form-urlencoded");
        connection.addRequestProperty("Content-Type", "text/xml; charset=utf-8");
        connection.setInstanceFollowRedirects(false);
        OutputStream out = connection.getOutputStream();
        Writer writer = new OutputStreamWriter(out);
        writer.write(token);
        writer.flush();
        out.flush();
        writer.close();
        out.close();
        String rtFa = null;
        String fedAuth = null;
        Map<String, List<String>> headerFields = connection.getHeaderFields();
        List<String> cookiesHeader = headerFields.get("Set-Cookie");
        if (cookiesHeader != null) {
            for (String cookie : cookiesHeader) {
                if (cookie.startsWith("rtFa=")) {
                    rtFa = "rtFa=" + ((HttpCookie) HttpCookie.parse(cookie).get(0)).getValue();
                    continue;
                }
                if (cookie.startsWith("FedAuth=")) {
                    fedAuth = "FedAuth=" + ((HttpCookie) HttpCookie.parse(cookie).get(0)).getValue();
                    continue;
                }
                logger.info("Waste=" + ((HttpCookie) HttpCookie.parse(cookie).get(0)).getValue());
            }
        }
        return (Pair<String, String>) ImmutablePair.of(rtFa, fedAuth);
    }

    public static String contextinfo(Pair<String, String> token, String domain) throws Exception {
        logger.info("Iniciando contextinfo::::");
        CloseableHttpClient httpClient = HttpClients.createDefault();
        try {
            HttpPost getRequest = new HttpPost(domain + "/_api/contextinfo");
            getRequest.addHeader("Cookie", (String) token.getLeft() + ";" + (String) token.getRight());
            getRequest.addHeader("accept", "application/json;odata=verbose");
            CloseableHttpResponse closeableHttpResponse = httpClient.execute((HttpUriRequest) getRequest);
            if (closeableHttpResponse.getStatusLine().getStatusCode() == 200) {
                return IOUtils.toString(closeableHttpResponse.getEntity().getContent(), "utf-8");
            }
            throw new RuntimeException("Fallo : HTTP Codigo error : " + closeableHttpResponse.getStatusLine().getStatusCode());
        } catch (ClientProtocolException e) {
            throw new Exception(e.getMessage(), e.getCause());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                httpClient.close();
            } catch (IOException ex) {
                throw new Exception(ex.getMessage(), ex.getCause());
            }
        }
        return null;
    }

    public static byte[] get(String url) throws Exception {
        logger.info("Iniciando get::::");
        CloseableHttpClient httpClient = HttpClients.createDefault();
        try {
            HttpGet getRequest = new HttpGet(domain + url);
            getRequest.addHeader("Cookie", (String) tokens.getLeft() + ";" + (String) tokens.getRight());
            getRequest.addHeader("accept", "application/json;odata=verbose");
            CloseableHttpResponse closeableHttpResponse = httpClient.execute((HttpUriRequest) getRequest);
            if (closeableHttpResponse.getStatusLine().getStatusCode() == 200) {
                return EntityUtils.toByteArray(closeableHttpResponse.getEntity());
            }
            throw new RespondException("" + closeableHttpResponse.getStatusLine().getStatusCode(), new Date(), url);
        } catch (ClientProtocolException e) {
            throw new Exception(e.getMessage(), e.getCause());
        } catch (IOException e) {
            throw new Exception(e.getMessage(), e.getCause());
        } finally {
            try {
                httpClient.close();
            } catch (IOException ex) {
                throw new Exception(ex.getMessage(), ex.getCause());
            }
        }
    }

    public static String post(String path, String data, boolean isXHttpMerge) throws Exception {
        logger.info("Iniciando post (Parametros)::::");
        CloseableHttpClient httpClient = HttpClients.createDefault();
        try {
            HttpPost postRequest = new HttpPost(domain + path);
            postRequest.addHeader("Cookie", (String) tokens.getLeft() + ";" + (String) tokens.getRight());
            postRequest.addHeader("accept", "application/json;odata=verbose");
            postRequest.addHeader("content-type", "application/json;odata=verbose");
            postRequest.addHeader("X-RequestDigest", formDigestValue);
            postRequest.addHeader("IF-MATCH", "*");
            if (isXHttpMerge) {
                postRequest.addHeader("X-HTTP-Method", "MERGE");
            }
            if (data != null) {
                StringEntity input = new StringEntity(data, "UTF-8");
                input.setContentType("application/json");
                postRequest.setEntity((HttpEntity) input);
            }
            CloseableHttpResponse closeableHttpResponse = httpClient.execute((HttpUriRequest) postRequest);
            if (closeableHttpResponse.getStatusLine().getStatusCode() != 200 && closeableHttpResponse.getStatusLine().getStatusCode() != 204 && closeableHttpResponse
                    .getStatusLine().getStatusCode() != 201) {
                throw new RespondException("" + closeableHttpResponse.getStatusLine().getStatusCode(), new Date(), path);
            }
            if (closeableHttpResponse.getEntity() == null || closeableHttpResponse.getEntity().getContent() == null) {
                return null;
            }
            return IOUtils.toString(closeableHttpResponse.getEntity().getContent(), "utf-8");
        } catch (ClientProtocolException e) {
            throw new Exception(e.getMessage(), e.getCause());
        } catch (IOException e) {
            throw new Exception(e.getMessage(), e.getCause());
        } finally {
            try {
                httpClient.close();
            } catch (IOException ex) {
                throw new Exception(ex.getMessage());
            }
        }
    }

    public static String uploadFile(String path, byte[] data) throws RespondException {
        logger.info("uploadFile::::");
        CloseableHttpClient httpClient = HttpClients.createDefault();
        try {
            HttpPost postRequest = new HttpPost(domain + path);
            postRequest.addHeader("Cookie", (String) tokens.getLeft() + ";" + (String) tokens.getRight());
            postRequest.addHeader("X-RequestDigest", formDigestValue);
            postRequest.setEntity((HttpEntity) new ByteArrayEntity(data));
            CloseableHttpResponse closeableHttpResponse = httpClient.execute((HttpUriRequest) postRequest);
            if (closeableHttpResponse.getStatusLine().getStatusCode() != 200 && closeableHttpResponse.getStatusLine().getStatusCode() != 204) {
                throw new RespondException("" + closeableHttpResponse.getStatusLine().getStatusCode(), new Date(), path);
            }
            if (closeableHttpResponse.getEntity() == null || closeableHttpResponse.getEntity().getContent() == null) {
                return null;
            }
            return IOUtils.toString(closeableHttpResponse.getEntity().getContent(), "utf-8");
        } catch (ClientProtocolException e) {
            throw new RespondException(e.getMessage().toString(), new Date(), e.getStackTrace().toString());
        } catch (IOException e) {
            throw new RespondException(e.getMessage().toString(), new Date(), e.getStackTrace().toString());
        } finally {
            try {
                httpClient.close();
            } catch (IOException ex) {
                throw new RespondException(ex.getMessage().toString(), new Date(), ex.getStackTrace().toString());
            }
        }
    }

    public static String delete(String path) throws Exception {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        try {
            HttpDelete deleteRequest = new HttpDelete(domain + path);
            deleteRequest.addHeader("Cookie", (String) tokens.getLeft() + ";" + (String) tokens.getRight());
            deleteRequest.addHeader("accept", "application/json;odata=verbose");
            deleteRequest.addHeader("content-type", "application/json;odata=verbose");
            deleteRequest.addHeader("X-RequestDigest", formDigestValue);
            deleteRequest.addHeader("X-HTTP-Method", "DELETE");
            deleteRequest.addHeader("IF-MATCH", "*");
            CloseableHttpResponse closeableHttpResponse = httpClient.execute((HttpUriRequest) deleteRequest);
            if (closeableHttpResponse.getStatusLine().getStatusCode() != 200 && closeableHttpResponse.getStatusLine().getStatusCode() != 204) {
                throw new RespondException("" + closeableHttpResponse.getStatusLine().getStatusCode(), new Date(), path);
            }
            if (closeableHttpResponse.getEntity() == null || closeableHttpResponse.getEntity().getContent() == null) {
                return null;
            }
            return IOUtils.toString(closeableHttpResponse.getEntity().getContent(), "utf-8");
        } catch (ClientProtocolException e) {
            throw new Exception(e.getMessage(), e.getCause());
        } catch (IOException e) {
            throw new Exception(e.getMessage(), e.getCause());
        } finally {
            try {
                httpClient.close();
            } catch (IOException ex) {
                throw new RespondException(ex.getMessage().toString(), new Date(), ex.getStackTrace().toString());
            }
        }
    }
}

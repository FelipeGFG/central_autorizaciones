package co.fsfb.ws.rest.util;

import co.fsfb.ws.rest.exception.DataException;
import co.fsfb.ws.rest.exception.LogicException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

public class DataBaseUtil {
  private static Logger logger = LoggerFactory.getLogger(DataBaseUtil.class);
  
  public static Connection conn;
  
  @Deprecated
  public static Connection inicializar() throws DataException {
    String url = PropertiesManager.CONFIG.getString("DB_IP") + ":" + PropertiesManager.CONFIG.getString("DB_PORT") + ":" + PropertiesManager.CONFIG.getString("DB_SCHEMA");
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName(PropertiesManager.CONFIG.getString("DB_DRIVER"));
    dataSource.setUrl(url);
    dataSource.setUsername(PropertiesManager.CONFIG.getString("DB_USER"));
    dataSource.setPassword(PropertiesManager.CONFIG.getString("DB_PASS"));
    try {
      conn = dataSource.getConnection();
    } catch (SQLException e) {
      logger.error("Error coonectando a la base de datos", e);
      throw new DataException(e.getMessage(), new Date(), e.getMessage());
    } 
    return conn;
  }
  
  public static Connection inicializarDesportal() throws DataException {
    String url = PropertiesManager.CONFIG.getString("DB_URL_SQL_DESPORTAL");
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName(PropertiesManager.CONFIG.getString("DB_DRIVER_SQL_DESPORTAL"));
    dataSource.setUrl(url);
    dataSource.setUsername(PropertiesManager.CONFIG.getString("DB_USER_SQL_DESPORTAL"));
    dataSource.setPassword(PropertiesManager.CONFIG.getString("DB_PASS_SQL_DESPORTAL"));
    try {
      conn = dataSource.getConnection();
    } catch (SQLException e) {
      logger.error("Error coonectando a la base de datos", e);
      throw new DataException(e.getMessage(), new Date(), e.getMessage());
    } 
    return conn;
  }
  
  @Deprecated
  public static Connection inicializarSQL() throws DataException {
    String url = PropertiesManager.CONFIG.getString("DB_IP_SQL") + ":" + PropertiesManager.CONFIG.getString("DB_PORT_SQL") + ";" + PropertiesManager.CONFIG.getString("DB_SCHEMA_SQL");
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName(PropertiesManager.CONFIG.getString("DB_DRIVER_SQL"));
    dataSource.setUrl(url);
    dataSource.setUsername(PropertiesManager.CONFIG.getString("DB_USER_SQL"));
    dataSource.setPassword(PropertiesManager.CONFIG.getString("DB_PASS_SQL"));
    try {
      conn = dataSource.getConnection();
    } catch (SQLException e) {
      throw new DataException(e.getMessage(), new Date(), e.getMessage().toString());
    } 
    return conn;
  }
  
  public static void closeConnection(Connection connection) throws Exception {
    try {
      connection.close();
      connection = null;
    } catch (SQLException e) {
      throw new DataException(e.getMessage(), new Date(), e.getMessage().toString());
    } 
  }
  
  public static void closeConnectionSQL(Connection connection) throws Exception {
    try {
      connection.close();
      connection = null;
    } catch (SQLException e) {
      throw new DataException(e.getMessage(), new Date(), e.getMessage().toString());
    } 
  }
  
  public static String evaluarRespuesta(CallableStatement cstmt, String respuesta, int firstIndexTable, int maxRows) throws SQLException, DataException {
    if (respuesta == null || respuesta.length() < 7)
      throw new DataException(102); 
    String codigoRespuesta = respuesta.trim().substring(0, 7);
    if (codigoRespuesta.equals("0780000")) {
      String[] paramMsgError = { respuesta.trim().substring(7) };
      throw new DataException("100", new Date(), paramMsgError.toString());
    } 
    if (codigoRespuesta.equals("0380000")) {
      String[] paramMsgError = { respuesta.trim().substring(7) };
      throw new DataException("101", new Date(), paramMsgError.toString());
    } 
    if (firstIndexTable > 0) {
      Object result = cstmt.getObject(firstIndexTable);
      if (result != null) {
        Object[] objArray = (Object[])result;
        if (maxRows == objArray.length)
          try {
            throw new LogicException(112);
          } catch (LogicException e) {
            return e.getClientMessage();
          }  
      } 
    } 
    return null;
  }
}

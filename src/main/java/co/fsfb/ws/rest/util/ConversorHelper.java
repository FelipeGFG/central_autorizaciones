package co.fsfb.ws.rest.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ConversorHelper {
  public static Calendar stringToCalendar(String fechaStr, String formato) throws ParseException {
    SimpleDateFormat sdf = new SimpleDateFormat(formato);
    Calendar fecha = Calendar.getInstance();
    fecha.set(11, 0);
    fecha.set(12, 0);
    fecha.set(13, 0);
    fecha.set(14, 0);
    fecha.setTime(sdf.parse(fechaStr));
    return fecha;
  }
  
  public static Calendar stringToCalendar(String fechaStr, String formato, int minutos) throws ParseException {
    SimpleDateFormat sdf = new SimpleDateFormat(formato);
    Calendar fecha = Calendar.getInstance();
    fecha.setTime(sdf.parse(fechaStr));
    fecha.set(11, minutos / 60);
    fecha.set(12, minutos % 60);
    fecha.set(13, 0);
    return fecha;
  }
  
  public static String calendarToString(Calendar fecha, String formato) {
    SimpleDateFormat sdf = new SimpleDateFormat(formato);
    return sdf.format(fecha.getTime()).toUpperCase();
  }
  
  public static String rellenarConChar(String string, char c, int newSize, int side) {
    if (string == null)
      return null; 
    String extraString = "";
    if (side == 1)
      extraString = string; 
    if (string.length() > newSize)
      return string.substring(0, newSize); 
    for (int i = 0; i < newSize - string.length(); i++)
      extraString = extraString + String.valueOf(c); 
    if (side == 0)
      return extraString + string; 
    return extraString;
  }
  
  public static boolean isNumeric(String str) {
    try {
      Double.parseDouble(str);
    } catch (NumberFormatException nfe) {
      return false;
    } 
    return true;
  }
}

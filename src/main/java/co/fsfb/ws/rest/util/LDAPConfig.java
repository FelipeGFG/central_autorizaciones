package co.fsfb.ws.rest.util;

import co.fsfb.ws.rest.exception.CreacionFallidaException;
import co.fsfb.ws.rest.exception.CredencialesFallidasException;
import co.fsfb.ws.rest.exception.DataException;
import co.fsfb.ws.rest.exception.EmailDuplicadoException;
import co.fsfb.ws.rest.exception.FechaNacimientoException;
import co.fsfb.ws.rest.exception.NombreErroneoException;
import co.fsfb.ws.rest.exception.PacienteNoRegistradoException;
import co.fsfb.ws.rest.exception.SexoException;
import co.fsfb.ws.rest.exception.UsuarioDuplicadoException;
import co.fsfb.ws.rest.exception.UsuarioNoEncontradoException;
import co.fsfb.ws.rest.vo.Usuario;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Date;
import java.util.Hashtable;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.naming.NameClassPair;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LDAPConfig {
  private static Logger logger = LoggerFactory.getLogger(LDAPConfig.class);
  
  private static String SECRET_KEY;
  
  private static String MD5;
  
  public static final String UTF_8 = Charset.forName("UTF-8").toString();
  
  public LDAPConfig() {
    SECRET_KEY = PropertiesManager.CONFIG.getString("SECRET_KEY");
    MD5 = PropertiesManager.CONFIG.getString("MD5");
  }
  
  public static Hashtable<String, String> LDAPEnv() {
    logger.info("Iniciando LDAPEnv::::");
    Hashtable<String, String> env = new Hashtable<>();
    env.put("java.naming.factory.initial", PropertiesManager.CONFIG.getString("ldap.init.ctx"));
    env.put("java.naming.provider.url", PropertiesManager.CONFIG.getString("ldap.url"));
    env.put("java.naming.security.authentication", PropertiesManager.CONFIG.getString("ldap.security.authentication"));
    env.put("java.naming.security.principal", PropertiesManager.CONFIG.getString("ldap.username"));
    env.put("java.naming.security.credentials", PropertiesManager.CONFIG.getString("ldap.password"));
    return env;
  }
  
  public Usuario crearUsrLDAP(Usuario usr) throws UsuarioNoEncontradoException, UsuarioDuplicadoException, DataException, EmailDuplicadoException, NombreErroneoException, PacienteNoRegistradoException, FechaNacimientoException, SexoException {
    logger.info("Iniciando crearUsrLDAP::::");
    Attributes attrs = new BasicAttributes();
    try {
      DirContext ctxDir = new InitialDirContext(LDAPEnv());
      if (ctxDir != null) {
        ctxDir.bind("uid=" + usr.getUid() + "," + PropertiesManager.CONFIG.getString("ldap.base.dn"), (Object)null, 
            createAttributes(usr));
        if (usr.getRole().equals("")) {
          attrs.put("member", "uid=" + usr
              .getUid() + ",cn=Pacientes," + PropertiesManager.CONFIG.getString("ldap.base.groups"));
        } else {
          attrs.put("member", "uid=" + usr
              .getUid() + "," + PropertiesManager.CONFIG.getString("ldap.base.groups"));
        } 
        ModificationItem[] mods = new ModificationItem[1];
        mods[0] = new ModificationItem(1, attrs.get("member"));
        ctxDir.modifyAttributes("ou=" + buscarGrupoByID(usr.getRole(), ctxDir) + "," + PropertiesManager.CONFIG
            .getString("ldap.base.groups"), mods);
        usr.setUserValido(true);
        ctxDir.close();
      } 
    } catch (Exception e) {
      throw new UsuarioDuplicadoException(e.getMessage(), new Date(), e.getMessage().toString());
    } 
    return usr;
  }
  
  private String buscarGrupoByID(String role, DirContext ctx) throws NamingException {
    NamingEnumeration<?> nEnum = ctx.list(PropertiesManager.CONFIG.getString("ldap.base.groups"));
    while (nEnum.hasMoreElements()) {
      NameClassPair ncp = (NameClassPair)nEnum.nextElement();
      SearchControls searchContrG = new SearchControls();
      searchContrG.setSearchScope(2);
      String[] attributes = { "ou", "cn" };
      searchContrG.setReturningAttributes(attributes);
      NamingEnumeration<?> nEnumG = ctx.search(ncp
          .getName() + "," + PropertiesManager.CONFIG.getString("ldap.base.groups"), "objectClass=*", searchContrG);
      while (nEnumG.hasMoreElements()) {
        SearchResult searchReslAtt = (SearchResult)nEnumG.nextElement();
        Attributes attrsG = searchReslAtt.getAttributes();
        Attribute attrG = attrsG.get("cn");
        if (attrG.contains(role))
          return attrsG.get("ou").toString().replaceAll("ou: ", ""); 
      } 
    } 
    return null;
  }
  
  private Attributes createAttributes(Usuario usr) throws CreacionFallidaException {
    Attributes attrs = new BasicAttributes();
    BasicAttribute ocattr = new BasicAttribute("objectclass");
    ocattr.add("top");
    ocattr.add("person");
    ocattr.add("organizationalPerson");
    ocattr.add("inetOrgPerson");
    attrs.put(ocattr);
    attrs.put("cn", usr.getCn());
    attrs.put("sn", usr.getSn());
    attrs.put("employeeNumber", String.valueOf(usr.getUsuario()));
    attrs.put("employeeType", usr.getTipoDoc());
    attrs.put("givenName", usr.getGivenName());
    attrs.put("mail", usr.getMail());
    attrs.put("uid", usr.getUid());
    attrs.put("postalAddress", usr.getFechaNacimiento());
    attrs.put("telephoneNumber", usr.getPacnumero());
    attrs.put("title", usr.getSexo());
    attrs.put("userPassword", usr.getPassword());
    attrs.put("initials", usr.getApellidoPaterno());
    return attrs;
  }
  
  public Usuario autentica(Usuario usr) throws UsuarioNoEncontradoException {
    logger.info("Iniciando autentica::::");
    try {
      DirContext ctxDir = new InitialDirContext(LDAPEnv());
      if (ctxDir != null) {
        usr = encontrarUsuarioID(ctxDir, usr);
        ctxDir.close();
      } 
    } catch (NamingException e) {
      logger.info("Error en LDAP:::: " + e.getCause());
      throw new UsuarioNoEncontradoException(e.getMessage(), new Date(), e.getExplanation().toString());
    } 
    return usr;
  }
  
  public boolean cambioContrasena(Usuario usr) throws UsuarioNoEncontradoException {
    logger.info("Iniciando cambioContrasena::::");
    try {
      DirContext ctxDir = new InitialDirContext(LDAPEnv());
      Usuario usrValidar = new Usuario();
      usrValidar.setUid(usr.getUid());
      if (ctxDir != null) {
        usrValidar = encontrarUsuarioID(ctxDir, usrValidar);
        if (usrValidar.getMail() != null && 
          usrValidar.getMail().toUpperCase().equals(usr.getMail().toUpperCase())) {
          String vlrEncriptado = Encriptar(usrValidar.getPassword().toString());
          if (vlrEncriptado != null) {
            String[] emails = { usr.getMail() };
            (new EnviarEmail()).postMail(emails, PropertiesManager.CONFIG.getString("SUBJECT_CAMBIO_PWD"), PropertiesManager.CONFIG
                .getString("TEXTO_CAMBIO_PWD") + PropertiesManager.CONFIG
                .getString("URL_CAMBIO_PWD") + 
                Encriptar(usrValidar.getPassword().toString()));
            ctxDir.close();
            return true;
          } 
          return false;
        } 
      } 
    } catch (Exception e) {
      throw new UsuarioNoEncontradoException(e.getMessage(), new Date(), e.getMessage());
    } 
    return false;
  }
  
  private Usuario encontrarUsuarioID(DirContext ctx, Usuario usr) throws UsuarioNoEncontradoException, NamingException {
      
    String searchFilter = "(employeeNumber=" + usr.getUid() + ")";
    SearchControls searchControls = new SearchControls();
    searchControls.setSearchScope(2);
    
    NamingEnumeration<SearchResult> results = ctx.search(PropertiesManager.CONFIG.getString("ldap.base.dn"), searchFilter, searchControls);
   
    if (results.hasMoreElements()) {
        
      SearchResult searchResult = results.nextElement();
      
      NamingEnumeration<?> nEnum = ctx.list(PropertiesManager.CONFIG.getString("ldap.base.groups"));
      
      while (nEnum.hasMoreElements()) {
          
        NameClassPair ncp = (NameClassPair)nEnum.nextElement();
        
        SearchControls searchContrG = new SearchControls();
        
        searchContrG.setSearchScope(2);
        String[] attributes = { "member", "cn" };
        searchContrG.setReturningAttributes(attributes);
        
        NamingEnumeration<?> nEnumG = ctx.search(ncp
            .getName() + "," + PropertiesManager.CONFIG.getString("ldap.base.groups"), "objectClass=*", searchContrG);
        
        while (nEnumG.hasMoreElements()) {
            
          SearchResult searchReslAtt = (SearchResult)nEnumG.nextElement();
          Attributes attrsG = searchReslAtt.getAttributes();
          Attribute attrG = attrsG.get("member");
          if (attrG.contains("uid=" + searchResult.getAttributes().get("uid").get() + "," + PropertiesManager.CONFIG
              .getString("ldap.base.groups"))) {
            Usuario usrVal = new Usuario();
            usrVal.setUsuario(usr.getUid());
            usrVal.setRole(attrsG.get("cn").toString().replaceAll("cn: ", ""));
            usrVal = mapeoAtributos(ctx, searchResult.getAttributes(), usrVal);
            if (results.hasMoreElements()) {
              usr.setUserValido(false);
              return usr;
            } 
            if (usrVal.getPassword().equals(usr.getPassword())) {
              usr = usrVal;
              usr.setUserValido(true);
            } else {
              usr = usrVal;
            } 
            return usr;
          } 
        } 
        
        
        
      } 
    } 
    return usr;
  }
  
  private Usuario mapeoAtributos(DirContext ctx, Attributes attrs, Usuario usr) throws UsuarioNoEncontradoException {
    try {
      if (attrs.get("uid") != null)
        usr.setUid((String)attrs.get("uid").get()); 
      if (attrs.get("cn") != null)
        usr.setCn((String)attrs.get("cn").get()); 
      if (attrs.get("sn") != null)
        usr.setSn((String)attrs.get("sn").get()); 
      if (attrs.get("mail") != null)
        usr.setMail((String)attrs.get("mail").get()); 
      if (attrs.get("userPassword") != null)
        usr.setPassword(new String((byte[])attrs.get("userPassword").get())); 
      if (attrs.get("givenName") != null)
        usr.setGivenName((String)attrs.get("givenName").get()); 
      if (attrs.get("initials") != null)
        usr.setApellidoPaterno((String)attrs.get("initials").get()); 
      if (attrs.get("postalAddress") != null)
        usr.setFechaNacimiento((String)attrs.get("postalAddress").get()); 
      if (attrs.get("title") != null)
        usr.setSexo((String)attrs.get("title").get()); 
      if (attrs.get("telephonenumber") != null)
        usr.setPacnumero(attrs.get("telephonenumber").get().toString()); 
      if (attrs.get("employeeType") != null)
        usr.setTipoDoc((String)attrs.get("employeeType").get()); 
      if (attrs.get("employeeNumber") != null)
        usr.setUsuario(attrs.get("employeeNumber").get().toString()); 
    } catch (NamingException e) {
      throw new UsuarioNoEncontradoException(e.getMessage(), new Date(), e.getExplanation());
    } 
    return usr;
  }
  
  public boolean confirmarCambioContrasena(Usuario usr) throws CredencialesFallidasException {
    logger.info("Iniciando confirmarCambioContrasena::::");
    try {
      DirContext ctx = new InitialDirContext(LDAPEnv());
      String vlrDesencriptado = Desencriptar(usr.getPassword());
      if (vlrDesencriptado != null) {
        usr.setPassword(vlrDesencriptado);
      } else {
        return false;
      } 
      Usuario usrCambiado = cambiarContrasena(usr, ctx);
      ctx.close();
      if (usrCambiado != null)
        return true; 
      return false;
    } catch (Exception e) {
      throw new CredencialesFallidasException(e.getMessage(), new Date(), e.getMessage());
    } 
  }
  
  private Usuario cambiarContrasena(Usuario usr, DirContext ctx) throws CredencialesFallidasException {
    Attributes nuevaPwd = new BasicAttributes();
    nuevaPwd.put("userPassword", usr.getNew_password());
    Attributes eliminarPwd = new BasicAttributes();
    eliminarPwd.put("userPassword", usr.getPassword());
    usr.setPassword(usr.getNew_password());
    ModificationItem[] mods = new ModificationItem[2];
    mods[0] = new ModificationItem(1, nuevaPwd.get("userPassword"));
    mods[1] = new ModificationItem(3, eliminarPwd.get("userPassword"));
    try {
      ctx.modifyAttributes("uid=" + usr.getUid() + "," + PropertiesManager.CONFIG.getString("ldap.base.dn"), mods);
    } catch (NamingException e) {
      throw new CredencialesFallidasException(e.getMessage(), new Date(), e.getExplanation());
    } 
    return usr;
  }
  
  public static String Encriptar(String texto) throws CredencialesFallidasException {
    logger.info("Iniciando Encriptar::::");
    String base64EncryptedString = "";
    try {
      MessageDigest md = MessageDigest.getInstance(MD5);
      byte[] digestOfPassword = md.digest(SECRET_KEY.getBytes(StandardCharsets.UTF_8));
      byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
      SecretKey key = new SecretKeySpec(keyBytes, "DESede");
      Cipher cipher = Cipher.getInstance("DESede");
      cipher.init(1, key);
      byte[] plainTextBytes = texto.getBytes(UTF_8);
      byte[] buf = cipher.doFinal(plainTextBytes);
      byte[] base64Bytes = Base64.encodeBase64(buf);
      base64EncryptedString = Base64.encodeBase64String(base64Bytes);
    } catch (Exception ex) {
      throw new CredencialesFallidasException(PropertiesManager.CONFIG.getString("CREDENTIAL_FAILED"), new Date(), ex
          .getMessage().toString());
    } 
    return base64EncryptedString;
  }
  
  public static String Desencriptar(String textoEncriptado) throws CredencialesFallidasException {
    logger.info("Iniciando Desencriptar::::");
    String base64EncryptedString = "";
    try {
      String msgEncriptado = new String(Base64.decodeBase64(textoEncriptado));
      byte[] message = Base64.decodeBase64(msgEncriptado.getBytes(StandardCharsets.UTF_8));
      MessageDigest md = MessageDigest.getInstance(MD5);
      byte[] digestOfPassword = md.digest(SECRET_KEY.getBytes(StandardCharsets.UTF_8));
      byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
      SecretKey key = new SecretKeySpec(keyBytes, "DESede");
      Cipher decipher = Cipher.getInstance("DESede");
      decipher.init(2, key);
      byte[] plainText = decipher.doFinal(message);
      base64EncryptedString = new String(plainText, StandardCharsets.UTF_8);
    } catch (Exception ex) {
      throw new CredencialesFallidasException(ex.getMessage(), new Date(), ex.getMessage());
    } 
    return base64EncryptedString;
  }
}

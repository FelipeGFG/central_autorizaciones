package co.fsfb.ws.rest.util;

public class Constantes {
  final String typeTableNameOrdNumero = "ORDNUMERO_ARR";
  
  final String typeTableNameOrdNombre = "ORDNOMBRE_ARR";
  
  final String typeTableNameOrdFecha = "ORDFECHA_ARR";
  
  final String typeTableNamePRE_PRE_Codigo = "PRE_PRE_CODIGO_ARR";
  
  final String typeTableNamePRE_PRE_Descripcio = "PRE_PRE_DESCRIPCIO_ARR";
  
  final String typeTableNameRango = "RANGO_ARR";
  
  final String typeTableNameOrdNumeroAlterno = "ORDNUMEROALTERNO_ARR";
  
  final String typeTableNameATE_PRE_TipoFormu = "ATE_PRE_TIPOFORMU_ARR";
  
  final String typeTableNameATE_PRE_NumerFormu = "ATE_PRE_NUMEROFORMU_ARR";
  
  public static final String FSFB = "FSFB ";
  
  public static final String JNDI_HIS = "jdbc/hisisis";
  
  public static final String JNDI_HIS_PRUEBAS = "jdbc/hisisisPruebas";
  
  public static final String JNDI_LIS = "jdbc/lis";
  
  public static final String JNDI_EJB_GENERAL_FACADE = "ejb/co/org/fsfb/ejb/FsfbFacadeHome";
  
  public static final String JNDI_EJB_MEDICOS_FACADE = "ejb/co/org/fsfb/ejb/medicos/MedicosFacadeHome";
  
  public static final String JNDI_EJB_PACIENTES_FACADE = "ejb/co/org/fsfb/ejb/pacientes/PacientesFacadeHome";
  
  public static final String JNDI_EJB_CACHE_FACADE = "ejb/co/org/fsfb/ejb/cache/CacheFacadeHome";
  
  public static final String JNDI_CACHE_GRAFICAS = "services/cache/fsfb/graficas";
  
  public static final String JNDI_CACHE_SESION = "services/cache/fsfb/sesion";
  
  public static final String CONTEXT_FACTORY = "com.ibm.websphere.naming.WsnInitialContextFactory";
  
  public static final String JDBC_INDEX_TABLE_CLASS = "oracle.jdbc.OracleCallableStatement";
  
  public static final String JDBC_INDEX_TABLE_REG_METHOD = "registerIndexTableOutParameter";
  
  public static final String JDBC_INDEX_TABLE_GET_METHOD = "getPlsqlIndexTable";
  
  public static final int JDBC_MAX_ELEMENT_TABLE_LENGTH_RET = 0;
  
  public static final String IDIOMA_ESPANOL = "es";
  
  public static final String IDIOMA_INGLES = "en";
  
  public static final String FORMATO_FECHA_HORA = "dd/MMM/yyyy HH:mm";
  
  public static final String FORMATO_FECHA = "dd/MMM/yyyy";
  
  public static final String FORMATO_FECHA_DB = "dd-MM-yyyy";
  
  public static final String FORMATO_FECHA_DB_SQL = "yyyy-MM-dd";
  
  public static final String FORMATO_FECHA_TDS = "yyyyMMdd";
  
  public static final String FORMATO_FECHA_HORA_DB = "dd-MM-yyyy HH:mm";
  
  public static final String FORMATO_HORA = "HH:mm";
  
  public static final String EMPTY_STRING = "";
  
  public static final char CHAR_SPACE = ' ';
  
  public static final char CHAR_UNDERSCORE = '_';
  
  public static final char CHAR_CIERRE_INTERROGACION = '?';
  
  public static final char CHAR_COMA = ',';
  
  public static final char CHAR_PUNTO = '.';
  
  public static final char CHAR_IGUAL = '=';
  
  public static final int LEFT = 0;
  
  public static final int RIGHT = 1;
  
  public static final String ERROR_PROPERTY_PREFIX = "ERROR_";
  
  public static final String CODE_ERROR_PREFIX = "_[[";
  
  public static final String CODE_ERROR_SUFIX = "]]_";
  
  public static final String PROP_NOMBRE_ARCHIVO_ERRORES = "co.org.fsfb.properties.fsfb_mensajes_de_error";
  
  public static final int PROPERTIES_NOT_FOUND_CODE = -1;
  
  public static final String PROPERTIES_NOT_FOUND = "No se encontro el archivo de propiedades solicitado.";
  
  public static final int PROPERTY_NOT_FOUND_CODE = -2;
  
  public static final String PROPERTY_NOT_FOUND = "No se encontro propiedad solicitada.";
  
  public static final int REMOTE_ERROR_CODE = -3;
  
  public static final String REMOTE_ERROR = "Error de invocacide mremoto.";
  
  public static final String ERROR_000 = "000";
  
  public static final String ERROR_100 = "100";
  
  public static final String ERROR_101 = "101";
  
  public static final int ERROR_102 = 102;
  
  public static final int ERROR_103 = 103;
  
  public static final int ERROR_104 = 104;
  
  public static final int ERROR_105 = 105;
  
  public static final int ERROR_106 = 106;
  
  public static final int ERROR_107 = 107;
  
  public static final int ERROR_108 = 108;
  
  public static final int ERROR_109 = 109;
  
  public static final int ERROR_110 = 110;
  
  public static final int ERROR_111 = 111;
  
  public static final int ERROR_112 = 112;
  
  public static final int ERROR_113 = 113;
  
  public static final int ERROR_114 = 114;
  
  public static final int ERROR_115 = 115;
  
  public static final int ERROR_116 = 116;
  
  public static final int ERROR_121 = 121;
  
  public static final int ERROR_122 = 122;
  
  public static final int ERROR_123 = 123;
  
  public static final int ERROR_124 = 124;
  
  public static final int ERROR_125 = 125;
  
  public static final int ERROR_126 = 126;
  
  public static final int ERROR_127 = 127;
  
  public static final int IMG_DEFAULT_ANCHO = 450;
  
  public static final int IMG_DEFAULT_ALTO = 300;
  
  public static final String IMG_SERVLET_PARAM = "ID";
  
  public static final String IMG_CONTENT_TYPE_PNG = "image/png";
  
  public static final String PROP_NOMBRE_ARCHIVO_CONFIG = "fsfb_config";
  
  public static final String PROP_MAIL_HOST = "MAIL_HOST";
  
  public static final String PROP_MAIL_FROM = "MAIL_FROM";
  
  public static final String PROP_MAIL_SMTP_PORT = "MAIL_SMTP_PORT";
  
  public static final String PROP_MAIL_SMTP_USER = "MAIL_SMTP_USER";
  
  public static final String PROP_MAIL_SMTP_PASSWORD = "MAIL_SMTP_PASSWORD";
  
  public static final String PROP_URL_SERVLET_GRAFICAS = "URL_SERVLET_GRAFICAS";
  
  public static final String PROP_URL_ESB_AGENDAR_CITA_TELEMEDICINA = "URL_ESB_AGENDAR_CITA_TELEMEDICINA";
  
  public static final String PROP_CONTEXT_PROVIDER_URL = "CONTEXT_PROVIDER_URL";
  
  public static final String PROP_JDBC_MAX_ROWS_SP_GENERAL = "JDBC_MAX_ROWS_SP_GENERAL";
  
  public static final String PROP_JDBC_MAX_ROWS_SP_SIGNOS_VITALES = "JDBC_MAX_ROWS_SP_SIGNOS_VITALES";
  
  public static final String PROP_JDBC_MAX_ROWS_SP_BALANCE_LIQUIDOS = "JDBC_MAX_ROWS_SP_BALANCE_LIQUIDOS";
  
  public static final String PROP_JDBC_MAX_ROWS_SP_HISTORICO_MEDICAMENTOS = "JDBC_MAX_ROWS_SP_HISTORICO_MEDICAMENTOS";
  
  public static final String PROP_JDBC_MAX_ROWS_SP_LISTADO_EXAMENES = "JDBC_MAX_ROWS_SP_LISTADO_EXAMENES";
  
  public static final String PROP_JDBC_MAX_ROWS_SP_LISTAS_PARAMETRICAS = "JDBC_MAX_ROWS_SP_LISTAS_PARAMETRICAS";
  
  public static final String PROP_JDBC_MAX_ROWS_SP_AGENDAS_TELEMEDICINA = "JDBC_MAX_ROWS_SP_AGENDAS_TELEMEDICINA";
  
  public static final String PROP_EQUIVALENCIAS_TIPOS_ID_HIS_TDS = "EQUIVALENCIAS_TIPOS_ID_HIS_TDS";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_TITULO = "GRAFICA_SIGNOS_VITALES_TITULO";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_ALTO = "GRAFICA_SIGNOS_VITALES_ALTO";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_ANCHO = "GRAFICA_SIGNOS_VITALES_ANCHO";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_FR_TITULO_EJE = "GRAFICA_SIGNOS_VITALES_FR_TITULO_EJE";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_FR_TITULO_CONVENCION = "GRAFICA_SIGNOS_VITALES_FR_TITULO_CONVENCION";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_FR_ESCALA_MINIMA = "GRAFICA_SIGNOS_VITALES_FR_ESCALA_MINIMA";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_FR_ESCALA_MAXIMA = "GRAFICA_SIGNOS_VITALES_FR_ESCALA_MAXIMA";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_FR_COLOR_R = "GRAFICA_SIGNOS_VITALES_FR_COLOR_R";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_FR_COLOR_G = "GRAFICA_SIGNOS_VITALES_FR_COLOR_G";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_FR_COLOR_B = "GRAFICA_SIGNOS_VITALES_FR_COLOR_B";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_FC_TITULO_EJE = "GRAFICA_SIGNOS_VITALES_FC_TITULO_EJE";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_FC_TITULO_CONVENCION = "GRAFICA_SIGNOS_VITALES_FC_TITULO_CONVENCION";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_FC_ESCALA_MINIMA = "GRAFICA_SIGNOS_VITALES_FC_ESCALA_MINIMA";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_FC_ESCALA_MAXIMA = "GRAFICA_SIGNOS_VITALES_FC_ESCALA_MAXIMA";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_FC_COLOR_R = "GRAFICA_SIGNOS_VITALES_FC_COLOR_R";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_FC_COLOR_G = "GRAFICA_SIGNOS_VITALES_FC_COLOR_G";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_FC_COLOR_B = "GRAFICA_SIGNOS_VITALES_FC_COLOR_B";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_TAS_TITULO_EJE = "GRAFICA_SIGNOS_VITALES_TAS_TITULO_EJE";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_TAS_TITULO_CONVENCION = "GRAFICA_SIGNOS_VITALES_TAS_TITULO_CONVENCION";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_TAS_ESCALA_MINIMA = "GRAFICA_SIGNOS_VITALES_TAS_ESCALA_MINIMA";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_TAS_ESCALA_MAXIMA = "GRAFICA_SIGNOS_VITALES_TAS_ESCALA_MAXIMA";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_TAS_COLOR_R = "GRAFICA_SIGNOS_VITALES_TAS_COLOR_R";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_TAS_COLOR_G = "GRAFICA_SIGNOS_VITALES_TAS_COLOR_G";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_TAS_COLOR_B = "GRAFICA_SIGNOS_VITALES_TAS_COLOR_B";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_TAD_TITULO_EJE = "GRAFICA_SIGNOS_VITALES_TAD_TITULO_EJE";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_TAD_TITULO_CONVENCION = "GRAFICA_SIGNOS_VITALES_TAD_TITULO_CONVENCION";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_TAD_ESCALA_MINIMA = "GRAFICA_SIGNOS_VITALES_TAD_ESCALA_MINIMA";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_TAD_ESCALA_MAXIMA = "GRAFICA_SIGNOS_VITALES_TAD_ESCALA_MAXIMA";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_TAD_COLOR_R = "GRAFICA_SIGNOS_VITALES_TAD_COLOR_R";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_TAD_COLOR_G = "GRAFICA_SIGNOS_VITALES_TAD_COLOR_G";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_TAD_COLOR_B = "GRAFICA_SIGNOS_VITALES_TAD_COLOR_B";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_TAM_TITULO_EJE = "GRAFICA_SIGNOS_VITALES_TAM_TITULO_EJE";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_TAM_TITULO_CONVENCION = "GRAFICA_SIGNOS_VITALES_TAM_TITULO_CONVENCION";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_TAM_ESCALA_MINIMA = "GRAFICA_SIGNOS_VITALES_TAM_ESCALA_MINIMA";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_TAM_ESCALA_MAXIMA = "GRAFICA_SIGNOS_VITALES_TAM_ESCALA_MAXIMA";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_TAM_COLOR_R = "GRAFICA_SIGNOS_VITALES_TAM_COLOR_R";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_TAM_COLOR_G = "GRAFICA_SIGNOS_VITALES_TAM_COLOR_G";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_TAM_COLOR_B = "GRAFICA_SIGNOS_VITALES_TAM_COLOR_B";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_TEMP_TITULO_EJE = "GRAFICA_SIGNOS_VITALES_TEMP_TITULO_EJE";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_TEMP_TITULO_CONVENCION = "GRAFICA_SIGNOS_VITALES_TEMP_TITULO_CONVENCION";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_TEMP_ESCALA_MINIMA = "GRAFICA_SIGNOS_VITALES_TEMP_ESCALA_MINIMA";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_TEMP_ESCALA_MAXIMA = "GRAFICA_SIGNOS_VITALES_TEMP_ESCALA_MAXIMA";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_TEMP_COLOR_R = "GRAFICA_SIGNOS_VITALES_TEMP_COLOR_R";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_TEMP_COLOR_G = "GRAFICA_SIGNOS_VITALES_TEMP_COLOR_G";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_TEMP_COLOR_B = "GRAFICA_SIGNOS_VITALES_TEMP_COLOR_B";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_OXIM_TITULO_EJE = "GRAFICA_SIGNOS_VITALES_OXIM_TITULO_EJE";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_OXIM_TITULO_CONVENCION = "GRAFICA_SIGNOS_VITALES_OXIM_TITULO_CONVENCION";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_OXIM_ESCALA_MINIMA = "GRAFICA_SIGNOS_VITALES_OXIM_ESCALA_MINIMA";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_OXIM_ESCALA_MAXIMA = "GRAFICA_SIGNOS_VITALES_OXIM_ESCALA_MAXIMA";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_OXIM_COLOR_R = "GRAFICA_SIGNOS_VITALES_OXIM_COLOR_R";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_OXIM_COLOR_G = "GRAFICA_SIGNOS_VITALES_OXIM_COLOR_G";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_OXIM_COLOR_B = "GRAFICA_SIGNOS_VITALES_OXIM_COLOR_B";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_CO2_TITULO_EJE = "GRAFICA_SIGNOS_VITALES_CO2_TITULO_EJE";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_CO2_TITULO_CONVENCION = "GRAFICA_SIGNOS_VITALES_CO2_TITULO_CONVENCION";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_CO2_ESCALA_MINIMA = "GRAFICA_SIGNOS_VITALES_CO2_ESCALA_MINIMA";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_CO2_ESCALA_MAXIMA = "GRAFICA_SIGNOS_VITALES_CO2_ESCALA_MAXIMA";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_CO2_COLOR_R = "GRAFICA_SIGNOS_VITALES_CO2_COLOR_R";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_CO2_COLOR_G = "GRAFICA_SIGNOS_VITALES_CO2_COLOR_G";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_CO2_COLOR_B = "GRAFICA_SIGNOS_VITALES_CO2_COLOR_B";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_PVC_TITULO_EJE = "GRAFICA_SIGNOS_VITALES_PVC_TITULO_EJE";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_PVC_TITULO_CONVENCION = "GRAFICA_SIGNOS_VITALES_PVC_TITULO_CONVENCION";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_PVC_ESCALA_MINIMA = "GRAFICA_SIGNOS_VITALES_PVC_ESCALA_MINIMA";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_PVC_ESCALA_MAXIMA = "GRAFICA_SIGNOS_VITALES_PVC_ESCALA_MAXIMA";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_PVC_COLOR_R = "GRAFICA_SIGNOS_VITALES_PVC_COLOR_R";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_PVC_COLOR_G = "GRAFICA_SIGNOS_VITALES_PVC_COLOR_G";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_PVC_COLOR_B = "GRAFICA_SIGNOS_VITALES_PVC_COLOR_B";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_DOLOR_TITULO_EJE = "GRAFICA_SIGNOS_VITALES_DOLOR_TITULO_EJE";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_DOLOR_TITULO_CONVENCION = "GRAFICA_SIGNOS_VITALES_DOLOR_TITULO_CONVENCION";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_DOLOR_ESCALA_MINIMA = "GRAFICA_SIGNOS_VITALES_DOLOR_ESCALA_MINIMA";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_DOLOR_ESCALA_MAXIMA = "GRAFICA_SIGNOS_VITALES_DOLOR_ESCALA_MAXIMA";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_DOLOR_COLOR_R = "GRAFICA_SIGNOS_VITALES_DOLOR_COLOR_R";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_DOLOR_COLOR_G = "GRAFICA_SIGNOS_VITALES_DOLOR_COLOR_G";
  
  public static final String PROP_GRAFICA_SIGNOS_VITALES_DOLOR_COLOR_B = "GRAFICA_SIGNOS_VITALES_DOLOR_COLOR_B";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_TITULO = "GRAFICA_BALANCE_LIQUIDOS_TITULO";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_ALTO = "GRAFICA_BALANCE_LIQUIDOS_ALTO";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_ANCHO = "GRAFICA_BALANCE_LIQUIDOS_ANCHO";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_ADMIN_TITULO_EJE = "GRAFICA_BALANCE_LIQUIDOS_ADMIN_TITULO_EJE";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_ADMIN_TITULO_CONVENCION = "GRAFICA_BALANCE_LIQUIDOS_ADMIN_TITULO_CONVENCION";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_ADMIN_ESCALA_MINIMA = "GRAFICA_BALANCE_LIQUIDOS_ADMIN_ESCALA_MINIMA";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_ADMIN_ESCALA_MAXIMA = "GRAFICA_BALANCE_LIQUIDOS_ADMIN_ESCALA_MAXIMA";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_ADMIN_COLOR_R = "GRAFICA_BALANCE_LIQUIDOS_ADMIN_COLOR_R";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_ADMIN_COLOR_G = "GRAFICA_BALANCE_LIQUIDOS_ADMIN_COLOR_G";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_ADMIN_COLOR_B = "GRAFICA_BALANCE_LIQUIDOS_ADMIN_COLOR_B";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_ELIM_TITULO_EJE = "GRAFICA_BALANCE_LIQUIDOS_ELIM_TITULO_EJE";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_ELIM_TITULO_CONVENCION = "GRAFICA_BALANCE_LIQUIDOS_ELIM_TITULO_CONVENCION";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_ELIM_ESCALA_MINIMA = "GRAFICA_BALANCE_LIQUIDOS_ELIM_ESCALA_MINIMA";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_ELIM_ESCALA_MAXIMA = "GRAFICA_BALANCE_LIQUIDOS_ELIM_ESCALA_MAXIMA";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_ELIM_COLOR_R = "GRAFICA_BALANCE_LIQUIDOS_ELIM_COLOR_R";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_ELIM_COLOR_G = "GRAFICA_BALANCE_LIQUIDOS_ELIM_COLOR_G";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_ELIM_COLOR_B = "GRAFICA_BALANCE_LIQUIDOS_ELIM_COLOR_B";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_ACUM_TITULO_EJE = "GRAFICA_BALANCE_LIQUIDOS_ACUM_TITULO_EJE";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_ACUM_TITULO_CONVENCION = "GRAFICA_BALANCE_LIQUIDOS_ACUM_TITULO_CONVENCION";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_ACUM_ESCALA_MINIMA = "GRAFICA_BALANCE_LIQUIDOS_ACUM_ESCALA_MINIMA";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_ACUM_ESCALA_MAXIMA = "GRAFICA_BALANCE_LIQUIDOS_ACUM_ESCALA_MAXIMA";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_ACUM_COLOR_R = "GRAFICA_BALANCE_LIQUIDOS_ACUM_COLOR_R";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_ACUM_COLOR_G = "GRAFICA_BALANCE_LIQUIDOS_ACUM_COLOR_G";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_ACUM_COLOR_B = "GRAFICA_BALANCE_LIQUIDOS_ACUM_COLOR_B";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_PESO_TITULO_EJE = "GRAFICA_BALANCE_LIQUIDOS_PESO_TITULO_EJE";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_PESO_TITULO_CONVENCION = "GRAFICA_BALANCE_LIQUIDOS_PESO_TITULO_CONVENCION";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_PESO_ESCALA_MINIMA = "GRAFICA_BALANCE_LIQUIDOS_PESO_ESCALA_MINIMA";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_PESO_ESCALA_MAXIMA = "GRAFICA_BALANCE_LIQUIDOS_PESO_ESCALA_MAXIMA";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_PESO_COLOR_R = "GRAFICA_BALANCE_LIQUIDOS_PESO_COLOR_R";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_PESO_COLOR_G = "GRAFICA_BALANCE_LIQUIDOS_PESO_COLOR_G";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_PESO_COLOR_B = "GRAFICA_BALANCE_LIQUIDOS_PESO_COLOR_B";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_GASTO_TITULO_EJE = "GRAFICA_BALANCE_LIQUIDOS_GASTO_TITULO_EJE";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_GASTO_TITULO_CONVENCION = "GRAFICA_BALANCE_LIQUIDOS_GASTO_TITULO_CONVENCION";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_GASTO_ESCALA_MINIMA = "GRAFICA_BALANCE_LIQUIDOS_GASTO_ESCALA_MINIMA";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_GASTO_ESCALA_MAXIMA = "GRAFICA_BALANCE_LIQUIDOS_GASTO_ESCALA_MAXIMA";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_GASTO_COLOR_R = "GRAFICA_BALANCE_LIQUIDOS_GASTO_COLOR_R";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_GASTO_COLOR_G = "GRAFICA_BALANCE_LIQUIDOS_GASTO_COLOR_G";
  
  public static final String PROP_GRAFICA_BALANCE_LIQUIDOS_GASTO_COLOR_B = "GRAFICA_BALANCE_LIQUIDOS_GASTO_COLOR_B";
  
  public static final int DB_SIZE_CODIGO = 7;
  
  public static final String DB_RESPUESTA_OK = "1000000";
  
  public static final String DB_RESPUESTA_ERROR = "0780000";
  
  public static final String DB_RESPUESTA_OBS = "0380000";
  
  public static final int DB_PARAM_HISTORIA = 0;
  
  public static final int DB_PARAM_TIPO_Y_NUM_ID = 1;
  
  public static final int DB_PARAM_CAMA = 2;
  
  public static final int DB_PARAM_NOMBRES_Y_APELLIDOS = 3;
  
  public static final int DB_PARAM_SIZE_ID_MEDICO = 13;
  
  public static final int DB_PARAM_SIZE_NO_DOCUMENTO = 13;
  
  public static final int DB_PARAM_SIZE_NO_CAMA = 8;
  
  public static final int DB_PARAM_SIZE_COD_CENTRO_ATENCION = 5;
  
  public static final int DB_PARAM_SIZE_COD_SERVICIO = 8;
  
  public static final int DB_PARAM_SIZE_COD_PRESTACION = 8;
  
  public static final int DB_PARAM_SIZE_COD_CONVENIO = 8;
  
  public static final int DB_PARAM_SIZE_RUT_PROFESIONAL = 13;
  
  public static final int DB_TIPO_BALANCE_LIQUIDO_ADMIN = 1;
  
  public static final int DB_TIPO_BALANCE_LIQUIDO_ELIM = 0;
  
  public static final String DB_TIPO_BALANCE_LIQUIDO_ADMIN_DESC = "ADMINISTRADO";
  
  public static final String DB_TIPO_BALANCE_LIQUIDO_ELIM_DESC = "ELIMINADO";
  
  public static final String FSFB_PACIENTE_INTERNACIONAL = "I";
  
  public static final String FSFB_PACIENTE_NO_INTERNACIONAL = "N";
  
  public static final String FSFB_COD_MEDICO_TRATANTE = "T";
  
  public static final String FSFB_COD_MEDICO_COTRATANTE = "C";
  
  public static final String FSFB_DESC_MEDICO_TRATANTE = "TRATANTE";
  
  public static final String FSFB_DESC_MEDICO_COTRATANTE = "COTRATANTE";
  
  public static final String FSFB_TIPO_ANTECEDENTE_ALERGIA = "G";
  
  public static final String FSFB_TIPO_ANTECEDENTE_ALERTA = "T";
  
  public static final String FSFB_TIPO_ANTECEDENTE_PATOLOGICO = "P";
  
  public static final String FSFB_TIPO_ANTECEDENTE_QUIRURGICO = "Q";
  
  public static final String FSFB_COD_CENTRO_MEDICO_FUNDACION = "FSFB";
  
  public static final String FSFB_CONSULTAS_TELEMEDICINA = "TELE";
  
  public static final String FSFB_ORDEN_ASCENDENTE = "ASC";
  
  public static final String FSFB_ORDEN_DESCENDENTE = "DESC";
  
  public static final String FSFB_TIPO_USUARIO_MEDICO_COD = "ME";
  
  public static final String FSFB_TIPO_USUARIO_MEDICO_DESC = "Medico";
  
  public static final String FSFB_TIPO_USUARIO_PACIENTE_COD = "PA";
  
  public static final String FSFB_TIPO_USUARIO_PACIENTE_DESC = "Paciente";
  
  public static final String FSFB_TIPO_USUARIO_TELE_REMISOR_COD = "TR";
  
  public static final String FSFB_TIPO_USUARIO_TELE_REMISOR_DESC = "Telemedicina Remisor";
  
  public static final String FSFB_TIPO_USUARIO_TELE_APROBADOR_COD = "TA";
  
  public static final String FSFB_TIPO_USUARIO_TELE_APROBADOR_DESC = "Telemedicina Aprobador";
  
  public static final String FSFB_TIPO_USUARIO_ADMINISTRADOR_COD = "AD";
  
  public static final String FSFB_TIPO_USUARIO_ADMINISTRADOR_DESC = "Administrador";
  
  public static final String FSFB_ROL_ADMINISTRADORES = "Administradores Portal";
  
  public static final String FSFB_ROL_ADMINISTRADORES_WPS = "wpsadmins";
  
  public static final String FSFB_ROL_MEDICOS = "Medicos Portal";
  
  public static final String FSFB_ROL_PACIENTES = "Pacientes";
  
  public static final String FSFB_ROL_REMISORES_TELE = "Remisores";
  
  public static final String FSFB_ROL_APROBADORES_TELE = "Aprobadores Portal";
  
  public static final String SESION_ATRIB_USUARIO = "USER";
  
  public static final String SESION_ATRIB_PASSWORD = "PASSWORD";
  
  public static final String SESION_ATRIB_TIPO_USUARIO = "TIPO_USUARIO";
  
  public static final String SESION_ATRIB_ID_HIS = "ID_HIS";
  
  public static final String SESION_ATRIB_PAC_NUMERO = "PAC_NUMERO";
  
  public static final String SESION_ATRIB_NOMBRES = "NOMBRES";
  
  public static final String SESION_ATRIB_APELLIDOS = "APELLIDOS";
  
  public static final String SESION_ATRIB_TIPO_ID = "TIPO_ID";
  
  public static final String SESION_ATRIB_NUM_ID = "NUM_ID";
  
  public static final String SESION_ATRIB_CONVENIO = "CONVENIO";
  
  public static final String SESION_ATRIB_PARAM_PACNUM = "PARAM_PACNUM";
  
  public static final String SESION_ATRIB_PARAM_TEXTID = "PARAM_TEXTID";
  
  public static final String SESION_ATRIB_PARAM_PACNOM = "PARAM_PACNOM";
  
  public static final String LDAP_WMM_ATRIB_NOMBRE_ROL = "cn";
  
  public static final String LDAP_ADS_ATRIB_NUM_ID = "postalCode";
  
  public static final String LDAP_ADS_ATRIB_TIPO_ID = "facsimileTelephoneNumber";
  
  public static final String LDAP_ADS_ATRIB_NOMBRES = "givenName";
  
  public static final String LDAP_ADS_ATRIB_APELLIDOS = "sn";
  
  public static final String LDAP_ADS_ATRIB_GENERO = "pager";
  
  public static final String LDAP_ADS_ATRIB_EMAIL = "mail";
  
  public static final String LDAP_ADS_ATRIB_FECHA_NACIMIENTO = "mobile";
  
  public static final String LDAP_ADS_ATRIB_USUARIO = "sAMAccountName";
  
  public static final String LDAP_TDS_ATRIB_NUM_ID = "employeeNumber";
  
  public static final String LDAP_TDS_ATRIB_TIPO_ID = "tipo";
  
  public static final String LDAP_TDS_ATRIB_NOMBRES = "givenName";
  
  public static final String LDAP_TDS_ATRIB_PRIMER_APELLIDO = "initials";
  
  public static final String LDAP_TDS_ATRIB_SEGUNDO_APELLIDO = "sn";
  
  public static final String LDAP_TDS_ATRIB_GENERO = "title";
  
  public static final String LDAP_TDS_ATRIB_EMAIL = "mail";
  
  public static final String LDAP_TDS_ATRIB_FECHA_NACIMIENTO = "postalAddress";
  
  public static final String LDAP_TDS_ATRIB_USUARIO = "uid";
  
  public static final String LDAP_TDS_ATRIB_PASSWORD = "password";
  
  public static final String LDAP_TDS_ATRIB_PAC_PAC_NUMERO = "telephoneNumber";
  
  public static final String LDAP_TDS_ATRIB_CONVENIO = "convenio";
  
  public static final String LDAP_TDS_ATRIB_COMMON_NAME = "cn";
  
  public static final String LDAP_TDS_ATRIB_PARENT_NAME = "cn=users,dc=efsfb,dc=org,dc=co";
}

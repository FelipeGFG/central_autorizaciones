/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.fsfb.ws.rest.util;

/**
 *
 * @author Felipe
 */
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.NamingException;
import oracle.jdbc.OracleDriver;

/**
 *
 * @author SISCOMPUTO
 */
public class ConexionDirectaDesportal implements AutoCloseable, Serializable {

    private static Logger log = Logger.getLogger(ConexionDirectaDesportal.class.getName());
    private static ConexionDirectaDesportal conexion;
    private Connection conn;
    private String usuario;
    private String host;
    private String contra;
    private String urlJdbc;

    public ConexionDirectaDesportal() throws NamingException {
        traerDatos();
    }

    /**
     * Funcion con la cual genero la conexion a la base de datos
     *
     * @return
     */
    public boolean generaConexion() {
        try {
            DriverManager.registerDriver(new OracleDriver());
            conn = DriverManager.getConnection(this.urlJdbc, this.usuario, this.contra);
            return true;
        } catch (Exception e) {
            log.log(Level.SEVERE, "Error ", e);
            return false;
        }
    }

    /**
     * Metodo con el cual obtengo la instancia del objeto conexion
     *
     * @return
     */
    public static ConexionDirectaDesportal getInstance() throws NamingException {
        if (conexion == null) {
            conexion = new ConexionDirectaDesportal();
        }
        return conexion;
    }

    @Override
    public void close() throws Exception {
        try {
            this.conn.close();
        } catch (SQLException ex) {
            log.log(Level.SEVERE, "Error ", ex);

        }
    }

    public void traerDatos() throws NamingException {

        try {
            String url = PropertiesManager.CONFIG.getString("DB_URL_SQL_DESPORTAL");
            this.usuario = PropertiesManager.CONFIG.getString("DB_USER_SQL_DESPORTAL");
            this.contra = PropertiesManager.CONFIG.getString("DB_PASS_SQL_DESPORTAL");
            this.urlJdbc = url;
        } catch (Exception e) {
            log.log(Level.SEVERE, "Error ", e);
        }
    }

    public static ConexionDirectaDesportal getConexion() {
        return conexion;
    }

    public static void setConexion(ConexionDirectaDesportal conexion) {
        ConexionDirectaDesportal.conexion = conexion;
    }

    public Connection getConn() {
        return conn;
    }

    public void setConn(Connection conn) {
        this.conn = conn;
    }

}

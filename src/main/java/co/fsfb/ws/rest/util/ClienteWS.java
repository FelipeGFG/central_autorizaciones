/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.fsfb.ws.rest.util;

import co.fsfb.ws.rest.service.ClienteCitaServices;
import co.fsfb.ws.rest.vo.GestionAutorizacionCitaVO_WSBus;
import java.util.UUID;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import static org.hibernate.annotations.common.util.impl.LoggerFactory.logger;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Felipe
 */
public class ClienteWS {

    private static Logger logger = LoggerFactory.getLogger(ClienteWS.class);

    public void consumoAuthorizationRegister(GestionAutorizacionCitaVO_WSBus vo) {
        try {
            JSONObject transactionInfo = new JSONObject();
            transactionInfo.accumulate("idTransaction", UUID.randomUUID().toString().substring(0, 5));

            JSONObject patientInfo = new JSONObject();
            patientInfo.accumulate("patientNumber", vo.getPacNum());

            JSONObject authorizationInfo = new JSONObject();
            authorizationInfo.accumulate("number", vo.getGauCodigoAutorizacion());
            authorizationInfo.accumulate("formNumber", vo.getNroFormulario()    );
            authorizationInfo.accumulate("description", vo.getGauObservaciones());
            authorizationInfo.accumulate("authDate", vo.getFechaCita());
            authorizationInfo.accumulate("agreementCode", vo.getCodConvenio());
            authorizationInfo.accumulate("authPersonName", vo.getNombreConvenio());
            authorizationInfo.accumulate("CUPS", vo.getCodigoPrestacion());
            authorizationInfo.accumulate("value", vo.getGauValorPrestacion());
            authorizationInfo.accumulate("userCode", vo.getCodUsrCita());
            
            JSONObject objeto = new JSONObject();
            objeto.append("transactionInfo", transactionInfo);
            objeto.append("patientInfo", patientInfo);
            objeto.append("authorizationInfo", authorizationInfo);
            
            logger.info("" + objeto);

            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, objeto.toString().replaceAll("\\[", "").replaceAll("\\]",""));
            Request request = new Request.Builder()
                    .url("http://10.1.0.95:7080/service/ambulatory/authorizationRegister")
                    .method("POST", body)
                    .addHeader("Content-Type", "application/json")
                    .build();
            Response response = client.newCall(request).execute();
            
            logger.info("Petici�n realizada.");
            logger.info(""+response.body().string());

        } catch (Exception e) {
            logger.error("Error en la petici�n: " + e.getMessage());
            e.printStackTrace();
        }
    }
}

package co.fsfb.ws.rest.util;

import co.fsfb.ws.rest.exception.DataException;
import co.fsfb.ws.rest.exception.EnvioEmailException;
import java.util.Date;
import java.util.Properties;
import javax.activation.CommandMap;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.activation.MailcapCommandMap;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.naming.Context;
import javax.naming.InitialContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EnviarEmail {
  private static Logger logger = LoggerFactory.getLogger(EnviarEmail.class);
  
  public static void enviarConGMail(String destinatario, String asunto, String cuerpo) throws EnvioEmailException {
    logger.info("Iniciando enviarConGMail::::");
    Properties props = System.getProperties();
    props.put("mail.smtp.host", PropertiesManager.CONFIG.getString("MAIL_SMTP"));
    props.put("mail.smtp.user", PropertiesManager.CONFIG.getString("MAIL_REMITENTE"));
    props.put("mail.smtp.clave", PropertiesManager.CONFIG.getString("MAIL_CLAVE"));
    props.put("mail.smtp.auth", PropertiesManager.CONFIG.getString("MAIL_AUTH"));
    props.put("mail.smtp.starttls.enable", PropertiesManager.CONFIG.getString("MAIL_TLS"));
    props.put("mail.smtp.port", PropertiesManager.CONFIG.getString("MAIL_PORT"));
    Session session = Session.getDefaultInstance(props);
    MimeMessage message = new MimeMessage(session);
    try {
      message.setFrom((Address)new InternetAddress(PropertiesManager.CONFIG.getString("MAIL_REMITENTE")));
      message.addRecipient(Message.RecipientType.TO, (Address)new InternetAddress(destinatario));
      message.setSubject(asunto);
      message.setText(cuerpo, "UTF-8", PropertiesManager.CONFIG.getString("MAIL_SUBTYPE"));
      Transport transport = session.getTransport("smtp");
      transport.connect(PropertiesManager.CONFIG.getString("MAIL_SMTP"), PropertiesManager.CONFIG
          .getString("MAIL_REMITENTE"), PropertiesManager.CONFIG
          .getString("MAIL_CLAVE"));
      transport.sendMessage((Message)message, message.getAllRecipients());
      transport.close();
    } catch (MessagingException me) {
      throw new EnvioEmailException(me.getMessage(), new Date(), me.getCause().toString());
    } 
  }
  
  public static void enviarConJNDI(String destinatario, String asunto, String cuerpo) throws EnvioEmailException {
    logger.info("Iniciando enviarConJNDI::::");
    try {
      Context context = new InitialContext();
      Session session = (Session)context.lookup(PropertiesManager.CONFIG.getString("spring.mail.jndi-name"));
      MimeMessage message = new MimeMessage(session);
      message.setFrom((Address)new InternetAddress(PropertiesManager.CONFIG.getString("MAIL_REMITENTE")));
      message.addRecipient(Message.RecipientType.TO, (Address)new InternetAddress(destinatario));
      message.setSubject(asunto);
      message.setText(cuerpo, "UTF-8", PropertiesManager.CONFIG.getString("MAIL_SUBTYPE"));
      Transport.send((Message)message);
    } catch (Exception e) {
      throw new EnvioEmailException(e.getMessage(), new Date(), e.getCause().toString());
    } 
  }
  
  public EnviarEmail() throws DataException {
    MailcapCommandMap mc = (MailcapCommandMap)CommandMap.getDefaultCommandMap();
    mc.addMailcap("text/html;; x-java-content-handler=com.sun.mail.handlers.text_html");
    mc.addMailcap("text/plain;; x-java-content-handler=com.sun.mail.handlers.text_plain");
    mc.addMailcap("multipart/*;; x-java-content-handler=com.sun.mail.handlers.multipart_mixed");
    CommandMap.setDefaultCommandMap(mc);
  }
  
  public void postMail(String[] recipients, String subject, String messageBody, String fileNameToAttach) throws DataException {
    String mailHost = PropertiesManager.CONFIG.getString("MAIL_HOST");
    postMail(recipients, null, subject, messageBody, mailHost, fileNameToAttach);
  }
  
  public void postMail(String[] recipients, String subject, String messageBody) throws DataException {
    String mailHost = PropertiesManager.CONFIG.getString("MAIL_HOST");
    postMail(recipients, null, subject, messageBody, mailHost, null);
  }
  
  public void postMail(String[] toRecipients, String[] ccRecipients, String subject, String messageBody, String mailHost, String fileNameToAttach) throws DataException {
    if (toRecipients == null || toRecipients.length == 0)
      throw new DataException(115); 
    boolean debug = false;
    Properties props = new Properties();
    props.put("mail.smtp.auth", "true");
    props.put("mail.smtp.auth.ntlm.domain", "");
    props.put("mail.smtp.host", PropertiesManager.CONFIG.getString("MAIL_HOST"));
    props.put("mail.smtp.port", PropertiesManager.CONFIG.getString("MAIL_SMTP_PORT"));
    Authenticator auth = new Authenticator() {
        protected PasswordAuthentication getPasswordAuthentication() {
          String user = "";
          String password = "";
          try {
            user = PropertiesManager.CONFIG.getString("MAIL_SMTP_USER");
            password = PropertiesManager.CONFIG.getString("MAIL_SMTP_PASSWORD");
          } catch (Exception e) {
            EnviarEmail.logger.error(e.getMessage(), e);
          } 
          return new PasswordAuthentication(user, password);
        }
      };
    Session session = Session.getInstance(props, auth);
    session.setDebug(debug);
    try {
      MimeMessage mimeMessage = new MimeMessage(session);
      InternetAddress internetAddress = new InternetAddress(PropertiesManager.CONFIG.getString("MAIL_FROM"));
      mimeMessage.setFrom((Address)internetAddress);
      InternetAddress[] arrayOfInternetAddress = new InternetAddress[toRecipients.length];
      for (int i = 0; i < toRecipients.length; i++)
        arrayOfInternetAddress[i] = new InternetAddress(toRecipients[i]); 
      mimeMessage.setRecipients(Message.RecipientType.TO, (Address[])arrayOfInternetAddress);
      if (ccRecipients != null && ccRecipients.length > 0) {
        InternetAddress[] arrayOfInternetAddress1 = new InternetAddress[ccRecipients.length];
        for (int j = 0; j < ccRecipients.length; j++)
          arrayOfInternetAddress1[j] = new InternetAddress(ccRecipients[j]); 
        mimeMessage.setRecipients(Message.RecipientType.CC, (Address[])arrayOfInternetAddress1);
      } 
      mimeMessage.setSubject(subject);
      MimeBodyPart mimeBodyPart = new MimeBodyPart();
      String htmlText1 = "<html><body  style='font-family: Arial, Helvetica, sans-serif; font-size: 12px;'>";
      String htmlText3 = "</body></html>";
      mimeBodyPart.setContent(htmlText1 + messageBody + htmlText3, "text/html");
      MimeMultipart multipart = new MimeMultipart("related");
      multipart.addBodyPart((BodyPart)mimeBodyPart);
      mimeBodyPart = new MimeBodyPart();
      if (fileNameToAttach != null) {
        mimeBodyPart = new MimeBodyPart();
        DataSource fds = new FileDataSource(fileNameToAttach);
        mimeBodyPart.setDataHandler(new DataHandler(fds));
        mimeBodyPart.setFileName(fds.getName());
        multipart.addBodyPart((BodyPart)mimeBodyPart);
      } 
      mimeMessage.setContent((Multipart)multipart);
      Transport.send((Message)mimeMessage);
    } catch (Exception ex) {
      ex.printStackTrace();
      logger.error(ex.getMessage(), ex);
      throw new DataException(116, ex);
    } 
  }
}

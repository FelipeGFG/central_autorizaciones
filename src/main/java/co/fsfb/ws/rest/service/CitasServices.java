package co.fsfb.ws.rest.service;

import co.fsfb.ws.rest.dto.CitaDTO;
import co.fsfb.ws.rest.exception.CitaNoEncontradaException;
import co.fsfb.ws.rest.response.GenericResponse;
import co.fsfb.ws.rest.util.PropertiesManager;
import co.fsfb.ws.rest.vo.CitaPorPagarVO;
import co.fsfb.ws.rest.vo.ConsultaCitaPacientePorAutoVO;
import co.fsfb.ws.rest.vo.ConsultaCitaPacienteVO;
import co.fsfb.ws.rest.vo.ConsultaCitaPorPacienteAutoVO;
import co.fsfb.ws.rest.vo.ResultadoCitaPacienteVO;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/Citas")
public class CitasServices {
  private static Logger logger = LoggerFactory.getLogger(CitasServices.class);
  
  private CitaDTO cita = new CitaDTO();
  
  @POST
  @Path("/CCita")
  @Consumes({"application/json"})
  @Produces({"application/json"})
  public Response ConsultaCita(ConsultaCitaPacienteVO consultaCitaPacienteVO) {
    logger.info("Iniciando Consulta Cita::::");
    try {
      List<ResultadoCitaPacienteVO> resultadoCitaPacienteVO = this.cita.citas(consultaCitaPacienteVO);
      return Response.status(Response.Status.OK).entity(resultadoCitaPacienteVO).build();
    } catch (CitaNoEncontradaException e) {
      return Response.status(Response.Status.NOT_FOUND).entity(new GenericResponse(e.getCause(), PropertiesManager.CONFIG
            .getString("NOT_FOUND_RESULT_CONSULTA_CITAS_PACIENTES"))).build();
    } 
  }
  
  @POST
  @Path("/CCitaPorAutorizar")
  @Consumes({"application/json"})
  @Produces({"application/json"})
  public Response consultaCitaPorAutorizar(ConsultaCitaPacientePorAutoVO consultaCitaPacientePorAutoVO) {
    logger.info("Iniciando Consulta Cita Por Auto::::");
    try {
      List<ResultadoCitaPacienteVO> resultadoCitaPacienteVO = this.cita.citasPorAutorizar(consultaCitaPacientePorAutoVO);
      return Response.status(Response.Status.OK).entity(resultadoCitaPacienteVO).build();
    } catch (CitaNoEncontradaException e) {
      return Response.status(Response.Status.NOT_FOUND).entity(new GenericResponse(e.getCause(), PropertiesManager.CONFIG
            .getString("NOT_FOUND_RESULT_CONSULTA_CITAS_PACIENTES"))).build();
    } 
  }
  
  @POST
  @Path("/CCitasParaPagar")
  @Consumes({"application/json"})
  @Produces({"application/json"})
  public Response consultaCitasParaPagar(ConsultaCitaPorPacienteAutoVO consultaCitaPacientePorAutoVO) {
    logger.info("Iniciando Consulta citas autorizadas::::");
    try {
      List<CitaPorPagarVO> citaPorPagarVOs = this.cita.citasParaPagar(consultaCitaPacientePorAutoVO);
      return Response.status(Response.Status.OK).entity(citaPorPagarVOs).build();
    } catch (Exception e) {
      return Response.status(Response.Status.NOT_FOUND).entity(new GenericResponse(e.getCause(), PropertiesManager.CONFIG
            .getString("NOT_FOUND_RESULT_CONSULTA_CITAS_PACIENTES"))).build();
    } 
  }
  
  @GET
  @Path("/listaUbicacion")
  @Produces({"application/json"})
  public Response listaUbicacion() {
    logger.info("Iniciando Lista motivos eliminacion::::");
    try {
      return Response.status(Response.Status.OK)
        .entity(this.cita.listaUbicacion()).build();
    } catch (Exception e) {
      return Response.status(Response.Status.BAD_REQUEST).build();
    } 
  }
}

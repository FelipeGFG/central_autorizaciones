package co.fsfb.ws.rest.service;

import co.fsfb.ws.rest.dto.OrdenesMedicasDTO;
import co.fsfb.ws.rest.dto.SharePointDTO;
import co.fsfb.ws.rest.exception.CredencialesFallidasException;
import co.fsfb.ws.rest.exception.RespondException;
import co.fsfb.ws.rest.response.GenericResponse;
import co.fsfb.ws.rest.util.PropertiesManager;
import co.fsfb.ws.rest.vo.Documento;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/SharePoint")
public class SharePointServices {
  private static Logger logger = LoggerFactory.getLogger(SharePointServices.class);
  
  private SharePointDTO SP = new SharePointDTO();
  
  private OrdenesMedicasDTO ordenesMedicasDTO = new OrdenesMedicasDTO();
  
  @POST
  @Path("/subirDoc")
  @Consumes({"application/json"})
  @Produces({"application/json"})
  public Response subirDoc(Documento doc) {
    logger.info("Iniciando subirDoc::::");
    try {
      if (this.SP.uploadDocument(doc).isRespuesta()) {
        if (doc.getOrmIdOrdmNumero() != null)
          this.ordenesMedicasDTO.saveFile(doc.getOrmIdOrdmNumero(), doc.getNombreArchivo()); 
        return Response.status(Response.Status.OK).entity(PropertiesManager.CONFIG.getString("SP_SUCCESS"))
          .build();
      } 
      return Response.status(Response.Status.NOT_MODIFIED)
        .entity(PropertiesManager.CONFIG.getString("SP_NOT_MODIFIED")).build();
    } catch (CredencialesFallidasException|co.fsfb.ws.rest.exception.DataException e) {
      logger.error("Error guardando archivo: ", (Throwable)e);
      return Response.status(Response.Status.NOT_MODIFIED)
        .entity(new GenericResponse(e.getCause(), PropertiesManager.CONFIG.getString("SP_NOT_MODIFIED")))
        .build();
    } 
  }
  
  @POST
  @Path("/actualizarDoc")
  @Consumes({"application/json"})
  @Produces({"application/json"})
  public Response actualizarDoc(Documento doc) {
    logger.info("Iniciando actualizarDoc::::");
    try {
      if (this.SP.updateDocument(doc))
        return Response.status(Response.Status.OK).entity(PropertiesManager.CONFIG.getString("SP_SUCCESS"))
          .build(); 
      return Response.status(Response.Status.NOT_MODIFIED)
        .entity(PropertiesManager.CONFIG.getString("SP_NOT_MODIFIED")).build();
    } catch (CredencialesFallidasException e) {
      return Response.status(Response.Status.NOT_MODIFIED)
        .entity(new GenericResponse(e.getCause(), PropertiesManager.CONFIG.getString("SP_NOT_MODIFIED")))
        .build();
    } 
  }
  
  @POST
  @Path("/eliminarDoc")
  @Consumes({"application/json"})
  @Produces({"application/json"})
  public Response eliminarDoc(Documento doc) {
    logger.info("Iniciando actualizarDoc::::");
    try {
      if (this.SP.deleteDocument(doc))
        return Response.status(Response.Status.OK)
          .entity(PropertiesManager.CONFIG.getString("SP_DELETE_SUCCESS")).build(); 
      return Response.status(Response.Status.NOT_MODIFIED)
        .entity(PropertiesManager.CONFIG.getString("SP_NOT_MODIFIED")).build();
    } catch (CredencialesFallidasException e) {
      return Response.status(Response.Status.NOT_MODIFIED)
        .entity(new GenericResponse(e.getCause(), PropertiesManager.CONFIG.getString("SP_NOT_MODIFIED")))
        .build();
    } 
  }
  
  @POST
  @Path("/buscarDoc")
  @Consumes({"application/json"})
  @Produces({"application/json"})
  public void buscarDoc(Documento doc, @Context HttpServletResponse response) throws IOException {
    logger.info("Iniciando buscarDoc::::");
    try {
      byte[] fileByte = this.SP.findDocument(doc);
      response.setContentLength(fileByte.length);
      BufferedOutputStream outStream = new BufferedOutputStream((OutputStream)response.getOutputStream());
      outStream.write(fileByte);
      outStream.flush();
    } catch (CredencialesFallidasException e) {
      logger.error("Error descargando archivo", (Throwable)e);
    } 
  }
}

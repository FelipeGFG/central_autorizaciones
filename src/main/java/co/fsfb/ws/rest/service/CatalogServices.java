package co.fsfb.ws.rest.service;

import co.fsfb.ws.rest.dto.CatalogDTO;
import co.fsfb.ws.rest.exception.CitaNoEncontradaException;
import co.fsfb.ws.rest.exception.ConsultaNoEncontradaException;
import co.fsfb.ws.rest.response.GenericResponse;
import co.fsfb.ws.rest.util.PropertiesManager;
import co.fsfb.ws.rest.vo.CIE;
import co.fsfb.ws.rest.vo.CitasAgendadas;
import co.fsfb.ws.rest.vo.ClasificaConsulta;
import co.fsfb.ws.rest.vo.Convenio;
import co.fsfb.ws.rest.vo.Departamento;
import co.fsfb.ws.rest.vo.DiagnosticosVO;
import co.fsfb.ws.rest.vo.Medicos;
import co.fsfb.ws.rest.vo.Sede;
import co.fsfb.ws.rest.vo.Servicio;
import co.fsfb.ws.rest.vo.SubEspecialidad;
import co.fsfb.ws.rest.vo.TipoDoc;
import co.fsfb.ws.rest.vo.Usuario;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/Catalogo")
public class CatalogServices {

    private static Logger logger = LoggerFactory.getLogger(CatalogServices.class);

    private CatalogDTO dto = new CatalogDTO();

    @GET
    @Path("/Departamento")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response departamento() {
        logger.info("Iniciando Departamento::::");
        try {
            List<Departamento> depto = this.dto.departamento();
            return Response.status(Response.Status.OK).entity(depto).build();
        } catch (ConsultaNoEncontradaException e) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(new GenericResponse(e.getCause(), PropertiesManager.CONFIG.getString("NOT_FOUND_RESULT_CONSULTA_CITAS_PACIENTES")))
                    .build();
        }
    }

    @GET
    @Path("/SubEspecialidades")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response subEspecialidades() {
        logger.info("Iniciando SubEspecialidades::::");
        try {
            List<SubEspecialidad> subs = this.dto.subEspecialidades();
            return Response.status(Response.Status.OK).entity(subs).build();
        } catch (ConsultaNoEncontradaException e) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(new GenericResponse(e.getCause(), PropertiesManager.CONFIG.getString("NOT_FOUND_RESULT_CONSULTA_CITAS_PACIENTES")))
                    .build();
        }
    }

    @GET
    @Path("/Servicios")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response servicios() {
        logger.info("Iniciando Servicios::::");
        try {
            List<Servicio> subs = this.dto.servicios();
            return Response.status(Response.Status.OK).entity(subs).build();
        } catch (ConsultaNoEncontradaException e) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(new GenericResponse(e.getCause(), PropertiesManager.CONFIG.getString("NOT_FOUND_RESULT_CONSULTA_CITAS_PACIENTES")))
                    .build();
        }
    }

    @GET
    @Path("/Servicio")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response servicioxDepto() {
        logger.info("Iniciando servicioxDepto::::");
        try {
            List<Servicio> servicio = this.dto.servicio();
            return Response.status(Response.Status.OK).entity(servicio).build();
        } catch (ConsultaNoEncontradaException e) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(new GenericResponse(e.getCause(), PropertiesManager.CONFIG.getString("NOT_FOUND_RESULT_CONSULTA_CITAS_PACIENTES")))
                    .build();
        }
    }

    @POST
    @Path("/ClasifiConsult")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response ConsDeptoServicio(ClasificaConsulta consul) {
        logger.info("Iniciando servicioxDepto::::");
        try {
            List<ClasificaConsulta> consult = this.dto.clasifiConsult(consul);
            return Response.status(Response.Status.OK).entity(consult).build();
        } catch (ConsultaNoEncontradaException e) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(new GenericResponse(e.getCause(), PropertiesManager.CONFIG.getString("NOT_FOUND_RESULT_CONSULTA_CITAS_PACIENTES")))
                    .build();
        }
    }

    @POST
    @Path("/CIE")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response cie(CIE cie) {
        logger.info("Iniciando cie::::");
        try {
            List<CIE> lstCie = this.dto.cie(cie);
            return Response.status(Response.Status.OK).entity(lstCie).build();
        } catch (ConsultaNoEncontradaException e) {
            return Response.status(Response.Status.UNAUTHORIZED)
                    .entity(new GenericResponse(e.getCause(), PropertiesManager.CONFIG.getString("NOT_FOUND_RESULT_CONSULTA_CITAS_PACIENTES")))
                    .build();
        }
    }

    @GET
    @Path("/Sede")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response sede() {
        logger.info("Iniciando sede::::");
        try {
            List<Sede> sede = this.dto.sede();
            return Response.status(Response.Status.OK).entity(sede).build();
        } catch (ConsultaNoEncontradaException e) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(new GenericResponse(e.getCause(), PropertiesManager.CONFIG.getString("NOT_FOUND_RESULT_CONSULTA_CITAS_PACIENTES")))
                    .build();
        }
    }

    @GET
    @Path("/TipDoc")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response tipoDoc() {
        logger.info("Iniciando tipoDoc::::");
        try {
            List<TipoDoc> tipDoc = this.dto.tipoDoc();
            return Response.status(Response.Status.OK).entity(tipDoc).build();
        } catch (ConsultaNoEncontradaException e) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(new GenericResponse(e.getCause(), PropertiesManager.CONFIG.getString("NOT_FOUND_RESULT_CONSULTA_CITAS_PACIENTES")))
                    .build();
        }
    }

    @GET
    @Path("/Convenio")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response convenio() {
        logger.info("Iniciando convenio::::");
        try {
            List<Convenio> convenio = this.dto.convenio();
            return Response.status(Response.Status.OK).entity(convenio).build();
        } catch (ConsultaNoEncontradaException e) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(new GenericResponse(e.getCause(), PropertiesManager.CONFIG.getString("NOT_FOUND_RESULT_CONSULTA_CITAS_PACIENTES")))
                    .build();
        }
    }

    @GET
    @Path("/Medicos")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response medicos() {
        logger.info("Iniciando medicos::::");
        try {
            List<Medicos> medicos = this.dto.medicos();
            return Response.status(Response.Status.OK).entity(medicos).build();
        } catch (ConsultaNoEncontradaException e) {
            return Response.status(Response.Status.UNAUTHORIZED)
                    .entity(new GenericResponse(e.getCause(), PropertiesManager.CONFIG.getString("NOT_FOUND_RESULT_CONSULTA_CITAS_PACIENTES")))
                    .build();
        }
    }

    @GET
    @Path("/Prestacion")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response prestacion() {
        logger.info("Iniciando Prestacion::::");
        try {
            List<CitasAgendadas> citasA = this.dto.prestacion();
            return Response.status(Response.Status.OK).entity(citasA).build();
        } catch (CitaNoEncontradaException e) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(new GenericResponse(e.getCause(), PropertiesManager.CONFIG.getString("NOT_FOUND_RESULT_CONSULTA_CITAS_PACIENTES")))
                    .build();
        }
    }

    @GET
    @Path("/diagnosticos")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response diagnosticos() {
        logger.info("Iniciando diagnosticos::::");
        try {
            List<DiagnosticosVO> citasA = this.dto.getDiagnosticos();
            return Response.status(Response.Status.OK).entity(citasA).build();
        } catch (ConsultaNoEncontradaException e) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(new GenericResponse(e.getCause(), PropertiesManager.CONFIG.getString("NOT_FOUND_RESULT_CONSULTA_CITAS_PACIENTES")))
                    .build();
        }
    }

    @POST
    @Path("/diagnosticosLike")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response diagnosticosLike(Usuario usuario) {
        logger.info("Iniciando diagnosticos::::");
        try {
            List<DiagnosticosVO> citasA = this.dto.getDiagnosticosLike(usuario.getCn());
            return Response.status(Response.Status.OK).entity(citasA).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(new GenericResponse(e.getCause(), PropertiesManager.CONFIG.getString("NOT_FOUND_RESULT_CONSULTA_CITAS_PACIENTES")))
                    .build();
        }
    }
}

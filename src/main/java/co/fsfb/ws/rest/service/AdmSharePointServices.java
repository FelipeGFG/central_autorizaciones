package co.fsfb.ws.rest.service;

import co.fsfb.ws.rest.cliente.SharePointImpl;
import co.fsfb.ws.rest.response.GenericResponse;
import co.fsfb.ws.rest.util.PropertiesManager;
import co.fsfb.ws.rest.vo.DocumentoVO;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/AdmSharePoint")
public class AdmSharePointServices {
  private static Logger logger = LoggerFactory.getLogger(AdmSharePointServices.class);
  
  SharePointImpl admSharePointImpl = new SharePointImpl();
  
  @POST
  @Path("/administrarDocumento")
  @Consumes({"application/json"})
  @Produces({"application/json"})
  public Response administrarDocumento(DocumentoVO documentoVO) {
    logger.info("Iniciando administrarDocumento(Documento) ::::");
    try {
      this.admSharePointImpl.administrarDocumento(documentoVO);
      return Response.status(Response.Status.OK).build();
    } catch (Exception e) {
      return Response.status(Response.Status.NOT_FOUND).entity(new GenericResponse(e.getCause(), PropertiesManager.CONFIG
            .getString("NOT_FOUND_RESULT_CONSULTA_CITAS_PACIENTES"))).build();
    } 
  }
}

package co.fsfb.ws.rest.service;

import co.fsfb.ws.rest.dto.AutenticacionDTO;
import co.fsfb.ws.rest.exception.CredencialesFallidasException;
import co.fsfb.ws.rest.exception.EmailDuplicadoException;
import co.fsfb.ws.rest.exception.FechaNacimientoException;
import co.fsfb.ws.rest.exception.NombreErroneoException;
import co.fsfb.ws.rest.exception.PacienteNoRegistradoException;
import co.fsfb.ws.rest.exception.SexoException;
import co.fsfb.ws.rest.exception.UsuarioDuplicadoException;
import co.fsfb.ws.rest.exception.UsuarioNoEncontradoException;
import co.fsfb.ws.rest.response.GenericResponse;
import co.fsfb.ws.rest.util.PropertiesManager;
import co.fsfb.ws.rest.vo.Usuario;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/Service")
public class AuthService {

    private static Logger logger = LoggerFactory.getLogger(AuthService.class);

    private AutenticacionDTO aut = new AutenticacionDTO();

    @POST
    @Path("/ingresar")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response ingresar(Usuario usuario) {

        logger.info("Iniciando Ingresar Usuario::::" + usuario.getUid());
        usuario.setUserValido(false);
        AutenticacionDTO aut = new AutenticacionDTO();
        try {
            Usuario usrDev = aut.validarUsuario(usuario);
            if (usrDev.isUserValido()) {
                return Response.status(Response.Status.OK).entity(usrDev).build();
            }
        } catch (CredencialesFallidasException e) {
            logger.info("Fallo Ingresar Usuario::::" + e);
            return Response.status(Response.Status.UNAUTHORIZED)
                    .entity(new GenericResponse(usuario, PropertiesManager.CONFIG.getString("CREDENTIAL_FAILED")))
                    .build();
        }

        return Response.status(Response.Status.UNAUTHORIZED)
                .entity(new GenericResponse(usuario, PropertiesManager.CONFIG.getString("CREDENTIAL_FAILED"))).build();

    }

    @POST
    @Path("/crear")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response crearUsuarioLDAP(Usuario usuario) {
        logger.info("Creacion Usuario::::" + usuario.getUid());
        usuario.setUserValido(false);
        AutenticacionDTO aut = new AutenticacionDTO();
        try {
            Usuario usrDev = aut.crearUsuarioLDAP(usuario);
            if (usrDev.isUserValido()) {
                return Response.status(Response.Status.OK).entity(usrDev).build();
            }
        } catch (CredencialesFallidasException e) {
            return Response.status(Response.Status.UNAUTHORIZED)
                    .entity(new GenericResponse(usuario, e.getMensaje()))
                    .build();
        } catch (UsuarioDuplicadoException e) {
            return Response.status(Response.Status.NOT_ACCEPTABLE)
                    .entity(new GenericResponse(usuario, e.getMensaje()))
                    .build();
        } catch (UsuarioNoEncontradoException e) {
            return Response.status(Response.Status.NOT_ACCEPTABLE)
                    .entity(new GenericResponse(usuario, e.getMensaje()))
                    .build();
        } catch (EmailDuplicadoException e) {
            return Response.status(Response.Status.NOT_ACCEPTABLE)
                    .entity(new GenericResponse(usuario, e.getMensaje()))
                    .build();
        } catch (NombreErroneoException e) {
            return Response.status(Response.Status.NOT_ACCEPTABLE)
                    .entity(new GenericResponse(usuario, e.getMensaje()))
                    .build();
        } catch (PacienteNoRegistradoException e) {
            return Response.status(Response.Status.NOT_ACCEPTABLE)
                    .entity(new GenericResponse(usuario, e.getMensaje()))
                    .build();
        } catch (FechaNacimientoException e) {
            return Response.status(Response.Status.NOT_ACCEPTABLE)
                    .entity(new GenericResponse(usuario, e.getMensaje()))
                    .build();
        } catch (SexoException e) {
            return Response.status(Response.Status.NOT_ACCEPTABLE)
                    .entity(new GenericResponse(usuario, e.getMensaje()))
                    .build();
        }
        return Response.status(Response.Status.UNAUTHORIZED)
                .entity(new GenericResponse(usuario, PropertiesManager.CONFIG.getString("CREDENTIAL_FAILED"))).build();
    }

    @POST
    @Path("/salir")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response salir(Usuario usuario) {
        logger.info("Iniciando Salir::::" + usuario.getUid());
        usuario.setUserValido(false);
        try {
            if (this.aut.validarUsuario(usuario).isUserValido()) {
                return Response.status(Response.Status.OK).entity(usuario).build();
            }
        } catch (CredencialesFallidasException e) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(new GenericResponse(usuario, PropertiesManager.CONFIG.getString("PASSWORD_UPDATE_FAILED")))
                    .build();
        }
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(new GenericResponse(usuario, PropertiesManager.CONFIG.getString("PASSWORD_UPDATE_FAILED")))
                .build();
    }

    @POST
    @Path("/restablecerpwd")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response restablecerContrasena(Usuario usuario) {
        logger.info("Iniciando restablecer Contrasena::::" + usuario.getUid());
        usuario.setUserValido(false);
        try {
            if (this.aut.cambioContrasena(usuario)) {
                return Response.status(Response.Status.OK)
                        .entity(new GenericResponse(usuario, PropertiesManager.CONFIG.getString("PASSWORD_UPDATE_OK")))
                        .build();
            }
            return Response.status(Response.Status.NOT_ACCEPTABLE).entity(new GenericResponse(usuario, PropertiesManager.CONFIG
                    .getString("PASSWORD_UPDATE_FAILED")))
                    .build();
        } catch (CredencialesFallidasException e) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(new GenericResponse(usuario, PropertiesManager.CONFIG.getString("PASSWORD_UPDATE_FAILED")))
                    .build();
        }
    }

    @POST
    @Path("/confrestpwd")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response confirmarRestablecerContrasena(Usuario usuario) {
        logger.info("Confirmar restablecer Contrasena::::" + usuario.getUid());
        try {
            if (this.aut.confirmaCambioContrasena(usuario)) {
                return Response.status(Response.Status.OK)
                        .entity(PropertiesManager.CONFIG.getString("PASSWORD_UPDATE_OK"))
                        .entity(new GenericResponse(usuario, PropertiesManager.CONFIG.getString("PASSWORD_UPDATE_OK")))
                        .build();
            }
            return Response.status(Response.Status.NOT_ACCEPTABLE).entity(new GenericResponse(usuario, PropertiesManager.CONFIG
                    .getString("PASSWORD_UPDATE_FAILED")))
                    .build();
        } catch (CredencialesFallidasException e) {
            return Response.status(Response.Status.UNAUTHORIZED)
                    .entity(new GenericResponse(usuario, PropertiesManager.CONFIG.getString("PASSWORD_UPDATE_FAILED")))
                    .build();
        }
    }
}

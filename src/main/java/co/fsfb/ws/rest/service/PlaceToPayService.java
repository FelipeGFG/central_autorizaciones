package co.fsfb.ws.rest.service;

import co.fsfb.ws.rest.dto.PagosDTO;
import co.fsfb.ws.rest.response.GenericResponse;
import co.fsfb.ws.rest.util.PropertiesManager;
import co.fsfb.ws.rest.vo.ConsultaPagosVO;
import co.fsfb.ws.rest.vo.PagosTransaccionConfirmada;
import co.fsfb.ws.rest.vo.PagosTransaccionCreada;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/PagosService")
public class PlaceToPayService {
  private static Logger logger = LoggerFactory.getLogger(PlaceToPayService.class);
  
  private PagosDTO pagosDTO = new PagosDTO();
  
  @POST
  @Path("/guardarTransaccion")
  @Consumes({"application/json"})
  @Produces({"application/json"})
  public Response guardarTransaccion(PagosTransaccionCreada pagosTransaccionCreada) {
    logger.info("Inicio generaciregistro de pagos. ::::");
    try {
      return Response.status(Response.Status.OK).entity(new GenericResponse(this.pagosDTO.guardarPagos(pagosTransaccionCreada), PropertiesManager.CONFIG
            .getString("SUCCESS_RESULT_CONSULTA_CITAS_PACIENTES"))).build();
    } catch (Exception e) {
      return Response.status(Response.Status.NOT_FOUND).entity(new GenericResponse(e
            .getMessage(), PropertiesManager.CONFIG.getString("ERROR_PAYMENT_URL")))
        .build();
    } 
  }
  
  @GET
  @Path("/resultadoTransaccion/{idTransaccion}")
  @Produces({"application/json"})
  public Response consultarTrazabilidadOrden(@PathParam("idTransaccion") String idTransaccion) {
    try {
      PagosTransaccionConfirmada pagosTransaccionConfirmada = this.pagosDTO.getStatusEarlier(Long.valueOf(Long.parseLong(idTransaccion)));
      return Response.status(Response.Status.OK).entity(pagosTransaccionConfirmada).build();
    } catch (Exception e) {
      return Response.status(Response.Status.NOT_FOUND).entity(new GenericResponse(e.getCause(), PropertiesManager.CONFIG
            .getString("NOT_FOUND_RESULT_CONSULTA_CITAS_PACIENTES"))).build();
    } 
  }
  
  @POST
  @Path("/listaPagos")
  @Consumes({"application/json"})
  @Produces({"application/json"})
  public Response listaPagosPorCliente(ConsultaPagosVO consultaPagosVO) {
    logger.info("Inicio generaciregistro de pagos. ::::");
    try {
      return Response.status(Response.Status.OK).entity(new GenericResponse(this.pagosDTO.listaPagosPorCliente(consultaPagosVO), PropertiesManager.CONFIG
            .getString("SUCCESS_RESULT_CONSULTA_CITAS_PACIENTES"))).build();
    } catch (Exception e) {
      return Response.status(Response.Status.NOT_FOUND).entity(new GenericResponse(e
            .getMessage(), PropertiesManager.CONFIG.getString("ERROR_PAYMENT_URL")))
        .build();
    } 
  }
}

package co.fsfb.ws.rest.service;

import co.fsfb.ws.rest.dto.OrdenesMedicasDTO;
import co.fsfb.ws.rest.entidades.CaPrestacionesOrdMed;
import co.fsfb.ws.rest.exception.DataException;
import co.fsfb.ws.rest.response.GenericResponse;
import co.fsfb.ws.rest.util.PropertiesManager;
import co.fsfb.ws.rest.vo.AdmOrdenesMedicasVO;
import co.fsfb.ws.rest.vo.ConsultaTrazabilidadCitaVO;
import co.fsfb.ws.rest.vo.CrearPrestacionesOrdMedVO;
import co.fsfb.ws.rest.vo.FiltrosOrdenMedicaVO;
import co.fsfb.ws.rest.vo.FinalizarOrdenMedicaVO;
import co.fsfb.ws.rest.vo.GestionAutorizacionVO;
import co.fsfb.ws.rest.vo.GestionContinuidadVO;
import co.fsfb.ws.rest.vo.RespuestaGestionContinuidadVO;
import co.fsfb.ws.rest.vo.RespuestaOrdenMedicaVO;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/ordenesMedicas")
public class OrdenesMedicasServices {
	private static Logger logger = LoggerFactory.getLogger(OrdenesMedicasServices.class);

	private OrdenesMedicasDTO ordenesMedicasDTO = new OrdenesMedicasDTO();

	@POST
	@Path("/consultarOrdenMedica")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response consultarOrdenMedica(FiltrosOrdenMedicaVO filtrosOrdenMedicaVO) {
		logger.info("Iniciando consultarOrdenMedica(Long) - CaCitasGestionadasDAO ::::");
		try {
			List<RespuestaOrdenMedicaVO> respuestaOrdenMedicaVO = this.ordenesMedicasDTO
					.consultarOrdenMedica(filtrosOrdenMedicaVO);
			return Response.status(Response.Status.OK).entity(respuestaOrdenMedicaVO).build();
		} catch (Exception e) {
			return Response.status(Response.Status.NOT_FOUND).entity(new GenericResponse(e.getCause(),
					PropertiesManager.CONFIG.getString("NOT_FOUND_RESULT_CONSULTA_CITAS_PACIENTES"))).build();
		}
	}

	@POST
	@Path("/consultarOrdenesMedicas")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response consultarOrdenesMedicas(FiltrosOrdenMedicaVO filtrosOrdenMedicaVO) {
		logger.info("Iniciando consultarOrdenMedica(Long) - CaCitasGestionadasDAO ::::");
		try {
			List<RespuestaOrdenMedicaVO> respuestaOrdenMedicaVO = this.ordenesMedicasDTO
					.consultarOrdenesMedicas(filtrosOrdenMedicaVO);
			return Response.status(Response.Status.OK).entity(respuestaOrdenMedicaVO).build();
		} catch (Exception e) {
			return Response.status(Response.Status.NOT_FOUND).entity(new GenericResponse(e.getCause(),
					PropertiesManager.CONFIG.getString("NOT_FOUND_RESULT_CONSULTA_CITAS_PACIENTES"))).build();
		}
	}

	@GET
	@Path("/consultarTrazabilidadOrden/{ormIdOrdmNumero}")
	@Produces({ "application/json" })
	public Response consultarTrazabilidadOrden(@PathParam("ormIdOrdmNumero") Long ormIdOrdmNumero) {
		logger.info("Iniciando consultarTrazabilidadOrden(Long) ::::");
		try {
			List<ConsultaTrazabilidadCitaVO> consultaTrazabilidadCitaVO = this.ordenesMedicasDTO
					.consultarTrazabilidadOrden(ormIdOrdmNumero);
			return Response.status(Response.Status.OK).entity(consultaTrazabilidadCitaVO).build();
		} catch (Exception e) {
			return Response.status(Response.Status.NOT_FOUND).entity(new GenericResponse(e.getCause(),
					PropertiesManager.CONFIG.getString("NOT_FOUND_RESULT_CONSULTA_CITAS_PACIENTES"))).build();
		}
	}

	@POST
	@Path("/gestionContinuidad")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response gestionContinuidad(GestionContinuidadVO gestionContinuidadVO) {
		logger.info("Iniciando creacionGestionContinuidad(Long) - CaCitasGestionadasDAO ::::");
		try {
			RespuestaGestionContinuidadVO respuestaGestionContinuidad = this.ordenesMedicasDTO
					.crearGestionContinuidad(gestionContinuidadVO);
			return Response.status(Response.Status.OK).entity(respuestaGestionContinuidad).build();
		} catch (Exception e) {
			logger.error(PropertiesManager.CONFIG.getString("GESTION_CONTINUIDAD_ERROR")
					+ gestionContinuidadVO.getPomIdPrestOrdm(), e);
			return Response.status(Response.Status.OK).entity(new RespuestaGestionContinuidadVO(false,
					PropertiesManager.CONFIG.getString("GESTION_CONTINUIDAD_ERROR_FRONT"))).build();
		}
	}

	@GET
	@Path("/detalleOrdenMedica/{ormIdOrdmNumero}")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response detalleOrdenMedica(@PathParam("ormIdOrdmNumero") Long ormIdOrdmNumero) {
		logger.info("Iniciando Consulta detalleOrdenMedica(ormIdOrdmNumero)::::");
		try {
			return Response.status(Response.Status.OK)
					.entity(this.ordenesMedicasDTO.detalleOrdenMedica(ormIdOrdmNumero)).build();
		} catch (DataException e) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@POST
	@Path("/administrarOrdenMedica")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response administrarOrdenMedica(AdmOrdenesMedicasVO admOrdenesMedicasVO) {
		logger.info("Iniciando administrarOrdenMedica(AdmOrdenesMedicasVO)::::");
		try {
			this.ordenesMedicasDTO.administrarOrdenMedica(admOrdenesMedicasVO);
			return Response.status(Response.Status.OK)
					.entity(admOrdenesMedicasVO.getCaDetalleOrdenesMedicas().getOrmIdOrdmNumero()).build();
		} catch (Exception e) {
			return Response.status(Response.Status.NOT_FOUND)
					.entity(admOrdenesMedicasVO.getCaDetalleOrdenesMedicas().getOrmIdOrdmNumero()).build();
//			return Response.status(Response.Status.NOT_FOUND).entity(new GenericResponse(e.getCause(),
//					PropertiesManager.CONFIG.getString("NOT_FOUND_RESULT_CONSULTA_CITAS_PACIENTES"))).build();
		}
	}

	@POST
	@Path("/consultaPrestaciones")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response consultaPrestaciones(CaPrestacionesOrdMed consultaPrestacionesVO) {
		logger.info("Iniciando Consulta Pestaciones(AdmOrdenesMedicasVO)::::");
		try {
			List<CaPrestacionesOrdMed> prestaciones = this.ordenesMedicasDTO
					.consultaPrestaciones(consultaPrestacionesVO);
			return Response.status(Response.Status.OK).entity(prestaciones).build();
		} catch (Exception e) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}

	@POST
	@Path("/crearPrestaciones")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response crearPrestaciones(CrearPrestacionesOrdMedVO crearPrestacionesOrdMedVO) {
		logger.info("Iniciando Creacion Pestaciones(prestacionesVO)::::");
		try {
			return Response.status(Response.Status.OK)
					.entity(this.ordenesMedicasDTO.crearPrestaciones(crearPrestacionesOrdMedVO)).build();
		} catch (Exception e) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@POST
	@Path("/registrarAutorizacion")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response registrarAutorizacion(GestionAutorizacionVO gestionAutorizacionVO) {
		logger.info("Iniciando RegistrarAutorizacion(crearPrestacionesOrdMedVO)::::");
		try {
			return Response.status(Response.Status.OK)
					.entity(Boolean.valueOf(this.ordenesMedicasDTO.registrarAutorizacion(gestionAutorizacionVO)))
					.build();
		} catch (Exception e) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@POST
	@Path("/finalizarOrden")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response finalizarOrden(FinalizarOrdenMedicaVO finalizarOrdenMedicaVO) {
		logger.info("Iniciando finalizarOrden(ormIdOrdmNumero)::::");
		try {
			return Response.status(Response.Status.OK)
					.entity(Boolean.valueOf(this.ordenesMedicasDTO.finalizarOrden(finalizarOrdenMedicaVO))).build();
		} catch (Exception e) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@POST
	@Path("/eliminarOrden")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response eliminarOrden(FinalizarOrdenMedicaVO finalizarOrdenMedicaVO) {
		logger.info("Iniciando finalizarOrden(ormIdOrdmNumero)::::");
		try {
			return Response.status(Response.Status.OK)
					.entity(Boolean.valueOf(this.ordenesMedicasDTO.eliminarOrden(finalizarOrdenMedicaVO))).build();
		} catch (Exception e) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@GET
	@Path("/motivoEliminacion")
	@Produces({ "application/json" })
	public Response obtenerMotivos() {
		logger.info("Iniciando Lista motivos eliminacion::::");
		try {
			return Response.status(Response.Status.OK).entity(this.ordenesMedicasDTO.motivoEliminacion()).build();
		} catch (Exception e) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@GET
	@Path("/consultarPrestacion/{pomIdPrestOrdm}")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response consultarPrestacion(@PathParam("pomIdPrestOrdm") long pomIdPrestOrdm) {
		logger.info("Iniciando consultarPrestacion(pomIdPrestOrdm)::::");
		try {
			return Response.status(Response.Status.OK)
					.entity(this.ordenesMedicasDTO.consultaPrestacionPorPomIdPrestOrdm(Long.valueOf(pomIdPrestOrdm)))
					.build();
		} catch (Exception e) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@POST
	@Path("/registrarOrdenMedica")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response registrarOrdenMedica(FiltrosOrdenMedicaVO filtrosOrdenMedicaVO) {
		logger.info("Iniciando registrarOrdenMedica(Long) - CaCitasGestionadasDAO ::::");
		try {
			RespuestaOrdenMedicaVO respuestaOrdenMedicaVO = this.ordenesMedicasDTO
					.registrarOrdenMedica(filtrosOrdenMedicaVO);
			return Response.status(Response.Status.OK).entity(respuestaOrdenMedicaVO).build();
		} catch (Exception e) {
			return Response.status(Response.Status.NOT_FOUND).entity(new GenericResponse(e.getCause(),
					PropertiesManager.CONFIG.getString("NOT_FOUND_RESULT_CONSULTA_CITAS_PACIENTES"))).build();
		}
	}
}

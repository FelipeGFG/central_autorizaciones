package co.fsfb.ws.rest.service;

import co.fsfb.ws.rest.dto.PacientesDTO;
import co.fsfb.ws.rest.exception.PacienteNoRegistradoException;
import co.fsfb.ws.rest.response.GenericResponse;
import co.fsfb.ws.rest.util.PropertiesManager;
import co.fsfb.ws.rest.vo.ConsultaPacienteHISVO;
import co.fsfb.ws.rest.vo.PacienteHIS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/pacientes")
public class PacienteServices {
  private static Logger logger = LoggerFactory.getLogger(PacienteServices.class);
  
  private PacientesDTO pacientesDTO = new PacientesDTO();
  
  @POST
  @Path("/consultaPaciente")
  public Response consultarPacienteHIS(ConsultaPacienteHISVO consultaPacienteHIS) {
    logger.info("Consulta de paciente en HIS::::");
    try {
      PacienteHIS pacienteHIS = this.pacientesDTO.consultarPacienteHIS(consultaPacienteHIS);
      return Response.status(Response.Status.OK).entity(pacienteHIS).build();
    } catch (PacienteNoRegistradoException e) {
      return Response.status(Response.Status.NOT_FOUND).entity(new GenericResponse(e
            .getMessage(), PropertiesManager.CONFIG.getString("ERROR_PAYMENT_URL")))
        .build();
    } 
  }
}

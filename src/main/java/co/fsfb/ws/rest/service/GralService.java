package co.fsfb.ws.rest.service;

import co.fsfb.ws.rest.dto.GeneralDTO;
import co.fsfb.ws.rest.exception.DataException;
import co.fsfb.ws.rest.exception.EnvioEmailException;
import co.fsfb.ws.rest.response.GenericResponse;
import co.fsfb.ws.rest.util.PropertiesManager;
import co.fsfb.ws.rest.vo.EnviarEmailVO;
import java.io.IOException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.apache.commons.mail.EmailException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/General")
public class GralService {
  private static Logger logger = LoggerFactory.getLogger(GralService.class);
  
  private GeneralDTO gral = new GeneralDTO();
  
  @POST
  @Path("/email")
  @Consumes({"application/json"})
  @Produces({"application/json"})
  public Response enviarEmail(EnviarEmailVO enviarEmailVO) {
    logger.info("Iniciando enviarEmail::::" + enviarEmailVO.getMail());
    try {
      if (this.gral.enviarEmail(enviarEmailVO))
        return Response.status(Response.Status.OK).entity(enviarEmailVO).build(); 
      return Response.status(Response.Status.NOT_ACCEPTABLE)
        .entity(new GenericResponse(enviarEmailVO, PropertiesManager.CONFIG.getString("WRONG_EMAIL_FAILED")))
        .build();
    } catch (EnvioEmailException e) {
      return Response.status(Response.Status.NOT_ACCEPTABLE)
        .entity(new GenericResponse(enviarEmailVO, e.getMensaje())).build();
    } catch (IOException e) {
      return Response.status(Response.Status.NOT_ACCEPTABLE)
        .entity(new GenericResponse(enviarEmailVO, e.getMessage())).build();
    } catch (EmailException e) {
      return Response.status(Response.Status.NOT_ACCEPTABLE)
        .entity(new GenericResponse(enviarEmailVO, e.getMessage())).build();
    } catch (DataException e) {
      return Response.status(Response.Status.NOT_ACCEPTABLE)
        .entity(new GenericResponse(enviarEmailVO, e.getMessage())).build();
    } 
  }
}

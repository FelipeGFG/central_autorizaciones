package co.fsfb.ws.rest.service;

import co.fsfb.ws.rest.dto.ClienteCitaDTO;
import co.fsfb.ws.rest.response.GenericResponse;
import co.fsfb.ws.rest.util.ClienteWS;
import co.fsfb.ws.rest.util.PropertiesManager;
import co.fsfb.ws.rest.vo.CambioEstadoCitaVO;
import co.fsfb.ws.rest.vo.ConsultaDetalleCitaPacienteVO;
import co.fsfb.ws.rest.vo.ConsultaTrazabilidadCitaVO;
import co.fsfb.ws.rest.vo.FiltrosDetalleCitaPacienteVO;
import co.fsfb.ws.rest.vo.GestionAutorizacionCitaVO;
import co.fsfb.ws.rest.vo.GestionAutorizacionCitaVO_WSBus;
import co.fsfb.ws.rest.vo.ResCambioEstadoCitaVO;
import co.fsfb.ws.rest.vo.TrazabilidadCitaVO;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/ClienteCita")
public class ClienteCitaServices {

    private static Logger logger = LoggerFactory.getLogger(ClienteCitaServices.class);

    ClienteCitaDTO clienteCitaDTO = new ClienteCitaDTO();

    @POST
    @Path("/cambiarEstadoCita")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response cambiarEstadoCita(CambioEstadoCitaVO cambioEstadoCitaVO) {
        logger.info("Iniciando cambiarEstadoCita::::");
        try {
            ResCambioEstadoCitaVO resCambioEstadoCitaVO = this.clienteCitaDTO.cambiarEstadoCita(cambioEstadoCitaVO);
            return Response.status(Response.Status.OK).entity(resCambioEstadoCitaVO).build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_FOUND).entity(new GenericResponse(e.getCause(), PropertiesManager.CONFIG
                    .getString("NOT_FOUND_RESULT_CONSULTA_CITAS_PACIENTES"))).build();
        }
    }

    @GET
    @Path("/consultarTrazabilidadCita/{cGIdCitaNumero}")
    @Produces({"application/json"})
    public Response consultarTrazabilidadCita(@PathParam("cGIdCitaNumero") Long cGIdCitaNumero) {
        logger.info("Iniciando consultarTrazabilidadCita(Long) ::::");
        try {
            List<ConsultaTrazabilidadCitaVO> consultaTrazabilidadCitaVO = this.clienteCitaDTO.consultarTrazabilidadCita(cGIdCitaNumero);
            return Response.status(Response.Status.OK).entity(consultaTrazabilidadCitaVO).build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_FOUND).entity(new GenericResponse(e.getCause(), PropertiesManager.CONFIG
                    .getString("NOT_FOUND_RESULT_CONSULTA_CITAS_PACIENTES"))).build();
        }
    }

    @POST
    @Path("/consultarTrazabilidadCita")
    @Produces({"application/json"})
    public Response consultarTrazabilidadCita(TrazabilidadCitaVO trazabilidadCitaVO) {
        logger.info("Iniciando consultarTrazabilidadCita(Long) ::::");
        try {
            List<ConsultaTrazabilidadCitaVO> consultaTrazabilidadCitaVO = this.clienteCitaDTO.consultarTrazabilidadCita(trazabilidadCitaVO);
            return Response.status(Response.Status.OK).entity(consultaTrazabilidadCitaVO).build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_FOUND).entity(new GenericResponse(e.getCause(), PropertiesManager.CONFIG
                    .getString("NOT_FOUND_RESULT_CONSULTA_CITAS_PACIENTES"))).build();
        }
    }

    @POST
    @Path("/consultarDetalleCitaPaciente")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response consultarDetalleCitaPaciente(FiltrosDetalleCitaPacienteVO filtrosDetalleCitaPacienteVO) {
        logger.info("Iniciando consultarDetalleCitaPaciente(Long) - CaCitasGestionadasDAO ::::");
        try {
            ConsultaDetalleCitaPacienteVO consultaDetalleCitaPacienteVO = this.clienteCitaDTO.consultarDetalleCitaPaciente(filtrosDetalleCitaPacienteVO);

            return Response.status(Response.Status.OK).entity(consultaDetalleCitaPacienteVO).build();

        } catch (Exception e) {
            return Response.status(Response.Status.NOT_FOUND).entity(new GenericResponse(e.getCause(), PropertiesManager.CONFIG
                    .getString("NOT_FOUND_RESULT_CONSULTA_CITAS_PACIENTES"))).build();
        }
    }

    @POST
    @Path("/registrarAutorizacion")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response registrarAutorizacion(GestionAutorizacionCitaVO gestionAutorizacionCitaVO) {
        logger.info("Iniciando RegistrarAutorizacionCita(gestionAutorizacionVO)::::");
        try {
            return Response.status(Response.Status.OK)
                    .entity(Boolean.valueOf(this.clienteCitaDTO.registrarAutorizacion(gestionAutorizacionCitaVO))).build();
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @POST
    @Path("/registrarAutorizacionWsBus")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response registrarAutorizacionWsBus(GestionAutorizacionCitaVO_WSBus gestionAutorizacionCitaVO) {
        logger.info("Iniciando RegistrarAutorizacionCita(gestionAutorizacionVO)::::");
        try {
            logger.info("Inicio Consumo");
            ClienteWS ws = new ClienteWS();
            ws.consumoAuthorizationRegister(gestionAutorizacionCitaVO);
            return Response.status(Response.Status.OK).build();
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @GET
    @Path("/poliza/{pacNumero}")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response consultaPoliza(@PathParam("pacNumero") long pacNumero) {
        try {
            return Response.status(Response.Status.OK)
                    .entity(this.clienteCitaDTO.consultarPoliza(pacNumero)).build();
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
}

package co.fsfb.ws.rest.filter;

import com.sun.jersey.spi.container.ContainerRequestFilter;
import com.sun.jersey.spi.container.ContainerResponse;
import com.sun.jersey.spi.container.ContainerResponseFilter;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Provider
public class SecurityFilter implements ContainerResponseFilter, ContainerRequestFilter {

    private static Logger logger = LoggerFactory.getLogger(SecurityFilter.class);


    @Override
    public ContainerResponse filter(com.sun.jersey.spi.container.ContainerRequest request, ContainerResponse response) {
        logger.info("Filtro ContainerRequest");
        return response;
    }

    @Override
    public com.sun.jersey.spi.container.ContainerRequest filter(com.sun.jersey.spi.container.ContainerRequest response) {
        logger.info("Fitro ContainerResponse!!!!!!!!!!!!!!!!");
        MultivaluedMap<String, String> headers = response.getRequestHeaders();
        headers.add("Access-Control-Allow-Origin", "*");
        headers.add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
        headers.add("Access-Control-Allow-Headers", "*");
        return response;
    }
}

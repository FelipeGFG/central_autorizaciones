package co.fsfb.ws.rest.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CA_MOTIVOS_ELIMINAR_OM")
public class CaMotivosEliminarOm {
  @Id
  @Column(name = "MOM_ID")
  private Long momIdCodigo;
  
  @Column(name = "MOM_DESCRIPCION")
  private String momDescripcion;
  
  @Column(name = "EOM_ESTADO")
  private String eomEstado;
  
  public Long getMomIdCodigo() {
    return this.momIdCodigo;
  }
  
  public void setMomIdCodigo(Long momIdCodigo) {
    this.momIdCodigo = momIdCodigo;
  }
  
  public String getMomDescripcion() {
    return this.momDescripcion;
  }
  
  public void setMomDescripcion(String momDescripcion) {
    this.momDescripcion = momDescripcion;
  }
  
  public String getEomEstado() {
    return this.eomEstado;
  }
  
  public void setEomEstado(String eomEstado) {
    this.eomEstado = eomEstado;
  }
}

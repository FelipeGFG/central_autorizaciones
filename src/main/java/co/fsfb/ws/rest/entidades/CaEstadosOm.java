package co.fsfb.ws.rest.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CA_ESTADOS_OM")
public class CaEstadosOm {
  @Id
  @Column(name = "EOM_IDCODIGO")
  private Long eomIdCodigo;
  
  @Column(name = "EOM_DESCRIPCION")
  private String eomDescripcion;
  
  @Column(name = "EOM_ESTADO")
  private String eomEstado;
  
  public Long getEomIdCodigo() {
    return this.eomIdCodigo;
  }
  
  public void setEomIdCodigo(Long eomIdCodigo) {
    this.eomIdCodigo = eomIdCodigo;
  }
  
  public String getEomDescripcion() {
    return this.eomDescripcion;
  }
  
  public void setEomDescripcion(String eomDescripcion) {
    this.eomDescripcion = eomDescripcion;
  }
  
  public String getEomEstado() {
    return this.eomEstado;
  }
  
  public void setEomEstado(String eomEstado) {
    this.eomEstado = eomEstado;
  }
}

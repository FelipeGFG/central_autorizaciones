package co.fsfb.ws.rest.entidades;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "CA_TRAZA_GESTION_AUTORIZACION")
@SequenceGenerator(sequenceName = "CA_TRAZA_GESTION_AUTORIZACION_SEQ", allocationSize = 1, name = "CA_TRAZA_GESTION_AUTORIZACION_SEQ")
public class CaTrazaGestionAutorizacion {
  @Id
  @Column(name = "TGA_ID_ORDM_NUMERO")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CA_TRAZA_GESTION_AUTORIZACION_SEQ")
  private Long tgaIdOrdmNumero;
  
  @Column(name = "EGA_IDCODIGO")
  private Long egaIdcodigo;
  
  @Column(name = "PCA_AGE_CODIGRECEP")
  private String pcaAgeCodigRecep;
  
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "CG_FECHA_PROCESO")
  private Date cgFechaProceso;
  
  public Long getTgaIdOrdmNumero() {
    return this.tgaIdOrdmNumero;
  }
  
  public void setTgaIdOrdmNumero(Long tgaIdOrdmNumero) {
    this.tgaIdOrdmNumero = tgaIdOrdmNumero;
  }
  
  public Long getEgaIdcodigo() {
    return this.egaIdcodigo;
  }
  
  public void setEgaIdcodigo(Long egaIdcodigo) {
    this.egaIdcodigo = egaIdcodigo;
  }
  
  public String getPcaAgeCodigRecep() {
    return this.pcaAgeCodigRecep;
  }
  
  public void setPcaAgeCodigRecep(String pcaAgeCodigRecep) {
    this.pcaAgeCodigRecep = pcaAgeCodigRecep;
  }
  
  public Date getCgFechaProceso() {
    return this.cgFechaProceso;
  }
  
  public void setCgFechaProceso(Date cgFechaProceso) {
    this.cgFechaProceso = cgFechaProceso;
  }
}

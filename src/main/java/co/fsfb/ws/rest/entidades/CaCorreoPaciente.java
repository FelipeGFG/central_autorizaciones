package co.fsfb.ws.rest.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "CA_CORREO_PACIENTE")
public class CaCorreoPaciente implements Serializable {
  @Id
  @Column(name = "PAC_PAC_NUMERO")
  private Long pacPacNumero;
  
  @Column(name = "TC_IDCODIGO")
  private Long tcIdCodigo;
  
  @Column(name = "TP_CORREO_E")
  private String tpCorreoE;
  
  @Column(name = "PCA_AGE_CODIGRECEP")
  private String pcaAgeCodigRecep;
  
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "CG_FECHA_PROCESO")
  private Date cgFechaProceso;
  
  public Long getPacPacNumero() {
    return this.pacPacNumero;
  }
  
  public void setPacPacNumero(Long pacPacNumero) {
    this.pacPacNumero = pacPacNumero;
  }
  
  public Long getTcIdCodigo() {
    return this.tcIdCodigo;
  }
  
  public void setTcIdCodigo(Long tcIdCodigo) {
    this.tcIdCodigo = tcIdCodigo;
  }
  
  public String getTpCorreoE() {
    return this.tpCorreoE;
  }
  
  public void setTpCorreoE(String tpCorreoE) {
    this.tpCorreoE = tpCorreoE;
  }
  
  public String getPcaAgeCodigRecep() {
    return this.pcaAgeCodigRecep;
  }
  
  public void setPcaAgeCodigRecep(String pcaAgeCodigRecep) {
    this.pcaAgeCodigRecep = pcaAgeCodigRecep;
  }
  
  public Date getCgFechaProceso() {
    return this.cgFechaProceso;
  }
  
  public void setCgFechaProceso(Date cgFechaProceso) {
    this.cgFechaProceso = cgFechaProceso;
  }
}

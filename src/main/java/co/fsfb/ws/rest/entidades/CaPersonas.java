package co.fsfb.ws.rest.entidades;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CA_PERSONAS")
public class CaPersonas {
  @Id
  @Column(name = "PAC_PAC_NUMERO")
  private Long pacPacNumero;
  
  @Column(name = "PAC_PAC_TIPOIDENTCODIGO")
  private String pacPacTipoIdentCodigo;
  
  @Column(name = "PAC_PAC_RUT")
  private String pacPacRut;
  
  @Column(name = "PAC_PAC_APELLPATER")
  private String pacPacApellPater;
  
  @Column(name = "PAC_PAC_APELLMATER")
  private String pacPacApellMater;
  
  @Column(name = "PAC_PAC_NOMBRE")
  private String pacPacNombre;
  
  @Column(name = "PAC_PAC_FECHANACIM")
  private Date pacPacFechaNacim;
  
  public Long getPacPacNumero() {
    return this.pacPacNumero;
  }
  
  public void setPacPacNumero(Long pacPacNumero) {
    this.pacPacNumero = pacPacNumero;
  }
  
  public String getPacPacTipoIdentCodigo() {
    return this.pacPacTipoIdentCodigo;
  }
  
  public void setPacPacTipoIdentCodigo(String pacPacTipoIdentCodigo) {
    this.pacPacTipoIdentCodigo = pacPacTipoIdentCodigo;
  }
  
  public String getPacPacRut() {
    return this.pacPacRut;
  }
  
  public void setPacPacRut(String pacPacRut) {
    this.pacPacRut = pacPacRut;
  }
  
  public String getPacPacApellPater() {
    return this.pacPacApellPater;
  }
  
  public void setPacPacApellPater(String pacPacApellPater) {
    this.pacPacApellPater = pacPacApellPater;
  }
  
  public String getPacPacApellMater() {
    return this.pacPacApellMater;
  }
  
  public void setPacPacApellMater(String pacPacApellMater) {
    this.pacPacApellMater = pacPacApellMater;
  }
  
  public String getPacPacNombre() {
    return this.pacPacNombre;
  }
  
  public void setPacPacNombre(String pacPacNombre) {
    this.pacPacNombre = pacPacNombre;
  }
  
  public Date getPacPacFechaNacim() {
    return this.pacPacFechaNacim;
  }
  
  public void setPacPacFechaNacim(Date pacPacFechaNacim) {
    this.pacPacFechaNacim = pacPacFechaNacim;
  }
}

package co.fsfb.ws.rest.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "CA_GESTION_AUTORIZACION_CITA")
public class CaGestionAutorizacionCita implements Serializable {
  @Id
  @Column(name = "CG_ID_CITA_NUMERO")
  private Long cgIdCitaNumero;
  
  @Column(name = "PAC_PAC_NUMERO")
  private Long pacPacNumero;
  
  @Column(name = "GAU_NOMBRE_AUTORIZADOR")
  private String gauNombreAutorizador;
  
  @Column(name = "GAU_TELEFONO_AUTORIZADOR")
  private String gauTelefonoAutorizador;
  
  @Column(name = "GAU_AUTORIZA_SERV")
  private String gauAutorizaServ;
  
  @Column(name = "MNA_IDCODIGO")
  private Long mnaIdcodigo;
  
  @Column(name = "OMN_DESC")
  private String omnDesc;
  
  @Column(name = "GAU_CODIGO_AUTORIZACION")
  private String gauCodigoAutorizacion;
  
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "GAU_FECHA_AUTORIZACION")
  private Date gauFechaAutorizacion;
  
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "GAU_FECHA_VENC_AUTORIZACION")
  private Date gauFechaVencAutorizacion;
  
  @Column(name = "GAU_VIGENCIA_AUTORIZACION")
  private Long gauVigenciaAutorizacion;
  
  @Column(name = "GAU_VALOR_PRESTACION")
  private Long gauValorPrestacion;
  
  @Column(name = "GAU_COSTO_CONVENIO")
  private Long gauCostoConvenio;
  
  @Column(name = "GAU_COSTO_PAC")
  private Long gauCostoPac;
  
  @Column(name = "PCA_AGE_CODIGRECEP")
  private String pcaAgeCodigoRecep;
  
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "GAU_FECHA_PROCESO")
  private Date cgFechaProceso;
  
  @Column(name = "OGA_DESCRIPCION")
  private String ogaDescripcion;
  
  public Long getCgIdCitaNumero() {
    return this.cgIdCitaNumero;
  }
  
  public void setCgIdCitaNumero(Long cgIdCitaNumero) {
    this.cgIdCitaNumero = cgIdCitaNumero;
  }
  
  public Long getPacPacNumero() {
    return this.pacPacNumero;
  }
  
  public void setPacPacNumero(Long pacPacNumero) {
    this.pacPacNumero = pacPacNumero;
  }
  
  public String getGauNombreAutorizador() {
    return this.gauNombreAutorizador;
  }
  
  public void setGauNombreAutorizador(String gauNombreAutorizador) {
    this.gauNombreAutorizador = gauNombreAutorizador;
  }
  
  public String getGauTelefonoAutorizador() {
    return this.gauTelefonoAutorizador;
  }
  
  public void setGauTelefonoAutorizador(String gauTelefonoAutorizador) {
    this.gauTelefonoAutorizador = gauTelefonoAutorizador;
  }
  
  public String getGauAutorizaServ() {
    return this.gauAutorizaServ;
  }
  
  public void setGauAutorizaServ(String gauAutorizaServ) {
    this.gauAutorizaServ = gauAutorizaServ;
  }
  
  public Long getMnaIdcodigo() {
    return this.mnaIdcodigo;
  }
  
  public void setMnaIdcodigo(Long mnaIdcodigo) {
    this.mnaIdcodigo = mnaIdcodigo;
  }
  
  public String getOmnDesc() {
    return this.omnDesc;
  }
  
  public void setOmnDesc(String omnDesc) {
    this.omnDesc = omnDesc;
  }
  
  public String getGauCodigoAutorizacion() {
    return this.gauCodigoAutorizacion;
  }
  
  public void setGauCodigoAutorizacion(String gauCodigoAutorizacion) {
    this.gauCodigoAutorizacion = gauCodigoAutorizacion;
  }
  
  public Date getGauFechaAutorizacion() {
    return this.gauFechaAutorizacion;
  }
  
  public void setGauFechaAutorizacion(Date gauFechaAutorizacion) {
    this.gauFechaAutorizacion = gauFechaAutorizacion;
  }
  
  public Date getGauFechaVencAutorizacion() {
    return this.gauFechaVencAutorizacion;
  }
  
  public void setGauFechaVencAutorizacion(Date gauFechaVencAutorizacion) {
    this.gauFechaVencAutorizacion = gauFechaVencAutorizacion;
  }
  
  public Long getGauVigenciaAutorizacion() {
    return this.gauVigenciaAutorizacion;
  }
  
  public void setGauVigenciaAutorizacion(Long gauVigenciaAutorizacion) {
    this.gauVigenciaAutorizacion = gauVigenciaAutorizacion;
  }
  
  public Long getGauValorPrestacion() {
    return this.gauValorPrestacion;
  }
  
  public void setGauValorPrestacion(Long gauValorPrestacion) {
    this.gauValorPrestacion = gauValorPrestacion;
  }
  
  public Long getGauCostoConvenio() {
    return this.gauCostoConvenio;
  }
  
  public void setGauCostoConvenio(Long gauCostoConvenio) {
    this.gauCostoConvenio = gauCostoConvenio;
  }
  
  public Long getGauCostoPac() {
    return this.gauCostoPac;
  }
  
  public void setGauCostoPac(Long gauCostoPac) {
    this.gauCostoPac = gauCostoPac;
  }
  
  public String getPcaAgeCodigoRecep() {
    return this.pcaAgeCodigoRecep;
  }
  
  public void setPcaAgeCodigoRecep(String pcaAgeCodigoRecep) {
    this.pcaAgeCodigoRecep = pcaAgeCodigoRecep;
  }
  
  public Date getCgFechaProceso() {
    return this.cgFechaProceso;
  }
  
  public void setCgFechaProceso(Date cgFechaProceso) {
    this.cgFechaProceso = cgFechaProceso;
  }
  
  public String getOgaDescripcion() {
    return this.ogaDescripcion;
  }
  
  public void setOgaDescripcion(String ogaDescripcion) {
    this.ogaDescripcion = ogaDescripcion;
  }
}

package co.fsfb.ws.rest.entidades;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "CA_DETALLE_ORDENES_MEDICAS")
public class CaDetalleOrdenesMedicas {
    @Id
    @Column(name = "ORM_ID_ORDM_NUMERO")
    private Long ormIdOrdmNumero;

    @Column(name = "PAC_PAC_NUMERO")
    private Long pacPacNumero;

    @Column(name = "PAC_PAC_TIPOIDENTCODIGO")
    private String pacPacTipoIdentCodigo;

    @Column(name = "PAC_PAC_RUT")
    private String pacPacRut;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DOR_FECHA_ORDENM")
    private Date dorFechaOrdenm;

    @Column(name = "SER_ESP_CODIGO")
    private String serEspCodigo;

    @Column(name = "SER_SER_CODSUBESPE")
    private String serSerCodSubEspe;

    @Column(name = "SER_SER_CODIGO")
    private String serSerCodigo;

    @Column(name = "PCA_AGE_LUGAR")
    private String pcaAgeLugar;

    @Column(name = "DIA_AGR_CODIGO")
    private String diaAgrCodigo;

    @Column(name = "DIA_OTRO_AGR")
    private String diaOtroAgr;

    @Column(name = "CON_CON_CODIGO")
    private String conConCodigo;

    @Column(name = "CON_PLA_CODIGO")
    private String conPlaCodigo;

    @Column(name = "PCA_AGE_CODIGPROFE")
    private String pcaAgeCodigProfe;

    @Column(name = "DOM_REGISTRO_MEDICO")
    private String domRegistroMedico;

    @Column(name = "PCA_AGE_CODIGRECEP")
    private String pcaAgeCodigRecep;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DOM_SHA_FECHA_PROCESO")
    private Date domShaFechaProceso;

    @MapsId
    @JoinColumn(name = "ORM_ID_ORDM_NUMERO", referencedColumnName = "ORM_ID_ORDM_NUMERO", columnDefinition = "ORM_ID_ORDM_NUMERO")
    @OneToOne(optional = false)
    private CaOrdenesMedicas caOrdenesMedicas;

    public Long getOrmIdOrdmNumero() {
        return this.ormIdOrdmNumero;
    }

    public void setOrmIdOrdmNumero(Long ormIdOrdmNumero) {
        this.ormIdOrdmNumero = ormIdOrdmNumero;
    }

    public Long getPacPacNumero() {
        return this.pacPacNumero;
    }

    public void setPacPacNumero(Long pacPacNumero) {
        this.pacPacNumero = pacPacNumero;
    }

    public String getPacPacTipoIdentCodigo() {
        return this.pacPacTipoIdentCodigo;
    }

    public void setPacPacTipoIdentCodigo(String pacPacTipoIdentCodigo) {
        this.pacPacTipoIdentCodigo = pacPacTipoIdentCodigo;
    }

    public String getPacPacRut() {
        return this.pacPacRut;
    }

    public void setPacPacRut(String pacPacRut) {
        this.pacPacRut = pacPacRut;
    }

    public Date getDorFechaOrdenm() {
        return this.dorFechaOrdenm;
    }

    public void setDorFechaOrdenm(Date dorFechaOrdenm) {
        this.dorFechaOrdenm = dorFechaOrdenm;
    }

    public String getSerEspCodigo() {
        return this.serEspCodigo;
    }

    public void setSerEspCodigo(String serEspCodigo) {
        this.serEspCodigo = serEspCodigo;
    }

    public String getSerSerCodSubEspe() {
        return this.serSerCodSubEspe;
    }

    public void setSerSerCodSubEspe(String serSerCodSubEspe) {
        this.serSerCodSubEspe = serSerCodSubEspe;
    }

    public String getSerSerCodigo() {
        return this.serSerCodigo;
    }

    public void setSerSerCodigo(String serSerCodigo) {
        this.serSerCodigo = serSerCodigo;
    }

    public String getPcaAgeLugar() {
        return this.pcaAgeLugar;
    }

    public void setPcaAgeLugar(String pcaAgeLugar) {
        this.pcaAgeLugar = pcaAgeLugar;
    }

    public String getDiaAgrCodigo() {
        return this.diaAgrCodigo;
    }

    public void setDiaAgrCodigo(String diaAgrCodigo) {
        this.diaAgrCodigo = diaAgrCodigo;
    }

    public String getDiaOtroAgr() {
        return this.diaOtroAgr;
    }

    public void setDiaOtroAgr(String diaOtroAgr) {
        this.diaOtroAgr = diaOtroAgr;
    }

    public String getConConCodigo() {
        return this.conConCodigo;
    }

    public void setConConCodigo(String conConCodigo) {
        this.conConCodigo = conConCodigo;
    }

    public String getConPlaCodigo() {
        return this.conPlaCodigo;
    }

    public void setConPlaCodigo(String conPlaCodigo) {
        this.conPlaCodigo = conPlaCodigo;
    }

    public String getPcaAgeCodigProfe() {
        return this.pcaAgeCodigProfe;
    }

    public void setPcaAgeCodigProfe(String pcaAgeCodigProfe) {
        this.pcaAgeCodigProfe = pcaAgeCodigProfe;
    }

    public String getDomRegistroMedico() {
        return this.domRegistroMedico;
    }

    public void setDomRegistroMedico(String domRegistroMedico) {
        this.domRegistroMedico = domRegistroMedico;
    }

    public String getPcaAgeCodigRecep() {
        return this.pcaAgeCodigRecep;
    }

    public void setPcaAgeCodigRecep(String pcaAgeCodigRecep) {
        this.pcaAgeCodigRecep = pcaAgeCodigRecep;
    }

    public Date getDomShaFechaProceso() {
        return this.domShaFechaProceso;
    }

    public void setDomShaFechaProceso(Date domShaFechaProceso) {
        this.domShaFechaProceso = domShaFechaProceso;
    }

    public CaOrdenesMedicas getCaOrdenesMedicas() {
        return this.caOrdenesMedicas;
    }

    public void setCaOrdenesMedicas(CaOrdenesMedicas caOrdenesMedicas) {
        this.caOrdenesMedicas = caOrdenesMedicas;
    }

    private CaDetalleOrdenesMedicas marshall(String accountTransactionsJSON) {
        ObjectMapper objectMapper = new ObjectMapper();
        CaDetalleOrdenesMedicas transactionDTO = null;
        try {
            objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
            transactionDTO = objectMapper.readValue(accountTransactionsJSON, CaDetalleOrdenesMedicas.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return transactionDTO;
    }

}

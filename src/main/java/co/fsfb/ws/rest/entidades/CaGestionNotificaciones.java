package co.fsfb.ws.rest.entidades;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "CA_GESTION_NOTIFICACIONES")
@SequenceGenerator(sequenceName = "CA_GESTION_NOTIFICACIONES_SEQ", allocationSize = 1, name = "CA_GESTION_NOTIFICACIONES_SEQ")
public class CaGestionNotificaciones {
  @Id
  @Column(name = "NOT_IDCODIGO")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CA_GESTION_NOTIFICACIONES_SEQ")
  private Long notIdCodigo;
  
  @Column(name = "ACC_ID_NUMERO")
  private Long accIdNumero;
  
  @Column(name = "NOT_IDENTIFICADOR")
  private Long notIdentificador;
  
  @Column(name = "PAC_PAC_NUMERO")
  private Long pacPacNumero;
  
  @Column(name = "PCA_AGE_CODIGRECEP")
  private String pcaAgeCodigRecep;
  
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "CG_FECHA_PROCESO")
  private Date cgFechaProceso;
  
  public Long getNotIdCodigo() {
    return this.notIdCodigo;
  }
  
  public void setNotIdCodigo(Long notIdCodigo) {
    this.notIdCodigo = notIdCodigo;
  }
  
  public Long getAccIdNumero() {
    return this.accIdNumero;
  }
  
  public void setAccIdNumero(Long accIdNumero) {
    this.accIdNumero = accIdNumero;
  }
  
  public Long getNotIdentificador() {
    return this.notIdentificador;
  }
  
  public void setNotIdentificador(Long notIdentificador) {
    this.notIdentificador = notIdentificador;
  }
  
  public Long getPacPacNumero() {
    return this.pacPacNumero;
  }
  
  public void setPacPacNumero(Long pacPacNumero) {
    this.pacPacNumero = pacPacNumero;
  }
  
  public String getPcaAgeCodigRecep() {
    return this.pcaAgeCodigRecep;
  }
  
  public void setPcaAgeCodigRecep(String pcaAgeCodigRecep) {
    this.pcaAgeCodigRecep = pcaAgeCodigRecep;
  }
  
  public Date getCgFechaProceso() {
    return this.cgFechaProceso;
  }
  
  public void setCgFechaProceso(Date cgFechaProceso) {
    this.cgFechaProceso = cgFechaProceso;
  }
}

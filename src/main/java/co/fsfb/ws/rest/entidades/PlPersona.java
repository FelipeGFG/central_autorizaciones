package co.fsfb.ws.rest.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "PL_PERSONA")
@NamedQueries({@NamedQuery(name = "PL_PERSONA.findByNumeroDocumento", query = "SELECT p FROM PlPersona p where p.numeroDocumento=:idDocumento")})
public class PlPersona {
  @Id
  @Column(name = "ID_PERSONA")
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "PL_PERSONA_ID_SEQ")
  private Long idPersona;
  
  @Column(name = "PAC_PAC_TIPOIDENTCODIGO")
  private String pacPacTipoIdentCodigo;
  
  @Column(name = "NUMERO_DOCUMENTO")
  private String numeroDocumento;
  
  public Long getIdPersona() {
    return this.idPersona;
  }
  
  public void setIdPersona(Long idPersona) {
    this.idPersona = idPersona;
  }
  
  public String getPacPacTipoIdentCodigo() {
    return this.pacPacTipoIdentCodigo;
  }
  
  public void setPacPacTipoIdentCodigo(String pacPacTipoIdentCodigo) {
    this.pacPacTipoIdentCodigo = pacPacTipoIdentCodigo;
  }
  
  public String getNumeroDocumento() {
    return this.numeroDocumento;
  }
  
  public void setNumeroDocumento(String numeroDocumento) {
    this.numeroDocumento = numeroDocumento;
  }
}

package co.fsfb.ws.rest.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CA_TIPOS_CONTACTO_PACIENTE")
public class CaTiposContactoPaciente {
  @Id
  @Column(name = "TC_IDCODIGO")
  private Long tcIdCodigo;
  
  @Column(name = "TC_DESCRIPCION")
  private String tcDescripcion;
  
  public Long getTcIdCodigo() {
    return this.tcIdCodigo;
  }
  
  public void setTcIdCodigo(Long tcIdCodigo) {
    this.tcIdCodigo = tcIdCodigo;
  }
  
  public String getTcDescripcion() {
    return this.tcDescripcion;
  }
  
  public void setTcDescripcion(String tcDescripcion) {
    this.tcDescripcion = tcDescripcion;
  }
}

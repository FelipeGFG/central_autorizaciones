package co.fsfb.ws.rest.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CA_ESTADOS_GEST_CONT")
public class CaEstadosGestCont {
  @Id
  @Column(name = "GCO_IDCODIGO")
  private Long gcoIdCodigo;
  
  @Column(name = "GCO_DESCRIPCION")
  private String gcoDescripcion;
  
  @Column(name = "GCO_ESTADO")
  private String gcoEstado;
  
  public Long getGcoIdCodigo() {
    return this.gcoIdCodigo;
  }
  
  public void setGcoIdCodigo(Long gcoIdCodigo) {
    this.gcoIdCodigo = gcoIdCodigo;
  }
  
  public String getGcoDescripcion() {
    return this.gcoDescripcion;
  }
  
  public void setGcoDescripcion(String gcoDescripcion) {
    this.gcoDescripcion = gcoDescripcion;
  }
  
  public String getGcoEstado() {
    return this.gcoEstado;
  }
  
  public void setGcoEstado(String gcoEstado) {
    this.gcoEstado = gcoEstado;
  }
}

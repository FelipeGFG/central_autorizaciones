package co.fsfb.ws.rest.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CA_TIPO_DOCUMENTAL_SHAREPOINT")
public class CaTipoDocumentalSharePoint {
  @Id
  @Column(name = "TDS_ID_NUMERO")
  private Long tdsIdNumero;
  
  @Column(name = "TDS_SIGLA")
  private String tdsSigla;
  
  @Column(name = "TDS_DESCRIPCION")
  private String tdsDescripcion;
  
  public Long getTdsIdNumero() {
    return this.tdsIdNumero;
  }
  
  public void setTdsIdNumero(Long tdsIdNumero) {
    this.tdsIdNumero = tdsIdNumero;
  }
  
  public String getTdsSigla() {
    return this.tdsSigla;
  }
  
  public void setTdsSigla(String tdsSigla) {
    this.tdsSigla = tdsSigla;
  }
  
  public String getTdsDescripcion() {
    return this.tdsDescripcion;
  }
  
  public void setTdsDescripcion(String tdsDescripcion) {
    this.tdsDescripcion = tdsDescripcion;
  }
}

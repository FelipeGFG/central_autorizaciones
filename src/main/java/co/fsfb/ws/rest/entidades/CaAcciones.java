package co.fsfb.ws.rest.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CA_ACCIONES")
public class CaAcciones {
  @Id
  @Column(name = "ACC_ID_NUMERO")
  private Long accIdNumero;
  
  @Column(name = "ACC_DESCRIPCION")
  private String accDescripcion;
  
  @Column(name = "ACC_TABLA_CA")
  private String accTablaCa;
  
  @Column(name = "ACC_ID_IDENTIFICADOR")
  private String accIdIdentificador;
  
  public Long getAccIdNumero() {
    return this.accIdNumero;
  }
  
  public void setAccIdNumero(Long accIdNumero) {
    this.accIdNumero = accIdNumero;
  }
  
  public String getAccDescripcion() {
    return this.accDescripcion;
  }
  
  public void setAccDescripcion(String accDescripcion) {
    this.accDescripcion = accDescripcion;
  }
  
  public String getAccTablaCa() {
    return this.accTablaCa;
  }
  
  public void setAccTablaCa(String accTablaCa) {
    this.accTablaCa = accTablaCa;
  }
  
  public String getAccIdIdentificador() {
    return this.accIdIdentificador;
  }
  
  public void setAccIdIdentificador(String accIdIdentificador) {
    this.accIdIdentificador = accIdIdentificador;
  }
}

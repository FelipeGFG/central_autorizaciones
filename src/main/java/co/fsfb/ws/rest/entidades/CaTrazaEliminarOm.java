package co.fsfb.ws.rest.entidades;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "CA_TRAZA_ELIMINAR_OM")
@SequenceGenerator(sequenceName = "CA_TRAZA_ELIMINAR_OM_SEQ", allocationSize = 1, name = "CA_TRAZA_ELIMINAR_OM_SEQ")
public class CaTrazaEliminarOm {
  @Id
  @Column(name = "TEO_ID_ELIM_OM")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CA_TRAZA_ELIMINAR_OM_SEQ")
  private Long teoIdElimOm;
  
  @Column(name = "TOM_ID_TRAZA_ORDM_NUM")
  private Long tomIdTrazaOrdmNum;
  
  @Column(name = "ORM_ID_ORDM_NUMERO")
  private Long ormIdOrdmNumero;
  
  @Column(name = "PCA_AGE_CODIGRECEP")
  private String pcaAgeCodigRecep;
  
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "CG_FECHA_PROCESO")
  private Date cgFechaProceso;
  
  @Column(name = "MOM_ID")
  private Long momId;
  
  public Long getTeoIdElimOm() {
    return this.teoIdElimOm;
  }
  
  public void setTeoIdElimOm(Long teoIdElimOm) {
    this.teoIdElimOm = teoIdElimOm;
  }
  
  public Long getTomIdTrazaOrdmNum() {
    return this.tomIdTrazaOrdmNum;
  }
  
  public void setTomIdTrazaOrdmNum(Long tomIdTrazaOrdmNum) {
    this.tomIdTrazaOrdmNum = tomIdTrazaOrdmNum;
  }
  
  public Long getOrmIdOrdmNumero() {
    return this.ormIdOrdmNumero;
  }
  
  public void setOrmIdOrdmNumero(Long ormIdOrdmNumero) {
    this.ormIdOrdmNumero = ormIdOrdmNumero;
  }
  
  public String getPcaAgeCodigRecep() {
    return this.pcaAgeCodigRecep;
  }
  
  public void setPcaAgeCodigRecep(String pcaAgeCodigRecep) {
    this.pcaAgeCodigRecep = pcaAgeCodigRecep;
  }
  
  public Date getCgFechaProceso() {
    return this.cgFechaProceso;
  }
  
  public void setCgFechaProceso(Date cgFechaProceso) {
    this.cgFechaProceso = cgFechaProceso;
  }
  
  public Long getMomId() {
    return this.momId;
  }
  
  public void setMomId(Long momId) {
    this.momId = momId;
  }
}

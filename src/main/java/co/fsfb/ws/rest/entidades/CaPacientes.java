package co.fsfb.ws.rest.entidades;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CA_PACIENTES")
public class CaPacientes implements Serializable {
  @Id
  @Column(name = "PAC_PAC_NUMERO")
  private Long pacPacNumero;
  
  @Column(name = "PAC_PAC_TIPOIDENTCODIGO")
  private String pacPacTipoIdentCodigo;
  
  @Column(name = "TIPIDAV")
  private String tipIdav;
  
  @Column(name = "PAC_PAC_RUT")
  private String pacPacRut;
  
  public Long getPacPacNumero() {
    return this.pacPacNumero;
  }
  
  public void setPacPacNumero(Long pacPacNumero) {
    this.pacPacNumero = pacPacNumero;
  }
  
  public String getPacPacTipoIdentCodigo() {
    return this.pacPacTipoIdentCodigo;
  }
  
  public void setPacPacTipoIdentCodigo(String pacPacTipoIdentCodigo) {
    this.pacPacTipoIdentCodigo = pacPacTipoIdentCodigo;
  }
  
  public String getTipIdav() {
    return this.tipIdav;
  }
  
  public void setTipIdav(String tipIdav) {
    this.tipIdav = tipIdav;
  }
  
  public String getPacPacRut() {
    return this.pacPacRut;
  }
  
  public void setPacPacRut(String pacPacRut) {
    this.pacPacRut = pacPacRut;
  }
}

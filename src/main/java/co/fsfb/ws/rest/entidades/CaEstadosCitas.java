package co.fsfb.ws.rest.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CA_ESTADOS_CITAS")
public class CaEstadosCitas {
  @Id
  @Column(name = "EC_IDCODIGO")
  private Long ecIdCodigo;
  
  @Column(name = "EC_DESCRIPCION")
  private String ecDescripcion;
  
  @Column(name = "EC_ESTADO")
  private String ecEstado;
  
  public Long getEcIdCodigo() {
    return this.ecIdCodigo;
  }
  
  public void setEcIdCodigo(Long ecIdCodigo) {
    this.ecIdCodigo = ecIdCodigo;
  }
  
  public String getEcDescripcion() {
    return this.ecDescripcion;
  }
  
  public void setEcDescripcion(String ecDescripcion) {
    this.ecDescripcion = ecDescripcion;
  }
  
  public String getEcEstado() {
    return this.ecEstado;
  }
  
  public void setEcEstado(String ecEstado) {
    this.ecEstado = ecEstado;
  }
}

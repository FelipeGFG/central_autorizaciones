package co.fsfb.ws.rest.entidades;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CA_DOCUMENTOS_SHAREPOINT")
@SequenceGenerator(sequenceName = "CA_DOCUMENTOS_SHAREPOINT_SEQ", allocationSize = 1, name = "CA_DOCUMENTOS_SHAREPOINT_SEQ")
public class CaDocumentosSharePoint {
  @Id
  @Column(name = "DOM_SHA_IDCODIGO")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CA_DOCUMENTOS_SHAREPOINT_SEQ")
  private Long domShaIdCodigo;
  
  @Column(name = "PAC_PAC_NUMERO")
  private Long pacPacNumero;
  
  @Column(name = "PAC_PAC_TIPOIDENTCODIGO")
  private String pacPacTipoIdentCodigo;
  
  @Column(name = "TIPIDAV")
  private String tipIdDav;
  
  @Column(name = "PAC_PAC_RUT")
  private String pacPacRut;
  
  @Column(name = "ACC_ID_NUMERO")
  private Long accIdNumero;
  
  @Column(name = "DOM_SHA_IDENTIFICADOR")
  private Long domShaIdentificador;
  
  @Column(name = "TDS_SIGLA")
  private String tdsSigla;
  
  @Column(name = "SER_ESP_CODIGO")
  private String serEspCodigo;
  
  @Column(name = "SER_SER_CODSUBESPE")
  private String serSerCodSubEspe;
  
  @Column(name = "SER_SER_CODIGO")
  private String serSerCodigo;
  
  @Column(name = "DOM_SHA_URL")
  private String domShaUrl;
  
  @Column(name = "EOM_IDCODIGO")
  private Long eomIdCodigo;
  
  @Column(name = "PCA_AGE_CODIGRECEP")
  private String pcaAgeCodigRecep;
  
  @Column(name = "DOM_SHA_FECHA_PROCESO")
  private Date domShaFechaProceso;
  
  public Long getDomShaIdCodigo() {
    return this.domShaIdCodigo;
  }
  
  public void setDomShaIdCodigo(Long domShaIdCodigo) {
    this.domShaIdCodigo = domShaIdCodigo;
  }
  
  public Long getPacPacNumero() {
    return this.pacPacNumero;
  }
  
  public void setPacPacNumero(Long pacPacNumero) {
    this.pacPacNumero = pacPacNumero;
  }
  
  public String getPacPacTipoIdentCodigo() {
    return this.pacPacTipoIdentCodigo;
  }
  
  public void setPacPacTipoIdentCodigo(String pacPacTipoIdentCodigo) {
    this.pacPacTipoIdentCodigo = pacPacTipoIdentCodigo;
  }
  
  public String getTipIdDav() {
    return this.tipIdDav;
  }
  
  public void setTipIdDav(String tipIdDav) {
    this.tipIdDav = tipIdDav;
  }
  
  public String getPacPacRut() {
    return this.pacPacRut;
  }
  
  public void setPacPacRut(String pacPacRut) {
    this.pacPacRut = pacPacRut;
  }
  
  public Long getAccIdNumero() {
    return this.accIdNumero;
  }
  
  public void setAccIdNumero(Long accIdNumero) {
    this.accIdNumero = accIdNumero;
  }
  
  public Long getDomShaIdentificador() {
    return this.domShaIdentificador;
  }
  
  public void setDomShaIdentificador(Long domShaIdentificador) {
    this.domShaIdentificador = domShaIdentificador;
  }
  
  public String getTdsSigla() {
    return this.tdsSigla;
  }
  
  public void setTdsSigla(String tdsSigla) {
    this.tdsSigla = tdsSigla;
  }
  
  public String getSerEspCodigo() {
    return this.serEspCodigo;
  }
  
  public void setSerEspCodigo(String serEspCodigo) {
    this.serEspCodigo = serEspCodigo;
  }
  
  public String getSerSerCodSubEspe() {
    return this.serSerCodSubEspe;
  }
  
  public void setSerSerCodSubEspe(String serSerCodSubEspe) {
    this.serSerCodSubEspe = serSerCodSubEspe;
  }
  
  public String getSerSerCodigo() {
    return this.serSerCodigo;
  }
  
  public void setSerSerCodigo(String serSerCodigo) {
    this.serSerCodigo = serSerCodigo;
  }
  
  public String getDomShaUrl() {
    return this.domShaUrl;
  }
  
  public void setDomShaUrl(String domShaUrl) {
    this.domShaUrl = domShaUrl;
  }
  
  public Long getEomIdCodigo() {
    return this.eomIdCodigo;
  }
  
  public void setEomIdCodigo(Long eomIdCodigo) {
    this.eomIdCodigo = eomIdCodigo;
  }
  
  public String getPcaAgeCodigRecep() {
    return this.pcaAgeCodigRecep;
  }
  
  public void setPcaAgeCodigRecep(String pcaAgeCodigRecep) {
    this.pcaAgeCodigRecep = pcaAgeCodigRecep;
  }
  
  public Date getDomShaFechaProceso() {
    return this.domShaFechaProceso;
  }
  
  public void setDomShaFechaProceso(Date domShaFechaProceso) {
    this.domShaFechaProceso = domShaFechaProceso;
  }
}

package co.fsfb.ws.rest.entidades;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CA_OBS_TRAZA_GEST_CONTINUIDAD")
public class CaObsTrazaGestContinuidad implements Serializable {
  @Id
  @Column(name = "POM_ID_PREST_ORDM")
  private Long pomIdPrestOrdm;
  
  @Column(name = "GCO_IDCODIGO")
  private Long gcoIdCodigo;
  
  @Column(name = "OTC_OBSERVACION")
  private String otcObservacion;
  
  public Long getPomIdPrestOrdm() {
    return this.pomIdPrestOrdm;
  }
  
  public void setPomIdPrestOrdm(Long pomIdPrestOrdm) {
    this.pomIdPrestOrdm = pomIdPrestOrdm;
  }
  
  public Long getGcoIdCodigo() {
    return this.gcoIdCodigo;
  }
  
  public void setGcoIdCodigo(Long gcoIdCodigo) {
    this.gcoIdCodigo = gcoIdCodigo;
  }
  
  public String getOtcObservacion() {
    return this.otcObservacion;
  }
  
  public void setOtcObservacion(String otcObservacion) {
    this.otcObservacion = otcObservacion;
  }
}

package co.fsfb.ws.rest.entidades;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "CA_CITAS_GESTIONADAS")
@SequenceGenerator(sequenceName = "CA_CITAS_GESTIONADAS_SEQ", allocationSize = 1, name = "CA_CITAS_GESTIONADAS_SEQ")
public class CaCitasGestionadas {
  @Id
  @Column(name = "CG_ID_CITA_NUMERO")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CA_CITAS_GESTIONADAS_SEQ")
  private Long cGIdCitaNumero;
  
  @Column(name = "PAC_PAC_NUMERO")
  private Long pacPacNumero;
  
  @Column(name = "PCA_AGE_FECHACITAC")
  private Date pcaAgeFechaCitac;
  
  @Column(name = "PCA_AGE_HORACITAC")
  private String pcaAgeHoraCitac;
  
  @Column(name = "PCA_AGE_CODIGSERVI")
  private String pcaAgeCodigServi;
  
  @Column(name = "PCA_AGE_CODIGPROFE")
  private String pcaAgeCodigProfe;
  
  @Column(name = "PCA_AGE_OBJETO")
  private String pcaAgeObjeto;
  
  @Column(name = "PRE_PRE_CODIGO")
  private String prePreCodigo;
  
  @Column(name = "PCA_AGE_LUGAR")
  private String pcaAgeLugar;
  
  @Column(name = "CON_CON_CODIGO")
  private String conConCodigo;
  
  @Column(name = "PCA_AGE_CODIGRECEP")
  private String pcaAgeCodigRecep;
  
  @Column(name = "PCA_AGE_FECHADIGIT")
  private Date pcaAgeFechaDigit;
  
  @Column(name = "PCA_AGE_RECEPCIONADO")
  private String pcaAgeRecepcionado;
  
  @Column(name = "EC_IDCODIGO")
  private Long ecIdCodigo;
  
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "CG_FECHA_PROCESO")
  private Date cgFechaProceso;
  
  public Long getcGIdCitaNumero() {
    return this.cGIdCitaNumero;
  }
  
  public void setcGIdCitaNumero(Long cGIdCitaNumero) {
    this.cGIdCitaNumero = cGIdCitaNumero;
  }
  
  public Long getPacPacNumero() {
    return this.pacPacNumero;
  }
  
  public void setPacPacNumero(Long pacPacNumero) {
    this.pacPacNumero = pacPacNumero;
  }
  
  public Date getPcaAgeFechaCitac() {
    return this.pcaAgeFechaCitac;
  }
  
  public void setPcaAgeFechaCitac(Date pcaAgeFechaCitac) {
    this.pcaAgeFechaCitac = pcaAgeFechaCitac;
  }
  
  public String getPcaAgeHoraCitac() {
    return this.pcaAgeHoraCitac;
  }
  
  public void setPcaAgeHoraCitac(String pcaAgeHoraCitac) {
    this.pcaAgeHoraCitac = pcaAgeHoraCitac;
  }
  
  public String getPcaAgeCodigServi() {
    return this.pcaAgeCodigServi;
  }
  
  public void setPcaAgeCodigServi(String pcaAgeCodigServi) {
    this.pcaAgeCodigServi = pcaAgeCodigServi;
  }
  
  public String getPcaAgeCodigProfe() {
    return this.pcaAgeCodigProfe;
  }
  
  public void setPcaAgeCodigProfe(String pcaAgeCodigProfe) {
    this.pcaAgeCodigProfe = pcaAgeCodigProfe;
  }
  
  public String getPcaAgeObjeto() {
    return this.pcaAgeObjeto;
  }
  
  public void setPcaAgeObjeto(String pcaAgeObjeto) {
    this.pcaAgeObjeto = pcaAgeObjeto;
  }
  
  public String getPrePreCodigo() {
    return this.prePreCodigo;
  }
  
  public void setPrePreCodigo(String prePreCodigo) {
    this.prePreCodigo = prePreCodigo;
  }
  
  public String getPcaAgeLugar() {
    return this.pcaAgeLugar;
  }
  
  public void setPcaAgeLugar(String pcaAgeLugar) {
    this.pcaAgeLugar = pcaAgeLugar;
  }
  
  public String getConConCodigo() {
    return this.conConCodigo;
  }
  
  public void setConConCodigo(String conConCodigo) {
    this.conConCodigo = conConCodigo;
  }
  
  public String getPcaAgeCodigRecep() {
    return this.pcaAgeCodigRecep;
  }
  
  public void setPcaAgeCodigRecep(String pcaAgeCodigRecep) {
    this.pcaAgeCodigRecep = pcaAgeCodigRecep;
  }
  
  public Date getPcaAgeFechaDigit() {
    return this.pcaAgeFechaDigit;
  }
  
  public void setPcaAgeFechaDigit(Date pcaAgeFechaDigit) {
    this.pcaAgeFechaDigit = pcaAgeFechaDigit;
  }
  
  public String getPcaAgeRecepcionado() {
    return this.pcaAgeRecepcionado;
  }
  
  public void setPcaAgeRecepcionado(String pcaAgeRecepcionado) {
    this.pcaAgeRecepcionado = pcaAgeRecepcionado;
  }
  
  public Long getEcIdCodigo() {
    return this.ecIdCodigo;
  }
  
  public void setEcIdCodigo(Long ecIdCodigo) {
    this.ecIdCodigo = ecIdCodigo;
  }
  
  public Date getCgFechaProceso() {
    return this.cgFechaProceso;
  }
  
  public void setCgFechaProceso(Date cgFechaProceso) {
    this.cgFechaProceso = cgFechaProceso;
  }
}

package co.fsfb.ws.rest.entidades;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "CA_TRAZA_GEST_CONTINUIDAD")
public class CaTrazaGestContinuidad {
  @Id
  @Column(name = "POM_ID_PREST_ORDM")
  private Long pomIdPrestOrdm;
  
  @Column(name = "GCO_IDCODIGO_ESTADO")
  private Long gcoIdCodigoEstado;
  
  @Column(name = "GCO_IDCODIGO_MOTIVO")
  private Long gcoIdCodigoMotivo;
  
  @Column(name = "GCO_DIREC_PACIENTE")
  private Boolean gcoDirecPaciente;
  
  @Column(name = "GCO_REALIZO_AGENDAMIENTO")
  private Boolean gcoRealizoAgendamiento;
  
  @Column(name = "GCO_OBSERVACIONES")
  private String gcoObservaciones;
  
  @Column(name = "PCA_AGE_CODIGRECEP")
  private String pcaAgeCodigRecep;
  
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "CG_FECHA_PROCESO")
  private Date cgFechaProceso;
  
  @MapsId
  @JoinColumn(name = "POM_ID_PREST_ORDM", referencedColumnName = "POM_ID_PREST_ORDM", columnDefinition = "POM_ID_PREST_ORDM")
  @OneToOne(optional = false)
  private CaPrestacionesOrdMed caPrestacionesOrdMed;
  
  public Long getPomIdPrestOrdm() {
    return this.pomIdPrestOrdm;
  }
  
  public void setPomIdPrestOrdm(Long pomIdPrestOrdm) {
    this.pomIdPrestOrdm = pomIdPrestOrdm;
  }
  
  public Long getGcoIdCodigoEstado() {
    return this.gcoIdCodigoEstado;
  }
  
  public void setGcoIdCodigoEstado(Long gcoIdCodigoEstado) {
    this.gcoIdCodigoEstado = gcoIdCodigoEstado;
  }
  
  public Long getGcoIdCodigoMotivo() {
    return this.gcoIdCodigoMotivo;
  }
  
  public void setGcoIdCodigoMotivo(Long gcoIdCodigoMotivo) {
    this.gcoIdCodigoMotivo = gcoIdCodigoMotivo;
  }
  
  public Boolean isGcoDirecPaciente() {
    return this.gcoDirecPaciente;
  }
  
  public void setGcoDirecPaciente(Boolean gcoDirecPaciente) {
    this.gcoDirecPaciente = gcoDirecPaciente;
  }
  
  public Boolean isGcoRealizoAgendamiento() {
    return this.gcoRealizoAgendamiento;
  }
  
  public void setGcoRealizoAgendamiento(Boolean gcoRealizoAgendamiento) {
    this.gcoRealizoAgendamiento = gcoRealizoAgendamiento;
  }
  
  public String getGcoObservaciones() {
    return this.gcoObservaciones;
  }
  
  public void setGcoObservaciones(String gcoObservaciones) {
    this.gcoObservaciones = gcoObservaciones;
  }
  
  public String getPcaAgeCodigRecep() {
    return this.pcaAgeCodigRecep;
  }
  
  public void setPcaAgeCodigRecep(String pcaAgeCodigRecep) {
    this.pcaAgeCodigRecep = pcaAgeCodigRecep;
  }
  
  public Date getCgFechaProceso() {
    return this.cgFechaProceso;
  }
  
  public void setCgFechaProceso(Date cgFechaProceso) {
    this.cgFechaProceso = cgFechaProceso;
  }
  
  public CaPrestacionesOrdMed getCaPrestacionesOrdMed() {
    return this.caPrestacionesOrdMed;
  }
  
  public void setCaPrestacionesOrdMed(CaPrestacionesOrdMed caPrestacionesOrdMed) {
    this.caPrestacionesOrdMed = caPrestacionesOrdMed;
  }
}

package co.fsfb.ws.rest.entidades;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "CA_ORDENES_MEDICAS")
@SequenceGenerator(sequenceName = "ORDENES_MEDICAS_SEQ", allocationSize = 1, name = "ORDENES_MEDICAS_SEQ")
public class CaOrdenesMedicas {
  @Id
  @Column(name = "ORM_ID_ORDM_NUMERO")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ORDENES_MEDICAS_SEQ")
  private Long ormIdOrdmNumero;
  
  @Column(name = "PAC_PAC_NUMERO")
  private Long pacPacNumero;
  
  @Column(name = "PAC_PAC_TIPOIDENTCODIGO")
  private String pacPacTipoIdentCodigo;
  
  @Column(name = "PAC_PAC_RUT")
  private String pacPacRut;
  
  @Column(name = "PCA_AGE_CODIGRECEP")
  private String pcaAgeCodigoRecep;
  
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "CG_FECHA_PROCESO")
  private Date cgFechaProceso;
  
  @Column(name = "FECHA_REGISTRO_FILE")
  private Date fechaRegistroFile;
  
  @Column(name = "ORM_FILENAME")
  private String ormFilename;
  
  @OneToOne(cascade = {CascadeType.ALL}, mappedBy = "caOrdenesMedicas")
  private CaDetalleOrdenesMedicas caDetalleOrdenesMedicas;
  
  @OneToMany(mappedBy = "caOrdenesMedicas", cascade = {CascadeType.ALL})
  private List<CaPrestacionesOrdMed> caPrestacionesOrdMed;
  
  @OneToMany(mappedBy = "caOrdenesMedicas", cascade = {CascadeType.ALL})
  private List<CaTrazaOrdenMedicas> caTrazaOrdenMedicas;
  
  public Long getOrmIdOrdmNumero() {
    return this.ormIdOrdmNumero;
  }
  
  public void setOrmIdOrdmNumero(Long ormIdOrdmNumero) {
    this.ormIdOrdmNumero = ormIdOrdmNumero;
  }
  
  public Long getPacPacNumero() {
    return this.pacPacNumero;
  }
  
  public void setPacPacNumero(Long pacPacNumero) {
    this.pacPacNumero = pacPacNumero;
  }
  
  public String getPacPacTipoIdentCodigo() {
    return this.pacPacTipoIdentCodigo;
  }
  
  public void setPacPacTipoIdentCodigo(String pacPacTipoIdentCodigo) {
    this.pacPacTipoIdentCodigo = pacPacTipoIdentCodigo;
  }
  
  public String getPacPacRut() {
    return this.pacPacRut;
  }
  
  public void setPacPacRut(String pacPacRut) {
    this.pacPacRut = pacPacRut;
  }
  
  public String getPcaAgeCodigoRecep() {
    return this.pcaAgeCodigoRecep;
  }
  
  public void setPcaAgeCodigoRecep(String pcaAgeCodigoRecep) {
    this.pcaAgeCodigoRecep = pcaAgeCodigoRecep;
  }
  
  public Date getCgFechaProceso() {
    return this.cgFechaProceso;
  }
  
  public void setCgFechaProceso(Date cgFechaProceso) {
    this.cgFechaProceso = cgFechaProceso;
  }
  
  public String getOrmFilename() {
    return this.ormFilename;
  }
  
  public void setOrmFilename(String ormFilename) {
    this.ormFilename = ormFilename;
  }
  
  public CaDetalleOrdenesMedicas getCaDetalleOrdenesMedicas() {
    return this.caDetalleOrdenesMedicas;
  }
  
  public void setCaDetalleOrdenesMedicas(CaDetalleOrdenesMedicas caDetalleOrdenesMedicas) {
    this.caDetalleOrdenesMedicas = caDetalleOrdenesMedicas;
  }
  
  public List<CaPrestacionesOrdMed> getCaPrestacionesOrdMed() {
    return this.caPrestacionesOrdMed;
  }
  
  public void setCaPrestacionesOrdMed(List<CaPrestacionesOrdMed> caPrestacionesOrdMed) {
    this.caPrestacionesOrdMed = caPrestacionesOrdMed;
  }
  
  public List<CaTrazaOrdenMedicas> getCaTrazaOrdenMedicas() {
    return this.caTrazaOrdenMedicas;
  }
  
  public void setCaTrazaOrdenMedicas(List<CaTrazaOrdenMedicas> caTrazaOrdenMedicas) {
    this.caTrazaOrdenMedicas = caTrazaOrdenMedicas;
  }
  
  public Date getFechaRegistroFile() {
    return this.fechaRegistroFile;
  }
  
  public void setFechaRegistroFile(Date fechaRegistroFile) {
    this.fechaRegistroFile = fechaRegistroFile;
  }
}

package co.fsfb.ws.rest.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CA_USUARIOS")
public class CaUsuarios {
  @Id
  @Column(name = "PCA_AGE_CODIGRECEP")
  private String pcaAgeCodigRecep;
  
  @Column(name = "CAU_DESC_USUARIOS")
  private String cauDescUsuarios;
  
  @Column(name = "CAU_ESTADO")
  private String cauEstado;
  
  public String getPcaAgeCodigRecep() {
    return this.pcaAgeCodigRecep;
  }
  
  public void setPcaAgeCodigRecep(String pcaAgeCodigRecep) {
    this.pcaAgeCodigRecep = pcaAgeCodigRecep;
  }
  
  public String getCauDescUsuarios() {
    return this.cauDescUsuarios;
  }
  
  public void setCauDescUsuarios(String cauDescUsuarios) {
    this.cauDescUsuarios = cauDescUsuarios;
  }
  
  public String getCauEstado() {
    return this.cauEstado;
  }
  
  public void setCauEstado(String cauEstado) {
    this.cauEstado = cauEstado;
  }
}

package co.fsfb.ws.rest.entidades;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "CA_TRAZA_CITAS")
@SequenceGenerator(sequenceName = "CA_TRAZA_CITAS_SEQ", allocationSize = 1, name = "CA_TRAZA_CITAS_SEQ")
public class CaTrazaCitas {
  @Id
  @Column(name = "CG_ID_TRAZA_CITA_NUMERO")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CA_TRAZA_CITAS_SEQ")
  private Long cgIdTrazaCitaNumero;
  
  @Column(name = "CG_ID_CITA_NUMERO")
  private Long cgIdCitaNumero;
  
  @Column(name = "EC_IDCODIGO")
  private Long ecIdCodigo;
  
  @Column(name = "PCA_AGE_CODIGRECEP")
  private String pcaAgeCodigRecep;
  
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "CG_FECHA_PROCESO")
  private Date cgFechaProceso;
  
  public Long getCgIdCitaNumero() {
    return this.cgIdCitaNumero;
  }
  
  public void setCgIdCitaNumero(Long cgIdCitaNumero) {
    this.cgIdCitaNumero = cgIdCitaNumero;
  }
  
  public Long getEcIdCodigo() {
    return this.ecIdCodigo;
  }
  
  public void setEcIdCodigo(Long ecIdCodigo) {
    this.ecIdCodigo = ecIdCodigo;
  }
  
  public String getPcaAgeCodigRecep() {
    return this.pcaAgeCodigRecep;
  }
  
  public void setPcaAgeCodigRecep(String pcaAgeCodigRecep) {
    this.pcaAgeCodigRecep = pcaAgeCodigRecep;
  }
  
  public Date getCgFechaProceso() {
    return this.cgFechaProceso;
  }
  
  public void setCgFechaProceso(Date cgFechaProceso) {
    this.cgFechaProceso = cgFechaProceso;
  }
  
  public Long getCgIdTrazaCitaNumero() {
    return this.cgIdTrazaCitaNumero;
  }
  
  public void setCgIdTrazaCitaNumero(Long cgIdTrazaCitaNumero) {
    this.cgIdTrazaCitaNumero = cgIdTrazaCitaNumero;
  }
}

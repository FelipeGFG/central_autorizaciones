package co.fsfb.ws.rest.entidades;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "CA_TRAZA_ORDENMEDICAS")
@SequenceGenerator(sequenceName = "CA_TRAZA_ORDENMEDICAS_SEQ", allocationSize = 1, name = "CA_TRAZA_ORDENMEDICAS_SEQ")
public class CaTrazaOrdenMedicas implements Serializable {
  @Id
  @Column(name = "TOM_ID_TRAZA_ORDM_NUM")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CA_TRAZA_ORDENMEDICAS_SEQ")
  private Long tomIdTrazaOrdmNum;
  
  @Column(name = "EOM_IDCODIGO")
  private Long eomIdCodigo;
  
  @Column(name = "PCA_AGE_CODIGRECEP")
  private String pcaAgeCodigRecep;
  
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "CG_FECHA_PROCESO")
  private Date cgFechaProceso;
  
  @ManyToOne
  @JoinColumn(name = "ORM_ID_ORDM_NUMERO", referencedColumnName = "ORM_ID_ORDM_NUMERO")
  private CaOrdenesMedicas caOrdenesMedicas;
  
  public Long getEomIdCodigo() {
    return this.eomIdCodigo;
  }
  
  public void setEomIdCodigo(Long eomIdCodigo) {
    this.eomIdCodigo = eomIdCodigo;
  }
  
  public String getPcaAgeCodigRecep() {
    return this.pcaAgeCodigRecep;
  }
  
  public void setPcaAgeCodigRecep(String pcaAgeCodigRecep) {
    this.pcaAgeCodigRecep = pcaAgeCodigRecep;
  }
  
  public Date getCgFechaProceso() {
    return this.cgFechaProceso;
  }
  
  public void setCgFechaProceso(Date cgFechaProceso) {
    this.cgFechaProceso = cgFechaProceso;
  }
  
  public Long getTomIdTrazaOrdmNum() {
    return this.tomIdTrazaOrdmNum;
  }
  
  public void setTomIdTrazaOrdmNum(Long tomIdTrazaOrdmNum) {
    this.tomIdTrazaOrdmNum = tomIdTrazaOrdmNum;
  }
  
  public CaOrdenesMedicas getCaOrdenesMedicas() {
    return this.caOrdenesMedicas;
  }
  
  public void setCaOrdenesMedicas(CaOrdenesMedicas caOrdenesMedicas) {
    this.caOrdenesMedicas = caOrdenesMedicas;
  }
}

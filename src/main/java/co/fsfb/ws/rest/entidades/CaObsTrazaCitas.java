package co.fsfb.ws.rest.entidades;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CA_OBS_TRAZA_CITAS")
@SequenceGenerator(sequenceName = "CA_OBS_TRAZA_CITAS_SEQ", allocationSize = 1, name = "CA_OBS_TRAZA_CITAS_SEQ")
public class CaObsTrazaCitas implements Serializable {
  @Id
  @Column(name = "CG_ID_OBS_TRAZA_CITA_NUMERO")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CA_OBS_TRAZA_CITAS_SEQ")
  private Long cgIdObsTrazaCitaNumero;
  
  @Column(name = "CG_ID_CITA_NUMERO")
  private Long cgIdCitaNumero;
  
  @Column(name = "EC_IDCODIGO")
  private Long ecIdCodigo;
  
  @Column(name = "OTC_OBSERVACION")
  private String otcObservacion;
  
  public Long getCgIdCitaNumero() {
    return this.cgIdCitaNumero;
  }
  
  public void setCgIdCitaNumero(Long cgIdCitaNumero) {
    this.cgIdCitaNumero = cgIdCitaNumero;
  }
  
  public Long getEcIdCodigo() {
    return this.ecIdCodigo;
  }
  
  public void setEcIdCodigo(Long ecIdCodigo) {
    this.ecIdCodigo = ecIdCodigo;
  }
  
  public String getOtcObservacion() {
    return this.otcObservacion;
  }
  
  public void setOtcObservacion(String otcObservacion) {
    this.otcObservacion = otcObservacion;
  }
  
  public Long getCgIdObsTrazaCitaNumero() {
    return this.cgIdObsTrazaCitaNumero;
  }
  
  public void setCgIdObsTrazaCitaNumero(Long cgIdObsTrazaCitaNumero) {
    this.cgIdObsTrazaCitaNumero = cgIdObsTrazaCitaNumero;
  }
}

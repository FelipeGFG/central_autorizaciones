package co.fsfb.ws.rest.entidades;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TRAZABILIDAD_TRANSACCION")
public class PlTrazabilidadTransaccion {
  @Id
  @Column(name = "ID_TRAZABILIDAD_TRANSACCION")
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "TRAZABILIDAD_TRANSACCION_ID967")
  private Long idTrazabilidadTransaccion;
  
  @ManyToOne
  @JoinColumn(name = "ID_DATOS_TRANSACCION")
  private PlDatosTransaccion plDatosTransaccion;
  
  @Column(name = "FECHA_INICIO")
  private Date fechaInicio;
  
  @Column(name = "ESTADO_TRANSACCION")
  private String estadoTransaccion;
  
  public Long getIdTrazabilidadTransaccion() {
    return this.idTrazabilidadTransaccion;
  }
  
  public void setIdTrazabilidadTransaccion(Long idTrazabilidadTransaccion) {
    this.idTrazabilidadTransaccion = idTrazabilidadTransaccion;
  }
  
  public PlDatosTransaccion getPlDatosTransaccion() {
    return this.plDatosTransaccion;
  }
  
  public void setPlDatosTransaccion(PlDatosTransaccion plDatosTransaccion) {
    this.plDatosTransaccion = plDatosTransaccion;
  }
  
  public Date getFechaInicio() {
    return this.fechaInicio;
  }
  
  public void setFechaInicio(Date fechaInicio) {
    this.fechaInicio = fechaInicio;
  }
  
  public String getEstadoTransaccion() {
    return this.estadoTransaccion;
  }
  
  public void setEstadoTransaccion(String estadoTransaccion) {
    this.estadoTransaccion = estadoTransaccion;
  }
}

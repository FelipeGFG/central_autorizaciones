package co.fsfb.ws.rest.entidades;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "PL_DETALLE_TRANSACCION")
@NamedQueries({@NamedQuery(name = "PL_DETALLE_TRANSACCION.findByTransactionData", query = "SELECT det FROM PlDetalleTransaccion det where det.plDatosTransaccion=:plDatosTransaccion")})
public class PlDetalleTransaccion {
  @Id
  @Column(name = "ID_DETALLE_TRANSACCION")
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "PL_DETALLE_TRANSACCION_ID_D166")
  private Long idDetalleTransaccion;
  
  @ManyToOne
  @JoinColumn(name = "ID_DATOS_TRANSACCION")
  private PlDatosTransaccion plDatosTransaccion;
  
  @Column(name = "NUMERO_OBLIGACION")
  private String numeroObligacion;
  
  @Column(name = "VALOR_OBLIGACION")
  private BigDecimal valorObligacion;
  
  public Long getIdDetalleTransaccion() {
    return this.idDetalleTransaccion;
  }
  
  public void setIdDetalleTransaccion(Long idDetalleTransaccion) {
    this.idDetalleTransaccion = idDetalleTransaccion;
  }
  
  public PlDatosTransaccion getPlDatosTransaccion() {
    return this.plDatosTransaccion;
  }
  
  public void setPlDatosTransaccion(PlDatosTransaccion plDatosTransaccion) {
    this.plDatosTransaccion = plDatosTransaccion;
  }
  
  public String getNumeroObligacion() {
    return this.numeroObligacion;
  }
  
  public void setNumeroObligacion(String numeroObligacion) {
    this.numeroObligacion = numeroObligacion;
  }
  
  public BigDecimal getValorObligacion() {
    return this.valorObligacion;
  }
  
  public void setValorObligacion(BigDecimal valorObligacion) {
    this.valorObligacion = valorObligacion;
  }
}

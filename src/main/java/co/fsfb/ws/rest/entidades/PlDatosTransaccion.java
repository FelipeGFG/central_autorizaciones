package co.fsfb.ws.rest.entidades;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "PL_DATOS_TRANSACCION")
@NamedQueries({@NamedQuery(name = "PL_DATOS_TRANSACCION.findByPacNumberAndState", query = "SELECT dt FROM PlDatosTransaccion dt where dt.pacPacNumero=:pacPacNumero order by dt.fechaRegistro desc ")})
public class PlDatosTransaccion {
  @Id
  @Column(name = "ID_DATOS_TRANSACCION")
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "PL_DATOS_TRANSACCION_ID_SEQ")
  private Long idDatosTransaccion;
  
  @ManyToOne
  @JoinColumn(name = "ID_PERSONA")
  private PlPersona plPersona;
  
  @Column(name = "NUMERO_TRANSACCION")
  private String numeroTransaccion;
  
  @Column(name = "VALOR_TRANSACCION")
  private BigDecimal valorTransacion;
  
  @Column(name = "FECHA_REGISTRO")
  private Date fechaRegistro;
  
  @Column(name = "AUTORIZACION_CUS")
  private String autorizacionCus;
  
  @Column(name = "PAC_PAC_NUMERO")
  private Integer pacPacNumero;
  
  @Column(name = "ESTADO_TRANSACCION")
  private String estadoTransaccion;
  
  @Column(name = "NUMERO_FACTURA")
  private String numeroFactura;
  
  @Column(name = "FRANQUICIA")
  private String franquicia;
  
  @Column(name = "METODO_PAGO")
  private String metodoPago;
  
  public Long getIdDatosTransaccion() {
    return this.idDatosTransaccion;
  }
  
  public void setIdDatosTransaccion(Long idDatosTransaccion) {
    this.idDatosTransaccion = idDatosTransaccion;
  }
  
  public PlPersona getPlPersona() {
    return this.plPersona;
  }
  
  public void setPlPersona(PlPersona plPersona) {
    this.plPersona = plPersona;
  }
  
  public String getNumeroTransaccion() {
    return this.numeroTransaccion;
  }
  
  public void setNumeroTransaccion(String numeroTransaccion) {
    this.numeroTransaccion = numeroTransaccion;
  }
  
  public BigDecimal getValorTransacion() {
    return this.valorTransacion;
  }
  
  public void setValorTransacion(BigDecimal valorTransacion) {
    this.valorTransacion = valorTransacion;
  }
  
  public Date getFechaRegistro() {
    return this.fechaRegistro;
  }
  
  public void setFechaRegistro(Date fechaRegistro) {
    this.fechaRegistro = fechaRegistro;
  }
  
  public String getAutorizacionCus() {
    return this.autorizacionCus;
  }
  
  public void setAutorizacionCus(String autorizacionCus) {
    this.autorizacionCus = autorizacionCus;
  }
  
  public Integer getPacPacNumero() {
    return this.pacPacNumero;
  }
  
  public void setPacPacNumero(Integer pacPacNumero) {
    this.pacPacNumero = pacPacNumero;
  }
  
  public String getEstadoTransaccion() {
    return this.estadoTransaccion;
  }
  
  public void setEstadoTransaccion(String estadoTransaccion) {
    this.estadoTransaccion = estadoTransaccion;
  }
  
  public String getNumeroFactura() {
    return this.numeroFactura;
  }
  
  public void setNumeroFactura(String numeroFactura) {
    this.numeroFactura = numeroFactura;
  }
  
  public String getFranquicia() {
    return this.franquicia;
  }
  
  public void setFranquicia(String franquicia) {
    this.franquicia = franquicia;
  }
  
  public String getMetodoPago() {
    return this.metodoPago;
  }
  
  public void setMetodoPago(String metodoPago) {
    this.metodoPago = metodoPago;
  }
}

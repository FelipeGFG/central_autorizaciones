package co.fsfb.ws.rest.entidades;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "CA_PRESTACIONES_ORDMED")
@SequenceGenerator(sequenceName = "PRESTACIONES_ORDEN_SEQ", allocationSize = 1, name = "PRESTACIONES_ORDEN_SEQ")
public class CaPrestacionesOrdMed implements Serializable {
  @Id
  @Column(name = "POM_ID_PREST_ORDM")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PRESTACIONES_ORDEN_SEQ")
  private Long pomIdPrestOrdm;
  
  @Column(name = "SER_SER_CODIGO")
  private String serSerCodigo;
  
  @Column(name = "SER_SER_DESC")
  private String serSerDesc;
  
  @Column(name = "PRE_PRE_CODIGO")
  private String prePreCodigo;
  
  @Column(name = "PRE_PRE_DESC")
  private String prePreDesc;
  
  @Column(name = "PCA_AGE_CODIGRECEP")
  private String pcaAgeCodigRecep;
  
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "CG_FECHA_PROCESO")
  private Date cgFechaProceso;
  
  @ManyToOne
  @JoinColumn(name = "ORM_ID_ORDM_NUMERO", referencedColumnName = "ORM_ID_ORDM_NUMERO")
  private CaOrdenesMedicas caOrdenesMedicas;
  
  @OneToOne(cascade = {CascadeType.ALL}, mappedBy = "caPrestacionesOrdMed")
  private CaTrazaGestContinuidad caTrazaGestContinuidad;
  
  @OneToOne(cascade = {CascadeType.ALL}, mappedBy = "caPrestacionesOrdMed")
  private CaGestionAutorizacion caGestionAutorizacion;
  
  public CaPrestacionesOrdMed() {}
  
  public CaPrestacionesOrdMed(Long pomIdPrestOrdm) {
    this.pomIdPrestOrdm = pomIdPrestOrdm;
  }
  
  public Long getPomIdPrestOrdm() {
    return this.pomIdPrestOrdm;
  }
  
  public void setPomIdPrestOrdm(Long pomIdPrestOrdm) {
    this.pomIdPrestOrdm = pomIdPrestOrdm;
  }
  
  public String getSerSerCodigo() {
    return this.serSerCodigo;
  }
  
  public void setSerSerCodigo(String serSerCodigo) {
    this.serSerCodigo = serSerCodigo;
  }
  
  public String getSerSerDesc() {
    return this.serSerDesc;
  }
  
  public void setSerSerDesc(String serSerDesc) {
    this.serSerDesc = serSerDesc;
  }
  
  public String getPrePreCodigo() {
    return this.prePreCodigo;
  }
  
  public void setPrePreCodigo(String prePreCodigo) {
    this.prePreCodigo = prePreCodigo;
  }
  
  public String getPrePreDesc() {
    return this.prePreDesc;
  }
  
  public void setPrePreDesc(String prePreDesc) {
    this.prePreDesc = prePreDesc;
  }
  
  public String getPcaAgeCodigRecep() {
    return this.pcaAgeCodigRecep;
  }
  
  public void setPcaAgeCodigRecep(String pcaAgeCodigRecep) {
    this.pcaAgeCodigRecep = pcaAgeCodigRecep;
  }
  
  public Date getCgFechaProceso() {
    return this.cgFechaProceso;
  }
  
  public void setCgFechaProceso(Date cgFechaProceso) {
    this.cgFechaProceso = cgFechaProceso;
  }
  
  public CaOrdenesMedicas getCaOrdenesMedicas() {
    return this.caOrdenesMedicas;
  }
  
  public void setCaOrdenesMedicas(CaOrdenesMedicas caOrdenesMedicas) {
    this.caOrdenesMedicas = caOrdenesMedicas;
  }
  
  public CaTrazaGestContinuidad getCaTrazaGestContinuidad() {
    return this.caTrazaGestContinuidad;
  }
  
  public void setCaTrazaGestContinuidad(CaTrazaGestContinuidad caTrazaGestContinuidad) {
    this.caTrazaGestContinuidad = caTrazaGestContinuidad;
  }
  
  public CaGestionAutorizacion getCaGestionAutorizacion() {
    return this.caGestionAutorizacion;
  }
  
  public void setCaGestionAutorizacion(CaGestionAutorizacion caGestionAutorizacion) {
    this.caGestionAutorizacion = caGestionAutorizacion;
  }
}

package co.fsfb.ws.rest.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "CA_POLIZAS_PACIENTES")
public class CaPolizasPacientes implements Serializable {

    @Id
    @Column(name = "PAC_PAC_NUMERO")
    private Long pacPacNumero;

    @Column(name = "ACC_ID_NUMERO")
    private Long accIdNumero;

    @Column(name = "EC_POLIZA_NUMERO")
    private String ecPolizaNumero;

    @Column(name = "PCA_AGE_CODIGRECEP")
    private String pcaAgeCodigRecep;

    @Temporal(TemporalType.DATE)
    @Column(name = "CG_FECHA_PROCESO")
    private Date cgFechaProceso;

    public Long getPacPacNumero() {
        return this.pacPacNumero;
    }

    public void setPacPacNumero(Long pacPacNumero) {
        this.pacPacNumero = pacPacNumero;
    }

    public String getEcPolizaNumero() {
        return this.ecPolizaNumero;
    }

    public void setEcPolizaNumero(String ecPolizaNumero) {
        this.ecPolizaNumero = ecPolizaNumero;
    }

    public String getPcaAgeCodigRecep() {
        return this.pcaAgeCodigRecep;
    }

    public void setPcaAgeCodigRecep(String pcaAgeCodigRecep) {
        this.pcaAgeCodigRecep = pcaAgeCodigRecep;
    }

    public Date getCgFechaProceso() {
        return this.cgFechaProceso;
    }

    public void setCgFechaProceso(Date cgFechaProceso) {
        this.cgFechaProceso = cgFechaProceso;
    }

    public Long getAccIdNumero() {
        return this.accIdNumero;
    }

    public void setAccIdNumero(Long accIdNumero) {
        this.accIdNumero = accIdNumero;
    }
}

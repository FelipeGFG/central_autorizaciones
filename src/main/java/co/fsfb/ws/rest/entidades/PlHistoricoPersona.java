package co.fsfb.ws.rest.entidades;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "PL_HISTORICO_PERSONA")
public class PlHistoricoPersona {
  @Id
  @Column(name = "ID_HISTORICO_PERSONA")
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "PL_HISTORICO_PERSONA_ID_TRA261")
  private Long idHistoricoPersona;
  
  @ManyToOne
  @JoinColumn(name = "ID_PERSONA")
  private PlPersona plPersona;
  
  @Column(name = "CELULAR")
  private Long celular;
  
  @Column(name = "FECHA_REGISTRO")
  private Date fechaRegistro;
  
  @Column(name = "EMAIL")
  private String email;
  
  @Column(name = "NOMBRES")
  private String nonbres;
  
  @Column(name = "APELLIDOS")
  private String apellidos;
  
  public Long getIdHistoricoPersona() {
    return this.idHistoricoPersona;
  }
  
  public void setIdHistoricoPersona(Long idHistoricoPersona) {
    this.idHistoricoPersona = idHistoricoPersona;
  }
  
  public PlPersona getPlPersona() {
    return this.plPersona;
  }
  
  public void setPlPersona(PlPersona plPersona) {
    this.plPersona = plPersona;
  }
  
  public Long getCelular() {
    return this.celular;
  }
  
  public void setCelular(Long celular) {
    this.celular = celular;
  }
  
  public Date getFechaRegistro() {
    return this.fechaRegistro;
  }
  
  public void setFechaRegistro(Date fechaRegistro) {
    this.fechaRegistro = fechaRegistro;
  }
  
  public String getEmail() {
    return this.email;
  }
  
  public void setEmail(String email) {
    this.email = email;
  }
  
  public String getNonbres() {
    return this.nonbres;
  }
  
  public void setNonbres(String nonbres) {
    this.nonbres = nonbres;
  }
  
  public String getApellidos() {
    return this.apellidos;
  }
  
  public void setApellidos(String apellidos) {
    this.apellidos = apellidos;
  }
}

package co.fsfb.ws.rest.dto;

import co.fsfb.ws.rest.cliente.ClienteCitaImpl;
import co.fsfb.ws.rest.exception.DataException;
import co.fsfb.ws.rest.vo.CaPolizasPacientesVO;
import co.fsfb.ws.rest.vo.CambioEstadoCitaVO;
import co.fsfb.ws.rest.vo.ConsultaDetalleCitaPacienteVO;
import co.fsfb.ws.rest.vo.ConsultaTrazabilidadCitaVO;
import co.fsfb.ws.rest.vo.FiltrosDetalleCitaPacienteVO;
import co.fsfb.ws.rest.vo.GestionAutorizacionCitaVO;
import co.fsfb.ws.rest.vo.ResCambioEstadoCitaVO;
import co.fsfb.ws.rest.vo.TrazabilidadCitaVO;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClienteCitaDTO {
  private static Logger logger = LoggerFactory.getLogger(ClienteCitaDTO.class);
  
  private ClienteCitaImpl clienteImpl = new ClienteCitaImpl();
  
  public ResCambioEstadoCitaVO cambiarEstadoCita(CambioEstadoCitaVO cambioEstadoCitaVO) throws Exception {
    logger.info("Creando cambiarEstadoCita(CambioEstadoCitaVO) ::::");
    return this.clienteImpl.cambiarEstadoCita(cambioEstadoCitaVO);
  }
  
  public List<ConsultaTrazabilidadCitaVO> consultarTrazabilidadCita(Long cGIdCitaNumero) {
    logger.info("Creando consultarTrazabilidadCita(Long) ::::");
    return this.clienteImpl.consultarTrazabilidadCita(cGIdCitaNumero);
  }
  
  public List<ConsultaTrazabilidadCitaVO> consultarTrazabilidadCita(TrazabilidadCitaVO trazabilidadCitaVO) {
    logger.info("Creando consultarTrazabilidadCita(trazabilidadCitaVO) ::::");
    return this.clienteImpl.consultarTrazabilidadCita(trazabilidadCitaVO);
  }
  
  public ConsultaDetalleCitaPacienteVO consultarDetalleCitaPaciente(FiltrosDetalleCitaPacienteVO filtrosDetalleCitaPacienteVO) throws DataException {
    logger.info("Creando consultarDetalleCitaPaciente(FiltrosDetalleCitaPacienteVO) ::::");
    return this.clienteImpl.consultarDetalleCitaPaciente(filtrosDetalleCitaPacienteVO);
  }
  
  public boolean registrarAutorizacion(GestionAutorizacionCitaVO gestionAutorizacionCitaVO) throws DataException {
    logger.info("registrarAutorizacion(gestionAutorizacionVO) ::::");
    return this.clienteImpl.registrarAutorizacion(gestionAutorizacionCitaVO);
  }
  
  public CaPolizasPacientesVO consultarPoliza(long pacNum) {
    return this.clienteImpl.consultarPoliza(pacNum);
  }
}

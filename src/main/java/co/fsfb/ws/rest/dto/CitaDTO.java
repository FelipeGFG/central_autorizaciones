package co.fsfb.ws.rest.dto;

import co.fsfb.ws.rest.dao.CitasDAO;
import co.fsfb.ws.rest.exception.CitaNoEncontradaException;
import co.fsfb.ws.rest.exception.DataException;
import co.fsfb.ws.rest.vo.CitaPorPagarVO;
import co.fsfb.ws.rest.vo.ConsultaCitaPacientePorAutoVO;
import co.fsfb.ws.rest.vo.ConsultaCitaPacienteVO;
import co.fsfb.ws.rest.vo.ConsultaCitaPorPacienteAutoVO;
import co.fsfb.ws.rest.vo.ListaUbicacionVO;
import co.fsfb.ws.rest.vo.ResultadoCitaPacienteVO;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CitaDTO {
  private static Logger logger = LoggerFactory.getLogger(CitaDTO.class);
  
  private CitasDAO dao = new CitasDAO();
  
  public List<ResultadoCitaPacienteVO> citas(ConsultaCitaPacienteVO consultaCitaPacienteVO) throws CitaNoEncontradaException {
    logger.info("Creando citas (ConsultaCitaPacienteVO)::::");
    try {
      return this.dao.cCitas(consultaCitaPacienteVO);
    } catch (DataException e) {
      throw new CitaNoEncontradaException(e.getMessage(), new Date(), e.getMessage());
    } 
  }
  
  public List<ResultadoCitaPacienteVO> citasPorAutorizar(ConsultaCitaPacientePorAutoVO consultaCitaPacientePorAutoVO) throws CitaNoEncontradaException {
    logger.info("Consultando citas por autorizar (consultaCitaPacientePorAutoVO)::::");
    try {
      return this.dao.cCitasPorAutorizar(consultaCitaPacientePorAutoVO);
    } catch (DataException e) {
      throw new CitaNoEncontradaException(e.getMessage(), new Date(), e.getMessage());
    } 
  }
  
  public List<CitaPorPagarVO> citasParaPagar(ConsultaCitaPorPacienteAutoVO consultaCitaPorPacienteAutoVO) throws DataException {
    logger.info("Consultando citas por autorizar (consultaCitaPacientePorAutoVO)::::");
    return this.dao.citasParaPago(consultaCitaPorPacienteAutoVO);
  }
  
  public List<ListaUbicacionVO> listaUbicacion() throws DataException {
    logger.info("Consultando ubicaciones disponibles::::");
    return this.dao.listaUbicacion();
  }
}

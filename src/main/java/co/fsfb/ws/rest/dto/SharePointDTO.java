package co.fsfb.ws.rest.dto;

import co.fsfb.ws.rest.dao.SharePointDAO;
import co.fsfb.ws.rest.exception.CredencialesFallidasException;
import co.fsfb.ws.rest.vo.Documento;
import co.fsfb.ws.rest.vo.RegistroSharePointVO;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SharePointDTO {
  private static Logger logger = LoggerFactory.getLogger(SharePointDTO.class);
  
  private SharePointDAO SP = new SharePointDAO();
  
  public RegistroSharePointVO uploadDocument(Documento doc) throws CredencialesFallidasException {
    logger.info("Creando uploadDocument (Documento)::::");
    RegistroSharePointVO registroSharePointVO = null;
    try {
      if (this.SP.createFolderGral(doc.getPac().getTipoDocId() + doc.getPac().getNumDocId(), doc.getNombreArchivo()))
        if (!doc.isArchivoGral()) {
          registroSharePointVO = this.SP.createArchivoEventos(doc
              .getPac().getTipoDocId() + doc.getPac().getNumDocId(), 
              (doc.getPac().getIdTipoExamen() != null) ? (doc
              .getPac().getIdTipoExamen() + "-" + doc.getPac().getTipoExamen()) : null, doc
              
              .getArchivo(), doc.getNombreArchivo());
        } else {
          registroSharePointVO = this.SP.createArchivoGeneral(doc
              .getPac().getTipoDocId() + doc.getPac().getNumDocId(), doc.getArchivo(), doc
              .getNombreArchivo());
        }  
      registroSharePointVO.setRespuestaVer(this.SP.createFolderGral(doc
            .getPac().getTipoDocId() + doc.getPac().getNumDocId(), doc.getNombreArchivo()));
    } catch (Exception e) {
      throw new CredencialesFallidasException(e.getMessage(), new Date(), e.getMessage());
    } 
    return registroSharePointVO;
  }
  
  public boolean updateDocument(Documento doc) throws CredencialesFallidasException {
    logger.info("Creando updateDocument::::");
    try {
      if (doc.isArchivoGral())
        return this.SP.updateArchivo(doc.getPac().getTipoDocId() + doc.getPac().getNumDocId(), doc.getArchivo(), doc
            .getNombreArchivo(), true, null, null); 
      return this.SP.updateArchivo(doc.getPac().getTipoDocId() + doc.getPac().getNumDocId(), doc.getArchivo(), doc
          .getNombreArchivo(), false, doc.getFecha(), doc
          .getPac().getIdTipoExamen() + "-" + doc.getPac().getTipoExamen());
    } catch (Exception e) {
      throw new CredencialesFallidasException(e.getMessage(), new Date(), e.getMessage());
    } 
  }
  
  public boolean deleteDocument(Documento doc) throws CredencialesFallidasException {
    logger.info("Creando deleteDocument::::");
    try {
      if (doc.isArchivoGral())
        return this.SP.deleteArchivo(doc.getPac().getTipoDocId() + doc.getPac().getNumDocId(), doc
            .getNombreArchivo(), true, null, null); 
      return this.SP.deleteArchivo(doc.getPac().getTipoDocId() + doc.getPac().getNumDocId(), doc.getNombreArchivo(), false, doc
          .getFecha(), 
          (doc.getPac().getIdTipoExamen() != null) ? (doc
          .getPac().getIdTipoExamen() + "-" + doc.getPac().getTipoExamen()) : null);
    } catch (Exception e) {
      throw new CredencialesFallidasException(e.getMessage(), new Date(), e.getMessage());
    } 
  }
  
  public byte[] findDocument(Documento doc) throws CredencialesFallidasException {
    logger.info("Creando findDocument::::");
    try {
      if (doc.isArchivoGral())
        return this.SP.findArchivo(doc.getPac().getTipoDocId() + doc.getPac().getNumDocId(), doc.getNombreArchivo(), true, null, null); 
      return this.SP.findArchivo(doc.getPac().getTipoDocId() + doc.getPac().getNumDocId(), doc.getNombreArchivo(), false, doc
          .getFecha(), 
          (doc.getPac().getIdTipoExamen() != null) ? (doc
          .getPac().getIdTipoExamen() + "-" + doc.getPac().getTipoExamen()) : null);
    } catch (Exception e) {
      throw new CredencialesFallidasException(e.getMessage(), new Date(), e.getMessage());
    } 
  }
}

package co.fsfb.ws.rest.dto;

import co.fsfb.ws.rest.dao.CaPacientesDAO;
import co.fsfb.ws.rest.dao.PagosDAO;
import co.fsfb.ws.rest.entidades.PlDatosTransaccion;
import co.fsfb.ws.rest.entidades.PlDetalleTransaccion;
import co.fsfb.ws.rest.exception.DataException;
import co.fsfb.ws.rest.exception.ValorException;
import co.fsfb.ws.rest.util.PropertiesManager;
import co.fsfb.ws.rest.vo.ConsultaPagosVO;
import co.fsfb.ws.rest.vo.ListaPagosVO;
import co.fsfb.ws.rest.vo.PacienteHIS;
import co.fsfb.ws.rest.vo.PagosCitasTransaccionCreada;
import co.fsfb.ws.rest.vo.PagosTransaccionConfirmada;
import co.fsfb.ws.rest.vo.PagosTransaccionCreada;
import co.fsfb.ws.rest.vo.PersonP2P;
import co.fsfb.ws.rest.vo.placetopay.Amount;
import co.fsfb.ws.rest.vo.placetopay.Payment;
import co.fsfb.ws.rest.vo.placetopay.Person;
import co.fsfb.ws.rest.vo.placetopay.RequestPlaceToPlay;
import com.placetopay.java_placetopay.Entities.Models.RedirectInformation;
import com.placetopay.java_placetopay.Entities.Models.RedirectRequest;
import com.placetopay.java_placetopay.Entities.Models.RedirectResponse;
import com.placetopay.java_placetopay.Entities.Transaction;
import com.placetopay.java_placetopay.PlaceToPay;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PagosDTO {
  private static Logger logger = LoggerFactory.getLogger(PagosDTO.class);
  
  private PagosDAO pagosDAO = new PagosDAO();
  
  private CaPacientesDAO pacientesDAO = new CaPacientesDAO();
  
  private static final String NEW_LINE = "\",\r\n";
  
  public String guardarPagos(PagosTransaccionCreada pagosTransaccionCreada) throws DataException, IOException {
    logger.info("Guardando transaccion1");
    if (pagosTransaccionCreada.getPagadorP2P() == null && pagosTransaccionCreada.getCompradorP2P().getCelular() == null) {
      PacienteHIS pacienteHIS = this.pacientesDAO.consultarPacienteHIS(pagosTransaccionCreada.getCompradorP2P().getTipoDocumento(), pagosTransaccionCreada.getCompradorP2P().getNoDocumento());
      if (pacienteHIS.getCelular() == null) {
        logger.info("Entro al null");
        throw new NullPointerException(PropertiesManager.CONFIG.getString("NO_REGISTRA_CELULAR"));
      } 
      pagosTransaccionCreada.getCompradorP2P().setCelular(Long.valueOf(Long.parseLong(pacienteHIS.getCelular())));
    } 
    validarTipoDocumento(pagosTransaccionCreada.getCompradorP2P());
    validarTipoDocumento(pagosTransaccionCreada.getPagadorP2P());
    logger.debug("Guardando transaccion");
    System.out.println("Guardando transaccion");
    Long idTransacccion = this.pagosDAO.guardarPago(pagosTransaccionCreada);
    RequestPlaceToPlay requestPlaceToPlay = new RequestPlaceToPlay();
    Person payer = new Person();
    Person bayer = new Person();
    Payment payment = new Payment();
    Amount amount = new Amount();
    PersonP2P personP2P = null;
    if (pagosTransaccionCreada.getPagadorP2P() != null) {
      personP2P = pagosTransaccionCreada.getPagadorP2P();
    } else {
      personP2P = pagosTransaccionCreada.getCompradorP2P();
    } 
    payer.setDocumentType(personP2P.getTipoDocPlacetoPay());
    payer.setDocument(personP2P.getNoDocumento());
    payer.setEmail(personP2P.getEmail());
    payer.setMobile(toString(personP2P.getCelular()));
    payer.setName(personP2P.getNombres());
    payer.setSurname(personP2P.getApellidos());
    payer.setSurname(personP2P.getApellidos());
    requestPlaceToPlay.setPayer(payer);
    bayer.setDocumentType(pagosTransaccionCreada.getCompradorP2P().getTipoDocPlacetoPay());
    bayer.setDocument(pagosTransaccionCreada.getCompradorP2P().getNoDocumento());
    bayer.setEmail(pagosTransaccionCreada.getCompradorP2P().getEmail());
    bayer.setMobile(toString(pagosTransaccionCreada.getCompradorP2P().getCelular()));
    bayer.setName(pagosTransaccionCreada.getCompradorP2P().getNombres());
    bayer.setSurname(pagosTransaccionCreada.getCompradorP2P().getApellidos());
    bayer.setSurname(pagosTransaccionCreada.getCompradorP2P().getApellidos());
    requestPlaceToPlay.setBuyer(bayer);
    StringBuilder descripcion = new StringBuilder("Citas");
    Iterator<PagosCitasTransaccionCreada> var10 = pagosTransaccionCreada.getListaCitas().iterator();
    while (var10.hasNext()) {
      PagosCitasTransaccionCreada pagosCitasTransaccionCreada = var10.next();
      descripcion.append(" - ");
      descripcion.append(pagosCitasTransaccionCreada.getNombreCita());
    } 
    String descrip = (descripcion.toString().length() > 236) ? descripcion.substring(0, 236) : descripcion.toString();
    payment.setReference(idTransacccion.toString());
    payment.setDescription(descrip);
    amount.setCurrency("COP");
    amount.setTotal(pagosTransaccionCreada.valorTotal().toString());
    requestPlaceToPlay.setPayment(payment);
    requestPlaceToPlay.getPayment().setAmount(amount);
    requestPlaceToPlay.setExpiration(getExpirationDate());
    requestPlaceToPlay.setIpAddress(pagosTransaccionCreada.getIpCliente());
    requestPlaceToPlay.setReturnUrl(pagosTransaccionCreada.getUrlRedireccion().concat("/").concat(idTransacccion.toString()));
    requestPlaceToPlay.setUserAgent(pagosTransaccionCreada.getUserAgent());
    return generateUrlApi(requestPlaceToPlay);
  }
  
  private void validarTipoDocumento(PersonP2P compradorP2P) {
    if (compradorP2P != null && compradorP2P.getTipoDocumento() != null) {
      if ("6".equals(compradorP2P.getTipoDocumento()))
        compradorP2P.setTipoDocPlacetoPay("NIT"); 
      if ("4".equals(compradorP2P.getTipoDocumento()))
        compradorP2P.setTipoDocPlacetoPay("CC"); 
      if ("5".equals(compradorP2P.getTipoDocumento()))
        compradorP2P.setTipoDocPlacetoPay("CE"); 
      if ("3".equals(compradorP2P.getTipoDocumento()))
        compradorP2P.setTipoDocPlacetoPay("TI"); 
      if ("P".equals(compradorP2P.getTipoDocumento()))
        compradorP2P.setTipoDocPlacetoPay("PPN"); 
      if ("P".equals(compradorP2P.getTipoDocumento()))
        compradorP2P.setTipoDocPlacetoPay("PPN"); 
      if ("D".equals(compradorP2P.getTipoDocumento()) || 
        "S".equals(compradorP2P.getTipoDocumento()))
        compradorP2P.setTipoDocPlacetoPay("CIP"); 
      if ("A".equals(compradorP2P.getTipoDocumento()) || 
        "M".equals(compradorP2P.getTipoDocumento()))
        compradorP2P.setTipoDocPlacetoPay("SSN"); 
      if ("N".equals(compradorP2P.getTipoDocumento()) || 
        "2".equals(compradorP2P.getTipoDocumento()))
        compradorP2P.setTipoDocPlacetoPay("TI"); 
    } 
  }
  
  public String toString(Object o) {
    try {
      return o.toString();
    } catch (NullPointerException var3) {
      return null;
    } 
  }
  
  private String generateUrlApi(RequestPlaceToPlay requestPlaceToPlay) throws IOException, NumberFormatException, DataException {
    logger.info("Inicio URL placetopay");
    System.out.println("Inicio URL placetopay");
    ObjectMapper objectMapper = new ObjectMapper();
    PlaceToPay placetopay = new PlaceToPay(getLogin(), getKey(), new URL(PropertiesManager.CONFIG.getString("PLACE_TO_PAY_SERVICE_URL")));
    String data = objectMapper.writeValueAsString(requestPlaceToPlay);
    data = data.replace("�", "N").replace("�", "n");
    data = StringUtils.stripAccents(data);
    RedirectRequest request = new RedirectRequest(data);
    RedirectResponse response = placetopay.request(request);
    logger.info("URL: " + response.getProcessUrl());
    System.out.println("URL: " + response.getProcessUrl());
    logger.info("Estado transaccion " + response.isSuccessful());
    System.out.println("Estado transaccion " + response.isSuccessful());
    logger.info("Razon transaccion " + response.getStatus().getReason());
    System.out.println("Razon transaccion " + response.getStatus().getReason());
    if (response.isSuccessful()) {
      try {
        this.pagosDAO.persistReference(Long.valueOf(Long.parseLong(requestPlaceToPlay.getPayment().getReference())), response.getRequestId().toString());
      } catch (Exception e) {
        e.printStackTrace();
      } 
      return response.getProcessUrl();
    } 
    return response.getStatus().getMessage();
  }
  
  private String getExpirationDate() {
    Calendar cal = Calendar.getInstance();
    cal.setTime(new Date());
    cal.add(12, cal.get(12) + Integer.parseInt(PropertiesManager.CONFIG.getString("PLACE_TO_PAY_EXPIRATION_MINUTES")));
    return (new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ", Locale.getDefault())).format(cal.getTime());
  }
  
  private String getKey() {
    return PropertiesManager.CONFIG.getString("PLACE_TO_PAY_SECKEY");
  }
  
  private String getLogin() {
    return PropertiesManager.CONFIG.getString("PLACE_TO_PAY_LOGIN");
  }
  
  public PagosTransaccionConfirmada getStatusEarlier(Long referenceId) throws DataException, MalformedURLException, ValorException, ParseException {
    return getStatus(this.pagosDAO.getPlace2PayReference(referenceId), Boolean.valueOf(false));
  }
  
  private PagosTransaccionConfirmada getStatus(PlDatosTransaccion plDatosTransaccion, Boolean isFinalUpdate) throws MalformedURLException, ValorException, DataException, ParseException {
    String pattern = "yyyy-MM-dd HH:mm:ss";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
    String referencia = plDatosTransaccion.getNumeroTransaccion();
    PlaceToPay placetopay = new PlaceToPay(getLogin(), getKey(), new URL(PropertiesManager.CONFIG.getString("PLACE_TO_PAY_SERVICE_URL")));
    RedirectInformation response = placetopay.query(referencia);
    String autorizacionCus = null;
    String paymentMethod = null;
    String franquicia = null;
    BigDecimal payment = null;
    String idRecibo = null;
    if (response.isSuccessful()) {
      String estado = response.getStatus().getStatus();
      String idTransaccion = response.getRequest().getPayment().getReference();
      if (response.getPayment() != null && response.getPayment().get(0) != null) {
        autorizacionCus = ((Transaction)response.getPayment().get(0)).getAuthorization();
        paymentMethod = ((Transaction)response.getPayment().get(0)).getPaymentMethod();
        franquicia = ((Transaction)response.getPayment().get(0)).getFranchise();
        payment = BigDecimal.valueOf(((Transaction)response.getPayment().get(0)).getAmount().getTo().getTotal());
        idRecibo = (((Transaction)response.getPayment().get(0)).getReceipt().longValue() == 0L) ? "000000" : ((Transaction)response.getPayment().get(0)).getReceipt().toString();
      } 
      PagosTransaccionConfirmada pagosTransaccionConfirmada = new PagosTransaccionConfirmada();
      pagosTransaccionConfirmada.setAutorizacionCus(autorizacionCus);
      pagosTransaccionConfirmada.setEstadoTransaccion(estado);
      pagosTransaccionConfirmada.setFranquicia(franquicia);
      pagosTransaccionConfirmada.setIdTransaccion(Long.valueOf(Long.parseLong(referencia)));
      pagosTransaccionConfirmada.setTotalPagado(payment);
      pagosTransaccionConfirmada.setMetodoDePago(paymentMethod);
      pagosTransaccionConfirmada.setIdPlaceTopay(idTransaccion);
      pagosTransaccionConfirmada.setIdRecibo(idRecibo);
      pagosTransaccionConfirmada.setFecha(simpleDateFormat.format(plDatosTransaccion.getFechaRegistro()));
      return pagosTransaccionConfirmada;
    } 
    StringBuilder error = new StringBuilder("Erro en la conexion con PLACE TO PAY para buscar el id: ");
    error.append(referencia);
    logger.error(error.toString());
    return null;
  }
  
  public List<ListaPagosVO> listaPagosPorCliente(ConsultaPagosVO consultaPagosVO) {
    List<PlDatosTransaccion> listaPagos = this.pagosDAO.buscarPagosPorCliente(consultaPagosVO.getNoPacPaciente());
    List<ListaPagosVO> listaPagosVOs = new ArrayList<>();
    Iterator<PlDatosTransaccion> var4 = listaPagos.iterator();
    while (var4.hasNext()) {
      PlDatosTransaccion plDatosTransaccion = var4.next();
      ListaPagosVO listaPagosVO = new ListaPagosVO();
      listaPagosVO.setAutorizacion(plDatosTransaccion.getAutorizacionCus());
      DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      listaPagosVO.setFechaRegistro(dateFormat.format(plDatosTransaccion.getFechaRegistro()));
      listaPagosVO.setReferencia(plDatosTransaccion.getIdDatosTransaccion());
      listaPagosVO.setEstado(plDatosTransaccion.getEstadoTransaccion());
      listaPagosVO.setIdTransaccion(plDatosTransaccion.getNumeroTransaccion());
      listaPagosVO.setValor("COP " + NumberFormat.getCurrencyInstance().format(plDatosTransaccion.getValorTransacion()));
      listaPagosVO.setNumeroFactura(plDatosTransaccion.getNumeroFactura());
      List<PlDetalleTransaccion> plDetalleList = this.pagosDAO.buscarCitasPorTransaccion(plDatosTransaccion);
      StringBuilder descri = new StringBuilder("Citas en la transacci");
      Iterator<PlDetalleTransaccion> var10 = plDetalleList.iterator();
      while (var10.hasNext()) {
        PlDetalleTransaccion plDetalleTransaccion = var10.next();
        descri.append("- " + plDetalleTransaccion.getNumeroObligacion() + " valor : COP " + NumberFormat.getCurrencyInstance().format(plDetalleTransaccion.getValorObligacion()) + "\n");
      } 
      listaPagosVO.setDescripcion(descri.toString());
      listaPagosVOs.add(listaPagosVO);
    } 
    return listaPagosVOs;
  }
}

package co.fsfb.ws.rest.dto;

import co.fsfb.ws.rest.dao.ValidarUsuarioDAO;
import co.fsfb.ws.rest.exception.CredencialesFallidasException;
import co.fsfb.ws.rest.exception.DataException;
import co.fsfb.ws.rest.exception.EmailDuplicadoException;
import co.fsfb.ws.rest.exception.FechaNacimientoException;
import co.fsfb.ws.rest.exception.NombreErroneoException;
import co.fsfb.ws.rest.exception.PacienteNoRegistradoException;
import co.fsfb.ws.rest.exception.SexoException;
import co.fsfb.ws.rest.exception.UsuarioDuplicadoException;
import co.fsfb.ws.rest.exception.UsuarioNoEncontradoException;
import co.fsfb.ws.rest.util.LDAPConfig;
import co.fsfb.ws.rest.vo.Usuario;
import java.util.Date;
import java.util.logging.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AutenticacionDTO {

    private static Logger logger = LoggerFactory.getLogger(AutenticacionDTO.class);
    private static final java.util.logging.Logger LOG = java.util.logging.Logger.getLogger(AutenticacionDTO.class.getName());
    
    
    
    private LDAPConfig ldap = new LDAPConfig();

    private ValidarUsuarioDAO dao = new ValidarUsuarioDAO();

    public Usuario crearUsuarioLDAP(Usuario usuario) throws CredencialesFallidasException, UsuarioNoEncontradoException, UsuarioDuplicadoException, EmailDuplicadoException, NombreErroneoException, PacienteNoRegistradoException, FechaNacimientoException, SexoException {
        logger.info("Creando crearUsuarioLDAP::::" + usuario.getUid());
        try {
            if (usuario.getRole().equals("")) {
                usuario = this.dao.validaUsr(usuario);
                usuario.setRole("");
                return this.ldap.crearUsrLDAP(usuario);
            }
            return this.ldap.crearUsrLDAP(usuario);
        } catch (DataException e) {
            throw new UsuarioNoEncontradoException(e.getMessage(), new Date(), e.getMessage());
        }
    }

    public Usuario validarUsuario(Usuario usuario) throws CredencialesFallidasException {
        logger.info("Creando validarUsuario::::" + usuario.getUid());
        try {
            return this.ldap.autentica(usuario);
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "ERROR - CENTRAL AUTORIZAV2", e);
            logger.info("Error validarUsuario::::" + e.toString());
            throw new CredencialesFallidasException(e.getMessage(), new Date(), e.getMessage());
        }
    }

    public boolean cambioContrasena(Usuario usuario) throws CredencialesFallidasException {
        logger.info("Creando cambioContrasena::::" + usuario.getUid());
        try {
            return this.ldap.cambioContrasena(usuario);
        } catch (Exception e) {
            throw new CredencialesFallidasException(e.getMessage(), new Date(), e.getMessage());
        }
    }

    public boolean confirmaCambioContrasena(Usuario usuario) throws CredencialesFallidasException {
        logger.info("Creando confirmaCambioContrasena::::" + usuario.getUid());
        return this.ldap.confirmarCambioContrasena(usuario);
    }
}

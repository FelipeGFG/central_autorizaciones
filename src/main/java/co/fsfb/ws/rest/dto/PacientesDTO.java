package co.fsfb.ws.rest.dto;

import co.fsfb.ws.rest.dao.CaPacientesDAO;
import co.fsfb.ws.rest.exception.DataException;
import co.fsfb.ws.rest.exception.PacienteNoRegistradoException;
import co.fsfb.ws.rest.vo.ConsultaPacienteHISVO;
import co.fsfb.ws.rest.vo.PacienteHIS;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PacientesDTO {
  private static Logger logger = LoggerFactory.getLogger(PacientesDTO.class);
  
  private CaPacientesDAO pacientesDAO = new CaPacientesDAO();
  
  public PacienteHIS consultarPacienteHIS(ConsultaPacienteHISVO consultaPacienteHIS) throws PacienteNoRegistradoException {
    logger.info("Consultando pacientes en HIS (ConsultaPacienteHISVO)::::");
    try {
      return this.pacientesDAO.consultarPacienteHIS(consultaPacienteHIS.getTipoDocumento(), consultaPacienteHIS.getNumeroDocumento());
    } catch (DataException e) {
      throw new PacienteNoRegistradoException(e.getMessage(), new Date(), e.getDetalleExcepcion());
    } 
  }
}

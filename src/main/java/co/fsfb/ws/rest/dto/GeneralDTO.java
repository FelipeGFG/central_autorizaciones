package co.fsfb.ws.rest.dto;

import co.fsfb.ws.rest.cliente.NotificacionImpl;
import co.fsfb.ws.rest.entidades.CaGestionNotificaciones;
import co.fsfb.ws.rest.exception.DataException;
import co.fsfb.ws.rest.exception.EnvioEmailException;
import co.fsfb.ws.rest.util.EnviarEmail;
import co.fsfb.ws.rest.util.PropertiesManager;
import co.fsfb.ws.rest.vo.EnviarEmailVO;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.text.StrSubstitutor;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GeneralDTO {
  private static Logger logger = LoggerFactory.getLogger(GeneralDTO.class);
  
  private NotificacionImpl notificacionImpl = new NotificacionImpl();
  
  public boolean enviarEmail(EnviarEmailVO enviarEmailVO) throws IOException, EmailException, EnvioEmailException, DataException {
    logger.info("Creando enviarEmail::::");
    HtmlEmail email = new HtmlEmail();
    String html = null;
    StrSubstitutor strSub = null;
    String templateName = "/mail/email.html";
    Map<String, String> emailDatos = new HashMap<>();
    emailDatos.put("nombre", enviarEmailVO.getGivenName());
    emailDatos.put("entidad", enviarEmailVO.getEmail().getEntidad());
    emailDatos.put("estado", enviarEmailVO.getEmail().getEstado());
    emailDatos.put("medico", enviarEmailVO.getEmail().getMedico());
    emailDatos.put("consultorio", enviarEmailVO.getEmail().getConsultorio());
    emailDatos.put("direccion", enviarEmailVO.getEmail().getDireccion());
    emailDatos.put("ciudad", enviarEmailVO.getEmail().getCiudad());
    emailDatos.put("hora", enviarEmailVO.getEmail().getHora());
    emailDatos.put("dia", enviarEmailVO.getEmail().getDia());
    switch (enviarEmailVO.getEmail().getEstadoId()) {
      case "2":
        templateName = "/mail/email-confirm.html";
        break;
      case "4":
      case "5":
        templateName = "/mail/email-cancel.html";
        break;
    } 
    html = fileToString(GeneralDTO.class.getResourceAsStream(templateName), "utf-8");
    String[] images = { "CitaProgramada-01.png", "CitaProgramada-02.png", "CitaProgramada-03.png", "facebook-icon.png", "instagram-icon.png", "twitter-icon.png" };
    for (int i = 0; i < images.length; i++)
      emailDatos.put(images[i], PropertiesManager.CONFIG.getString("MAIL_IMAGES").concat(images[i])); 
    strSub = new StrSubstitutor(emailDatos);
    html = strSub.replace(html);
    String[] emails = { enviarEmailVO.getMail() };
    (new EnviarEmail()).postMail(emails, enviarEmailVO.getEmail().getAsunto(), html);
    if (enviarEmailVO.getPacnumero() != null && enviarEmailVO.getAccIdNumero() != null && enviarEmailVO
      .getNotIdentificador() != null && enviarEmailVO.getPcaAgeCodigRecep() != null) {
      CaGestionNotificaciones caGestionNotificaciones = new CaGestionNotificaciones();
      caGestionNotificaciones.setPacPacNumero(Long.valueOf(Long.parseLong(enviarEmailVO.getPacnumero())));
      caGestionNotificaciones.setAccIdNumero(enviarEmailVO.getAccIdNumero());
      caGestionNotificaciones.setNotIdentificador(enviarEmailVO.getNotIdentificador());
      caGestionNotificaciones.setPcaAgeCodigRecep(enviarEmailVO.getPcaAgeCodigRecep());
      this.notificacionImpl.registrarCaGestionNotificaciones(caGestionNotificaciones);
    } 
    return true;
  }
  
  private static String fileToString(InputStream input, String encoding) throws IOException {
    StringWriter sw = new StringWriter();
    InputStreamReader in = new InputStreamReader(input, encoding);
    char[] buffer = new char[2048];
    int n = 0;
    while (-1 != (n = in.read(buffer)))
      sw.write(buffer, 0, n); 
    return sw.toString();
  }
}

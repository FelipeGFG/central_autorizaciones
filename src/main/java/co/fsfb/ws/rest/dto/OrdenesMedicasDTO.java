package co.fsfb.ws.rest.dto;

import co.fsfb.ws.rest.cliente.OrdenesMedicasImpl;
import co.fsfb.ws.rest.entidades.CaMotivosEliminarOm;
import co.fsfb.ws.rest.entidades.CaPrestacionesOrdMed;
import co.fsfb.ws.rest.exception.DataException;
import co.fsfb.ws.rest.vo.AdmOrdenesMedicasVO;
import co.fsfb.ws.rest.vo.ConsultaTrazabilidadCitaVO;
import co.fsfb.ws.rest.vo.CrearPrestacionesOrdMedVO;
import co.fsfb.ws.rest.vo.FiltrosOrdenMedicaVO;
import co.fsfb.ws.rest.vo.FinalizarOrdenMedicaVO;
import co.fsfb.ws.rest.vo.GestionAutorizacionVO;
import co.fsfb.ws.rest.vo.GestionContinuidadVO;
import co.fsfb.ws.rest.vo.OrdenMedicaVO;
import co.fsfb.ws.rest.vo.PrestacionesOrdMedVO;
import co.fsfb.ws.rest.vo.RespuestaGestionContinuidadVO;
import co.fsfb.ws.rest.vo.RespuestaOrdenMedicaVO;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OrdenesMedicasDTO {
  private static Logger logger = LoggerFactory.getLogger(OrdenesMedicasDTO.class);
  
  private OrdenesMedicasImpl ordenesMedicasImpl = new OrdenesMedicasImpl();
  
  public List<RespuestaOrdenMedicaVO> consultarOrdenMedica(FiltrosOrdenMedicaVO filtrosOrdenMedicaVO) throws DataException {
    logger.info("Creando consultarOrdenMedica(FiltrosOrdenMedicaVO) ::::");
    return this.ordenesMedicasImpl.consultarOrdenMedica(filtrosOrdenMedicaVO);
  }
  
  public List<ConsultaTrazabilidadCitaVO> consultarTrazabilidadOrden(Long ormIdOrdmNumero) {
    logger.info("Creando consultarTrazabilidadOrden(Long) ::::");
    return this.ordenesMedicasImpl.consultarTrazabilidadOrden(ormIdOrdmNumero);
  }
  
  public List<RespuestaOrdenMedicaVO> consultarOrdenesMedicas(FiltrosOrdenMedicaVO filtrosOrdenMedicaVO) throws DataException {
    logger.info("Creando consultarOrdenesMedicas(FiltrosOrdenMedicaVO) ::::");
    return this.ordenesMedicasImpl.consultarOrdenesMedicas(filtrosOrdenMedicaVO);
  }
  
  public RespuestaGestionContinuidadVO crearGestionContinuidad(GestionContinuidadVO gestionContinuidadVO) throws DataException {
    logger.info("Creando gestionContinuidad(filtrosGestionContinuidadVO) ::::");
    return this.ordenesMedicasImpl.crearGestionContinuidad(gestionContinuidadVO);
  }
  
  public OrdenMedicaVO detalleOrdenMedica(Long ormIdOrdmNumero) throws DataException {
    logger.info("Creando detalleOrdenMedica(ormIdOrdmNumero) ::::");
    return this.ordenesMedicasImpl.detalleOrdenMedica(ormIdOrdmNumero);
  }
  
  public void administrarOrdenMedica(AdmOrdenesMedicasVO admOrdenesMedicasVO) throws DataException {
    logger.info("Creando administrarOrdenMedica(AdmOrdenesMedicasVO) ::::");
    this.ordenesMedicasImpl.administrarOrdenMedica(admOrdenesMedicasVO);
  }
  
  public List<CaPrestacionesOrdMed> consultaPrestaciones(CaPrestacionesOrdMed consultaPrestacionesVO) throws DataException {
    logger.info("consultaPrestaciones(consultaPrestacionesVO) ::::");
    return this.ordenesMedicasImpl.consultaPrestaciones(consultaPrestacionesVO);
  }
  
  public void saveFile(Long ormIdOrdmNumero, String filename) throws DataException {
    logger.info("Guardando file orden(filename) :::: OM" + ormIdOrdmNumero + "," + filename);
    this.ordenesMedicasImpl.saveFile(ormIdOrdmNumero, filename);
  }
  
  public List<PrestacionesOrdMedVO> crearPrestaciones(CrearPrestacionesOrdMedVO crearPrestacionesOrdMedVO) throws DataException {
    logger.info("consultaPrestaciones(consultaPrestacionesVO) ::::");
    return this.ordenesMedicasImpl.crearPrestaciones(crearPrestacionesOrdMedVO);
  }
  
  public boolean registrarAutorizacion(GestionAutorizacionVO gestionAutorizacionVO) throws DataException {
    logger.info("registrarAutorizacion(gestionAutorizacionVO) ::::");
    return this.ordenesMedicasImpl.registrarAutorizacion(gestionAutorizacionVO);
  }
  
  public boolean finalizarOrden(FinalizarOrdenMedicaVO finalizarOrdenMedicaVO) throws DataException {
    logger.info("finalizarOrden(consultaPrestacionesVO) ::::");
    return this.ordenesMedicasImpl.finalizarOrden(finalizarOrdenMedicaVO);
  }
  
  public boolean eliminarOrden(FinalizarOrdenMedicaVO finalizarOrdenMedicaVO) throws DataException {
    logger.info("finalizarOrden(consultaPrestacionesVO) ::::");
    return this.ordenesMedicasImpl.eliminarOrden(finalizarOrdenMedicaVO);
  }
  
  public PrestacionesOrdMedVO consultaPrestacionPorPomIdPrestOrdm(Long pomIdPrestOrdm) {
    return this.ordenesMedicasImpl.consultaPrestacionPorPomIdPrestOrdm(pomIdPrestOrdm);
  }
  
  public List<CaMotivosEliminarOm> motivoEliminacion() throws DataException {
    return this.ordenesMedicasImpl.motivoEliminacion();
  }
  
  public RespuestaOrdenMedicaVO registrarOrdenMedica(FiltrosOrdenMedicaVO filtrosOrdenMedicaVO) throws DataException {
    logger.info("Creando registrarOrdenMedica(FiltrosOrdenMedicaVO) ::::");
    return this.ordenesMedicasImpl.registrarOrdenMedica(filtrosOrdenMedicaVO);
  }
}

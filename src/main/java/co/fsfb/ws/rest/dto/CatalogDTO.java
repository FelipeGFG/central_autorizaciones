package co.fsfb.ws.rest.dto;

import co.fsfb.ws.rest.dao.CatalogDAO;
import co.fsfb.ws.rest.exception.CitaNoEncontradaException;
import co.fsfb.ws.rest.exception.ConsultaNoEncontradaException;
import co.fsfb.ws.rest.exception.DataException;
import co.fsfb.ws.rest.vo.CIE;
import co.fsfb.ws.rest.vo.CitasAgendadas;
import co.fsfb.ws.rest.vo.ClasificaConsulta;
import co.fsfb.ws.rest.vo.Convenio;
import co.fsfb.ws.rest.vo.Departamento;
import co.fsfb.ws.rest.vo.DiagnosticosVO;
import co.fsfb.ws.rest.vo.Medicos;
import co.fsfb.ws.rest.vo.Sede;
import co.fsfb.ws.rest.vo.Servicio;
import co.fsfb.ws.rest.vo.SubEspecialidad;
import co.fsfb.ws.rest.vo.TipoDoc;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CatalogDTO {
  private static Logger logger = LoggerFactory.getLogger(CatalogDTO.class);
  
  private CatalogDAO dao = new CatalogDAO();
  
  public List<Departamento> departamento() throws ConsultaNoEncontradaException {
    logger.info("Creando departamento::::");
    try {
      return this.dao.departamento();
    } catch (DataException e) {
      throw new ConsultaNoEncontradaException(e.getMessage(), new Date(), e.getMessage());
    } 
  }
  
  public List<SubEspecialidad> subEspecialidades() throws ConsultaNoEncontradaException {
    logger.info("Creando subEspecialidades::::");
    try {
      return this.dao.subEspecialidades();
    } catch (DataException e) {
      throw new ConsultaNoEncontradaException(e.getMessage(), new Date(), e.getMessage());
    } 
  }
  
  public List<Servicio> servicios() throws ConsultaNoEncontradaException {
    logger.info("Creando servicios::::");
    try {
      return this.dao.servicios();
    } catch (DataException e) {
      throw new ConsultaNoEncontradaException(e.getMessage(), new Date(), e.getMessage());
    } 
  }
  
  public List<Servicio> servicio() throws ConsultaNoEncontradaException {
    logger.info("Creando servicio::::");
    try {
      return this.dao.servicio();
    } catch (DataException e) {
      throw new ConsultaNoEncontradaException(e.getMessage(), new Date(), e.getMessage());
    } 
  }
  
  public List<ClasificaConsulta> clasifiConsult(ClasificaConsulta consul) throws ConsultaNoEncontradaException {
    logger.info("Creando clasifiConsult::::");
    try {
      return this.dao.clasificaConsul(consul);
    } catch (DataException e) {
      throw new ConsultaNoEncontradaException(e.getMessage(), new Date(), e.getMessage());
    } 
  }
  
  public List<CIE> cie(CIE cie) throws ConsultaNoEncontradaException {
    logger.info("Creando cie::::");
    try {
      return this.dao.cie(cie);
    } catch (DataException e) {
      throw new ConsultaNoEncontradaException(e.getMessage(), new Date(), e.getMessage());
    } 
  }
  
  public List<Sede> sede() throws ConsultaNoEncontradaException {
    logger.info("Creando sede::::");
    try {
      return this.dao.sede();
    } catch (DataException e) {
      throw new ConsultaNoEncontradaException(e.getMessage(), new Date(), e.getMessage());
    } 
  }
  
  public List<TipoDoc> tipoDoc() throws ConsultaNoEncontradaException {
    logger.info("Creando tipoDoc::::");
    try {
      return this.dao.tipoDoc();
    } catch (DataException e) {
      throw new ConsultaNoEncontradaException(e.getMessage(), new Date(), e.getMessage());
    } 
  }
  
  public List<Convenio> convenio() throws ConsultaNoEncontradaException {
    logger.info("Creando convenio::::");
    try {
      return this.dao.convenio();
    } catch (DataException e) {
      throw new ConsultaNoEncontradaException(e.getMessage(), new Date(), e.getMessage());
    } 
  }
  
  public List<Medicos> medicos() throws ConsultaNoEncontradaException {
    logger.info("Creando medicos::::");
    try {
      return this.dao.medicos();
    } catch (DataException e) {
      throw new ConsultaNoEncontradaException(e.getMessage(), new Date(), e.getMessage());
    } 
  }
  
  public List<CitasAgendadas> prestacion() throws CitaNoEncontradaException {
    logger.info("Creando prestacion::::");
    try {
      return this.dao.prestacion();
    } catch (DataException e) {
      throw new CitaNoEncontradaException(e.getMessage(), new Date(), e.getMessage());
    } 
  }
  
  public List<DiagnosticosVO> getDiagnosticos() throws ConsultaNoEncontradaException {
    logger.info("Creando diagnostivos::::");
    try {
      return this.dao.getDiagnosticos();
    } catch (DataException e) {
      throw new ConsultaNoEncontradaException(e.getMessage(), new Date(), e.getMessage());
    } 
  }
  public List<DiagnosticosVO> getDiagnosticosLike(String valor) throws ConsultaNoEncontradaException {
    logger.info("Creando diagnostivos::::");
    try {
      return this.dao.getDiagnosticosLike(valor);
    } catch (DataException e) {
      throw new ConsultaNoEncontradaException(e.getMessage(), new Date(), e.getMessage());
    } 
  }
}

package co.fsfb.ws.rest.response;

import java.io.Serializable;

public class GenericResponse implements Serializable {
  private static final long serialVersionUID = 1L;
  
  private Object Objeto;
  
  private String respuesta;
  
  public GenericResponse(Object objeto, String respuesta) {
    this.Objeto = objeto;
    this.respuesta = respuesta;
  }
  
  public Object getObjeto() {
    return this.Objeto;
  }
  
  public void setObjeto(Object objeto) {
    this.Objeto = objeto;
  }
  
  public String getRespuesta() {
    return this.respuesta;
  }
  
  public void setRespuesta(String respuesta) {
    this.respuesta = respuesta;
  }
}

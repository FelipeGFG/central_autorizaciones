package co.fsfb.ws.rest.response;

import co.fsfb.ws.rest.vo.ResultadoExamen;
import java.util.List;

public class ConsultarinfohistoriaclinicaResponse {
  private List<ResultadoExamen> examenes;
  
  public List<ResultadoExamen> getExamenes() {
    return this.examenes;
  }
  
  public void setExamenes(List<ResultadoExamen> examenes) {
    this.examenes = examenes;
  }
}

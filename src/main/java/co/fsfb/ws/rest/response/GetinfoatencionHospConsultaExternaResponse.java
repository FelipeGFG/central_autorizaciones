package co.fsfb.ws.rest.response;

import co.fsfb.ws.rest.vo.AtencionHospitalaria;
import java.util.List;

public class GetinfoatencionHospConsultaExternaResponse {
  private List<AtencionHospitalaria> antencionesHopitalarias;
  
  public List<AtencionHospitalaria> getAntencionesHopitalarias() {
    return this.antencionesHopitalarias;
  }
  
  public void setAntencionesHopitalarias(List<AtencionHospitalaria> antencionesHopitalarias) {
    this.antencionesHopitalarias = antencionesHopitalarias;
  }
}

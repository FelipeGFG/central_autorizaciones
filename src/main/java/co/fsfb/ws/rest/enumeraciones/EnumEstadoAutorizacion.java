package co.fsfb.ws.rest.enumeraciones;

public enum EnumEstadoAutorizacion {
  ACEPTADO("1", "ha autorizado"),
  NO_ACEPTADO("2", "ha autorizado");
  
  private String id;
  
  private String descripcion;
  
  EnumEstadoAutorizacion(String id, String descripcion) {
    this.id = id;
    this.descripcion = descripcion;
  }
  
  public String getId() {
    return this.id;
  }
  
  public String getDescripcion() {
    return this.descripcion;
  }
  
  public static String getDescripcion(String id) {
    for (EnumEstadoAutorizacion value : values()) {
      if (value.getId().equals(id))
        return value.getDescripcion(); 
    } 
    return "";
  }
}

package co.fsfb.ws.rest.enumeraciones;

public enum EnumEstadoContinuidad {
  ACEPTADO(Long.valueOf(1L), "ha aceptado"),
  NO_ACEPTADO(Long.valueOf(2L), "no ha aceptado a"),
  NO_CONTESTA(Long.valueOf(3L), "no ha contestado para");
  
  private Long id;
  
  private String descripcion;
  
  EnumEstadoContinuidad(Long id, String descripcion) {
    this.id = id;
    this.descripcion = descripcion;
  }
  
  public Long getId() {
    return this.id;
  }
  
  public String getDescripcion() {
    return this.descripcion;
  }
  
  public static String getDescripcion(Long id) {
    for (EnumEstadoContinuidad value : values()) {
      if (value.getId().equals(id))
        return value.getDescripcion(); 
    } 
    return "";
  }
}

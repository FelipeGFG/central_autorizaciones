package co.fsfb.ws.rest.enumeraciones;

public enum EnumTipoIdentDocumento {
  SIN_INFORMACION("1"),
  REG_CIVIL("2"),
  T_IDENTIDAD("3"),
  CEDULA_C("4"),
  CEDULA_EXT("5"),
  NIT("6"),
  MENOR_SIN_IDENTIFICACION("M"),
  PASAPORTE("P"),
  ADULTO_SIN_IDENTIFICACION("A"),
  NUIP("N"),
  CARNE_DIPLOMATICO("D"),
  SALVOCONDUCTO("S");
  
  private String id;
  
  EnumTipoIdentDocumento(String id) {
    this.id = id;
  }
  
  public String getId() {
    return this.id;
  }
}
